﻿
function ContainerHtmlNode(headerText ,tailText) {

    this.headerText = headerText;
    this.tailText = tailText;
    this.simpleNodeList = [];

    this.toHtml = function () {
        
        var content="";

        for (index = 0; index < this.simpleNodeList.length ; index++) {
            content += this.simpleNodeList[index].toHtml();
        }

        return this.headerText + content + this.tailText;
    }

    this.setID = function (id) {
        this.id = id;
    }

    this.addNode = function (simpleNode) {
        this.simpleNodeList.push(simpleNode);
        return this.simpleNodeList.length;
    }
}

function SimpleHtmlNode( ) {

    this.setID = function (id) {
        this.id = id;
    }

    this.setHeaderText = function (headerText) {
        this.headerText = headerText;
    }

    this.setTailText = function (tailText) {
        this.tailText = tailText;
    }

    this.toHtml = function () {

        //먼저 CkEditor를 찾는다.
        var editor = CKEDITOR.instances[this.id];
        var content = editor.getData();

        return this.headerText + content + this.tailText;
    }
}

function ComplexContainerHtmlNode(id , headerText , tailText) {

    this.toHtml = function () {
        return this.name;
    }

}

