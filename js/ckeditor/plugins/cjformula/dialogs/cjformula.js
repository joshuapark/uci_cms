﻿jQuery.extend(
    {
        getValues: function (texScript) {
          
            return $.ajax({
                type: "POST",
                url: "../main/EntryEdit.aspx/convertTexImage",
                data: '{"tex":"' + texScript + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (contents) {

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log('status code: ' + jqXHR.status + ' errorThrown: ' + errorThrown + ' jqXHR.responseText:' + jqXHR.responseText);
                }
            });

        }
    }
);

var imagePath;
CKEDITOR.dialog.add('cjformulaDialog', function (editor) {
    
        return {
            title: '수식 변환기',
            minWidth: 400,
            minHeight: 200,
            contents: 
            [
                {
                    id: 'latex',
                    label: 'TEX 수식 변환',
                    elements:
                    [
                        {
                            type: 'text',
                            id: 'inputtex',
                            label: 'TEX 수식을 입력해주세요',
                            size: 38
                        }
                        ,
                        {
                            type: 'button',
                            id: 'convertimage',
                            label: '변환',
                            size: 38,
                            onClick: function () {
                                
                                var dialog = this.getDialog();
                                //alert( dialog.getValueOf('latex', 'inputtex') );
                                var tex = dialog.getValueOf('latex', 'inputtex');
                                if (tex == "") {
                                    alert('tex 수식을 입력해주세요');
                                    return;
                                }

                                tex = tex.replace(/\n/gi, "\\\\");
                                tex = tex.replace(/\+/gi, "!PLUS!");

                                tex = escape(tex);
                                var rtnValue = $.getValues(tex);
                                //alert(rtnValue.responseText.d);
                                var obj = JSON.parse(rtnValue.responseText);
                                imagePath = obj.d;
                                //alert(imagePath);
                                
                                var document = this.getElement().getDocument();
                                var element = document.getById('formulaimage');
                                if (element)
                                {
                                    element.setHtml('<img src=' + imagePath + ' />');
                                }
                            },
                        }
                        ,
                        {
                            type: 'html',
                            id: 'convertimage',
                            label: '수식 이미지',
                            style: 'margin-top:30px;width:400px;height:100px;',
                            html : '<div id="formulaimage"></div>'
                        }

                    ]
                }
            ],

            onOk: function ()
            {
                var imageElement = editor.document.createElement('img')
                imageElement.setAttribute('src', imagePath);
                editor.insertElement(imageElement);
            }
    };
});