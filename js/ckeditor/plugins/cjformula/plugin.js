﻿﻿CKEDITOR.plugins.add('cjformula', {

    icons: 'cjformula',
    init: function (editor) {
            editor.addCommand('cjformula', new CKEDITOR.dialogCommand('cjformulaDialog'));
            editor.ui.addButton('CJFormula', {
                    label: 'Insert Formula',
                    command: 'cjformula',
                    toolbar: 'insert'
                });

        CKEDITOR.dialog.add('cjformulaDialog', this.path + 'dialogs/cjformula.js');
    }
});