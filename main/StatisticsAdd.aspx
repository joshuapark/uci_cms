﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="StatisticsAdd.aspx.cs" Inherits="main_opencontents" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->

    <script type="text/javascript">
        function checkClick() {
            if ($("#chkAll").prop("checked")) {
                $(".de-check1").each(function () {
                    $(this).attr("checked", true);
                });
            } else {
                $(".de-check1").each(function () {
                    $(this).attr("checked", false);
                });
            }
        }
    </script>
</head>

<body>
<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title title-success">콘텐츠통계생성</h2>
		</div><!-- // title -->

		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
            <tr>
				<th>상태</th>
                <td>
<script type="text/javascript">
    function CheckStatusSelect(cbControl)
    {   
        var chkBoxList = document.getElementById(cbControl);
        var chkAll =  document.getElementById('chkAllStatus');
        var chkBoxCount= chkBoxList.getElementsByTagName("input");
        if ($(chkAll).prop("checked")) {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = true;
            }
            return false; 
        } else {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = false;
            }
            return false; 
        }
    }
</script>
                    <table class="de-check" style="float:left"><tr><td><input type="checkbox" id="chkAllStatus" onclick="javascript:CheckStatusSelect('<%= cblStatus.ClientID %>');" /><label for="chkAllStatus">전체</label></td></tr></table>
					<asp:CheckBoxList ID="cblStatus" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Table">
                        <asp:ListItem value="1">배포/작업중</asp:ListItem>
                        <asp:ListItem value="3">작업완료/검수중</asp:ListItem>
                        <asp:ListItem value="7">서비스중(최종검수완료)</asp:ListItem>
					</asp:CheckBoxList>
                </td>
            </tr>
			<tr>
				<th>개정</th>
				<td>
<script type="text/javascript">
    function CheckRevisionSelect(cbControl)
    {   
        var chkBoxList2 = document.getElementById(cbControl);
        var chkAll2 =  document.getElementById('chkAllRevision');
        var chkBoxCount= chkBoxList2.getElementsByTagName("input");
        if ($(chkAll2).prop("checked")) {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = true;
            }
            return false; 
        } else {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = false;
            }
            return false; 
        }
    }
</script>
                    <table class="de-check" style="float:left"><tr><td><input type="checkbox"id="chkAllRevision" onclick="javascript:CheckRevisionSelect('<%= cblRevision.ClientID %>');" /><label for="chkAllRevision">전체</label></td></tr></table>
                    <asp:CheckBoxList ID="cblRevision" CssClass="de-check" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Table">                        
                    </asp:CheckBoxList>
				</td>
			</tr>
			<tr>
				<th>과목</th>
				<td>
<script type="text/javascript">
    function CheckSubjectSelect(cbControl)
    {   
        var chkBoxList3 = document.getElementById(cbControl);
        var chkAll3 =  document.getElementById('chkAllSubject');
        var chkBoxCount= chkBoxList3.getElementsByTagName("input");
        if ($(chkAll3).prop("checked")) {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = true;
            }
            return false; 
        } else {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = false;
            }
            return false; 
        }
    }
</script>
                    <table class="de-check"><tr><td><input type="checkbox"id="chkAllSubject" onclick="javascript:CheckSubjectSelect('<%= cblSubject.ClientID %>');" /><label for="chkAllSubject">전체</label></td></tr></table>
                    <asp:CheckBoxList ID="cblSubject" CssClass="de-check" runat="server" RepeatColumns="7" RepeatDirection="Horizontal" RepeatLayout="Table">
                        <asp:ListItem value="0">전체</asp:ListItem>
                    </asp:CheckBoxList>
				</td>
			</tr>
			<tr>
				<th>브랜드</th>
				<td>
<script type="text/javascript">
    function CheckBrandSelect(cbControl)
    {   
        var chkBoxList4 = document.getElementById(cbControl);
        var chkAll4 =  document.getElementById('chkAllBrand');
        var chkBoxCount= chkBoxList4.getElementsByTagName("input");
        if ($(chkAll4).prop("checked")) {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = true;
            }
            return false; 
        } else {
            for(var i=0;i<chkBoxCount.length;i++)
            {
                chkBoxCount[i].checked = false;
            }
            return false; 
        }
    }
</script>
                    <table class="de-check"><tr><td><input type="checkbox"id="chkAllBrand" onclick="javascript:CheckBrandSelect('<%= cblBrand.ClientID %>');" /><label for="chkAllBrand">전체</label></td></tr></table>
                    <asp:CheckBoxList ID="cblBrand" CssClass="de-check" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Table">
                        <asp:ListItem value="0">전체</asp:ListItem>
                    </asp:CheckBoxList>
				</td>
			</tr>
            <tr>
				<th>작업명</th>
                <td>
					<asp:TextBox runat="server" ID="txtTitle" class="large" />
                    <asp:RequiredFieldValidator ID="rfvTxtTitle" runat="server" ErrorMessage="* 작업명을 입력하세요." ControlToValidate="txtTitle" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
                    <asp:HiddenField ID="txtStatIdx" runat="server"/>
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

		<div class="section-button"><!-- section-button -->
			<asp:Button ID="btnExport" cssClass="btn btn-lg btn-success" runat="server" OnClick="btnExport_Click" Text="통계생성"></asp:Button>
		</div><!-- // section-button -->

	</div><!-- // contents -->
    
    </form>

</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>  
</body>
</html>