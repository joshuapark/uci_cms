﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EntryExport.aspx.cs" Inherits="XmlImport2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>


<body>

<div class="container" style="width: 700px; height: 300px; margin-left: 0px; margin-right: 0px;">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">Export</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>작업명</th>
				    <td>
				        <asp:Label ID="lblTaskTitle" runat="server"></asp:Label>                            
                        <asp:HiddenField ID="txtTaskIdx" runat="server"></asp:HiddenField>                            
				    </td>
				    <th>Entry수</th>
				    <td>
				        <asp:Label ID="lblEntryCount" runat="server"></asp:Label>                            
				    </td>
                </tr>
		    </tbody>
		</table><!-- // table-a -->
        <div class="section-button"><!-- section-button -->
			<asp:Button ID="btnExport" cssClass="btn btn-lg btn-success" runat="server" OnClick="btnExport_Click" Text="Export"></asp:Button>
            <a ID="btnClose" class="btn btn-lg btn-info" onclick="window.opener=self;self.close();" >닫기</a>
		</div><!-- // section-button -->
	</div><!-- // contents -->
    </form>

</div><!-- // container -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/jquery-1.4.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.js"></script>
<link rel="stylesheet" href="../css/jquery-ui.css" type="text/css" media="all" />
<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui-1.8.18.min.js" type="text/javascript"></script>
</body>
</html>


