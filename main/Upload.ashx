﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;


public class Upload : IHttpHandler {

    private string connectionString = string.Empty;
    
    public void ProcessRequest (HttpContext context) {
        HttpPostedFile uploads = context.Request.Files["upload"];
        
        string CKEditorFuncNum = context.Request["CKEditorFuncNum"];
        //string file = System.IO.Path.GetFileName(uploads.FileName);

        string entryIdx = null;
        string taskIdx = null;
        string extension = System.IO.Path.GetExtension(uploads.FileName); 
        if (context.Request.Cookies["UserSettings"] != null)
        {
            if (context.Request.Cookies["UserSettings"]["EntryIdx"] != null)
            { 
                entryIdx = context.Request.Cookies["UserSettings"]["EntryIdx"]; 
            }

            if (context.Request.Cookies["UserSettings"]["TaskIdx"] != null) {
                taskIdx = context.Request.Cookies["UserSettings"]["TaskIdx"];
            }
        }
        
        string file = taskIdx + "_" + entryIdx + "_" + DateTime.Now.ToString("HHmmss") + extension;
        string imgDirectory = context.Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + taskIdx + "\\";
        if (!Directory.Exists(imgDirectory)) {
            Directory.CreateDirectory(imgDirectory);
        }
        
        uploads.SaveAs(context.Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + taskIdx + "\\" + file);
        
        //데이터베이스에 이름을 저장한다.
        String strWidth = string.Empty;
        String strHeight = string.Empty;
        String fileType = string.Empty;

        if (extension.Equals(".jpg", StringComparison.InvariantCultureIgnoreCase) || extension.Equals(".gif", StringComparison.InvariantCultureIgnoreCase) || extension.Equals(".png", StringComparison.InvariantCultureIgnoreCase))
        {
            System.Drawing.Size size = System.Drawing.Image.FromFile(context.Server.MapPath("../") + "\\CMS100Data\\EntryData\\" + taskIdx + "\\" + file).Size;
            strWidth = Convert.ToString(size.Width);
            strHeight = Convert.ToString(size.Height);
            fileType = "IMG";
        }
        else if 
            (
            extension.Equals(".wav", StringComparison.InvariantCultureIgnoreCase) || 
            extension.Equals(".mp3", StringComparison.InvariantCultureIgnoreCase) || 
            extension.Equals(".ogg", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".aac", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".3gp", StringComparison.InvariantCultureIgnoreCase)
            ) 
        {
            fileType = "SOU";
        }
        else if (
            extension.Equals(".avi", StringComparison.InvariantCultureIgnoreCase) || 
            extension.Equals(".mpg", StringComparison.InvariantCultureIgnoreCase) || 
            extension.Equals(".mpeg", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".wmv", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".asf", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".asx", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".mov", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".mp4", StringComparison.InvariantCultureIgnoreCase) ||
            extension.Equals(".flv", StringComparison.InvariantCultureIgnoreCase)
            )
        { 
            fileType="MOV";
        }
        
        //이미지명 추출
        string strFileName = uploads.FileName;
        
        //새로운 이미지코드명 생성
        string strImageCode = file;

        string serverIP = context.Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
        
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdn = new SqlCommand();
        Cmdn.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@EntryIdx", SqlDbType.Int);
        Cmdn.Parameters.Add("@OriginName", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Description", SqlDbType.VarChar);
        Cmdn.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
        Cmdn.Parameters.Add("@Width", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@Height", SqlDbType.VarChar, 10);
        Cmdn.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

        Cmdn.Parameters["@FileName"].Value = strImageCode;  //신규 이미지 명
        Cmdn.Parameters["@TaskIdx"].Value = taskIdx;
        Cmdn.Parameters["@EntryIdx"].Value = entryIdx;
        Cmdn.Parameters["@OriginName"].Value = strFileName; //원 이미지파일 명
        Cmdn.Parameters["@Description"].Value = "";
        Cmdn.Parameters["@Caption"].Value = "";
        Cmdn.Parameters["@Width"].Value = strWidth;
        Cmdn.Parameters["@Height"].Value = strHeight;
        Cmdn.Parameters["@FileType"].Value = fileType;


        Cmdn.CommandText = "USP_File_INSERT";
        Cmdn.CommandType = CommandType.StoredProcedure;
        Cmdn.Connection = Conn;
        Conn.Open();
        Cmdn.ExecuteNonQuery();
        Conn.Close();
        
        
        string rootUrl = "http://" + context.Request.ServerVariables["HTTP_HOST"];
        string url = rootUrl + "/CMS100Data/EntryData/"+ taskIdx +"/"+ file;
        
        
        context.Response.Write("<script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", '" + url + "');</script>");
        context.Response.End();             
        
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }

}