﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Drawing;

public partial class main_List : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private bool _refreshState;
    private bool _isRefresh;

    public bool IsRefresh
    {
        get { return _isRefresh; }
    }
    protected override void LoadViewState(object savedState)
    {
        object[] allStates = (object[])savedState;
        base.LoadViewState(allStates[0]);
        _refreshState = (bool)allStates[1];
        _isRefresh = _refreshState == (bool)Session["__ISREFRESH"];
    }
    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] allStates = new object[2];
        allStates[0] = base.SaveViewState();
        allStates[1] = !_refreshState;
        return allStates;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  


        if (!IsPostBack)
        {
            CodeTypeListing();

            CodeTypeBinding();

        }

    }

    protected void CodeTypeBinding()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("SELECT CodeType, CodeTypeName FROM TCodeType WHERE DelFlag=0 ORDER BY CodeTypeName ASC", Conn);
        Conn.Open();
        SqlDataReader Reader = Cmd.ExecuteReader();

        //Set up the data binding.
        ddlCodeType.DataSource = Reader;
        ddlCodeType.DataTextField = "CodeTypeName";
        ddlCodeType.DataValueField = "CodeType";
        ddlCodeType.DataBind();

        //Close the connection.
        Conn.Close();
        Reader.Close();


    }
    protected void CodeTypeListing()
    {
        // DB Connection String, File Path 설정
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_CodeType_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        CodeTypeListRepeater.DataSource = ds;
        CodeTypeListRepeater.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        //TableRow tr;
        //TableCell td;

        //for (int i = 0; i < ds.Tables["data_list"].Rows.Count; i++)
        //{
        //    string CodeType, CodeTypeName;
        //    CodeType = ds.Tables["data_list"].Rows[i]["CodeType"].ToString();
        //    CodeTypeName = ds.Tables["data_list"].Rows[i]["CodeTypeName"].ToString();
        //    tr = new TableRow();
        //    // 
        //    td = new TableCell();
        //    HiddenField tb0 = new HiddenField();
        //    tb0.ID = "CodeID" + i;
        //    tb0.Value = CodeType;
        //    td.Controls.Add(tb0);
        //    tr.Cells.Add(td);

        //    Label tb1 = new Label();
        //    tb1.ID = "CodeName" + i;
        //    tb1.Text = "<a Onclick=\'document.getElementById(\"viewFrame\").src=\"CodeView.aspx?CodeType=" + CodeType + "\";\'>"+ CodeTypeName +"</a>";
        //    td.Controls.Add(tb1);
        //    tr.Cells.Add(td);
        //    // 
        //    td = new TableCell();
        //    //개정<a href="#!" class="btn-close"><!-- close --></a></li>

        //    Button bt = new Button();
        //    bt.CommandName = "DELETE";
        //    bt.BorderWidth = 0;
        //    bt.CssClass = "btn-code-del";
        //    //bt.Text = "X";
        //    bt.ID = CodeType;
        //    bt.OnClientClick = "javascript:return confirm('삭제하시겠습니까?')";
        //    bt.Click += new EventHandler(this.DelButton_Click);
        //    td.Controls.Add(bt); 

        //    tr.Cells.Add(td);
        //    CodeTypeTable.Rows.Add(tr);
        //}
    }
    protected void CodeListing(string codeTypeID)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_Code_SELECT";
        Cmd.Parameters.Add("@CodeType", SqlDbType.Int);
        Cmd.Parameters["@CodeType"].Value = codeTypeID;
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "status_list");

        CodeTableRepeater.DataSource = ds;
        CodeTableRepeater.DataBind();
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        if (txtCodeType.Text.Trim().Length > 1)  //코드타입명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT CodeType FROM TCodeType WHERE CodeTypeName='" + txtCodeType.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                //msgIDCheck.InnerText = "* " + txtCodeType.Text.Trim() + " 코드타입은 이미 등록되어 있습니다. 다른 코드타입을 입력하세요.";
                string script = "<script>alert('" + txtCodeType.Text.Trim() + " 코드타입은 이미 등록되어 있습니다.');</script>";
                txtCodeType.Text = "";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
                Response.Write(script);
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("INSERT INTO TCodeType (CodeTypeName) VALUES ( '" + txtCodeType.Text.Trim() + "')", Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;
                txtCodeType.Text = "";
                selectedCodeTypeName.Text = "";
                selectedCodeType.Value = "";

                CodeTypeListing();
                //Response.Redirect("CodeList.aspx");
                //string script = "<script>alert('저장되었습니다');location.href='CodeList.aspx'</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            }
        }
    }

    protected void CodeTypeListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string delID;
        //Button clickedButton = (Button)sender;
        //delID = clickedButton.ID;
        Control ctl = source as Control;
        var argument = e.CommandArgument;
        
        //CommandEventArgs cea = e as CommandEventArgs;
        if (ctl != null)
        {
            if(argument.ToString()=="D")
            {
                RepeaterItem repeaterItem = CodeTypeListRepeater.Items[e.Item.ItemIndex];
                HiddenField hfID = repeaterItem.FindControl("CodeID") as HiddenField;
                string delID = hfID.Value;

                DBFileInfo();

                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("UPDATE TCodeType SET DelFlag=1	WHERE CodeType = " + delID, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                string script = "<script>alert('삭제되었습니다');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

                CodeTypeListing();
            }
            else if (argument.ToString()=="G")
            {
                RepeaterItem repeaterItem = CodeTypeListRepeater.Items[e.Item.ItemIndex];
                HiddenField hfID = repeaterItem.FindControl("CodeID") as HiddenField;
                TextBox txtCodeTypeName = repeaterItem.FindControl("txtCodeTypeName") as TextBox;
                string codeTypeID = hfID.Value;

                selectedCodeTypeName.Text = txtCodeTypeName.Text;
                selectedCodeType.Value = codeTypeID;
                ddlCodeType.SelectedValue = codeTypeID;

                CodeListing(codeTypeID);

            }
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    //protected void SaveButton_Click(object sender, EventArgs e)
    //{
    //    int RowCnt = CodeTable.Rows.Count;
    //    //Boolean chkDupl = false;

    //    for (int i = 0; i < (RowCnt-1) ; i++)
    //    {
    //        HiddenField tb0 = editForm.FindControl("CodeID" + i) as HiddenField;
    //        TextBox tb1 = editForm.FindControl("CodeName" + i) as TextBox;
    //        TextBox tb2 = editForm.FindControl("Priority" + i) as TextBox;
    //        DropDownList ddl1 = editForm.FindControl("Enabled" + i) as DropDownList;
            
    //        int intPriority = 100;
    //        if (tb2.Text.Length > 0)
    //        {
    //            intPriority=int.Parse(tb2.Text);
    //        }
    //        SqlConnection Con = new SqlConnection(connectionString);

    //        SqlCommand Cmd = new SqlCommand("USP_Organization_UPDATE", Con);
    //        Cmd.CommandType = CommandType.StoredProcedure;

    //        Cmd.Parameters.Add("@ID", SqlDbType.Int);
    //        Cmd.Parameters.Add("@Name", SqlDbType.VarChar);
    //        Cmd.Parameters.Add("@priority", SqlDbType.Int);
    //        Cmd.Parameters.Add("@enabled", SqlDbType.Bit);
    //        Cmd.Parameters["@ID"].Value = int.Parse(tb0.Value);
    //        Cmd.Parameters["@Name"].Value = tb1.Text;
    //        Cmd.Parameters["@priority"].Value = intPriority;
    //        Cmd.Parameters["@enabled"].Value = ddl1.SelectedValue;

    //        Con.Open();
    //        Cmd.ExecuteNonQuery();
    //        Con.Close();

    //        Cmd = null;
    //        Con = null;
    //    }
    //    //if (chkDupl)
    //    //{
    //    //    string script = "<script>alert('중복된 병원이 있습니다.');</script>";
    //    //    ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
    //    //}
    //    //else
    //    //{
    //        Response.Redirect("OrgCode.aspx");
    //    //}   
    //}

    protected void btnSaveCodeType_Click(object sender, EventArgs e)
    {
        try
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            SqlCommand Cmd = null;

            foreach (RepeaterItem item in CodeTypeListRepeater.Items)
            {
                HiddenField txtIDX = item.FindControl("CodeID") as HiddenField;
                TextBox txtCodeTypeName = item.FindControl("txtCodeTypeName") as TextBox;
                if (txtCodeTypeName.Text.Length > 0)
                {
                    Cmd = new SqlCommand("UPDATE TCodeType SET CodeTypeName='" + txtCodeTypeName.Text + "' WHERE CodeType=" + txtIDX.Value + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                }
            }

            Con.Close();

            Cmd = null;
            Con = null;

            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("코드타입 저장 중에 오류가 발생했습니다.<br />오류내용"+ ex.ToString());
        }

    }
    protected void btnSaveCode_Click(object sender, EventArgs e)
    {
        try
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            SqlCommand Cmd = null;

            foreach (RepeaterItem item in CodeTableRepeater.Items)
            {
                HiddenField txtIDX = item.FindControl("CodeIdx") as HiddenField;
                TextBox txtCodeName = item.FindControl("CodeName") as TextBox;
                if (txtCodeName.Text.Length > 0)
                {
                    Cmd = new SqlCommand("UPDATE TCode SET CodeName='" + txtCodeName.Text + "' WHERE Idx=" + txtIDX.Value + " ", Con);
                    Cmd.CommandType = CommandType.Text;

                    Cmd.ExecuteNonQuery();
                }
            }

            Con.Close();

            Cmd = null;
            Con = null;

            Response.Write("<script>alert('저장되었습니다.');</script>");
        }
        catch (Exception ex)
        {
            Response.Write("코드타입 저장 중에 오류가 발생했습니다.<br />오류내용" + ex.ToString());
        }
    }
    protected void btnCodeAdd_Click(object sender, EventArgs e)
    {
        if (txtCodeName.Text.Trim().Length > 0)  //코드명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT Code FROM TCode WHERE CodeType=" + ddlCodeType.SelectedValue + " AND CodeName='" + txtCodeName.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                //msgIDCheck2.InnerText = "* " + txtCode.Text.Trim() + " 코드는 이미 등록되어 있습니다. 다른 코드를 입력하세요.";
                string script = "<script>alert('" + txtCodeName.Text.Trim() + " 코드명은 이미 등록되어 있습니다.');</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
                txtCode.Text = "";
                txtCodeName.Text = "";
                Response.Write(script);
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("USP_Code_INSERT", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@Code", SqlDbType.VarChar, 255);
                Cmd.Parameters.Add("@CodeName", SqlDbType.VarChar, 255);
                Cmd.Parameters.Add("@CodeType", SqlDbType.Int);

                Cmd.Parameters["@Code"].Value = txtCode.Text.Trim();
                Cmd.Parameters["@CodeName"].Value = txtCodeName.Text;
                Cmd.Parameters["@CodeType"].Value = ddlCodeType.SelectedValue;

                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                txtCodeName.Text = "";
                txtCode.Text = "";

                //string script = "<script>alert('저장되었습니다');location.href='CodeView.aspx?CodeType="+paramCodeType+"'</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                //Response.Redirect("CodeView.aspx?CodeType=" + paramCodeType + "");
                selectedCodeTypeName.Text = ddlCodeType.SelectedItem.Text;
                selectedCodeType.Value = ddlCodeType.SelectedValue;

                CodeListing(ddlCodeType.SelectedValue);
            }
        }
        txtCode.Text = "";
        txtCodeName.Text = "";
    }
    protected void CodeTableRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string delID;
        RepeaterItem repeaterItem = CodeTableRepeater.Items[e.Item.ItemIndex];
        HiddenField hfID = repeaterItem.FindControl("CodeIdx") as HiddenField;
        delID = hfID.Value;


        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("UPDATE TCode SET DelFlag=1	WHERE Idx = '" + delID + "'", Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        string script = "<script>alert('삭제되었습니다');</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

        CodeListing(selectedCodeType.Value);
    }
}