﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="OpenContents.aspx.cs" Inherits="main_opencontents" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script src="../js/jquery-2.1.3.min.js"></script>
</head>

<body>

<div class="container">
	<div class="title"><!-- title -->
		<h3 class="title title-success"><%=strTitle %> 불러오기</h3>
	</div><!-- // title -->

    <form id="searchForm" runat="server">
    <asp:HiddenField ID="searchRecType" runat="server" />
	<div id="contents"><!-- contents -->

	    <div class="col-md-3 box" style="width: 45%; height:1000px; overflow-y: scroll; max-height: 1200px;"> 

		    <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
		    </colgroup>
  
		    <tbody>
			    <tr>
				    <th>개정</th>
				    <td>
					    <div class="de-select">
                            <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
					    </div>
				    </td>
				    <th>과목</th>
				    <td>
					    <div class="de-select">
                            <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>					
					    </div>
				    </td>
                    <th>브랜드</th>
				    <td colspan="3">
					    <div class="de-select" id="divBrand">
                            <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
					    </div>
				    </td>
                </tr>
                <tr>
                    <th onclick="$('#divGrade').toggle();">학년</th>
				    <td>
					    <div class="de-select" id="divGrade">
                            <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
					    </div>			
				    </td>
				    <th onclick="$('#divSemester').toggle();">학기</th>
				    <td>
					    <div class="de-select" id="divSemester">
                            <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
					    </div>		
				    </td>
			    </tr>
                <tr>
                    <th onclick="$('#divLUnit').toggle();">대단원</th>
                    <td>
					    <asp:Button runat="server" ID="LUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="LUnitSearch_Click" />
                        <div class="de-select" id="divLUnit">
                            <asp:DropDownList class="de-select" ID="ddlLUnit" runat="server"></asp:DropDownList>					
					    </div>	
                    </td>
				    <th onclick="$('#divMUnit').toggle();">중단원</th>
                    <td>
					    <asp:Button runat="server" ID="MUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="MUnitSearch_Click" />
                        <div class="de-select" id="divMUnit">
                            <asp:DropDownList class="de-select" ID="ddlMUnit" runat="server"></asp:DropDownList>					
					    </div>	
                    </td>
                </tr>
                <tr>
				    <th>키워드</th>
                    <td colspan="7">
					    <asp:TextBox runat="server" ID="txtKeyWord" class="large" />
                    </td>
                </tr>
		    </tbody>
		    </table><!-- // table-a -->

		    <div class="section-button"><!-- section-button -->
			    <asp:Button runat="server" ID="SearchButton" class="btn btn-lg btn-success" text="조회" OnClick="SearchButton_Click"/>
		    </div><!-- // section-button -->
        
            <asp:Label ID="lblTotalCnt" runat="server" />
		    <table border="0" class="table table-list" style="padding:0; border-spacing:0">
                <asp:ListView ID="LV_Notice" runat="server" ItemPlaceholderID="phItemList" 
                        OnItemDataBound="ListView_ItemDataBound" >
                        <LayoutTemplate>
                            <colgroup>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>                            
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                            </colgroup>
                            <tr>
				                <th>No</th>
				                <th>개정</th>
				                <th>과목</th>
				                <th>브랜드</th>
				                <th>학년</th>
				                <th>학기</th>
				                <th>대단원</th>
				                <th>중단원</th>
				                <th>타이틀</th>
				                <th>작업자</th>
                            </tr>
                            <asp:PlaceHolder ID="phItemList" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Label ID="lblNo" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblRevision" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSubject" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblBrand" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblGrade" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblSemester" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblLUnit" runat="server" />
                                </td>
                                <td>
                                    <asp:Label ID="lblMUnit" runat="server" />
                                </td>
                                <td>
                                    <%--<asp:HyperLink ID="lblTitle" runat="server" />--%>
                                    <asp:LinkButton ID="btnTitle" runat="server" OnClick="TitleButton_Click"/>
                                </td>
                                <td>
                                    <asp:Label ID="lblUser" runat="server" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <colgroup>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>                            
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                                <col/>
                            </colgroup>
                            <tr>
				                <th>No</th>
				                <th>개정</th>
				                <th>과목군</th>
				                <th>과목</th>
				                <th>학교군</th>
				                <th>브랜드</th>
				                <th>학년</th>
				                <th>학기</th>
				                <th>대단원</th>
				                <th>중단원</th>
				                <th>타이틀</th>
				                <th>작업자</th>
                            </tr>
                            <tr>
                                <td colspan="12">등록된 엔트리가 없습니다.</td>
                            </tr>
                        </EmptyDataTemplate>
                </asp:ListView>
            </table>
        </div>
        <div class="col-md-9 box" style="width: 55%;">
            
            <asp:Repeater ID="rpEntryList" runat="server">
                <ItemTemplate>
                    <div style="border: 1px solid #48BAE4; height: auto;">
                        <asp:HiddenField ID="EntryID" runat="server" Value=<%#DataBinder.Eval(Container.DataItem , "idx")%> />
                        <div style="text-align:right;">
                        <asp:CheckBox ID="chkEntry" runat="server"/>
                        </div>
                        <div>
                            <%#DataBinder.Eval(Container.DataItem , "content")%>
                        </div>
                    </div><br/>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="section-button"><!-- section-button -->
		    <asp:Button runat="server" ID="OpenButton" class="btn btn-lg btn-success" text="완료" OnClick="OpenButton_Click"/>
	    </div><!-- // section-button -->
	</div><!-- // contents -->
    
    </form>



</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>