﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Text;

public partial class XmlImport2 : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string strTaskIdx = string.Empty;
    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    private int totalEntCount = 0;
    private string exportIdx = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth > 1)
        {
            string script = "<script>alert('권한이 없습니다.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  

        if(!IsPostBack)
        {
            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            string strQuery = string.Empty;

            strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE UserIdx=" + Session["uidx"].ToString()
                    + " AND DelFlag=0 AND Status>0  ORDER BY TaskTitle ASC";

            SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);
            DataSet ds = new DataSet();
            sda.Fill(ds, "TTaskID");
            //Set up the data binding. 
            Conn.Close();

            ListItem li = new ListItem("선택", "0");
            ddlTask.Items.Clear();
            ddlTask.Items.Add(li);
            ddlTask.AppendDataBoundItems = true;

            DataTable dtTask = ds.Tables["TTaskID"];
            ddlTask.DataSource = dtTask;
            ddlTask.DataTextField = "TaskTitle";
            ddlTask.DataValueField = "Idx";
            ddlTask.DataBind();

            ddlTask.AppendDataBoundItems = false;

            if (Conn.State == ConnectionState.Open)
                Conn.Close();

            sda = null;
            Conn = null;

        }
        else
        {

        }
    }
    
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        try
        {
            DBFileInfo();
            string strQuery = "INSERT INTO TReport (TaskIdx, UserIdx, KeyDate, ReportCount, Memo) Values "
                + " (" + ddlTask.SelectedValue + ", " + Session["uidx"].ToString() + ",'" + txtDate.Text + "', " + txtCount.Text + ", '" + Server.HtmlEncode(txtContents.Text) + "' )";
            SqlConnection Con = new SqlConnection(connectionString);

            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            Response.Write("<script>alert('저장되었습니다.');window.opener=self;self.close();</script>");
            return;
        }
        catch (Exception ex)
        {
            Response.Write("<script>alert('저장 중에 오류가 발생했습니다.\n"+ ex.ToString() +"');</script>");
            return;
        }

    }
}