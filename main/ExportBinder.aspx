﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExportBinder.aspx.cs" Inherits="XmlImport2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script type="text/javascript">
        function checkClick() {
            if ($("#chkAll").prop("checked")) {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", true);
                });
            } else {
                $("input[type=checkbox]").each(function () {
                    $(this).attr("checked", false);
                });
            }
        }
    </script>
</head>


<body>

<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title title-success">바인더 검색</h2>
		</div><!-- // title -->
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
            <tr>
				<th><asp:TextBox runat="server" ID="txtKeyword" CssClass="large" /></th>
                <td>
                    <asp:Button runat="server" ID="TaskSearch" class="btn btn-sm btn-success" text="조회" OnClick="SearchButton_Click" />
                    <asp:HiddenField ID="txtExportIdx" runat="server"/>
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->


        <asp:Label ID="lblTotalCnt" runat="server" />
        <div id="divEntryList" style="height:500px; overflow-y: scroll;">
		    <table border="0" class="table table-list" style="padding:0; border-spacing:0">
                <asp:Repeater ID="EntryList" runat="server" >
                    <HeaderTemplate>
                        <tr>
				            <th><input type="checkbox" id="chkAll" onclick="javascript:checkClick()" /></th>
				            <th>No</th>
				            <th>바인더명</th>
				            <th>바인더설명</th>
				            <th>엔트리수</th> 
				            <th>최종수정일</th>
                        </tr>
    <%
        if (EntryList.Items.Count == 0)
        {
    %>
                        <tr>
                            <td colspan="6" style="text-align:center">등록된 바인더가 없습니다.</td>
                        </tr>                    
    <%
        }
    %>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>' />
                                <asp:HiddenField ID="txtIDX" Value='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>' runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblBinderIdx" Text='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>' runat="server" />
                            </td>
                            <td><asp:Label ID="lblBinderTitle" Text='<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%>'  runat="server" /></td>
                            <td><asp:Label ID="lblBinderMemo" Text='<%#DataBinder.Eval(Container.DataItem , "BinderMemo")%>'  runat="server" /></td>
                            <td><asp:Label ID="lblEntryCount" Text='<%#DataBinder.Eval(Container.DataItem , "EntryCount")%>'  runat="server" /></td>
                            <td><asp:Label ID="lblEditDate" Text='<%#DataBinder.Eval(Container.DataItem , "EditDate")%>'  runat="server" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div class="section-button"><!-- section-button -->
			<asp:Button ID="btnExport" cssClass="btn btn-lg btn-success" runat="server" OnClick="btnExport_Click" Text="확인"></asp:Button>
		</div><!-- // section-button -->
	</div><!-- // contents -->
    </form>

</div><!-- // container -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/jquery-1.4.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.js"></script>

</body>
</html>


