﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Net;

public partial class main_TemplateAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //

    private const int INITIAL_RECTYPE_ID = 1;
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private DataTable dtRecType = new DataTable();
    private DataTable dtTemplate = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  
        if (!IsPostBack)
        {
            BindType();
        }
        else
        {
            dtTemplate.Columns.Add("NO");
            dtTemplate.Columns.Add("RECTYPEID");
            dtTemplate.Columns.Add("TEMPLATEMARKUP");

            GetRecType();
        }

    }

    private void GetRecType() {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);
        //SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "rectype_data");

        //Set up the data binding. 
        Con.Close();
        dtRecType = ds.Tables["rectype_data"];
    }


    protected void PopulateDataTable()
    {
        int i = 1;
        foreach (RepeaterItem item in rpDataList.Items )
        {
            Label lblNo = item.FindControl("lblNo") as Label;
            DropDownList ddlRecType = item.FindControl("ddlRecType") as DropDownList;
            TextBox txtTemplateMarkup = item.FindControl("txtTemplateMarkup") as TextBox;

            DataRow row = dtTemplate.NewRow();
            row["NO"] = i.ToString();
            row["RECTYPEID"] = ddlRecType.SelectedValue;
            row["TEMPLATEMARKUP"] = txtTemplateMarkup.Text;
            i++;
            dtTemplate.Rows.Add(row);
        }
    }

    private void BindType()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, CodeName FROM TCode WHERE CodeType=9 ORDER BY CodeName ASC", Con);

        ListItem li = new ListItem("선택", "0");
        ddlType.Items.Add(li);
        ddlType.AppendDataBoundItems = true;
        DataSet ds = new DataSet();
        sda.Fill(ds, "list_data");
        //Set up the data binding. 
        Con.Close();
        DataTable dtType = ds.Tables["list_data"];
        ddlType.DataSource = dtType;
        ddlType.DataTextField = "CodeName";
        ddlType.DataValueField = "Idx";
        ddlType.DataBind();

        ddlType.AppendDataBoundItems = false;

        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtType = null;
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {

        this.PopulateDataTable();

        DataRow row = dtTemplate.NewRow();
        
        row["RECTYPEID"] = INITIAL_RECTYPE_ID;
        row["NO"] = dtTemplate.Rows.Count;
        dtTemplate.Rows.Add(row);
        Bind();

    }

    protected void Bind(){

        rpDataList.DataSource = dtTemplate;
        rpDataList.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        if (rpDataList.Items.Count == 0) {
            return;
        }
        
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        //템플릿 정보를 저장함.
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Template_INSERT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@Title", SqlDbType.NVarChar , 255);
        Cmd.Parameters.Add("@TypeID", SqlDbType.Int);
        Cmd.Parameters.Add("@RuleCount", SqlDbType.Int);

        Cmd.Parameters["@Title"].Value = txtTemplateName.Text;
        Cmd.Parameters["@TypeID"].Value = Convert.ToInt16(ddlType.SelectedValue);
        Cmd.Parameters["@RuleCount"].Value = rpDataList.Items.Count;;
        //SqlParameter returnParameter = Cmd.Parameters.Add("@RetVal", SqlDbType.Int);
        //returnParameter.Direction = ParameterDirection.ReturnValue;
        //Cmd.Parameters["@RetVal"].Value = returnParameter;
        Con.Open(); 
        SqlDataReader reader;
        reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        string templateIdx = string.Empty;
        if (reader.Read())
        {
            templateIdx = reader["Idx"].ToString();
        }
        if (Con.State == ConnectionState.Open) 
            Con.Close();


        //룰 내용을 저장함. Repeater로부터 데이터를 가져와야함.
        foreach (RepeaterItem item in rpDataList.Items)
        {
            Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.CommandText = "USP_Template_Rule_INSERT";
            Cmd.CommandType = CommandType.StoredProcedure;

            Cmd.Parameters.Add("@TemplateIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@RecType", SqlDbType.Int);
            Cmd.Parameters.Add("@TransMarkup", SqlDbType.NVarChar, 255);

            DropDownList ddlRecType = item.FindControl("ddlRecType") as DropDownList;
            TextBox txtTemplateMarkup = item.FindControl("txtTemplateMarkup") as TextBox;

            Cmd.Parameters["@TemplateIdx"].Value = templateIdx;
            Cmd.Parameters["@RecType"].Value = Convert.ToInt16(ddlRecType.SelectedValue); ;
            Cmd.Parameters["@TransMarkup"].Value = HttpUtility.HtmlEncode((txtTemplateMarkup.Text));
            
            Con.Open();
            Cmd.ExecuteNonQuery();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        Response.Redirect("TemplateList.aspx");
    }

    protected void rpDataList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            DropDownList ddlRecType = e.Item.FindControl("ddlRecType") as DropDownList;

            ddlRecType.DataSource = dtRecType;
            ddlRecType.DataValueField = "Idx";
            ddlRecType.DataTextField = "Title";
            ddlRecType.DataBind();

            int selectRecTypeIndex = Convert.ToInt16( DataBinder.Eval(e.Item.DataItem, "RECTYPEID"));
            ddlRecType.SelectedIndex = selectRecTypeIndex - 1;
            Label lblCMSMarkup = e.Item.FindControl("lblCMSMarkup") as Label;
            lblCMSMarkup.Text = findCMSMarkupText(selectRecTypeIndex);
            Label lblNo = e.Item.FindControl("lblNo") as Label;
            lblNo.Text = (e.Item.ItemIndex+1).ToString();
        }
    }

    private string findCMSMarkupText( int searchIndex ) {
        int recTypeCount = dtRecType.Rows.Count;
        string result = "";

        for (int i = 0; i < recTypeCount; i++) {
            DataRow row = dtRecType.Rows[i];
            int index = Convert.ToInt16(row["Idx"]);

            if (index == searchIndex) {
                result = row["Markup"].ToString();
                break;
            }
            
        }

        return result;
    }

    protected void ddlRecType_OnSelectedIndexChanged(Object sender, EventArgs e){

        this.PopulateDataTable();
        Bind();
    }


    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

}
