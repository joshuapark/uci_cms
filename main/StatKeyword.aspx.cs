﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        
        if (!IsPostBack)
        {
            if (Request.Params["pageNo"] != null)
                pageNo =Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();
            footerTemplate.Visible = false;
            BindCheckBox();
            //BindCategory();
            //Listing();
        }
    }
    private void BindCheckBox()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC", Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dt = ds.Tables["TCode"];

        dt.DefaultView.RowFilter = "CodeType = 3";
        cblSubject.DataSource = dt;
        cblSubject.DataTextField = "CodeName";
        cblSubject.DataValueField = "Idx";
        cblSubject.CssClass = "de-radio";
        cblSubject.DataBind();

        //Close the connection.
        dt = null;
        sda = null;
    }
    private void Listing()
    {
        string SubjectItems = string.Empty;
        foreach (ListItem j in cblSubject.Items)
        {
            if (j.Selected == true)
            {
                SubjectItems += j.Value + ",";
            }
        }
        
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_StatKeyword_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@keyword", SqlDbType.NVarChar);
        Cmd.Parameters.Add("@SubjectList", SqlDbType.VarChar);
        Cmd.Parameters["@keyword"].Value = txtKeyword.Text.Replace(" ", "");
        Cmd.Parameters["@SubjectList"].Value = SubjectItems;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        
        sda.Fill(ds, "data_list");
        DataTable dt = new DataTable();
        sda.Fill(dt);
        
        EntryList.DataSource = ds;
        EntryList.DataBind();

        totalCnt = EntryList.Items.Count ;
        strTotalCnt.Text = totalCnt + "건의 검색결과가 있습니다.";

        lblTotal.Text = dt.Compute("Sum(totalCnt)", "").ToString();
        lblSumE1.Text = dt.Compute("Sum(E1)", "").ToString();
        lblSumE2.Text = dt.Compute("Sum(E2)", "").ToString();
        lblSumE3.Text = dt.Compute("Sum(E3)", "").ToString();
        lblSumE4.Text = dt.Compute("Sum(E4)", "").ToString();
        lblSumE5.Text = dt.Compute("Sum(E5)", "").ToString();
        lblSumE6.Text = dt.Compute("Sum(E6)", "").ToString();
        lblSumE0.Text = dt.Compute("Sum(E0)", "").ToString();
        lblSumM1.Text = dt.Compute("Sum(M1)", "").ToString();
        lblSumM2.Text = dt.Compute("Sum(M2)", "").ToString();
        lblSumM3.Text = dt.Compute("Sum(M3)", "").ToString();
        lblSumM0.Text = dt.Compute("Sum(M0)", "").ToString();
        lblSumH1.Text = dt.Compute("Sum(H1)", "").ToString();
        lblSumH2.Text = dt.Compute("Sum(H2)", "").ToString();
        lblSumH3.Text = dt.Compute("Sum(H3)", "").ToString();
        lblSumH0.Text = dt.Compute("Sum(H0)", "").ToString();
        lblSumCommon.Text = dt.Compute("Sum(Common)", "").ToString();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
        footerTemplate.Visible = true;
    }
    //private void BindCategory()
    //{
    //    ddlSubjectGroup.Items.Clear();

    //    ListItem li = new ListItem("선택", "0");
    //    ddlSubjectGroup.Items.Add(li);
    //    ddlSubjectGroup.AppendDataBoundItems = true;


    //    DBFileInfo();
    //    SqlConnection Conn = new SqlConnection(connectionString);
    //    Conn.Open();
    //    string strQuery = "SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY CodeName ASC";
    //    SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

    //    //SqlDataReader Reader = Cmd.ExecuteReader();
    //    DataSet ds = new DataSet();
    //    sda.Fill(ds, "TCode");
    //    //Set up the data binding. 
    //    Conn.Close();

    //    DataTable dtSubjectGroup = ds.Tables["TCode"];
    //    dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
    //    ddlSubjectGroup.DataSource = dtSubjectGroup;
    //    ddlSubjectGroup.DataTextField = "CodeName";
    //    ddlSubjectGroup.DataValueField = "Idx";
    //    ddlSubjectGroup.DataBind();

    //    //Close the connection.
    //    dtSubjectGroup = null;
    //    ddlSubjectGroup.AppendDataBoundItems = false;
    //    sda = null;


    //}
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        Listing();
    }
}
