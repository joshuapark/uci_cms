﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class main_opencontents : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string strType = string.Empty;
    public string strTitle = "대분류";
    private string strEntryIdx = string.Empty;
    private string exportIdx = string.Empty;

    private DataTable categoryTable = new DataTable();
    private DataTable entryTable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        if (!IsPostBack)
        {
            if (Request.Params["exportIdx"] != null)
            {
                exportIdx = Request.Params["exportIdx"].ToString();
                txtExportIdx.Value = exportIdx;
            }
            BindCategory();
        }
        else
        {
            exportIdx = txtExportIdx.Value;
        }
    }



    private void BindCategory()
    {
        DBFileInfo();

        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = "SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC";
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

        ListItem li = new ListItem("선택", "0");
        ddlRevision.Items.Add(li);
        //ddlSubjectGroup.Items.Add(li);
        ddlSubject.Items.Add(li);
        //ddlSchool.Items.Add(li);
        ddlBrand.Items.Add(li);
        ddlGrade.Items.Add(li);
        ddlSemester.Items.Add(li);
        ddlLUnit.Items.Add(li);
        ddlMUnit.Items.Add(li);

        ddlRevision.AppendDataBoundItems = true;
        //ddlSubjectGroup.AppendDataBoundItems = true;
        ddlSubject.AppendDataBoundItems = true;
        //ddlSchool.AppendDataBoundItems = true;
        ddlBrand.AppendDataBoundItems = true;
        ddlGrade.AppendDataBoundItems = true;
        ddlSemester.AppendDataBoundItems = true;
        ddlLUnit.AppendDataBoundItems = true;
        ddlMUnit.AppendDataBoundItems = true;
        
        //SqlDataReader Reader = Cmd.ExecuteReader();
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        ddlRevision.DataSource = dtRevision;
        ddlRevision.DataTextField = "CodeName";
        ddlRevision.DataValueField = "Idx";
        ddlRevision.DataBind();

        //DataTable dtSubjectGroup = ds.Tables["TCode"];
        //dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        //ddlSubjectGroup.DataSource = dtSubjectGroup;
        //ddlSubjectGroup.DataTextField = "CodeName";
        //ddlSubjectGroup.DataValueField = "Idx";
        //ddlSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        ddlSubject.DataSource = dtSubject;
        ddlSubject.DataTextField = "CodeName";
        ddlSubject.DataValueField = "Idx";
        ddlSubject.DataBind();

        //DataTable dtSchool = ds.Tables["TCode"];
        //dtSchool.DefaultView.RowFilter = "CodeType = 4";
        //ddlSchool.DataSource = dtSchool;
        //ddlSchool.DataTextField = "CodeName";
        //ddlSchool.DataValueField = "Idx";
        //ddlSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        ddlBrand.DataSource = dtBrand;
        ddlBrand.DataTextField = "CodeName";
        ddlBrand.DataValueField = "Idx";
        ddlBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        ddlGrade.DataSource = dtGrade;
        ddlGrade.DataTextField = "CodeName";
        ddlGrade.DataValueField = "Idx";
        ddlGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        ddlSemester.DataSource = dtSemester;
        ddlSemester.DataTextField = "CodeName";
        ddlSemester.DataValueField = "Idx";
        ddlSemester.DataBind();

        //Close the connection.
        dtRevision = null;
        //dtSubjectGroup = null;
        dtSubject = null;
        //dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;


        ddlRevision.SelectedValue = "0";
        //ddlSubjectGroup.SelectedValue = "0";
        ddlSubject.SelectedValue = "0";
        //ddlSchool.SelectedValue = "0";
        ddlBrand.SelectedValue = "0";
        ddlGrade.SelectedValue = "0";
        ddlSemester.SelectedValue = "0";
        ddlLUnit.SelectedValue = "0";
        ddlMUnit.SelectedValue = "0";
    }

    private void LUnitListing()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_LUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
        //Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
        //Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TLUnit");
        //Set up the data binding. 
        Conn.Close();
        ListItem li = new ListItem("선택", "0");
        ddlLUnit.Items.Add(li);
        ddlLUnit.AppendDataBoundItems = true;
        DataTable dtLUnit = ds.Tables["TLUnit"];
        ddlLUnit.DataSource = dtLUnit;
        ddlLUnit.DataTextField = "LUnitName";
        ddlLUnit.DataValueField = "Idx";
        ddlLUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
        ddlLUnit.AppendDataBoundItems = false;
    }
    private void MUnitListing()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_MUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters["@LUnitIdx"].Value = ddlLUnit.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TMUnit");
        //Set up the data binding. 
        Conn.Close();
        ListItem li = new ListItem("선택", "0");
        ddlMUnit.Items.Add(li);
        ddlMUnit.AppendDataBoundItems = true;
        DataTable dtLUnit = ds.Tables["TMUnit"];
        ddlMUnit.DataSource = dtLUnit;
        ddlMUnit.DataTextField = "MUnitName";
        ddlMUnit.DataValueField = "Idx";
        ddlMUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;

        ddlMUnit.AppendDataBoundItems = false;
    }
    private void Listing()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Entry_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar, 30);
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = Convert.ToInt32(ddlRevision.SelectedValue);
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0; //Convert.ToInt32(ddlSubjectGroup.SelectedValue);
        Cmd.Parameters["@SubjectIdx"].Value = Convert.ToInt32(ddlSubject.SelectedValue);
        Cmd.Parameters["@SchoolIdx"].Value = 0; // Convert.ToInt32(ddlSchool.SelectedValue);
        Cmd.Parameters["@BrandIdx"].Value = Convert.ToInt32(ddlBrand.SelectedValue);
        Cmd.Parameters["@GradeIdx"].Value = Convert.ToInt32(ddlGrade.SelectedValue);
        Cmd.Parameters["@SemesterIdx"].Value = Convert.ToInt32(ddlSemester.SelectedValue);
        if (ddlLUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@LUnitIdx"].Value = Convert.ToInt32(ddlLUnit.SelectedValue);
        else
            Cmd.Parameters["@LUnitIdx"].Value = 0;
        if (ddlMUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@MUnitIdx"].Value = Convert.ToInt32(ddlMUnit.SelectedValue);
        else
            Cmd.Parameters["@MUnitIdx"].Value = 0;
        Cmd.Parameters["@UserIdx"].Value = null;
        Cmd.Parameters["@KeyWord"].Value = txtKeyWord.Text;
        Cmd.Parameters["@TaskIdx"].Value = null;
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();
        if (ds.Tables["data_list"].Rows.Count>0)
        {
            totalCnt = Convert.ToInt32(ds.Tables["data_list"].Rows[0]["totalCnt"].ToString());
        }        
        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);
        lblTotalCnt.Text = totalCnt.ToString() + "건의 검색결과가 있습니다.";
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }
    
    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void MUnitSearch_Click(object sender, EventArgs e)
    {
        MUnitListing();
    }
    protected void LUnitSearch_Click(object sender, EventArgs e)
    {
        LUnitListing();
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        Listing();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        string strQuery = string.Empty;
        string strSelectEntryIdx = string.Empty;
        string strSelectEntryTitle = string.Empty;
        int totalEntCount = 0;
        foreach (RepeaterItem item in EntryList.Items)
        {
            CheckBox chkID = item.FindControl("chkID") as CheckBox;
            HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
            Label lblTitle = item.FindControl("lblTitle") as Label;
            if (chkID.Checked)
            {
                strSelectEntryIdx += txtIDX.Value + ",";
                //strSelectEntryTitle += lblTitle.Text +",";
                
                totalEntCount++;
            }
            
        }
        string strRevision = string.Empty, strSubject = string.Empty, strBrand = string.Empty, strGrade = string.Empty, strSemester = string.Empty;
        if (ddlRevision.SelectedValue != "0")
            strRevision = ddlRevision.SelectedItem.Text +">";
        if (ddlSubject.SelectedValue != "0" )
            strSubject = ddlSubject.SelectedItem.Text + ">";
        if (ddlBrand.SelectedValue != "0")
            strBrand = ddlBrand.SelectedItem.Text + ">";
        if (ddlGrade.SelectedValue != "0")
            strGrade = ddlGrade.SelectedItem.Text + ">";
        if (ddlSemester.SelectedValue != "0")
            strSemester = ddlSemester.SelectedItem.Text + ">";
        strSelectEntryTitle = strRevision + strSubject + strBrand + strGrade + strSemester;
        if (strSelectEntryTitle.Length > 1)
            strSelectEntryTitle = strSelectEntryTitle.Substring(0, strSelectEntryTitle.Length - 1);
        else 
            strSelectEntryTitle = txtKeyWord.Text;

        strSelectEntryIdx = strSelectEntryIdx.Substring(0, strSelectEntryIdx.Length - 1);
        //strSelectEntryTitle = strSelectEntryTitle.Substring(0, strSelectEntryTitle.Length - 1);

        DBFileInfo();
        strQuery = "INSERT INTO TExportData (Type, ExportIdx, Condition, Content, EntryCount) Values "
            + " ('Category', " + exportIdx + ",'" + strSelectEntryTitle + "', '" + strSelectEntryIdx + "', " + totalEntCount + " )";
        
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdd = new SqlCommand(strQuery, Conn);
        Cmdd.CommandType = CommandType.Text;
        Conn.Open();
        Cmdd.ExecuteNonQuery();
        Conn.Close();
        exportIdx = txtExportIdx.Value;
        //Response.Redirect("ExportAdd.aspx?exportIdx=" + txtExportIdx.Value + "");
        Response.Write("<script>opener.location.href='ExportAdd.aspx?exportIdx=" + exportIdx + "';window.opener=self;self.close();</script>");
        //Response.Write("<script>opener.document.getElementById('selCategory').value='" + strSelectTaskIdx + "';opener.document.getElementById('selCEntryCnt').value='" + totalEntCount + "';</script>");
        //Response.Write("<script>opener.EntSum();self.close();</script>");
    }
}
