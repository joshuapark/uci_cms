﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.Security.Cryptography;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "500";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string Idx = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  
        
        if (!IsPostBack)
        {
            if (Request.Params["pageNo"] != null)
                pageNo =Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();
            if (Request.Params["Idx"] != null)
                Idx = Request.Params["Idx"].ToString();
            txtIdx.Value = Idx;
            Listing();
        }
    }


    private void Listing()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_USER_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "bbs_notice");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage); 
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        { 
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 등급설정을 설정한다.
            Label lblType = item.FindControl("lblType") as Label;
            lblType.Text = DataBinder.Eval(item.DataItem, "UserType").ToString();

            // 제목과 상세보기 링크를 설정한다.
            HyperLink hlName = item.FindControl("hlName") as HyperLink;
            hlName.Text = DataBinder.Eval(item.DataItem, "UserName").ToString();

            // ID설정을 설정한다.
            Label lblUserId = item.FindControl("lblUserId") as Label;
            lblUserId.Text = DataBinder.Eval(item.DataItem, "UserId").ToString();

            // 부서설정을 설정한다.
            Label lblTeam = item.FindControl("lblTeam") as Label;
            lblTeam.Text = DataBinder.Eval(item.DataItem, "Team").ToString();
            
            // Email설정을 설정한다.
            Label lblEmail = item.FindControl("lblEmail") as Label;
            lblEmail.Text = DataBinder.Eval(item.DataItem, "Email").ToString();

            // 연락처설정을 설정한다.
            Label lblPhone = item.FindControl("lblPhone") as Label;
            lblPhone.Text = DataBinder.Eval(item.DataItem, "Phone").ToString();

            // 사용여부설정을 설정한다.
            Label lblEnabled = item.FindControl("lblEnabled") as Label;
            string strStatus = string.Empty;
            if ((bool)DataBinder.Eval(item.DataItem, "Enabled"))
                strStatus = "사용";
            else
                strStatus = "미사용";

            lblEnabled.Text = strStatus;
            
            // 등록일을 설정한다.
            Label lblInsertDate = item.FindControl("lblInsertDate") as Label;
            lblInsertDate.Text = DataBinder.Eval(item.DataItem, "InsertDate").ToString();
            
            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
            strTotalCnt.Text = totalCnt + "건의 검색결과가 있습니다.";
        }
    }

    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (rfvTxtEmail.IsValid && rfvTxtPwd1.IsValid && rfvTxtPwd2.IsValid && rfvTxtName.IsValid && rfvRadio.IsValid && ComparePwd.IsValid)
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT UserId FROM TUser WHERE UserId='" + txtID.Text.Trim() + "'", Con);
            Cmd0.CommandType = CommandType.Text;
            Con.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                msgIDCheck.InnerText = "* ID가 이미 사용 중 입니다. 다른 ID를 입력해주세요.";
            }
            else
            {
                try
                {
                    DBFileInfo();

                    SqlConnection Conn = new SqlConnection(connectionString);
                    SqlCommand Cmd = new SqlCommand("USP_User_INSERT", Conn);
                    Cmd.CommandType = CommandType.StoredProcedure;

                    Cmd.Parameters.Add("@ID", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@UserTypeIdx", SqlDbType.Int);
                    Cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    Cmd.Parameters.Add("@Name", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@Team", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@Password", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@Email", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@Phone", SqlDbType.VarChar, 255);
                    Cmd.Parameters.Add("@Enabled", SqlDbType.Bit);
                    Cmd.Parameters["@ID"].Value = txtID.Text;
                    Cmd.Parameters["@UserTypeIdx"].Value = int.Parse(ddlUserType.SelectedValue);
                    Cmd.Parameters["@UserType"].Value = ddlUserType.SelectedItem.Text;
                    Cmd.Parameters["@Name"].Value = txtName.Text;
                    Cmd.Parameters["@Team"].Value = txtTeam.Text;
                    Cmd.Parameters["@Password"].Value = EncodeSHA(txtPwd1.Text);
                    Cmd.Parameters["@Email"].Value = txtEmail.Text;
                    Cmd.Parameters["@Phone"].Value = txtPhone.Text;
                    Cmd.Parameters["@Enabled"].Value = Convert.ToInt32(radioEnabled.SelectedValue);

                    Conn.Open();
                    Cmd.ExecuteNonQuery();
                    if (Conn.State == ConnectionState.Open)
                        Conn.Close();
                    Cmd = null;
                    Conn = null;
                }
                catch (Exception ex)
                {
                    string script = "<script>alert('회원가입 도중 오류가 발생했습니다. 관리자에게 문의하세요.\n" + ex.Message + "');</script>";
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                }

                if (Con.State == ConnectionState.Open)
                    Con.Close();
                Con = null;
                Cmd0 = null;
                Response.Redirect("UserList.aspx");
            }
        }
        
    }
    private string EncodeSHA(string pw)
    {
        SHA256 crypt = SHA256Managed.Create();

        string hash = String.Empty;
        byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(pw), 0, Encoding.ASCII.GetByteCount(pw));
        foreach (byte bit in crypto)
        {
            hash += bit.ToString("x2");
        }
        hash = hash.Substring(0, 16);
        return hash;
    }
}
