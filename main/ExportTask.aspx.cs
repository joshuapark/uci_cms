﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Text;

public partial class XmlImport2 : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string strTaskIdx = string.Empty;
    private string pageNo = "1";
    private string pageSize = "100000";
    private int totalCnt = 0;
    private int totalEntCount = 0;
    private string exportIdx = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  
        if(!IsPostBack){
            if (Request.Params["exportIdx"] != null)
            {
                exportIdx = Request.Params["exportIdx"].ToString();
                txtExportIdx.Value = exportIdx;

            }

            BindUser();
            ListItem li = new ListItem("선택", "0");
            ddlTask.Items.Add(li);
            ddlTask.AppendDataBoundItems = true;
        }
        else
        {
            exportIdx = txtExportIdx.Value;
        }
    }
    private void BindUser()
    {
        ListItem li = new ListItem("선택", "0");
        ddlUser.Items.Add(li);
        ddlUser.AppendDataBoundItems = true;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, UserId, UserName FROM TUser WHERE UserType='작업자' AND DelFlag=0 ORDER BY UserName ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "TUser");
        //Set up the data binding. 
        Con.Close();
        DataTable dtUser = ds.Tables["TUser"];
        ddlUser.DataSource = dtUser;
        ddlUser.DataTextField = "UserName";
        ddlUser.DataValueField = "Idx";
        ddlUser.DataBind();
        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtUser = null;
        ddlTask.AppendDataBoundItems = false;
    }
    protected void TaskSearch_Click(object sender, EventArgs e)
    {
        ListItem li = new ListItem("선택", "0");
        ddlTask.Items.Clear();
        ddlTask.Items.Add(li);
        ddlTask.AppendDataBoundItems = true;
        
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = string.Empty;
        if (ddlUser.SelectedValue == "0" || ddlUser.SelectedValue.Length == 0)
        {
            strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE DelFlag=0 AND Status>0 ORDER BY TaskTitle ASC";
        }
        else
        {
            strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE UserIdx=" + ddlUser.SelectedValue
                    + " AND DelFlag=0 AND Status>0  ORDER BY TaskTitle ASC";
        }
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TTaskID");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtTask = ds.Tables["TTaskID"];
        ddlTask.DataSource = dtTask;
        ddlTask.DataTextField = "TaskTitle";
        ddlTask.DataValueField = "Idx";
        ddlTask.DataBind();


        ddlTask.AppendDataBoundItems = false;

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        Listing();
    }
    
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
            //rootPath = "http://test-www2012.isherpa.co.kr/UploadFile/Mobile/Sibung/Notice/";
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
            //rootPath = "http://www.isherpa.co.kr/UploadFile/Mobile/Sibung/Notice/";
        }
    }
    private void Listing()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_EntryAdd_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@dateType", SqlDbType.Char);
        Cmd.Parameters.Add("@startDate", SqlDbType.Date);
        Cmd.Parameters.Add("@endDate", SqlDbType.Date);
        Cmd.Parameters.Add("@userIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@taskIDX", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        string dateType = ddlDateType.SelectedValue;
        if (txtStartDate.Text.Length > 0)
            Cmd.Parameters["@startDate"].Value = Convert.ToDateTime(txtStartDate.Text);
        if (txtEndDate.Text.Length > 0)
            Cmd.Parameters["@endDate"].Value = Convert.ToDateTime(txtEndDate.Text);
        Cmd.Parameters["@userIdx"].Value = ddlUser.SelectedValue;
        if (ddlTask.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@taskIdx"].Value = Convert.ToInt32(ddlTask.SelectedValue);
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);
        if (txtStartDate.Text.Length == 0 || txtEndDate.Text.Length == 0)
            dateType = "A";
        Cmd.Parameters["@dateType"].Value = dateType;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();

        //int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        //MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);
        //lblTotalCnt.Text = totalCnt + "건의 검색결과가 있습니다.";
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        {
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 작업자
            Label lblUser = item.FindControl("lblUser") as Label;
            lblUser.Text = DataBinder.Eval(item.DataItem, "UserName").ToString();
            // 관리자
            Label lblManager = item.FindControl("lblManager") as Label;
            lblManager.Text = DataBinder.Eval(item.DataItem, "ManagerName").ToString();
            // 엔트리수
            Label lblEntryCount = item.FindControl("lblEntryCount") as Label;
            lblEntryCount.Text = DataBinder.Eval(item.DataItem, "EntryCount").ToString();
            // 작업배포일
            Label lblPublishDate = item.FindControl("lblPublishDate") as Label;
            lblPublishDate.Text = DataBinder.Eval(item.DataItem, "PublishDate").ToString();
            // 작업완료일
            Label lblCheckDate = item.FindControl("lblCheckDate") as Label;
            lblCheckDate.Text = DataBinder.Eval(item.DataItem, "CheckDate").ToString();
            // 상태
            Label lblStatus = item.FindControl("lblStatus") as Label;
            int intStatus = Convert.ToInt32(DataBinder.Eval(item.DataItem, "Status").ToString());
            string strStatus = string.Empty;
            if (intStatus == 1 || intStatus == 2)
            {
                strStatus = "작업중";
            }
            else if (intStatus == 3)
            {
                strStatus = "검수대기";
            }
            else if (intStatus == 4)
            {
                strStatus = "검수중";
            }
            else if (intStatus == 5)
            {
                strStatus = "검수완료";
            }

            lblStatus.Text = DataBinder.Eval(item.DataItem, "Status").ToString();
            // 작업명과 상세보기 링크를 설정한다.
            HyperLink lblTitle = item.FindControl("lblTitle") as HyperLink;
            lblTitle.Text = DataBinder.Eval(item.DataItem, "TaskTitle").ToString();
            lblTitle.NavigateUrl = string.Format("EntryAdd.aspx?Idx={0}&pageNo={1}&pageSize={2}", DataBinder.Eval(item.DataItem, "Idx"), pageNo, pageSize);

            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        //makeXMLByTask();
        string strQuery = string.Empty;
        string strSelectTaskIdx = string.Empty;
        string strSelectTaskTitle = string.Empty;
        foreach (RepeaterItem item in EntryList.Items)
        {
            CheckBox chkID = item.FindControl("chkID") as CheckBox;
            HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
            Label lblTitle = item.FindControl("lblTitle") as Label;
            if (chkID.Checked)
            {
                strSelectTaskIdx += txtIDX.Value +",";
                strSelectTaskTitle += lblTitle.Text + ",";
                DBFileInfo();
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Connection = Con;
                strQuery = "SELECT count(Idx) as Cnt FROM TEntry WHERE TaskIdx=" + txtIDX.Value + " ";
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = strQuery;
                Con.Open();
                SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    totalEntCount += Convert.ToInt32(reader["cnt"].ToString());
                }
                reader.Close();
                Con.Close();
            }
        }
        strSelectTaskIdx = strSelectTaskIdx.Substring(0, strSelectTaskIdx.Length - 1);
        strSelectTaskTitle = strSelectTaskTitle.Substring(0, strSelectTaskTitle.Length - 1);
        
        strQuery = "INSERT INTO TExportData (Type, ExportIdx, Condition, Content, EntryCount) Values "
            + " ('TASK', " + exportIdx + ",'" + strSelectTaskTitle + "', '" + strSelectTaskIdx + "', " + totalEntCount + " )";
        SqlConnection Conn = new SqlConnection(connectionString);

        SqlCommand Cmdd = new SqlCommand(strQuery, Conn);
        Cmdd.CommandType = CommandType.Text;
        Conn.Open();
        Cmdd.ExecuteNonQuery();
        Conn.Close();
        exportIdx = txtExportIdx.Value;
        //Response.Redirect("ExportAdd.aspx?exportIdx="+  txtExportIdx.Value +"");
        Response.Write("<script>opener.location.href='ExportAdd.aspx?exportIdx=" + exportIdx + "';window.opener=self;self.close();</script>");
    }


    protected void makeXMLByTask()
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        //strTaskIdx = "60";
        // Set a variable to the My Documents path. 
        string mydocpath = HttpContext.Current.Server.MapPath("~/DATA/"); //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        //string FileNewName = "XML_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + ImageFileUpload.PostedFile.FileName;
        //string sFileUri = HttpContext.Current.Server.MapPath("~/DATA/") + "XML_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";

        StringBuilder sb = new StringBuilder();
        string strXML = string.Empty;
        sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.AppendLine("<edu>");

        foreach (RepeaterItem item in EntryList.Items)
        {
            CheckBox chkID = item.FindControl("chkID") as CheckBox;
            HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
            if (chkID.Checked)
            {
                strTaskIdx = txtIDX.Value;

                DBFileInfo();
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Connection = Con;
                strQuery = "SELECT * FROM TEntry WHERE TaskIdx=" + strTaskIdx + " ORDER BY Idx ASC";
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = strQuery;
                Con.Open();

                DataTable dt = new DataTable();
                SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
                adapter.Fill(dt);
                totalEntCount += dt.Rows.Count;
                foreach (DataRow row in dt.Rows)
                {
                    //strXML = "";
                    //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">\n";
                    //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
                    //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                    //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                    //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

                    strEntryIdx = row["Idx"].ToString();

                    sb.AppendLine();
                    sb.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                    sb.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                    string strTitleK = string.Empty;
                    if (row["EntryTitleK"].ToString().Length > 0)
                        strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                    else
                        strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                    string strTitleE = string.Empty;
                    if (row["EntryTitleE"].ToString().Length > 0)
                        strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                    else
                        strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                    string strTitleC = string.Empty;
                    if (row["EntryTitleC"].ToString().Length > 0)
                        strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                    else
                        strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";

                    sb.AppendLine(strTitleK);
                    sb.AppendLine(strTitleE);
                    sb.AppendLine(strTitleC);
                    //reader.Close();
                    Con.Close();

                    SqlConnection Con2 = new SqlConnection(connectionString);
                    SqlCommand Cmd2 = new SqlCommand();
                    Cmd2.Connection = Con2;

                    //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
                    //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
                    Cmd2.CommandText = "SELECT A.CodeName + '>'+ B.CodeName+ '>'+ C.CodeName+ '>'+ D.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                        + " FROM TEntry as Y "
                        + " join TCode as A on A.Idx=Y.RevisionIdx "
                        + "	join TCode as B on B.Idx=Y.SubjectGroupIdx "
                        + "	join TCode as C on C.Idx=Y.SubjectIdx "
                        + " join TCode as D on D.Idx=Y.SchoolIdx "
                        + "	join TCode as E on E.Idx=Y.BrandIdx  "
                        + "	join TCode as F on F.Idx=Y.GradeIdx  "
                        + "	join TCode as G on G.Idx=Y.SemesterIdx "
                        + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                        + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                        + " WHERE Y.Idx=" + strEntryIdx + "";
                    Cmd2.CommandType = CommandType.Text;
                    Con2.Open();
                    SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                    if (reader2.Read())
                    {
                        string[] strCategory = reader2["category"].ToString().Split('>');
                        foreach (string strUnit in strCategory)
                        {
                            sb.AppendLine("<DIGIT>" + strUnit + "</DIGIT>");
                        }
                    }
                    reader2.Close();
                    Con2.Close();

                    Con2 = new SqlConnection(connectionString);
                    Cmd2 = new SqlCommand();
                    Cmd2.Connection = Con2;
                    Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 ORDER BY Idx ASC";
                    Cmd2.CommandType = CommandType.Text;
                    Con2.Open();
                    DataTable dt2 = new DataTable();
                    SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                    adapter2.Fill(dt2);
                    foreach (DataRow row2 in dt2.Rows)
                    {
                        sb.AppendLine("<" + row2["RecType"].ToString() + ">");
                        sb.AppendLine(row2["Content"].ToString());
                        sb.AppendLine("</" + row2["RecType"].ToString() + ">");

                    }
                    Con2.Close();
                    sb.AppendLine("</ENTRY>");
                }
            }
        }

        sb.AppendLine("</edu>");
        XmlDocument xd = new XmlDocument();
        xd.LoadXml(sb.ToString());

        // Write the stream contents to a new file named 
        string FileNewName = "XML_Export_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";
        using (StreamWriter outfile = new StreamWriter(mydocpath + @FileNewName))
        {
            outfile.Write(xd.InnerXml);
        }

        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmdd = new SqlCommand();
        try
        {
            strQuery = "Insert Into TImportFile (TaskIdx, ImgFileName) Values (" + strTaskIdx + ",'" + FileNewName + "')";
            Cmdd = new SqlCommand(strQuery, Conn);
            Cmdd.CommandType = CommandType.Text;
            Conn.Open();
            Cmdd.ExecuteNonQuery();
            Conn.Close();
        }
        catch (Exception ex)
        {
            //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
        }
        finally
        {

        }

    }
}