﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "100";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string paramType = string.Empty;
    private string paramRevision = "0";
    private string paramSubjectGroup = "0";
    private string paramSubject = "0";
    private string paramSchool = "0";
    private string paramBrand = "0";
    private string paramGrade = "0";
    private string paramSemester = "0";
    private string paramLUnit = "0";
    private string paramMUnit = "0";
    private string paramKeyword = string.Empty;
    private string paramUser = "0";
    private string paramTask = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }
        
        if (!IsPostBack)
        {
            //--- DropDownList 초기화
            BindCategory();
            BindUser();
            DownButton.Visible = false;
            pagepart.Visible = false;


        }
        else
        {
            if (txtPageNo.Value.Length > 0)
                pageNo = txtPageNo.Value;

            Listing();

        }
    }
    private void BindCategory()
    {
        ddlRevision.Items.Clear();
        //ddlSubjectGroup.Items.Clear();
        ddlSubject.Items.Clear();
        //ddlSchool.Items.Clear();
        ddlBrand.Items.Clear();
        ddlGrade.Items.Clear();
        ddlSemester.Items.Clear();

        ListItem li = new ListItem("선택", "0");
        ddlRevision.Items.Add(li);
        //ddlSubjectGroup.Items.Add(li);
        ddlSubject.Items.Add(li);
        //ddlSchool.Items.Add(li);
        ddlBrand.Items.Add(li);
        ddlGrade.Items.Add(li);
        ddlSemester.Items.Add(li);

        ddlRevision.AppendDataBoundItems = true;
        //ddlSubjectGroup.AppendDataBoundItems = true;
        ddlSubject.AppendDataBoundItems = true;
        //ddlSchool.AppendDataBoundItems = true;
        ddlBrand.AppendDataBoundItems = true;
        ddlGrade.AppendDataBoundItems = true;
        ddlSemester.AppendDataBoundItems = true;


        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = "SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY CodeName ASC";
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

        //SqlDataReader Reader = Cmd.ExecuteReader();
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        ddlRevision.DataSource = dtRevision;
        ddlRevision.DataTextField = "CodeName";
        ddlRevision.DataValueField = "Idx";
        ddlRevision.DataBind();

        //DataTable dtSubjectGroup = ds.Tables["TCode"];
        //dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        //ddlSubjectGroup.DataSource = dtSubjectGroup;
        //ddlSubjectGroup.DataTextField = "CodeName";
        //ddlSubjectGroup.DataValueField = "Idx";
        //ddlSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        dtSubject.DefaultView.Sort = "CodeName ASC";
        ddlSubject.DataSource = dtSubject;
        ddlSubject.DataTextField = "CodeName";
        ddlSubject.DataValueField = "Idx";
        ddlSubject.DataBind();

        //DataTable dtSchool = ds.Tables["TCode"];
        //dtSchool.DefaultView.RowFilter = "CodeType = 4";
        //ddlSchool.DataSource = dtSchool;
        //ddlSchool.DataTextField = "CodeName";
        //ddlSchool.DataValueField = "Idx";
        //ddlSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        dtBrand.DefaultView.Sort = "CodeName ASC";
        ddlBrand.DataSource = dtBrand;
        ddlBrand.DataTextField = "CodeName";
        ddlBrand.DataValueField = "Idx";
        ddlBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        dtGrade.DefaultView.Sort = "Idx ASC";
        ddlGrade.DataSource = dtGrade;
        ddlGrade.DataTextField = "CodeName";
        ddlGrade.DataValueField = "Idx";
        ddlGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        ddlSemester.DataSource = dtSemester;
        ddlSemester.DataTextField = "CodeName";
        ddlSemester.DataValueField = "Idx";
        ddlSemester.DataBind();

        //Close the connection.
        dtRevision = null;
        //dtSubjectGroup = null;
        dtSubject = null;
        //dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;

        ddlRevision.AppendDataBoundItems = false;
        //ddlSubjectGroup.AppendDataBoundItems = false;
        ddlSubject.AppendDataBoundItems = false;
        //ddlSchool.AppendDataBoundItems = false;
        ddlBrand.AppendDataBoundItems = false;
        ddlGrade.AppendDataBoundItems = false;
        ddlSemester.AppendDataBoundItems = false;

    }
    private void BindUser()
    {
        ddlUser.Items.Clear();
        ListItem li = new ListItem("선택", "0");
        ddlUser.Items.Add(li);
        ddlUser.AppendDataBoundItems = true;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, UserName FROM TUser WHERE UserType='작업자' AND DelFlag=0 ORDER BY UserName ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "TUser");
        //Set up the data binding. 
        Con.Close();
        DataTable dtUser = ds.Tables["TUser"];
        ddlUser.DataSource = dtUser;
        ddlUser.DataTextField = "UserName";
        ddlUser.DataValueField = "Idx";
        ddlUser.DataBind();
        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtUser = null;

        ddlUser.AppendDataBoundItems = false;
    }
    private void LUnitListing()
    {
        ddlLUnit.Items.Clear();
        ListItem li = new ListItem("선택", "0");
        ddlLUnit.Items.Add(li);
        ddlLUnit.AppendDataBoundItems = true; 
        
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_LUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
        //Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
        //Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TLUnit");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtLUnit = ds.Tables["TLUnit"];
        ddlLUnit.DataSource = dtLUnit;
        ddlLUnit.DataTextField = "LUnitName";
        ddlLUnit.DataValueField = "Idx";
        ddlLUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
        ddlLUnit.AppendDataBoundItems = false;
    }
    private void MUnitListing()
    {
        ddlMUnit.Items.Clear();
        ListItem li = new ListItem("선택", "0");
        ddlMUnit.Items.Add(li);
        ddlMUnit.AppendDataBoundItems = true; 
        
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_MUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters["@LUnitIdx"].Value = ddlLUnit.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TMUnit");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtLUnit = ds.Tables["TMUnit"];
        ddlMUnit.DataSource = dtLUnit;
        ddlMUnit.DataTextField = "MUnitName";
        ddlMUnit.DataValueField = "Idx";
        ddlMUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
        ddlMUnit.AppendDataBoundItems = false;
    }
    private void Listing()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Entry_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar, 30);
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = Convert.ToInt32(ddlRevision.SelectedValue);
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0;   //  Convert.ToInt32(ddlSubjectGroup.SelectedValue);
        Cmd.Parameters["@SubjectIdx"].Value = Convert.ToInt32(ddlSubject.SelectedValue);
        Cmd.Parameters["@SchoolIdx"].Value = 0;   // Convert.ToInt32(ddlSchool.SelectedValue);
        Cmd.Parameters["@BrandIdx"].Value = Convert.ToInt32(ddlBrand.SelectedValue);
        Cmd.Parameters["@GradeIdx"].Value = Convert.ToInt32(ddlGrade.SelectedValue);
        Cmd.Parameters["@SemesterIdx"].Value = Convert.ToInt32(ddlSemester.SelectedValue);
        if (ddlLUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@LUnitIdx"].Value = Convert.ToInt32(ddlLUnit.SelectedValue);
        else
            Cmd.Parameters["@LUnitIdx"].Value = 0;
        if (ddlMUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@MUnitIdx"].Value = Convert.ToInt32(ddlMUnit.SelectedValue);
        if (ddlUser.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@UserIdx"].Value = Convert.ToInt32(ddlUser.SelectedValue);
        Cmd.Parameters["@KeyWord"].Value = txtKeyWord.Text;
        if (ddlTask.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@TaskIdx"].Value = Convert.ToInt32(ddlTask.SelectedValue);
        else
            Cmd.Parameters["@TaskIdx"].Value = 0;
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);
        txtTotalCount.Value = totalCnt.ToString();
        txtTotalPage.Value = totalPage.ToString();
        lblTotalCnt.Text = totalCnt.ToString() + "건의 검색결과가 있습니다.";
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        { 
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 개정
            Label lblRevision = item.FindControl("lblRevision") as Label;
            lblRevision.Text = DataBinder.Eval(item.DataItem, "Revision").ToString();

            // 과목군
            //Label lblSubjectGroup = item.FindControl("lblSubjectGroup") as Label;
            //lblSubjectGroup.Text = DataBinder.Eval(item.DataItem, "SubjectGroup").ToString();

            // 과목
            Label lblSubject = item.FindControl("lblSubject") as Label;
            lblSubject.Text = DataBinder.Eval(item.DataItem, "Subject").ToString();
            
            // 학교군
            //Label lblSchool = item.FindControl("lblSchool") as Label;
            //lblSchool.Text = DataBinder.Eval(item.DataItem, "School").ToString();

            // 브랜드
            Label lblBrand = item.FindControl("lblBrand") as Label;
            lblBrand.Text = DataBinder.Eval(item.DataItem, "Brand").ToString();
            
            // 학년
            Label lblGrade = item.FindControl("lblGrade") as Label;
            lblGrade.Text = DataBinder.Eval(item.DataItem, "Grade").ToString();
            
            // 학기
            Label lblSemester = item.FindControl("lblSemester") as Label;
            lblSemester.Text = DataBinder.Eval(item.DataItem, "Semester").ToString();

            // 대단원
            Label lblLUnit = item.FindControl("lblLUnit") as Label;
            lblLUnit.Text = DataBinder.Eval(item.DataItem, "LUnit").ToString();

            // 중단원
            Label lblMUnit = item.FindControl("lblMUnit") as Label;
            lblMUnit.Text = DataBinder.Eval(item.DataItem, "MUnit").ToString();

            // 제목과 상세보기 링크를 설정한다.
            HyperLink lblTitle = item.FindControl("lblTitle") as HyperLink;
            lblTitle.Text = DataBinder.Eval(item.DataItem, "Title").ToString();
            lblTitle.NavigateUrl = string.Format("EntryEdit.aspx?Idx={0}&pageNo={1}&pageSize={2}", DataBinder.Eval(item.DataItem, "Idx"), pageNo, pageSize);
            
            // 작업자
            Label lblUser = item.FindControl("lblUser") as Label;
            lblUser.Text = DataBinder.Eval(item.DataItem, "UserName").ToString();
            
            // 검수자
            Label lblChecker = item.FindControl("lblChecker") as Label;
            lblChecker.Text = DataBinder.Eval(item.DataItem, "CheckerName").ToString();

            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
        }
    }

    protected void DownButton_Click(object sender, EventArgs e)
    {
        DataTable dt = executeQueryText();
        String excelFileName = "EntryList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";
        this.exportExcel(dt, excelFileName);
    }
    private DataTable executeQueryText()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.CommandText = "USP_Entry_LIST_SELECT_ALL";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar, 30);
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = Convert.ToInt32(ddlRevision.SelectedValue);
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0; // Convert.ToInt32(ddlSubjectGroup.SelectedValue);
        Cmd.Parameters["@SubjectIdx"].Value = Convert.ToInt32(ddlSubject.SelectedValue);
        Cmd.Parameters["@SchoolIdx"].Value = 0;  // Convert.ToInt32(ddlSchool.SelectedValue);
        Cmd.Parameters["@BrandIdx"].Value = Convert.ToInt32(ddlBrand.SelectedValue);
        Cmd.Parameters["@GradeIdx"].Value = Convert.ToInt32(ddlGrade.SelectedValue);
        Cmd.Parameters["@SemesterIdx"].Value = Convert.ToInt32(ddlSemester.SelectedValue);
        if (ddlLUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@LUnitIdx"].Value = Convert.ToInt32(ddlLUnit.SelectedValue);
        else
            Cmd.Parameters["@LUnitIdx"].Value = 0;
        if (ddlMUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@MUnitIdx"].Value = Convert.ToInt32(ddlMUnit.SelectedValue);
        if (ddlUser.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@UserIdx"].Value = Convert.ToInt32(ddlUser.SelectedValue);
        Cmd.Parameters["@KeyWord"].Value = txtKeyWord.Text;
        if (ddlTask.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@TaskIdx"].Value = Convert.ToInt32(ddlTask.SelectedValue);
        else
            Cmd.Parameters["@TaskIdx"].Value = 0;

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }
    private void exportExcel(DataTable dataTable, String excelFileName)
    {
        string attachment = "attachment; filename=" + excelFileName;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //pagepart.Controls.Clear();
       
        //btnPage1.Visible = false;
        //btnPage2.Visible = false;
        //btnPage3.Visible = false;
        //btnPage4.Visible = false;
        //btnPage5.Visible = false;
        //btnPage6.Visible = false;
        //btnPage7.Visible = false;
        //btnPage8.Visible = false;
        //btnPage9.Visible = false;
        //btnPage10.Visible = false;
        
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize + "&paramType=1&paramRevision=" + ddlRevision.SelectedValue
            + "&paramSubject=" + ddlSubject.SelectedValue + "&paramBrand=" + ddlBrand.SelectedValue + "&paramGrade=" + ddlGrade.SelectedValue
            + "&paramSemester=" + ddlSemester.SelectedValue + "&paramLUnit=" + ddlLUnit.SelectedValue + "&paramMunit=" + ddlMUnit.SelectedValue
            + "&paramUser=" + ddlUser.SelectedValue + "&paramTask=" + ddlTask.SelectedValue + "&paramKeyword=" + txtKeyWord.Text;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            //Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
            Button Cmd_Bt_Prev = new Button();                   //버튼 객체 생성
            Cmd_Bt_Prev.ID = (FromPage - 1).ToString();                //버튼 이름 설정
            Cmd_Bt_Prev.Text = "이전페이지";
            Cmd_Bt_Prev.OnClientClick = "javascript:ChangeTxtPageNo(" + (FromPage - 1).ToString() + ");";
            Cmd_Bt_Prev.CssClass = "list-group-item";
            pagepart.Controls.Add(Cmd_Bt_Prev);
        }
        Button[] Cmd_Bt;
        Cmd_Bt = new Button[ToPage+1];                   //버튼 객체 생성

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            
            Cmd_Bt[i] = new Button();                   //버튼 객체 생성
            Cmd_Bt[i].ID = i.ToString();                //버튼 이름 설정
            Cmd_Bt[i].Text = i.ToString();
            Cmd_Bt[i].OnClientClick = "javascript:ChangeTxtPageNo(" + i.ToString() + ");";

            if (i == CurPage)
            {
                Cmd_Bt[i].CssClass = "list-group-item active";
            //    Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Cmd_Bt[i].CssClass = "list-group-item";
            //    Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
            pagepart.Controls.Add(Cmd_Bt[i]);
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            //Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
            Button Cmd_Bt_Next = new Button();                   //버튼 객체 생성
            Cmd_Bt_Next.ID = (ToPage + 1).ToString();                //버튼 이름 설정
            Cmd_Bt_Next.Text = "다음페이지";
            Cmd_Bt_Next.OnClientClick = "javascript:ChangeTxtPageNo(" + (ToPage + 1).ToString() + ");";
            Cmd_Bt_Next.CssClass = "list-group-item";
            pagepart.Controls.Add(Cmd_Bt_Next);
        }

        //페이지 네비게이션 출력하기
        //lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP=="::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void MUnitSearch_Click(object sender, EventArgs e)
    {
        MUnitListing();
    }
    protected void LUnitSearch_Click(object sender, EventArgs e)
    {
        LUnitListing();
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        if(LV_Notice.Items.Count > 0)
        {
            DownButton.Visible = true;
            pagepart.Visible = true;
        }
        else
        {
            DownButton.Visible = false;
            pagepart.Visible = false;
        }
    }

    protected void TaskSearch_Click(object sender, EventArgs e)
    {
        ddlTask.Items.Clear();
        ListItem li = new ListItem("선택", "0");
        ddlTask.Items.Add(li);
        ddlTask.AppendDataBoundItems = true; 
        
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE UserIdx='" + ddlUser.SelectedValue
                + "' AND DelFlag=0 ORDER BY TaskTitle ASC";

        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

        //SqlDataReader Reader = Cmd.ExecuteReader();
        DataSet ds = new DataSet();
        sda.Fill(ds, "TTaskID");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtTask = ds.Tables["TTaskID"];
        ddlTask.DataSource = dtTask;
        ddlTask.DataTextField = "TaskTitle";
        ddlTask.DataValueField = "Idx";
        ddlTask.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
        ddlTask.AppendDataBoundItems = false; 
    }
    protected void Page_Click(object sender, EventArgs e)
    {
        Control ctl = sender as Control;
        var argument = ((Button)sender).CommandArgument;
        //CommandEventArgs cea = e as CommandEventArgs;
        if (ctl != null)
        {
            pageNo = argument.ToString();  //ctl.ID;
            Listing();
            //Response.Write(ctl.ID);   // 1번을 누르던 3번버튼을 누르던 무조건 이것만 나옴 --+
        }
    }
}
