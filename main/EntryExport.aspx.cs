﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Text;
using System.Xml.Serialization;

public partial class XmlImport2 : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string idx = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }

        if(!IsPostBack)
        {
            // 1. EntryIDX 받기
            if (Request.Params["idx"] != null)
            {
                idx = Request.Params["idx"].ToString();
                if (idx.Length == 0)
                    Response.Redirect("<script>window.opener=self;self.close();</script>");
                else
                    txtTaskIdx.Value = idx;
            }
            else
            {
                Response.Write("<script>window.opener=self;self.close();</script>");
            }
            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            
            Conn.Open();
            string strQuery = string.Empty;

            strQuery = "SELECT Idx, TaskTitle, EntryCount FROM TTaskID WHERE Idx=" + txtTaskIdx.Value + "";
            SqlCommand Cmd = new SqlCommand(strQuery, Conn);
            Cmd.CommandType = CommandType.Text;
            SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                lblTaskTitle.Text = Server.HtmlDecode(reader["TaskTitle"].ToString());
                lblEntryCount.Text = Server.HtmlDecode(reader["EntryCount"].ToString());
            }

            reader.Close();
            Conn.Close();
            Conn = null;

        }
        else
        {

        }
    }
    
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        //try
        //{
        //    DBFileInfo();
        //    string strQuery = "INSERT INTO TReport (TaskIdx, UserIdx, KeyDate, ReportCount, Memo) Values "
        //        + " (" + ddlTask.SelectedValue + ", " + Session["uidx"].ToString() + ",'" + txtDate.Text + "', " + txtCount.Text + ", '" + Server.HtmlEncode(txtContents.Text) + "' )";
        //    SqlConnection Con = new SqlConnection(connectionString);

        //    SqlCommand Cmd = new SqlCommand(strQuery, Con);
        //    Cmd.CommandType = CommandType.Text;
        //    Con.Open();
        //    Cmd.ExecuteNonQuery();
        //    Con.Close();
        //    Response.Write("<script>alert('저장되었습니다.');window.opener=self;self.close();</script>");
        //    return;
        //}
        //catch (Exception ex)
        //{
        //    Response.Write("<script>alert('저장 중에 오류가 발생했습니다.\n"+ ex.ToString() +"');</script>");
        //    return;
        //}

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();
        string strXML = string.Empty;
        sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.AppendLine("<edu>");

        sb.Append(sbXMLByTask(txtTaskIdx.Value));

        sb.AppendLine("</edu>");
        // Write the stream contents to a new file named 
        DateTime dtTime = DateTime.Now;
        string FileNewName = "XML_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
        string pathData = HttpContext.Current.Server.MapPath("~/DATA/");
        using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
        {
            //outfile.Write(xd.OuterXml);
            outfile.Write(sb.ToString());
        }
        // XML 파일 파싱

        //XmlSerializer serializer = new XmlSerializer(typeof(edu));
        //serializer.UnknownNode += new XmlNodeEventHandler(Serializer_UnknownNode);
        //serializer.UnknownAttribute += new XmlAttributeEventHandler(Serializer_UnknownAttribute);

        // A FileStream is needed to read the XML document.
        FileStream fs = new FileStream(pathData + @FileNewName, FileMode.Open);
        // Declare an object variable of the type to be deserialized.

        fs.Close();
        byte[] bts = System.IO.File.ReadAllBytes(pathData + FileNewName);
        Response.Clear();
        Response.AddHeader("Content-Type", "Application/otect-stream");
        Response.AddHeader("Content-Length", bts.Length.ToString());
        Response.AddHeader("Content-Disposition", "attachment; filename=" + FileNewName);
        Response.BinaryWrite(bts); Response.Flush(); Response.End();

    }

    private void Serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Serializer_UnknownNode(object sender, XmlNodeEventArgs e)
    {
        throw new NotImplementedException();
    }
    protected StringBuilder sbXMLByTask(string tmpTaskID)
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        string strImageLink = string.Empty;
        string strImgFileName = string.Empty;
        string strXML = string.Empty;

        StringBuilder sbTask = new StringBuilder();

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        strQuery = "SELECT * FROM TEntry WHERE TaskIdx=" + txtTaskIdx.Value + " AND DelFlag=0 ORDER BY SortNo ASC, Idx ASC";
        Cmd.CommandType = CommandType.Text;
        Cmd.CommandText = strQuery;
        Con.Open();

        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        //totalEntCount += dt.Rows.Count;
        foreach (DataRow row in dt.Rows)
        {
            //strXML = "";
            //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
            //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
            //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
            //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
            //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

            strEntryIdx = row["Idx"].ToString();

            //sbTask.AppendLine();
            sbTask.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
            sbTask.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
            string strTitleK = string.Empty;
            if (row["EntryTitleK"].ToString().Length > 0)
                strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
            else
                strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
            string strTitleE = string.Empty;
            if (row["EntryTitleE"].ToString().Length > 0)
                strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
            else
                strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
            string strTitleC = string.Empty;
            if (row["EntryTitleC"].ToString().Length > 0)
                strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
            else
                strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
            string strSummary = string.Empty;
            if (row["Summary"].ToString().Length > 0)
                strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
            else
                strSummary = "<SUMMARY></SUMMARY>";
            //##### 2015.09-15 추가
            string strTitleSub1 = string.Empty;
            if (row["EntryTitleSub1"].ToString().Length > 0)
                strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
            else
                strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
            string strTitleSub2 = string.Empty;
            if (row["EntryTitleSub2"].ToString().Length > 0)
                strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
            else
                strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
            string strSynonym = string.Empty;
            if (row["Synonym"].ToString().Length > 0)
                strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
            else
                strSynonym = "<SYNONYM></SYNONYM>";
            sbTask.AppendLine(strTitleK);
            sbTask.AppendLine(strTitleE);
            sbTask.AppendLine(strTitleC);
            sbTask.AppendLine(strSummary);
            sbTask.AppendLine(strTitleSub1);
            sbTask.AppendLine(strTitleSub2);
            sbTask.AppendLine(strSynonym);
            //reader.Close();
            Con.Close();

            SqlConnection Con2 = new SqlConnection(connectionString);
            SqlCommand Cmd2 = new SqlCommand();
            Cmd2.Connection = Con2;

            //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
            //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
            strQuery = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                + " FROM TEntry as Y "
                + " join TCode as A on A.Idx=Y.RevisionIdx "
                + "	join TCode as C on C.Idx=Y.SubjectIdx "
                + "	join TCode as E on E.Idx=Y.BrandIdx  "
                + "	join TCode as F on F.Idx=Y.GradeIdx  "
                + "	join TCode as G on G.Idx=Y.SemesterIdx "
                + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                + " WHERE Y.Idx=" + strEntryIdx + "";
            //Response.Write (strQuery);
            Cmd2.CommandText = strQuery;
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
            if (reader2.Read())
            {
                string[] strCategory = reader2["category"].ToString().Split('>');
                int i = 1;
                foreach (string strUnit in strCategory)
                {
                    sbTask.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                    i++;
                }
            }
            reader2.Close();
            Con2.Close();

            Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
            string strINFORMATION = string.Empty;
            if (reader2.HasRows)
            {
                sbTask.AppendLine("<INFOMATION>");
                while (reader2.Read())
                {
                    strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
                    sbTask.AppendLine(strINFORMATION);
                }
                sbTask.AppendLine("</INFOMATION>");
            }
            reader2.Close();
            Con2.Close();


            Con2 = new SqlConnection(connectionString);
            Cmd2 = new SqlCommand();
            Cmd2.Connection = Con2;
            Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            DataTable dt2 = new DataTable();
            SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
            adapter2.Fill(dt2);
            foreach (DataRow row2 in dt2.Rows)
            {
                string content = row2["Content"].ToString();
                content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
                content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
                content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
                content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
                content = content.Replace("><", ">" + Environment.NewLine + "<");
                sbTask.AppendLine("<" + row2["RecType"].ToString() + ">");
                sbTask.AppendLine(content);
                sbTask.AppendLine("</" + row2["RecType"].ToString() + ">");

            }
            Con2.Close();
            sbTask.AppendLine("</ENTRY>");
        }
        return sbTask;
    }
    protected StringBuilder sbXMLByEntry(string EntryIdx)
    {

        StringBuilder sbEntry = new StringBuilder();
        string strEntryIdx = string.Empty;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        string strQuery = "SELECT * FROM TEntry WHERE Idx=" + EntryIdx + " ORDER BY Idx ASC";
        Cmd.CommandType = CommandType.Text;
        Cmd.CommandText = strQuery;
        Con.Open();

        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        //totalEntCount += dt.Rows.Count;
        foreach (DataRow row in dt.Rows)
        {
            strEntryIdx = row["Idx"].ToString();

            sbEntry.AppendLine();
            sbEntry.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
            sbEntry.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
            string strTitleK = string.Empty;
            if (row["EntryTitleK"].ToString().Length > 0)
                strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
            else
                strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
            string strTitleE = string.Empty;
            if (row["EntryTitleE"].ToString().Length > 0)
                strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
            else
                strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
            string strTitleC = string.Empty;
            if (row["EntryTitleC"].ToString().Length > 0)
                strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
            else
                strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
            string strSummary = string.Empty;
            if (row["Summary"].ToString().Length > 0)
                strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
            else
                strSummary = "<SUMMARY></SUMMARY>";
            //##### 2015.09-15 추가
            string strTitleSub1 = string.Empty;
            if (row["EntryTitleSub1"].ToString().Length > 0)
                strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
            else
                strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
            string strTitleSub2 = string.Empty;
            if (row["EntryTitleSub2"].ToString().Length > 0)
                strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
            else
                strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
            string strSynonym = string.Empty;
            if (row["Synonym"].ToString().Length > 0)
                strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
            else
                strSynonym = "<SYNONYM></SYNONYM>";
            sbEntry.AppendLine(strTitleK);
            sbEntry.AppendLine(strTitleE);
            sbEntry.AppendLine(strTitleC);
            sbEntry.AppendLine(strSummary);
            sbEntry.AppendLine(strTitleSub1);
            sbEntry.AppendLine(strTitleSub2);
            sbEntry.AppendLine(strSynonym);
            //reader.Close();
            Con.Close();

            SqlConnection Con2 = new SqlConnection(connectionString);
            SqlCommand Cmd2 = new SqlCommand();
            Cmd2.Connection = Con2;

            Cmd2.CommandText = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                + " FROM TEntry as Y "
                + " join TCode as A on A.Idx=Y.RevisionIdx "
                + "	join TCode as C on C.Idx=Y.SubjectIdx "
                + "	join TCode as E on E.Idx=Y.BrandIdx  "
                + "	join TCode as F on F.Idx=Y.GradeIdx  "
                + "	join TCode as G on G.Idx=Y.SemesterIdx "
                + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                + " WHERE Y.Idx=" + strEntryIdx + "";
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
            if (reader2.Read())
            {
                string[] strCategory = reader2["category"].ToString().Split('>');
                int i = 1;
                foreach (string strUnit in strCategory)
                {
                    sbEntry.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                    i++;
                }

            }
            reader2.Close();
            Con2.Close();

            Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
            string strINFORMATION = string.Empty;
            if (reader2.HasRows)
            {
                sbEntry.AppendLine("<INFOMATION>");
                while (reader2.Read())
                {
                    strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
                    sbEntry.AppendLine(strINFORMATION);
                }
                sbEntry.AppendLine("</INFOMATION>");
            }
            reader2.Close();
            Con2.Close();

            Con2 = new SqlConnection(connectionString);
            Cmd2 = new SqlCommand();
            Cmd2.Connection = Con2;
            Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
            Cmd2.CommandType = CommandType.Text;
            Con2.Open();
            DataTable dt2 = new DataTable();
            SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
            adapter2.Fill(dt2);
            foreach (DataRow row2 in dt2.Rows)
            {
                string content = row2["Content"].ToString();
                content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
                content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
                content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
                content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
                content = content.Replace("><", ">" + Environment.NewLine + "<");
                sbEntry.AppendLine("<" + row2["RecType"].ToString() + ">");
                sbEntry.AppendLine(content);
                sbEntry.AppendLine("</" + row2["RecType"].ToString() + ">");

            }
            Con2.Close();
            sbEntry.AppendLine("</ENTRY>");
        }

        return sbEntry;
    }
}