﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.IO.Compression;

public partial class main_TemplateAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //

    private const int INITIAL_RECTYPE_ID = 1;
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private DataTable dtRecType = new DataTable();
    private DataTable dtTemplate = new DataTable();
    private string strQuery = string.Empty;
    public string strTaskIdx = string.Empty;
    public string exportIdx = string.Empty;
    // Set a variable to the /Data File path. 
    private string pathData = HttpContext.Current.Server.MapPath("~/DATA/"); 
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }   
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝   
        if (!IsPostBack)
        {
            // 1. exportIDX 받기
            if (Request.Params["exportIdx"] != null)
            {
                exportIdx = Request.Params["exportIdx"].ToString();
                if (exportIdx.Length == 0)
                    Response.Redirect("ExportList.aspx");
                txtExportIdx.Value = exportIdx;
            }
            else
            {
                Response.Redirect("ExportList.aspx");
            }
            

            // 1.1 IDX 없으면 ExportIdx생성
            if (exportIdx.Length == 0)
            {
                // ExportIdx 생성
                string strTimestamp = System.DateTime.Now.ToString("yyyyMMddhhmmss");
                DBFileInfo();
                strQuery = "INSERT INTO TExport (ExportTitle) Values ('" + strTimestamp + "')";
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                strQuery = "SELECT TOP 1 Idx FROM TExport WHERE ExportTItle ='" + strTimestamp + "' ORDER BY Idx DESC";
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Cmd.Connection = Con;
                Con.Open();
                SqlDataReader reader;
                reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    exportIdx = reader["Idx"].ToString();
                    txtExportIdx.Value = exportIdx;
                }
                Con.Close();

                Cmd = null;
                Con = null;
            }
            else  // 1.2 exportIdx를 param으로 받았으면 DB에서 값 가져오기
            {
                // 정보를 가져온다.
                GetExportInfo();

                DBFileInfo();
                exportIdx = txtExportIdx.Value;
                strQuery = "Declare @sumCnt int; SELECT @sumCnt=SUM(EntryCount) FROM TExportData WHERE ExportIdx =" + exportIdx + " ;SELECT *, @sumCnt as totalCount FROM TExportData WHERE ExportIdx =" + exportIdx + " ORDER BY Idx DESC";
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Cmd.Connection = Con;
                Con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                sda.Fill(ds, "data_list");

                EntryList.DataSource = ds;
                EntryList.DataBind();
                if (ds.Tables["data_list"].Rows.Count> 0)
                {
                    txtSumEntry.Text = ds.Tables["data_list"].Rows[0]["totalCount"].ToString();
                }                
                Con.Close();

                Cmd = null;
                Con = null;
            }
            BindType();
            BindTemplate();
        }
        else
        {
            dtTemplate.Columns.Add("NO");
            dtTemplate.Columns.Add("RECTYPEID");
            dtTemplate.Columns.Add("TEMPLATEMARKUP");

            GetRecType();
        }

    }
    private void GetExportInfo()
    {
        DBFileInfo();
        exportIdx = txtExportIdx.Value; 
        
        strQuery = "SELECT ExportTitle, TemplateIdx, IsNull(Target, 0) as Target, FirstDate, ImgFile, XMLFile, ImageCount FROM TExport WHERE Idx =" + exportIdx + "";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            txtTitle.Text = reader["ExportTitle"].ToString();
            ddlTemplate.SelectedValue = reader["TemplateIdx"].ToString();
            ddlType.SelectedValue = reader["Target"].ToString();
            lblFirstDate.Text = reader["FirstDate"].ToString();
            lblImageFile.Text = "<a target='_blank' href=\"/cms100data/EntryData/" + reader["ImgFile"].ToString() + "\">" + reader["ImgFile"].ToString() + "</a>";
            lblXMLFile.Text = "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + reader["XMLFile"].ToString() +"&Type=X\">" + reader["XMLFile"].ToString() +"</a>";
            txtSumImage.Text = reader["ImageCount"].ToString();
        }
        reader.Close();
        Con.Close();
    }
    private void GetRecType() {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);
        //SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "rectype_data");

        //Set up the data binding. 
        Con.Close();
        dtRecType = ds.Tables["rectype_data"];
    }
    
    private void BindType()
    {
        ListItem li = new ListItem("선택", "0");
        ddlType.Items.Add(li);
        ddlType.AppendDataBoundItems = true;
        try
        {
            DBFileInfo();

            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title FROM TTemplateType ORDER BY Title ASC", Con);

            DataSet ds = new DataSet();
            sda.Fill(ds, "list_data");
            //Set up the data binding. 
            Con.Close();
            DataTable dtType = ds.Tables["list_data"];
            ddlType.DataSource = dtType;
            ddlType.DataTextField = "Title";
            ddlType.DataValueField = "Idx";
            ddlType.DataBind();

            if (Con.State == ConnectionState.Open)
                Con.Close();
            Con = null;
            dtType = null;

        }
        catch (Exception ex)
        {
            Response.Write(ex.ToString());
        }
        ddlType.AppendDataBoundItems = false;
    }
    private void BindTemplate()
    {
        ListItem li = new ListItem("선택", "0");
        ddlTemplate.Items.Add(li);
        ddlTemplate.AppendDataBoundItems = true;
        
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title FROM TTemplate ORDER BY Title ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "list_data");
        //Set up the data binding. 
        Con.Close();
        DataTable dtType = ds.Tables["list_data"];
        ddlTemplate.DataSource = dtType;
        ddlTemplate.DataTextField = "Title";
        ddlTemplate.DataValueField = "Idx";
        ddlTemplate.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtType = null;
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }    
}
