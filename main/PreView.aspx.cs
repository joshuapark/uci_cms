﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class main_PreView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string content = string.Empty;
        string html = string.Empty;
        if (Request.Params["previewContent"] != null)
        {
            if (Request.Params["previewContent"].ToString().Length > 0)
            {
                content = Request.Params["previewContent"].ToString();

                Edu edu = JsonConvert.DeserializeObject<Edu>(content);
                html += edu.Title + "</br>";
                
                //대분류
                List<LUnit> lunitList = edu.LUnit;
                foreach (LUnit lunit in lunitList)
                {
                    List<Child> lunitChildList = lunit.list;
                    foreach (Child secondItem in lunitChildList)
                    {

                        if (secondItem.content != null)
                        {
                            string secondContent = string.Empty;
                            if (Convert.ToString(secondItem.rectype).Equals("IMAGE") || 
                                Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE") || 
                                Convert.ToString(secondItem.rectype).Equals("SOUND")  ||
                                Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE") 
                                )
                            {
                                List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                                string extraInfo = string.Empty;
                                string rectypeContent = string.Empty;

                                foreach (Child child in childList)
                                {
                                    if (!child.content.Equals("<p><br></p>"))
                                    {
                                        if (child.rectype.Equals(string.Empty))
                                        {
                                            rectypeContent = child.content;
                                        }
                                        else {
                                            extraInfo += child.content;
                                        }

                                    }
                                }
                                if (
                                    Convert.ToString(secondItem.rectype).Equals("IMAGE") ||
                                    Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE")
                                   )
                                {
                                    html += "<div id=\"aaa\" style=\"width:100%;text-align:center\">";
                                    html += "<div id=\"bbb\" style=\"margin:0 auto;\">";
                                    html += rectypeContent + extraInfo;
                                    html += "</div></div></br>";
                                }
                                else {
                                    html += rectypeContent + extraInfo;
                                }

                            }
                            else
                            {
                                html += secondItem.content;
                            }
                        }
                    }
                }

                //퀴즈
                List<Quiz> quizList = edu.Quiz;
                if (quizList != null)
                {
                    foreach (Quiz quiz in quizList)
                    {
                        List<Child> quizChildList = quiz.list;
                        foreach (Child secondItem in quizChildList)
                        {

                            if (secondItem.content != null)
                            {
                                string secondContent = string.Empty;
                                if (Convert.ToString(secondItem.rectype).Equals("IMAGE") || 
                                    Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE")  || 
                                    Convert.ToString(secondItem.rectype).Equals("SOUND") ||
                                    Convert.ToString(secondItem.rectype).Equals("PAIRSENTENCE")
                                    )
                                {
                                    List<Child> childList = JsonConvert.DeserializeObject<List<Child>>(secondItem.content);
                                    string extraInfo = string.Empty;
                                    string rectypeContent = string.Empty;

                                    foreach (Child child in childList)
                                    {
                                        if (!child.content.Equals("<p><br></p>"))
                                        {
                                            if (child.rectype.Equals(string.Empty))
                                            {
                                                rectypeContent = child.content;
                                            }
                                            else
                                            {
                                                extraInfo += child.content;
                                            }

                                        }
                                    }

                                    if (Convert.ToString(secondItem.rectype).Equals("IMAGE") ||
                                        Convert.ToString(secondItem.rectype).Equals("MATRIX_TABLE"))
                                    {
                                        html += "<div id=\"aaa\" style=\"width:100%;text-align:center\">";
                                        html += "<div id=\"bbb\" style=\"margin:0 auto;\">";
                                        html += rectypeContent + extraInfo;
                                        html += "</div></div></br>";
                                    }
                                    else 
                                    {
                                        html += rectypeContent + extraInfo;
                                    }
                                }
                                else
                                {
                                    html += secondItem.content;
                                }
                            }
                        }
                    }
                }

                //태그
                Tag tag = edu.Tag;
                if (tag != null)
                {
                    html += tag.content;
                }

                divContent.InnerHtml = html;
            }
        }
    }

    public class Edu
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("lunit")]
        public List<LUnit> LUnit { get; set; }
        [JsonProperty("quiz")]
        public List<Quiz> Quiz { get; set; }
        [JsonProperty("tag")]
        public Tag Tag { get; set; }
    }


    public class Tag
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

    public class LUnit
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }

    public class Quiz
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<Child> list { get; set; }
    }



    public class Child
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

}