﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    public string lblPage = string.Empty;
    //private string strCategory = string.Empty;
    public int totalCnt = 0;
    public string usrAuth = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
            usrAuth = uAuth.ToString();
        }
        if (uAuth < 9)
        {
            sectionBtn.Visible = false;
            AddButton.Visible = false;
            SaveButton.Visible = false;
        }
        //##### 권한 처리 끝


        if (!IsPostBack)
        {
            BindCategory();
            sectionBtn.Visible = false;
            //Listing();
        }
    }
    protected void BindCategory()
    {
        DBFileInfo();

        ListItem li = new ListItem("선택", "0");
        ddlRevision.Items.Add(li);
        //ddlSubjectGroup.Items.Add(li);
        ddlSubject.Items.Add(li);
        //ddlSchool.Items.Add(li);
        ddlBrand.Items.Add(li);
        ddlGrade.Items.Add(li);
        ddlSemester.Items.Add(li);

        ddlRevision.AppendDataBoundItems = true;
        //ddlSubjectGroup.AppendDataBoundItems = true;
        ddlSubject.AppendDataBoundItems = true;
        //ddlSchool.AppendDataBoundItems = true;
        ddlBrand.AppendDataBoundItems = true;
        ddlGrade.AppendDataBoundItems = true;
        ddlSemester.AppendDataBoundItems = true;

        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC", Conn);

        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");

        //Set up the data binding. 
        Conn.Close();

        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        ddlRevision.DataSource = dtRevision;
        ddlRevision.DataTextField = "CodeName";
        ddlRevision.DataValueField = "Idx";
        ddlRevision.DataBind();

        //DataTable dtSubjectGroup = ds.Tables["TCode"];
        //dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        //ddlSubjectGroup.DataSource = dtSubjectGroup;
        //ddlSubjectGroup.DataTextField = "CodeName";
        //ddlSubjectGroup.DataValueField = "Idx";
        //ddlSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        dtSubject.DefaultView.Sort = "CodeName ASC";
        ddlSubject.DataSource = dtSubject;
        ddlSubject.DataTextField = "CodeName";
        ddlSubject.DataValueField = "Idx";
        ddlSubject.DataBind();

        //DataTable dtSchool = ds.Tables["TCode"];
        //dtSchool.DefaultView.RowFilter = "CodeType = 4";
        //ddlSchool.DataSource = dtSchool;
        //ddlSchool.DataTextField = "CodeName";
        //ddlSchool.DataValueField = "Idx";
        //ddlSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        dtBrand.DefaultView.Sort = "CodeName ASC";
        ddlBrand.DataSource = dtBrand;
        ddlBrand.DataTextField = "CodeName";
        ddlBrand.DataValueField = "Idx";
        ddlBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        dtGrade.DefaultView.Sort = "IDX ASC";
        ddlGrade.DataSource = dtGrade;
        ddlGrade.DataTextField = "CodeName";
        ddlGrade.DataValueField = "Idx";
        ddlGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        ddlSemester.DataSource = dtSemester;
        ddlSemester.DataTextField = "CodeName";
        ddlSemester.DataValueField = "Idx";
        ddlSemester.DataBind();

        //Close the connection.
        //Conn.Close();
        dtRevision = null;
        //dtSubjectGroup = null;
        dtSubject = null;
        //dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;
    }
    protected void Listing()
    {        
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_LUnit_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0; //ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
        Cmd.Parameters["@SchoolIdx"].Value = 0; // ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;
        
        //Cmd.Parameters.Add("@category", SqlDbType.NChar);
        //strCategory = ddlRevision.SelectedValue + "-" + ddlSubjectGroup.SelectedValue + "-" + ddlSubject.SelectedValue + ddlSchool.SelectedValue + ddlBrand.SelectedValue + "-" + ddlGrade.SelectedValue + ddlSemester.SelectedValue;
        //Cmd.Parameters["@category"].Value = strCategory;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataTable dt = new DataTable();

        sda.Fill(dt);
        LUnitList.DataSource = dt;
        LUnitList.DataBind();

        totalCnt = dt.Rows.Count;

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlCommand Cmd = null;

        foreach (RepeaterItem item in LUnitList.Items )
        {
            Label lblID = item.FindControl("LUnitID") as Label;
            TextBox txtName = item.FindControl("LUnitName") as TextBox;

            Cmd = new SqlCommand("UPDATE TLUnit SET LUnitName='" + txtName.Text + "' WHERE Idx=" + lblID.Text + " ", Con);
            Cmd.CommandType = CommandType.Text;

            Cmd.ExecuteNonQuery();
        }
        
        Con.Close();

        Cmd = null;
        Con = null;

    }    

    protected void AddButton_Click(object sender, EventArgs e)
    {
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝

        if (txtLUnitName.Text.Trim().Length > 0)  //대분류명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            string strQuery = "SELECT LUnitName FROM TLUnit WHERE LUnitName='" + txtLUnitName.Text.Trim() + "' "
                + " AND RevisionIdx = " + Convert.ToInt16(ddlRevision.SelectedValue) 
                + " AND SubjectIdx=" + Convert.ToInt16(ddlSubject.SelectedValue) 
                + " AND BrandIdx =" + Convert.ToInt16(ddlBrand.SelectedValue) + " AND GradeIdx=" + Convert.ToInt16(ddlGrade.SelectedValue)
                + " AND SemesterIdx = " + ddlSemester.SelectedValue + " AND DelFlag=0 ";
            SqlCommand Cmd0 = new SqlCommand(strQuery, Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                msgIDCheck.InnerText = "* " + txtLUnitName.Text.Trim() + " 대단원명은 이미 등록되어 있습니다. 다른 대단원명을 입력하세요.";
                string script = "<script>alert('" + txtLUnitName.Text.Trim() + " 대단원명은 이미 등록되어 있습니다.');showPopup();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Connection = Con;

                Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@UnitName", SqlDbType.NVarChar, 100);
                //Cmd.Parameters.Add("@Category", SqlDbType.NChar, 10);
                
                Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
                Cmd.Parameters["@SubjectGroupIdx"].Value = 0; // ddlSubjectGroup.SelectedValue;
                Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
                Cmd.Parameters["@SchoolIdx"].Value = 0; //ddlSchool.SelectedValue;
                Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
                Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
                Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;
                Cmd.Parameters["@UnitName"].Value = txtLUnitName.Text.Trim();
                //Cmd.Parameters["@Category"].Value = ddlRevision.SelectedValue + "-" + ddlSubjectGroup.SelectedValue + "-" + ddlSubject.SelectedValue + ddlSchool.SelectedValue + ddlBrand.SelectedValue + "-" + ddlGrade.SelectedValue + ddlSemester.SelectedValue;

                Cmd.CommandText = "USP_LUnit_INSERT";
                Cmd.CommandType = CommandType.StoredProcedure;
                Con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(Cmd);
                DataSet ds = new DataSet();

                sda.Fill(ds, "data_list");
                Con.Close();

                Cmd = null;
                Con = null;

                Listing();
                txtLUnitName.Text = "";
                msgIDCheck.InnerText = "";

            }
        }
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        Listing();
        sectionBtn.Visible = true;
    }

    protected void LUnit_ItemCommand(object source, RepeaterCommandEventArgs e) 
    {
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝

        string delID;
        RepeaterItem repeaterItem = LUnitList.Items[e.Item.ItemIndex];
        Label lblID = repeaterItem.FindControl("LUnitID") as Label;
        delID = lblID.Text;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand("UPDATE TLUnit SET DelFlag=1 WHERE Idx = " + delID, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        Listing();
    }
    
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
}
