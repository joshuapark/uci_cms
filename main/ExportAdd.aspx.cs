﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.IO.Compression;

public partial class main_TemplateAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //

    private const int INITIAL_RECTYPE_ID = 1;
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private DataTable dtRecType = new DataTable();
    private DataTable dtTemplate = new DataTable();
    private string strQuery = string.Empty;
    public string strTaskIdx = string.Empty;
    public string exportIdx = string.Empty;
    // Set a variable to the /Data File path. 
    private string pathData = HttpContext.Current.Server.MapPath("~/DATA/"); 
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }   
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝  
        if (!IsPostBack)
        {
            // 1. exportIDX 받기
            if (Request.Params["exportIdx"] != null)
            {
                exportIdx = Request.Params["exportIdx"].ToString();
                txtExportIdx.Value = exportIdx;
            }

            BindType();
            BindTemplate();
            // 1.1 IDX 없으면 ExportIdx생성
            if (exportIdx.Length == 0)
            {
                // ExportIdx 생성
                string strTimestamp = System.DateTime.Now.ToString("yyyyMMddhhmmss");
                DBFileInfo();
                strQuery = "INSERT INTO TExport (TempKey) Values ('" + strTimestamp + "')";
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                strQuery = "SELECT TOP 1 Idx FROM TExport WHERE TempKey ='" + strTimestamp + "' ORDER BY Idx DESC";
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Cmd.Connection = Con;
                Con.Open();
                SqlDataReader reader;
                reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader.Read())
                {
                    exportIdx = reader["Idx"].ToString();
                    txtExportIdx.Value = exportIdx;
                }
                Con.Close();

                Cmd = null;
                Con = null;
            }
            else  // 1.2 exportIdx를 param으로 받았으면 DB에서 값 가져오기
            {
                // 정보를 가져온다.
                GetExportInfo();
                // 목록을 가져온다.
                Listing();
            }

        }
        else
        {
            dtTemplate.Columns.Add("NO");
            dtTemplate.Columns.Add("RECTYPEID");
            dtTemplate.Columns.Add("TEMPLATEMARKUP");

            GetRecType();
        }

    }
    protected void Listing() 
    {
        DBFileInfo();
        exportIdx = txtExportIdx.Value;
        strQuery = "Declare @sumCnt int; SELECT @sumCnt=SUM(EntryCount) FROM TExportData WHERE ExportIdx =" + exportIdx + " ;SELECT *, @sumCnt as totalCount FROM TExportData WHERE ExportIdx =" + exportIdx + " ORDER BY Idx DESC";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();
        if (ds.Tables["data_list"].Rows.Count > 0)
        {
            txtSumEntry.Text = ds.Tables["data_list"].Rows[0]["totalCount"].ToString();
        }
        Con.Close();

        Cmd = null;
        Con = null;
    }
    private void GetExportInfo()
    {
        DBFileInfo();
        exportIdx = txtExportIdx.Value; 
        
        strQuery = "SELECT * FROM TExport WHERE Idx =" + exportIdx + "";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            txtTitle.Text = reader["ExportTitle"].ToString();
            ddlTemplate.SelectedValue = reader["TemplateIdx"].ToString();
            ddlType.SelectedValue = reader["Target"].ToString();
        }
        reader.Close();
        Con.Close();
    }
    private void GetRecType() {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);
        //SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "rectype_data");

        //Set up the data binding. 
        Con.Close();
        dtRecType = ds.Tables["rectype_data"];
    }


    //protected void PopulateDataTable()
    //{
    //    int i = 1;
    //    foreach (RepeaterItem item in rpDataList.Items )
    //    {
    //        Label lblNo = item.FindControl("lblNo") as Label;
    //        DropDownList ddlRecType = item.FindControl("ddlRecType") as DropDownList;
    //        TextBox txtTemplateMarkup = item.FindControl("txtTemplateMarkup") as TextBox;

    //        DataRow row = dtTemplate.NewRow();
    //        row["NO"] = i.ToString();
    //        row["RECTYPEID"] = ddlRecType.SelectedValue;
    //        row["TEMPLATEMARKUP"] = txtTemplateMarkup.Text;
    //        i++;
    //        dtTemplate.Rows.Add(row);
    //    }
    //}

    private void BindType()
    {
        ListItem li = new ListItem("선택", "0");
        ddlType.Items.Clear();
        ddlType.Items.Add(li);
        ddlType.AppendDataBoundItems = true;

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title FROM TTemplateType ORDER BY Title ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "list_data");
        //Set up the data binding. 
        Con.Close();
        DataTable dtType = ds.Tables["list_data"];
        ddlType.DataSource = dtType;
        ddlType.DataTextField = "Title";
        ddlType.DataValueField = "Idx";
        ddlType.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtType = null;
        ddlType.AppendDataBoundItems = false;
    }
    private void BindTemplate()
    {
        ListItem li = new ListItem("선택", "0");
        ddlTemplate.Items.Clear();
        ddlTemplate.Items.Add(li);
        ddlTemplate.AppendDataBoundItems = true;
        
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title FROM TTemplate ORDER BY Title ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "list_data");
        //Set up the data binding. 
        Con.Close();
        DataTable dtType = ds.Tables["list_data"];
        ddlTemplate.DataSource = dtType;
        ddlTemplate.DataTextField = "Title";
        ddlTemplate.DataValueField = "Idx";
        ddlTemplate.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtType = null;
        ddlTemplate.AppendDataBoundItems = false;
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TExportData WHERE ExportIdx =" + exportIdx + "";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        if (dt.Rows.Count == 0)
        {
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            string strXML = string.Empty;
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<edu>");
            
            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
            {
                switch (row["Type"].ToString())
                {
                    case "TASK":
                        sb.Append(sbXMLByTask(row["Content"].ToString()));
                        continue;
                    case "Category":
                        sb.Append(sbXMLByCategory(row["Content"].ToString()));
                        continue;
                    case "Binder":
                        sb.Append(sbXMLByBinder(row["Content"].ToString()));
                        continue;
                }
            }
            sb.AppendLine("</edu>");
            //--- Export Template 적용한다.
            if (ddlTemplate.SelectedValue != "0")
            {
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = "SELECT A.TransMarkup, B.Markup FROM TTemplateData A, TRecType B WHERE A.RecType=B.Idx AND TemplateIdx =" + ddlTemplate.SelectedValue + "";
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                dt = new DataTable();
                adapter = new SqlDataAdapter(Cmd);
                adapter.Fill(dt);
                Con.Close();
                foreach(DataRow row in dt.Rows)
                {
                    sb.Replace(row["Markup"] + "", row["TransMarkup"] + "");
                }
            }
            // XML Document에 담는다.
            //XmlDocument xd = new XmlDocument();
            //xd.LoadXml(sb.ToString());

            // Write the stream contents to a new file named 
            DateTime dtTime = DateTime.Now;
            string FileNewName = "XML_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
            using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
            {
                //outfile.Write(xd.OuterXml);
                outfile.Write(sb.ToString());
            }

            try
            {
                lblXMLFile.Text = "<a target='_blank' href=\"DownloadFile.aspx?dfn=" + @FileNewName + "&Type=X\">"+ @FileNewName + "</a>";

                //lblImageFile.Text = strImageLink;
                lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
                int sumCnt = 0;
                sumCnt = Convert.ToInt32(txtSumEntry.Text);//Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
                    + " TemplateIdx =" + ddlTemplate.SelectedValue + ", EntryCount ="+sumCnt +", "
                    + " XMLFile ='" + FileNewName + "' , Target="+ ddlType.SelectedValue +", FirstDate=getdate(), "
                    + " SaveFlag=1  WHERE Idx=" + exportIdx + "";

                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
            }
            finally
            {

            }
        }
        makeImageFile();
        
        btnExport.Visible = false;
        btnImageChk.Visible = false;
        sectionBtn.Visible = false;
        ddlTemplate.Enabled = false;
        ddlType.Enabled = false;
        txtTitle.Enabled = false;
        //EntryList도 Enabled = false 해야 함 안그러면 삭제됨.
        
    }
    protected StringBuilder sbXMLByCategory(string tmpEntryID)
    {

        StringBuilder sbCategory = new StringBuilder();
        string strEntryIdx = string.Empty;
        string[] arrEntryID = tmpEntryID.Split(',');

        foreach (string strID in arrEntryID.Reverse<string>())
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            strQuery = "SELECT * FROM TEntry WHERE Idx=" + strID + " ORDER BY Idx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;
            Con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            //totalEntCount += dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                strEntryIdx = row["Idx"].ToString();

                sbCategory.AppendLine();
                sbCategory.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                sbCategory.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                string strTitleK = string.Empty;
                if (row["EntryTitleK"].ToString().Length > 0)
                    strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                else
                    strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                string strTitleE = string.Empty;
                if (row["EntryTitleE"].ToString().Length > 0)
                    strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                else
                    strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                string strTitleC = string.Empty;
                if (row["EntryTitleC"].ToString().Length > 0)
                    strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                else
                    strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
                string strSummary = string.Empty;
                if (row["Summary"].ToString().Length > 0)
                    strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
                else
                    strSummary = "<SUMMARY></SUMMARY>";
                //##### 2015.09-15 추가
                string strTitleSub1 = string.Empty;
                if (row["EntryTitleSub1"].ToString().Length > 0)
                    strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
                else
                    strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
                string strTitleSub2 = string.Empty;
                if (row["EntryTitleSub2"].ToString().Length > 0)
                    strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
                else
                    strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
                string strSynonym = string.Empty;
                if (row["Synonym"].ToString().Length > 0)
                    strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
                else
                    strSynonym = "<SYNONYM></SYNONYM>";
                sbCategory.AppendLine(strTitleK);
                sbCategory.AppendLine(strTitleE);
                sbCategory.AppendLine(strTitleC);
                sbCategory.AppendLine(strSummary);
                sbCategory.AppendLine(strTitleSub1);
                sbCategory.AppendLine(strTitleSub2);
                sbCategory.AppendLine(strSynonym);
                //reader.Close();
                Con.Close();

                SqlConnection Con2 = new SqlConnection(connectionString);
                SqlCommand Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;

                Cmd2.CommandText = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                    + " FROM TEntry as Y "
                    + " join TCode as A on A.Idx=Y.RevisionIdx "
                    + "	join TCode as C on C.Idx=Y.SubjectIdx "
                    + "	join TCode as E on E.Idx=Y.BrandIdx  "
                    + "	join TCode as F on F.Idx=Y.GradeIdx  "
                    + "	join TCode as G on G.Idx=Y.SemesterIdx "
                    + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                    + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                    + " WHERE Y.Idx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader2.Read())
                {
                    string[] strCategory = reader2["category"].ToString().Split('>');
                    int i = 1;
                    foreach (string strUnit in strCategory)
                    {
                        sbCategory.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                        i++;
                    }

                }
                reader2.Close();
                Con2.Close();

                Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                string strINFORMATION = string.Empty;
                if (reader2.HasRows)
                {
                    sbCategory.AppendLine("<INFOMATION>");
                    while (reader2.Read())
                    {
                        strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
                        sbCategory.AppendLine(strINFORMATION);
                    }
                    sbCategory.AppendLine("</INFOMATION>");
                }
                reader2.Close();
                Con2.Close();

                Con2 = new SqlConnection(connectionString);
                Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;
                Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                DataTable dt2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = row2["Content"].ToString();
                    content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
                    content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
                    content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
                    content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
                    content = content.Replace("><", ">" + Environment.NewLine + "<"); 
                    sbCategory.AppendLine("<" + row2["RecType"].ToString() + ">");
                    sbCategory.AppendLine(content);
                    sbCategory.AppendLine("</" + row2["RecType"].ToString() + ">");

                }
                Con2.Close();
                sbCategory.AppendLine("</ENTRY>");
            }
        }
        return sbCategory;
    }
    protected StringBuilder sbXMLByBinder(string tmpBinderID)
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        string strImageLink = string.Empty;
        string strImgFileName = string.Empty;
        string strXML = string.Empty;
        //strTaskIdx = "60";
        exportIdx = txtExportIdx.Value;

        StringBuilder sbTask = new StringBuilder();
        string[] arrBinderID = tmpBinderID.Split(',');

        foreach (string strID in arrBinderID)   //---Task 목록에서 TaskID를 추출한다. 시작
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            strQuery = "SELECT A.EntryIdx, B.EntryTitle, B.EntryTitleK, B.EntryTitleE, B.EntryTitleC, B.Summary FROM TBinderEntry A, TEntry B WHERE A.BinderIdx=" + strID + " AND A.EntryIdx=B.Idx AND B.DelFlag=0 ORDER BY A.EntryIdx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;
            Con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            Con.Close();
            //totalEntCount += dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                //strXML = "";
                //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
                //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
                //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

                strEntryIdx = row["EntryIdx"].ToString();

                //sbTask.AppendLine();
                sbTask.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                sbTask.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                string strTitleK = string.Empty;
                if (row["EntryTitleK"].ToString().Length > 0)
                    strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                else
                    strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                string strTitleE = string.Empty;
                if (row["EntryTitleE"].ToString().Length > 0)
                    strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                else
                    strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                string strTitleC = string.Empty;
                if (row["EntryTitleC"].ToString().Length > 0)
                    strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                else
                    strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
                string strSummary = string.Empty;
                if (row["Summary"].ToString().Length > 0)
                    strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
                else
                    strSummary = "<SUMMARY></SUMMARY>";
                //##### 2015.09-15 추가
                string strTitleSub1 = string.Empty;
                if (row["EntryTitleSub1"].ToString().Length > 0)
                    strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
                else
                    strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
                string strTitleSub2 = string.Empty;
                if (row["EntryTitleSub2"].ToString().Length > 0)
                    strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
                else
                    strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
                string strSynonym = string.Empty;
                if (row["Synonym"].ToString().Length > 0)
                    strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
                else
                    strSynonym = "<SYNONYM></SYNONYM>";
                sbTask.AppendLine(strTitleK);
                sbTask.AppendLine(strTitleE);
                sbTask.AppendLine(strTitleC);
                sbTask.AppendLine(strSummary);
                sbTask.AppendLine(strTitleSub1);
                sbTask.AppendLine(strTitleSub2);
                sbTask.AppendLine(strSynonym);                //reader.Close();
                Con.Close();

                SqlConnection Con2 = new SqlConnection(connectionString);
                SqlCommand Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;

                //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
                //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
                strQuery = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                    + " FROM TEntry as Y "
                    + " join TCode as A on A.Idx=Y.RevisionIdx "
                    + "	join TCode as C on C.Idx=Y.SubjectIdx "
                    + "	join TCode as E on E.Idx=Y.BrandIdx  "
                    + "	join TCode as F on F.Idx=Y.GradeIdx  "
                    + "	join TCode as G on G.Idx=Y.SemesterIdx "
                    + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                    + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                    + " WHERE Y.Idx=" + strEntryIdx + "";
                //Response.Write (strQuery);
                Cmd2.CommandText = strQuery;
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader2.Read())
                {
                    string[] strCategory = reader2["category"].ToString().Split('>');
                    int i = 1;
                    foreach (string strUnit in strCategory)
                    {
                        sbTask.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                        i++;
                    }
                }
                reader2.Close();
                Con2.Close();

                Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                string strINFORMATION = string.Empty;
                if (reader2.HasRows)
                {
                    sbTask.AppendLine("<INFOMATION>");
                    while (reader2.Read())
                    {
                        strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
                        sbTask.AppendLine(strINFORMATION);
                    }
                    sbTask.AppendLine("</INFOMATION>");
                }
                reader2.Close();
                Con2.Close();

                Con2 = new SqlConnection(connectionString);
                Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;
                Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                DataTable dt2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = row2["Content"].ToString();
                    content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
                    content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
                    content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
                    content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
                    content = content.Replace("><", ">" + Environment.NewLine + "<");
                    sbTask.AppendLine("<" + row2["RecType"].ToString() + ">");
                    sbTask.AppendLine(content);
                    sbTask.AppendLine("</" + row2["RecType"].ToString() + ">");

                }
                Con2.Close();
                sbTask.AppendLine("</ENTRY>");
            }
        } //---Task 목록에서 TaskID를 추출한다. 끝
        return sbTask;
    }
    protected StringBuilder sbXMLByTask(string tmpTaskID)
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        string strImageLink = string.Empty;
        string strImgFileName = string.Empty;
        string strXML = string.Empty;
        //strTaskIdx = "60";
        exportIdx = txtExportIdx.Value;

        StringBuilder sbTask = new StringBuilder();
        string[] arrTaskID = tmpTaskID.Split(',');

        foreach (string strID in arrTaskID)   //---Task 목록에서 TaskID를 추출한다. 시작
        {
            strTaskIdx = strID;

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            strQuery = "SELECT * FROM TEntry WHERE TaskIdx=" + strTaskIdx + " AND DelFlag=0 ORDER BY SortNo ASC, Idx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;
            Con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            Con.Close();
            //totalEntCount += dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                //strXML = "";
                //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
                //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
                //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

                strEntryIdx = row["Idx"].ToString();

                //sbTask.AppendLine();
                sbTask.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                sbTask.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                string strTitleK = string.Empty;
                if (row["EntryTitleK"].ToString().Length > 0)
                    strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                else
                    strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                string strTitleE = string.Empty;
                if (row["EntryTitleE"].ToString().Length > 0)
                    strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                else
                    strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                string strTitleC = string.Empty;
                if (row["EntryTitleC"].ToString().Length > 0)
                    strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                else
                    strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";
                string strSummary = string.Empty;
                if (row["Summary"].ToString().Length > 0)
                    strSummary = "<SUMMARY><![CDATA[" + row["Summary"].ToString() + "]]></SUMMARY>";
                else
                    strSummary = "<SUMMARY></SUMMARY>";
                //##### 2015.09-15 추가
                string strTitleSub1 = string.Empty;
                if (row["EntryTitleSub1"].ToString().Length > 0)
                    strTitleSub1 = "<ENTRYTITLE_SUB1><![CDATA[" + row["EntryTitleSub1"].ToString() + "]]></ENTRYTITLE_SUB1>";
                else
                    strTitleSub1 = "<ENTRYTITLE_SUB1></ENTRYTITLE_SUB1>";
                string strTitleSub2 = string.Empty;
                if (row["EntryTitleSub2"].ToString().Length > 0)
                    strTitleSub2 = "<ENTRYTITLE_SUB2><![CDATA[" + row["EntryTitleSub2"].ToString() + "]]></ENTRYTITLE_SUB2>";
                else
                    strTitleSub2 = "<ENTRYTITLE_SUB2></ENTRYTITLE_SUB2>";
                string strSynonym = string.Empty;
                if (row["Synonym"].ToString().Length > 0)
                    strSynonym = "<SYNONYM><![CDATA[" + row["Synonym"].ToString() + "]]></SYNONYM>";
                else
                    strSynonym = "<SYNONYM></SYNONYM>";
                sbTask.AppendLine(strTitleK);
                sbTask.AppendLine(strTitleE);
                sbTask.AppendLine(strTitleC);
                sbTask.AppendLine(strSummary);
                sbTask.AppendLine(strTitleSub1);
                sbTask.AppendLine(strTitleSub2);
                sbTask.AppendLine(strSynonym);
                //reader.Close();
                Con.Close();

                SqlConnection Con2 = new SqlConnection(connectionString);
                SqlCommand Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;

                //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
                //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
                strQuery = "SELECT A.CodeName + '>'+ C.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                    + " FROM TEntry as Y "
                    + " join TCode as A on A.Idx=Y.RevisionIdx "
                    + "	join TCode as C on C.Idx=Y.SubjectIdx "
                    + "	join TCode as E on E.Idx=Y.BrandIdx  "
                    + "	join TCode as F on F.Idx=Y.GradeIdx  "
                    + "	join TCode as G on G.Idx=Y.SemesterIdx "
                    + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                    + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                    + " WHERE Y.Idx=" + strEntryIdx + "";
                //Response.Write (strQuery);
                Cmd2.CommandText = strQuery;                    
                Cmd2.CommandType = CommandType.Text;                
                Con2.Open();
                SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader2.Read())
                {
                    string[] strCategory = reader2["category"].ToString().Split('>');
                    int i = 1;
                    foreach (string strUnit in strCategory)
                    {
                        sbTask.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                        i++;
                    }
                }
                reader2.Close();
                Con2.Close();

                Cmd2.CommandText = "SELECT * FROM TOutlineEntry A join TOutline B on A.OutlineIdx=B.Idx WHERE EntryIdx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                string strINFORMATION = string.Empty;
                if (reader2.HasRows)
                {
                    sbTask.AppendLine("<INFOMATION>");
                    while (reader2.Read())
                    {
                        strINFORMATION = " <ITEM TITLE=\"" + reader2["OutlineName"] + "\"><![CDATA[" + reader2["OutlineData"] + "]]></ITEM>";
                        sbTask.AppendLine(strINFORMATION);
                    }
                    sbTask.AppendLine("</INFOMATION>");
                }
                reader2.Close();
                Con2.Close();


                Con2 = new SqlConnection(connectionString);
                Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;
                Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                DataTable dt2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    string content = row2["Content"].ToString();
                    content = content.Replace("<CAPTION><TEXT>", "<CAPTION>");
                    content = content.Replace("</TEXT></CAPTION>", "</CAPTION>");
                    content = content.Replace("<DESCRIPTION><TEXT>", "<DESCRIPTION>");
                    content = content.Replace("</TEXT></DESCRIPTION>", "</DESCRIPTION>");
                    content = content.Replace("><", ">" + Environment.NewLine + "<");
                    sbTask.AppendLine("<" + row2["RecType"].ToString() + ">");
                    sbTask.AppendLine(content);
                    sbTask.AppendLine("</" + row2["RecType"].ToString() + ">");

                }
                Con2.Close();
                sbTask.AppendLine("</ENTRY>");
            }
        } //---Task 목록에서 TaskID를 추출한다. 끝
        return sbTask;
    }
    protected void makeXMLByCategory()
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        string strImageLink = string.Empty;
        string strImgFileName = string.Empty;
        //strTaskIdx = "60";
        
        //string FileNewName = "XML_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + ImageFileUpload.PostedFile.FileName;
        //string sFileUri = HttpContext.Current.Server.MapPath("~/DATA/") + "XML_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml";

        StringBuilder sb = new StringBuilder();
        string strXML = string.Empty;
        sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        sb.AppendLine("<edu>");
        //string tmpEntryID = Request.Form[selCategory.UniqueID].ToString(); //.Text;// +",";
        string tmpEntryID = string.Empty;
        string[] arrEntryID = tmpEntryID.Split(',');

        foreach (string strID in arrEntryID)
        {
            //CheckBox chkID = item.FindControl("chkID") as CheckBox;
            //HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
            //if (chkID.Checked)
            //{
            
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            strQuery = "SELECT * FROM TEntry WHERE Idx=" + strID + " ORDER BY Idx ASC";
            Cmd.CommandType = CommandType.Text;
            Cmd.CommandText = strQuery;
            Con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            //totalEntCount += dt.Rows.Count;
            foreach (DataRow row in dt.Rows)
            {
                //strXML = "";
                //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
                //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
                //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

                strEntryIdx = row["Idx"].ToString();

                sb.AppendLine();
                sb.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                sb.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                string strTitleK = string.Empty;
                if (row["EntryTitleK"].ToString().Length > 0)
                    strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                else
                    strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                string strTitleE = string.Empty;
                if (row["EntryTitleE"].ToString().Length > 0)
                    strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                else
                    strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                string strTitleC = string.Empty;
                if (row["EntryTitleC"].ToString().Length > 0)
                    strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                else
                    strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";

                sb.AppendLine(strTitleK);
                sb.AppendLine(strTitleE);
                sb.AppendLine(strTitleC);
                //reader.Close();
                Con.Close();

                SqlConnection Con2 = new SqlConnection(connectionString);
                SqlCommand Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;

                //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
                //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
                Cmd2.CommandText = "SELECT A.CodeName + '>'+ B.CodeName+ '>'+ C.CodeName+ '>'+ D.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                    + " FROM TEntry as Y "
                    + " join TCode as A on A.Idx=Y.RevisionIdx "
                    + "	left outer join TCode as B on B.Idx=Y.SubjectGroupIdx "
                    + "	join TCode as C on C.Idx=Y.SubjectIdx "
                    + " left outer join TCode as D on D.Idx=Y.SchoolIdx "
                    + "	join TCode as E on E.Idx=Y.BrandIdx  "
                    + "	join TCode as F on F.Idx=Y.GradeIdx  "
                    + "	join TCode as G on G.Idx=Y.SemesterIdx "
                    + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                    + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                    + " WHERE Y.Idx=" + strEntryIdx + "";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                if (reader2.Read())
                {
                    string[] strCategory = reader2["category"].ToString().Split('>');
                    int i = 1;
                    foreach (string strUnit in strCategory)
                    {
                        sb.AppendLine("<DIGIT" + i + ">" + strUnit + "</DIGIT" + i + ">");
                        i++;
                    }
                }
                reader2.Close();
                Con2.Close();

                Con2 = new SqlConnection(connectionString);
                Cmd2 = new SqlCommand();
                Cmd2.Connection = Con2;
                Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND DelFlag=0 AND ParentIdx=0 ORDER BY SortNo ASC";
                Cmd2.CommandType = CommandType.Text;
                Con2.Open();
                DataTable dt2 = new DataTable();
                SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                adapter2.Fill(dt2);
                foreach (DataRow row2 in dt2.Rows)
                {
                    sb.AppendLine("<" + row2["RecType"].ToString() + ">");
                    sb.AppendLine(row2["Content"].ToString());
                    sb.AppendLine("</" + row2["RecType"].ToString() + ">");

                }
                Con2.Close();
                sb.AppendLine("</ENTRY>");
            }
            //
            //SqlConnection Conn = new SqlConnection(connectionString);
            //SqlCommand Cmdd = new SqlCommand();
            //Conn = new SqlConnection(connectionString);
            //Cmdd = new SqlCommand();
            //Cmdd.Connection = Conn;
            //Cmdd.CommandText = "SELECT Top 1 ImgFileName FROM TImportFile WHERE TaskIdx=" + strTaskIdx + " ORDER BY Idx DESC";
            //Cmdd.CommandType = CommandType.Text;
            //Conn.Open();
            //DataTable dt3 = new DataTable();
            //SqlDataAdapter adapter3 = new SqlDataAdapter(Cmdd);
            //adapter3.Fill(dt3);
            //foreach (DataRow row2 in dt3.Rows)
            //{
            //    strImgFileName += row2["ZipFileName"].ToString() + ",";
            //    strImageLink += "<a target='_blank' href='/UploadFile/" + row2["ZipFileName"].ToString() + "'>" + row2["ZipFileName"].ToString() + "</a><br />";
            //}
            //Conn.Close();
            //}
        }

        sb.AppendLine("</edu>");
        XmlDocument xd = new XmlDocument();
        xd.LoadXml(sb.ToString());


        // Write the stream contents to a new file named 
        DateTime dtTime = DateTime.Now;
        string FileNewName = "XML_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
        using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
        {
            outfile.Write(xd.InnerXml);
        }
        // XML 파일 파싱

        XmlSerializer serializer = new XmlSerializer(typeof(edu));
        serializer.UnknownNode += new XmlNodeEventHandler(Serializer_UnknownNode);
        serializer.UnknownAttribute += new XmlAttributeEventHandler(Serializer_UnknownAttribute);

        // A FileStream is needed to read the XML document.
        FileStream fs = new FileStream(pathData + @FileNewName, FileMode.Open);
        // Declare an object variable of the type to be deserialized.

        edu eduroot;
        try
        {

            eduroot = (edu)serializer.Deserialize(fs);
            //데이터 베이스에 넣어야 함
            //파라미터 정의

            string entryCode, entryTitle, entryTitle_c, entryTitle_e, entryTitle_k;

            int intIDXCONT = eduroot.ENTRY[0].INDEXCONTENT.Count();
            for (int i = 0; i < intIDXCONT; i++)
            {
                int intBasicCount = eduroot.ENTRY[0].INDEXCONTENT[0].BASIC.Count();
                for (int j = 0; j < intBasicCount; j++)
                {
                    
                    //int intImageCount = eduroot.ENTRY[0].INDEXCONTENT[i].BASIC[j]
                    //for (int k = 0; k < intImageCount; k++)
                    //{
                    //string basicText = eduroot.ENTRY[0].INDEXCONTENT[i].BASIC[j].IMAGE.TEXT;
                }

            }


        }
        catch (Exception ex)
        {
            //오류다!
            Response.Write("XML을 분석하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "
                            + ex.ToString());
            return;
        }
        

        try
        {

            lblXMLFile.Text = "<a target='_blank' href=/Data/" + @FileNewName + ">" + @FileNewName + "</a>";
            //lblImageFile.Text = strImageLink;
            lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
            int sumCnt = 0;
            sumCnt = Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
            SqlConnection Con = new SqlConnection(connectionString);
            strQuery = "INSERT INTO TExport (ExportTitle, TemplateIdx, EntryCount, XMLFile, FirstDate) VALUES "
                + " ( '" + txtTitle + "', " + ddlTemplate.SelectedValue + ", "
                + sumCnt + " , '" + FileNewName + "', getdate())";
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
        }
        catch (Exception ex)
        {
            //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
        }
        finally
        {

        }

    }

    private void Serializer_UnknownAttribute(object sender, XmlAttributeEventArgs e)
    {
        throw new NotImplementedException();
    }

    private void Serializer_UnknownNode(object sender, XmlNodeEventArgs e)
    {
        throw new NotImplementedException();
    }

    protected void makeXMLByTask()
    {
        string strQuery = string.Empty;
        string strEntryIdx = string.Empty;
        string strImageLink = string.Empty;
        string strImgFileName = string.Empty;
        //strTaskIdx = "60";
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TExportData WHERE ExportIdx =" + exportIdx + " AND Type='TASK' ";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        if (dt.Rows.Count == 0)
        {

        }
        else
        {
            StringBuilder sb = new StringBuilder();
            string strXML = string.Empty;
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            sb.AppendLine("<edu>");
            //string tmpTaskID = Request.Form[selTaskID.UniqueID].ToString(); //.Text;// +",";

            foreach (DataRow taskrow in dt.Rows)  //---- Export Task 목록을 가져온다.
            {
                string tmpTaskID = string.Empty;
                tmpTaskID = taskrow["Content"].ToString();
                string[] arrTaskID = tmpTaskID.Split(',');

                foreach (string strID in arrTaskID)   //---Task 목록에서 TaskID를 추출한다.
                {
                    //CheckBox chkID = item.FindControl("chkID") as CheckBox;
                    //HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
                    //if (chkID.Checked)
                    //{
                    strTaskIdx = strID;

                    DBFileInfo();
                    Con = new SqlConnection(connectionString);
                    Cmd = new SqlCommand();
                    Cmd.Connection = Con;
                    strQuery = "SELECT * FROM TEntry WHERE TaskIdx=" + strTaskIdx + " ORDER BY Idx ASC";
                    Cmd.CommandType = CommandType.Text;
                    Cmd.CommandText = strQuery;
                    Con.Open();

                    dt = new DataTable();
                    adapter = new SqlDataAdapter(Cmd);
                    adapter.Fill(dt);
                    Con.Close();
                    //totalEntCount += dt.Rows.Count;
                    foreach (DataRow row in dt.Rows)
                    {
                        //strXML = "";
                        //strXML += "<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=" + row["Idx"].ToString() + ">";
                        //strXML += "<ENTRYTITLE><![CDATA["+ row["EntryTitle"].ToString() +"]]></ENTRYTITLE>";
                        //strXML += "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                        //strXML += "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                        //strXML += "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";

                        strEntryIdx = row["Idx"].ToString();

                        sb.AppendLine();
                        sb.AppendLine("<ENTRY timestamp=\"" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\" entryCode=\"" + strEntryIdx + "\">");
                        sb.AppendLine("<ENTRYTITLE><![CDATA[" + row["EntryTitle"].ToString() + "]]></ENTRYTITLE>");
                        string strTitleK = string.Empty;
                        if (row["EntryTitleK"].ToString().Length > 0)
                            strTitleK = "<ENTRYTITLE_K><![CDATA[" + row["EntryTitleK"].ToString() + "]]></ENTRYTITLE_K>";
                        else
                            strTitleK = "<ENTRYTITLE_K></ENTRYTITLE_K>";
                        string strTitleE = string.Empty;
                        if (row["EntryTitleE"].ToString().Length > 0)
                            strTitleE = "<ENTRYTITLE_E><![CDATA[" + row["EntryTitleE"].ToString() + "]]></ENTRYTITLE_E>";
                        else
                            strTitleE = "<ENTRYTITLE_E></ENTRYTITLE_E>";
                        string strTitleC = string.Empty;
                        if (row["EntryTitleC"].ToString().Length > 0)
                            strTitleC = "<ENTRYTITLE_C><![CDATA[" + row["EntryTitleC"].ToString() + "]]></ENTRYTITLE_C>";
                        else
                            strTitleC = "<ENTRYTITLE_C></ENTRYTITLE_C>";

                        sb.AppendLine(strTitleK);
                        sb.AppendLine(strTitleE);
                        sb.AppendLine(strTitleC);
                        //reader.Close();
                        Con.Close();

                        SqlConnection Con2 = new SqlConnection(connectionString);
                        SqlCommand Cmd2 = new SqlCommand();
                        Cmd2.Connection = Con2;

                        //Cmd2.Parameters.Add("@Idx", SqlDbType.Int);
                        //Cmd2.Parameters["@Idx"].Value = Convert.ToInt32(strEntryIdx);
                        Cmd2.CommandText = "SELECT A.CodeName + '>'+ B.CodeName+ '>'+ C.CodeName+ '>'+ D.CodeName+ '>'+ E.CodeName+ '>'+ F.CodeName+ '>'+ G.CodeName + '>'+ H.LUnitName + '>'+ I.MUnitName as category"
                            + " FROM TEntry as Y "
                            + " join TCode as A on A.Idx=Y.RevisionIdx "
                            + "	join TCode as B on B.Idx=Y.SubjectGroupIdx "
                            + "	join TCode as C on C.Idx=Y.SubjectIdx "
                            + " join TCode as D on D.Idx=Y.SchoolIdx "
                            + "	join TCode as E on E.Idx=Y.BrandIdx  "
                            + "	join TCode as F on F.Idx=Y.GradeIdx  "
                            + "	join TCode as G on G.Idx=Y.SemesterIdx "
                            + "	join TLUnit as H on H.Idx=Y.LUnitIdx "
                            + "	join TMUnit as I on I.Idx=Y.MUnitIdx "
                            + " WHERE Y.Idx=" + strEntryIdx + "";
                        Cmd2.CommandType = CommandType.Text;
                        Con2.Open();
                        SqlDataReader reader2 = Cmd2.ExecuteReader(CommandBehavior.CloseConnection);
                        if (reader2.Read())
                        {
                            string[] strCategory = reader2["category"].ToString().Split('>');
                            int i = 1;
                            foreach (string strUnit in strCategory)
                            {
                                sb.AppendLine("<DIGIT"+i+">" + strUnit + "</DIGIT"+i+">");
                                i++;
                            }
                        }
                        reader2.Close();
                        Con2.Close();

                        Con2 = new SqlConnection(connectionString);
                        Cmd2 = new SqlCommand();
                        Cmd2.Connection = Con2;
                        Cmd2.CommandText = "SELECT RecType, Content FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 AND DelFlag=0 ORDER BY SortNo ASC";
                        Cmd2.CommandType = CommandType.Text;
                        Con2.Open();
                        DataTable dt2 = new DataTable();
                        SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                        adapter2.Fill(dt2);
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            sb.AppendLine("<" + row2["RecType"].ToString() + ">");
                            sb.AppendLine(row2["Content"].ToString());
                            sb.AppendLine("</" + row2["RecType"].ToString() + ">");

                        }
                        Con2.Close();
                        sb.AppendLine("</ENTRY>");
                    }
                    //
                    //SqlConnection Conn = new SqlConnection(connectionString);
                    //SqlCommand Cmdd = new SqlCommand();
                    //Conn = new SqlConnection(connectionString);
                    //Cmdd = new SqlCommand();
                    //Cmdd.Connection = Conn;
                    //Cmdd.CommandText = "SELECT Top 1 ZipFileName FROM TImportFile WHERE TaskIdx=" + strTaskIdx + " ORDER BY Idx DESC";
                    //Cmdd.CommandType = CommandType.Text;
                    //Conn.Open();
                    //DataTable dt3 = new DataTable();
                    //SqlDataAdapter adapter3 = new SqlDataAdapter(Cmdd);
                    //adapter3.Fill(dt3);
                    //Conn.Close();
                    //foreach (DataRow row2 in dt3.Rows)
                    //{
                    //    strImgFileName += row2["ZipFileName"].ToString() + ",";
                    //    strImageLink += "<a target='_blank' href='/UploadFile/" + row2["ZipFileName"].ToString() + "'>" + row2["ZipFileName"].ToString() + "</a><br />";
                    //}
                } //---Task 목록에서 TaskID를 추출한다.
            }
            sb.AppendLine("</edu>");
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(sb.ToString());


            // Write the stream contents to a new file named 
            DateTime dtTime = DateTime.Now;
            string FileNewName = "XML_Export_" + dtTime.ToString("yyyyMMddHHmmss") + ".xml";
            using (StreamWriter outfile = new StreamWriter(pathData + @FileNewName))
            {
                outfile.Write(xd.InnerXml);
            }

            try
            {
                lblXMLFile.Text = "<a target='_blank' href=/Data/" + @FileNewName + ">" + @FileNewName + "</a>";
                //lblImageFile.Text = strImageLink;
                lblFirstDate.Text = dtTime.ToString("yyyy-MM-dd");
                int sumCnt = 0;
                sumCnt = Convert.ToInt32(Request.Form[txtSumEntry.UniqueID].ToString());
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle + "',"
                    + " TemplateIdx =" + ddlTemplate.SelectedValue + ", EntryCount ="+sumCnt +", "
                    + " XMLFile ='"+FileNewName +"' , FirstDate=getdate()  WHERE Idx="+ strEntryIdx +"";

                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : " + ex.ToString();
            }
            finally
            {

            }
        }
    }
    protected void btnImageChk_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        GetImageData();
        CheckImage();

    }
    protected void CheckImage()
    {
        exportIdx = txtExportIdx.Value;
        int totalImgCount = 0;
        int missImgCount = 0; 
        DBFileInfo();
        //########## 이미지 추출한 정보를 기반으로 파일이 존재하는지 체크한다 시작 ######
        string pathFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/");
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT Idx, ImageName, ExTaskIdx FROM TExportImage WHERE ExportIdx =" + exportIdx + " ";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        totalImgCount += dt.Rows.Count;
        Con.Close();
        foreach (DataRow row2 in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
        {
            string fileURI = pathFolder + row2["ExTaskIdx"].ToString() + "/" + row2["ImageName"].ToString();
            if (File.Exists(fileURI))
            {
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TExportImage SET ExistFlag=1 WHERE Idx=" + row2["Idx"].ToString() + "";
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
        }
        Con = new SqlConnection(connectionString);
        Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT ImageName FROM TExportImage WHERE ExportIdx =" + exportIdx + " AND ExistFlag=0 Group By ImageName";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        dt = new DataTable();
        adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        MissImageList.DataSource = dt;
        MissImageList.DataBind();
        missImgCount += dt.Rows.Count;

        txtSumImage.Text = "전체:" + totalImgCount + " | 누락:" + missImgCount;
    }
    protected void GetImageData()
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT * FROM TExportData WHERE ExportIdx =" + exportIdx + "";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();

        foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 시작
        {
            switch (row["Type"].ToString() )
            {
                case "TASK":
                    string[] arrTaskIdx = row["Content"].ToString().Split(',');
                    foreach (string taskIdx in arrTaskIdx )
                    {
                        SqlConnection Con2 = new SqlConnection(connectionString);
                        SqlCommand Cmd2 = new SqlCommand();
                        Cmd2.Connection = Con2;
                        Cmd2.CommandText = "SELECT Content, TaskIdx FROM TEntryData WHERE TaskIdx =" + taskIdx + " AND DelFlag=0";
                        Cmd2.CommandType = CommandType.Text;
                        Con2.Open();
                        DataTable dt2 = new DataTable();
                        SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                        adapter2.Fill(dt2);
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            string[] arrImgFile = row2["Content"].ToString().Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                int nextVal = imgFile.ToString().IndexOf("</FILENAME>");
                                string imgName = string.Empty;
                                if (nextVal > 0)
                                {
                                    imgName = imgFile.ToString().Substring(0, nextVal);
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand();
                                    Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                    int intExDataIdx = 0;
                                    if (row["Idx"].ToString().Length > 0)
                                        intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                    Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);

                                    Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                    Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                    Cmd.Parameters["@ImageName"].Value = imgName;
                                    Cmd.Parameters["@ExTaskIdx"].Value = taskIdx;

                                    Cmd.CommandText = "USP_ExportImage_INSERT";
                                    Cmd.CommandType = CommandType.StoredProcedure;
                                    Cmd.Connection = Con;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                }
                            }
                            arrImgFile = row2["Content"].ToString().Split(new string[] { "<img src=\"" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                int nextVal = imgFile.ToString().IndexOf("\" style");
                                string imgName = string.Empty;
                                if (nextVal > 0)
                                {
                                    imgName = imgFile.ToString().Substring(0, nextVal);
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand();
                                    Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                    int intExDataIdx = 0;
                                    if (row["Idx"].ToString().Length > 0)
                                        intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                    Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);

                                    Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                    Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                    Cmd.Parameters["@ImageName"].Value = imgName;
                                    Cmd.Parameters["@ExTaskIdx"].Value = taskIdx;

                                    Cmd.CommandText = "USP_ExportImage_INSERT";
                                    Cmd.CommandType = CommandType.StoredProcedure;
                                    Cmd.Connection = Con;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                }
                            }
                        }
                    }
                    continue;
                case "Category":
                    string[] arrEntryIdx = row["Content"].ToString().Split(',');
                    foreach (string entryIdx in arrEntryIdx.Reverse<string>())
                    {
                        SqlConnection Con2 = new SqlConnection(connectionString);
                        SqlCommand Cmd2 = new SqlCommand();
                        Cmd2.Connection = Con2;
                        Cmd2.CommandText = "SELECT Content, B.TaskIdx FROM TEntryData A, TEntry B WHERE A.EntryIdx =" + entryIdx + " AND A.EntryIdx=B.Idx AND A.DelFlag=0";
                        Cmd2.CommandType = CommandType.Text;
                        Con2.Open();
                        DataTable dt2 = new DataTable();
                        SqlDataAdapter adapter2 = new SqlDataAdapter(Cmd2);
                        adapter2.Fill(dt2);
                        foreach (DataRow row2 in dt2.Rows)
                        {
                            string[] arrImgFile = row2["Content"].ToString().Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                int nextVal = imgFile.ToString().IndexOf("</FILENAME>");
                                string imgName = string.Empty;
                                if (nextVal > 0)
                                {
                                    imgName = imgFile.ToString().Substring(0, nextVal);
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand();
                                    Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                    int intExDataIdx = 0;
                                    if (row["Idx"].ToString().Length > 0)
                                        intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                    Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);
                                    Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                    Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                    Cmd.Parameters["@ImageName"].Value = imgName;
                                    Cmd.Parameters["@ExTaskIdx"].Value = row2["TaskIdx"].ToString();

                                    Cmd.CommandText = "USP_ExportImage_INSERT";
                                    Cmd.CommandType = CommandType.StoredProcedure;
                                    Cmd.Connection = Con;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                }
                            }
                            arrImgFile = row2["Content"].ToString().Split(new string[] { "<img src=\"" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                //int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                                int nextVal = imgFile.ToString().IndexOf("\" style");
                                string imgName = string.Empty;
                                if (nextVal > 0)
                                {
                                    imgName = imgFile.ToString().Substring(0, nextVal);
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand();
                                    Cmd.Parameters.Add("@ExportIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ExDataIdx", SqlDbType.Int);
                                    Cmd.Parameters.Add("@ImageName", SqlDbType.VarChar, 255);
                                    int intExDataIdx = 0;
                                    if (row["Idx"].ToString().Length > 0)
                                        intExDataIdx = Convert.ToInt32(row["Idx"].ToString());
                                    Cmd.Parameters.Add("@ExTaskIdx", SqlDbType.Int);

                                    Cmd.Parameters["@ExportIdx"].Value = exportIdx;
                                    Cmd.Parameters["@ExDataIdx"].Value = intExDataIdx;
                                    Cmd.Parameters["@ImageName"].Value = imgName;
                                    Cmd.Parameters["@ExTaskIdx"].Value = row2["TaskIdx"].ToString();

                                    Cmd.CommandText = "USP_ExportImage_INSERT";
                                    Cmd.CommandType = CommandType.StoredProcedure;
                                    Cmd.Connection = Con;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                }
                            }
                        }
                    }
                    continue;
            } //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다 끝
        }
    }
    protected void makeImageFile()
    {
        string pathFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/");
        string pathNewFolder = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/Temp/");
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "SELECT ImageName, ExTaskIdx FROM TExportImage WHERE ExportIdx =" + exportIdx + " Group By ImageName, ExTaskIdx";
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
        {
            string fileURI = pathFolder + row["ExTaskIdx"].ToString() +"/"+ row["ImageName"].ToString();
            string newFileURI = pathNewFolder + row["ImageName"].ToString();
            if (File.Exists(fileURI))
            {
                try
                {
                    File.Copy(fileURI, newFileURI);
                }
                catch(Exception ex)
                {
                    lblImageFile.Text = "이미지 파일 복사 중 오류가 발생했습니다.";
                    continue;
                }
            }
        }
        if (dt.Rows.Count > 0)
        {
            string ExportImageFile = "IMG_Export_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".zip";
            string startPath = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/Temp/"); ;
            string zipPath = HttpContext.Current.Server.MapPath("~/cms100data/EntryData/") + ExportImageFile;

            ZipFile.CreateFromDirectory(startPath, zipPath);
            lblImageFile.Text = "<a target='_blank' href='/cms100data/EntryData/" + ExportImageFile + "'>" + ExportImageFile + "</a><br />";
            // TExport Export테이블에 이미지 파일명 업데이트
            Con = new SqlConnection(connectionString);
            strQuery = "UPDATE TExport SET ImgFile='" + ExportImageFile + "', ImageCount="+ dt.Rows.Count +" WHERE Idx =" + exportIdx + "";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            foreach (DataRow row in dt.Rows)  //---- 각 항목별로 이미지 이름을 추출하여 TExportImage에 넣는다
            {
                string fileURI = pathFolder + row["ImageName"].ToString();
                string newFileURI = pathNewFolder + row["ImageName"].ToString();
                if (File.Exists(newFileURI))
                {
                    // ...or by using FileInfo instance method.
                    System.IO.FileInfo fi = new System.IO.FileInfo(@newFileURI);
                    try
                    {
                        fi.Delete();
                    }
                    catch (System.IO.IOException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }
    }
    protected void EntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string delID;
        RepeaterItem repeaterItem = EntryList.Items[e.Item.ItemIndex];
        HiddenField txtIDX = repeaterItem.FindControl("txtIDX") as HiddenField;
        delID = txtIDX.Value;

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand("DELETE FROM TExportData WHERE Idx = " + delID +"; DELETE FROM TExportImage WHERE ExDataIdx="+ delID  , Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        Listing();
    }
    protected void btnTask_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
            + " TemplateIdx =" + ddlTemplate.SelectedValue + ", Target="+ ddlType.SelectedValue +" "
            + " WHERE Idx=" + exportIdx + "";

        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();
        Cmd = null;
        Con = null;
        Response.Write("<script>window.open('ExportTask.aspx?exportIdx="+ exportIdx +"','popup','width=960,height=700, scrollbars=yes');</script>");
    }
    protected void btnCategory_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
            + " TemplateIdx =" + ddlTemplate.SelectedValue + ", Target="+ ddlType.SelectedValue +" "
            + " WHERE Idx=" + exportIdx + "";

        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();
        Cmd = null;
        Con = null;
        Response.Write("<script>window.open('ExportCategory.aspx?exportIdx=" + exportIdx + "','popup','width=960,height=700, scrollbars=yes');</script>");        
    }
    protected void btnBinder_Click(object sender, EventArgs e)
    {
        exportIdx = txtExportIdx.Value;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        strQuery = "UPDATE TExport SET ExportTitle='" + txtTitle.Text + "',"
            + " TemplateIdx =" + ddlTemplate.SelectedValue + ", Target=" + ddlType.SelectedValue + " "
            + " WHERE Idx=" + exportIdx + "";

        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();
        Cmd = null;
        Con = null;
        Response.Write("<script>window.open('ExportBinder.aspx?exportIdx=" + exportIdx + "','popup','width=960,height=700, scrollbars=yes');</script>");        
    }
}
