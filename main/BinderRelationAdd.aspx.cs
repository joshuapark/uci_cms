﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class main_BinderRelationAdd : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private string BinderIdx = string.Empty;
    private string Relation = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝   
        
        if (!IsPostBack)
        {
            lblTotalCount.Text = "";
            SaveButton.Visible = false;
        }
            // 1. BinderIDX 받기
            if (Request.Params["BinderIdx"] != null)
            {
                BinderIdx = Request.Params["BinderIdx"].ToString();
                Relation = Request.Params["Relation"].ToString();
                PrintTitle(Relation);
                txtBinderIdx.Value = BinderIdx;
                txtRelation.Value = Relation;
            }


        //Page.MaintainScrollPositionOnPostBack = true;
    }
    protected void PrintTitle(string val)
    {
        switch(val)
        {
            case "1":
                lblTypeTitle.Text = "상위바인더";
                break;
            case "2":
                lblTypeTitle.Text = "연관바인더";
                break;
        }
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        DBFileInfo();

        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();

        SqlDataAdapter sda = new SqlDataAdapter("SELECT BinderIdx, BinderTitle, BinderMemo, EntryCount, Convert(CHAR(10),EditDate,120) AS EditDate FROM TBinder WHERE ( BinderTitle like '%" + txtKeyword.Text + "%' OR BinderMemo like '%" + txtKeyword.Text + "%') AND DelFlag=0 AND (BinderIdx != " + BinderIdx + ") AND ( BinderIdx Not in (SELECT RelationBinderIdx FROM TBinderRelation WHERE RelationBinderIdx="+ BinderIdx +"AND Relation="+ Relation +" AND DelFlag=0)) ORDER BY BinderIdx DESC", Conn);
        DataTable dtBinder = new DataTable();
        sda.Fill(dtBinder);
        rpRelationList.DataSource = dtBinder;
        rpRelationList.DataBind();
        
        if (rpRelationList.Items.Count > 0)
        {
            lblTotalCount.Text = rpRelationList.Items.Count.ToString()+"개의 바인더가 검색되었습니다.";
            SaveButton.Visible = true;
        }
        else
        {
            lblTotalCount.Text = "";
            SaveButton.Visible = false;
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void SaveButton_Click(object sender, EventArgs e)
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();
        int intBinderIdx = Convert.ToInt32(txtBinderIdx.Value);
        foreach (RepeaterItem item in rpRelationList.Items)
        {
            CheckBox chkID = item.FindControl("chkID") as CheckBox;
            Label RBinderIdx = item.FindControl("lblNo") as Label;
            
            if (chkID.Checked)
            {
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand();
                Cmd.Connection = Con;
                Cmd.CommandText = "USP_Binder_Relation_INSERT";
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@BinderIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@RelationBinderIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@Relation", SqlDbType.Int);

                Cmd.Parameters["@BinderIdx"].Value = intBinderIdx;
                Cmd.Parameters["@RelationBinderIdx"].Value = Convert.ToInt32(RBinderIdx.Text);
                Cmd.Parameters["@Relation"].Value = Convert.ToInt32(txtRelation.Value);

                Con.Open();
                Cmd.ExecuteNonQuery();
                if (Con.State == ConnectionState.Open)
                    Con.Close();
            }
        }

        Response.Write("<script>opener.location.href='BinderEdit.aspx?Idx=" + txtBinderIdx.Value + "';window.opener=self;self.close();</script>");
    }
}