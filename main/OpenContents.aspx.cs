﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;

public partial class main_opencontents : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string strType = string.Empty;
    public string strTitle = "대분류";
    private string strEntryIdx = string.Empty;
    private static string imgURL = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }

        if (!IsPostBack)
        {
            int index = ddlGrade.SelectedIndex;
            
            if (Request.Params["entryIdx"] != null)
                strEntryIdx = Request.Params["entryIdx"].ToString();
            if (Request.Params["type"] != null)
                strType = Request.Params["type"].ToString();
                searchRecType.Value = strType;
            if (strType == "quiz")
                strTitle = "퀴즈";

            BindCategory();

            //if (strEntryIdx.Length > 0)
            //    BindContent();

            //Listing();
            //LUnitListing();
        }

    }
    private void BindContent( string entryIdx )
    {
        string strRecType = string.Empty;
        if (searchRecType.Value.Equals("cont"))
        {
            strRecType = "INDEXCONTENT";
        }
        else if (searchRecType.Value.Equals("quiz"))
        {
            strRecType = "QUIZ";
        }


        DataTable resultTable = new DataTable();
        resultTable.Columns.Add("idx");
        resultTable.Columns.Add("content");

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        string strQuery = "SELECT * FROM TEntryData WHERE EntryIdx = " + entryIdx + " AND DelFlag=0" ;
        SqlDataAdapter sda = new SqlDataAdapter(strQuery , Con);
        DataTable entryTable = new DataTable();
        sda.Fill(entryTable);

        string taskIdx = Convert.ToString ( entryTable.Rows[0]["taskidx"] );
        string rootUrl = "http://" + Request.ServerVariables["HTTP_HOST"];
        imgURL = rootUrl + "/CMS100Data/EntryData/" + taskIdx + "/";


        DataRow[] parentRowList = entryTable.Select("RecType='" + strRecType + "'");

        foreach( DataRow row in parentRowList ){

            DataRow dataRow = resultTable.NewRow();
            int parentId = Convert.ToInt32( row["idx"] );
            dataRow["idx"] = parentId;
            
            DataRow[] childRowList = entryTable.Select("ParentIdx=" + parentId);

            string content = string.Empty;
            foreach (DataRow childRow in childRowList) {

                content += this.convertXmlToHtml(Convert.ToString(childRow["content"]) , Convert.ToString(childRow["rectype"])) + "<br/>";

            }

            dataRow["content"] = content;
            resultTable.Rows.Add(dataRow);
        }

        rpEntryList.DataSource = resultTable;
        rpEntryList.DataBind();

        Con.Close();

    }


    private void BindCategory()
    {
        
        
        
        DBFileInfo();

        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = "SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC";
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

        ListItem li = new ListItem("선택", "0");
        ddlRevision.Items.Add(li);
        //ddlSubjectGroup.Items.Add(li);
        ddlSubject.Items.Add(li);
        //ddlSchool.Items.Add(li);
        ddlBrand.Items.Add(li);
        ddlGrade.Items.Add(li);
        ddlSemester.Items.Add(li);
        ddlLUnit.Items.Add(li);
        ddlMUnit.Items.Add(li);

        ddlRevision.AppendDataBoundItems = true;
        //ddlSubjectGroup.AppendDataBoundItems = true;
        ddlSubject.AppendDataBoundItems = true;
        //ddlSchool.AppendDataBoundItems = true;
        ddlBrand.AppendDataBoundItems = true;
        ddlGrade.AppendDataBoundItems = true;
        ddlSemester.AppendDataBoundItems = true;
        ddlLUnit.AppendDataBoundItems = true;
        ddlMUnit.AppendDataBoundItems = true;
        
        //SqlDataReader Reader = Cmd.ExecuteReader();
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        ddlRevision.DataSource = dtRevision;
        ddlRevision.DataTextField = "CodeName";
        ddlRevision.DataValueField = "Idx";
        ddlRevision.DataBind();

        //DataTable dtSubjectGroup = ds.Tables["TCode"];
        //dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        //ddlSubjectGroup.DataSource = dtSubjectGroup;
        //ddlSubjectGroup.DataTextField = "CodeName";
        //ddlSubjectGroup.DataValueField = "Idx";
        //ddlSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        dtSubject.DefaultView.Sort = "CodeName ASC";
        ddlSubject.DataSource = dtSubject;
        ddlSubject.DataTextField = "CodeName";
        ddlSubject.DataValueField = "Idx";
        ddlSubject.DataBind();

        //DataTable dtSchool = ds.Tables["TCode"];
        //dtSchool.DefaultView.RowFilter = "CodeType = 4";
        //ddlSchool.DataSource = dtSchool;
        //ddlSchool.DataTextField = "CodeName";
        //ddlSchool.DataValueField = "Idx";
        //ddlSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        dtBrand.DefaultView.Sort = "CodeName ASC";
        ddlBrand.DataSource = dtBrand;
        ddlBrand.DataTextField = "CodeName";
        ddlBrand.DataValueField = "Idx";
        ddlBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        dtGrade.DefaultView.Sort = "Idx ASC";
        ddlGrade.DataSource = dtGrade;
        ddlGrade.DataTextField = "CodeName";
        ddlGrade.DataValueField = "Idx";
        ddlGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        ddlSemester.DataSource = dtSemester;
        ddlSemester.DataTextField = "CodeName";
        ddlSemester.DataValueField = "Idx";
        ddlSemester.DataBind();

        //Close the connection.
        dtRevision = null;
        //dtSubjectGroup = null;
        dtSubject = null;
        //dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;


        ddlRevision.SelectedValue = "0";
        //ddlSubjectGroup.SelectedValue = "0";
        ddlSubject.SelectedValue = "0";
        //ddlSchool.SelectedValue = "0";
        ddlBrand.SelectedValue = "0";
        ddlGrade.SelectedValue = "0";
        ddlSemester.SelectedValue = "0";
        ddlLUnit.SelectedValue = "0";
        ddlMUnit.SelectedValue = "0";
    }

    private void LUnitListing()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_LUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0; // ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
        Cmd.Parameters["@SchoolIdx"].Value = 0;   // ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TLUnit");
        //Set up the data binding. 
        Conn.Close();
        ListItem li = new ListItem("선택", "0");
        ddlLUnit.Items.Add(li);
        ddlLUnit.AppendDataBoundItems = true;
        DataTable dtLUnit = ds.Tables["TLUnit"];
        ddlLUnit.DataSource = dtLUnit;
        ddlLUnit.DataTextField = "LUnitName";
        ddlLUnit.DataValueField = "Idx";
        ddlLUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
    }
    private void MUnitListing()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_MUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters["@LUnitIdx"].Value = ddlLUnit.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TMUnit");
        //Set up the data binding. 
        Conn.Close();
        ListItem li = new ListItem("선택", "0");
        ddlMUnit.Items.Add(li);
        ddlMUnit.AppendDataBoundItems = true;
        DataTable dtLUnit = ds.Tables["TMUnit"];
        ddlMUnit.DataSource = dtLUnit;
        ddlMUnit.DataTextField = "MUnitName";
        ddlMUnit.DataValueField = "Idx";
        ddlMUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
    }
    private void Listing()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_OpenContents_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar, 30);
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = Convert.ToInt32(ddlRevision.SelectedValue);
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0;   // Convert.ToInt32(ddlSubjectGroup.SelectedValue);
        Cmd.Parameters["@SubjectIdx"].Value = Convert.ToInt32(ddlSubject.SelectedValue);
        Cmd.Parameters["@SchoolIdx"].Value = 0;    // Convert.ToInt32(ddlSchool.SelectedValue);
        Cmd.Parameters["@BrandIdx"].Value = Convert.ToInt32(ddlBrand.SelectedValue);
        Cmd.Parameters["@GradeIdx"].Value = Convert.ToInt32(ddlGrade.SelectedValue);
        Cmd.Parameters["@SemesterIdx"].Value = Convert.ToInt32(ddlSemester.SelectedValue);
        if (ddlLUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@LUnitIdx"].Value = Convert.ToInt32(ddlLUnit.SelectedValue);
        else
            Cmd.Parameters["@LUnitIdx"].Value = 0;
        if (ddlMUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@MUnitIdx"].Value = Convert.ToInt32(ddlMUnit.SelectedValue);
        Cmd.Parameters["@UserIdx"].Value = null;
        Cmd.Parameters["@KeyWord"].Value = txtKeyWord.Text;
        Cmd.Parameters["@TaskIdx"].Value = null;
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);
        lblTotalCnt.Text = totalCnt.ToString() + "건의 검색결과가 있습니다.";
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        { 
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 개정
            Label lblRevision = item.FindControl("lblRevision") as Label;
            lblRevision.Text = DataBinder.Eval(item.DataItem, "Revision").ToString();

            // 개정
            Label lblSubject = item.FindControl("lblSubject") as Label;
            lblSubject.Text = DataBinder.Eval(item.DataItem, "Subject").ToString();

            // 개정
            Label lblBrand = item.FindControl("lblBrand") as Label;
            lblBrand.Text = DataBinder.Eval(item.DataItem, "Brand").ToString();
            // 개정
            Label lblGrade = item.FindControl("lblGrade") as Label;
            lblGrade.Text = DataBinder.Eval(item.DataItem, "Grade").ToString();
            // 개정
            Label lblSemester = item.FindControl("lblSemester") as Label;
            lblSemester.Text = DataBinder.Eval(item.DataItem, "Semester").ToString();
            // 개정
            Label lblLUnit = item.FindControl("lblLUnit") as Label;
            lblLUnit.Text = DataBinder.Eval(item.DataItem, "LUnit").ToString();
            // 개정
            Label lblMUnit = item.FindControl("lblMUnit") as Label;
            lblMUnit.Text = DataBinder.Eval(item.DataItem, "MUnit").ToString();
            // 제목과 상세보기 링크를 설정한다.
            //HyperLink lblTitle = item.FindControl("lblTitle") as HyperLink;
            //lblTitle.Text = DataBinder.Eval(item.DataItem, "Title").ToString();
            //lblTitle.NavigateUrl = string.Format("OpenContents.aspx?Idx={0}&pageNo={1}&pageSize={2}", DataBinder.Eval(item.DataItem, "Idx"), pageNo, pageSize);
            LinkButton btnTitle = item.FindControl("btnTitle") as LinkButton;
            btnTitle.Text = DataBinder.Eval(item.DataItem, "Title").ToString();
            btnTitle.CommandArgument = DataBinder.Eval(item.DataItem, "idx").ToString();
            //btnTitle.OnClientClick = "javascript:OpenPage(" + DataBinder.Eval(item.DataItem, "Idx").ToString() + ",'" + recType.Value + "');";

            // 개정
            Label lblUser = item.FindControl("lblUser") as Label;
            lblUser.Text = DataBinder.Eval(item.DataItem, "UserName").ToString();

            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
        }
    }

    protected void DownButton_Click(object sender, EventArgs e)
    {
        DataTable dt = executeQueryText();
        String excelFileName = "EntryList_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";
        this.exportExcel(dt, excelFileName);
    }
    private DataTable executeQueryText()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Con.Open();
        Cmd.CommandText = "USP_Entry_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@KeyWord", SqlDbType.NVarChar, 30);
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = Convert.ToInt32(ddlRevision.SelectedValue);
        Cmd.Parameters["@SubjectGroupIdx"].Value = 0;  //Convert.ToInt32(ddlSubjectGroup.SelectedValue);
        Cmd.Parameters["@SubjectIdx"].Value = Convert.ToInt32(ddlSubject.SelectedValue);
        Cmd.Parameters["@SchoolIdx"].Value = 0;    //Convert.ToInt32(ddlSchool.SelectedValue);
        Cmd.Parameters["@BrandIdx"].Value = Convert.ToInt32(ddlBrand.SelectedValue);
        Cmd.Parameters["@GradeIdx"].Value = Convert.ToInt32(ddlGrade.SelectedValue);
        Cmd.Parameters["@SemesterIdx"].Value = Convert.ToInt32(ddlSemester.SelectedValue);
        if (ddlLUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@LUnitIdx"].Value = Convert.ToInt32(ddlLUnit.SelectedValue);
        else
            Cmd.Parameters["@LUnitIdx"].Value = 0;
        if (ddlMUnit.SelectedValue.ToString().Length > 0)
            Cmd.Parameters["@MUnitIdx"].Value = Convert.ToInt32(ddlMUnit.SelectedValue);
        Cmd.Parameters["@UserIdx"].Value = 0;
        Cmd.Parameters["@KeyWord"].Value = txtKeyWord.Text;
        Cmd.Parameters["@TaskIdx"].Value = 0;
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }
    private void exportExcel(DataTable dataTable, String excelFileName)
    {
        string attachment = "attachment; filename=" + excelFileName;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void MUnitSearch_Click(object sender, EventArgs e)
    {
        MUnitListing();
    }
    protected void LUnitSearch_Click(object sender, EventArgs e)
    {
        LUnitListing();
    }
    protected void SearchButton_Click(object sender, EventArgs e)
    {
        Listing();
    }

    protected void TitleButton_Click(object sender, EventArgs e)
    {
        LinkButton titleButton = (LinkButton)sender;
        string entryIdx = titleButton.CommandArgument;

        BindContent(entryIdx);
    }


    protected void OpenButton_Click(object sender, EventArgs e)
    {
        string chkIdx= string.Empty;

        foreach (RepeaterItem item in rpEntryList.Items)
        {
            HiddenField entryIdx = item.FindControl("EntryID") as HiddenField;
            CheckBox chkEntry = item.FindControl("chkEntry") as CheckBox;
            if (chkEntry.Checked) {
                chkIdx += entryIdx.Value + ",";
            }
        }
        

        // 부모창 
        Response.Write("<script>opener.getContentsFromServer('" + chkIdx + "');self.close();</script>");
    }

    //public string convertXmlToHtml(string strXml)
    //{

    //    StringWriter stringWriter = new StringWriter();
    //    HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

    //    //루트태그를 붙여줌
    //    strXml = "<edu>" + strXml + "</edu>";

    //    //align이 있는지 확인한다.
    //    Boolean isAlign = false;
    //    string strAlign = String.Empty;
    //    int startAlignIndex = strXml.IndexOf("<ALIGN>");
    //    string alignEndTag = "</ALIGN>";
    //    int endAlignIndex = strXml.LastIndexOf(alignEndTag);

    //    if (startAlignIndex > -1)
    //    {
    //        isAlign = true;
    //        string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
    //        strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
    //        strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
    //    }

    //    //caption이 있는 확인한다.
    //    Boolean isCaption = false;
    //    string strCaption = string.Empty;

    //    strXml = strXml.Replace("<EMPHASIS>", "<b>");
    //    strXml = strXml.Replace("</EMPHASIS>", "</b>");
    //    //strXml = strXml.Trim();


    //    using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
    //    {

    //        // Parse the file and display each of the nodes.
    //        //reader.MoveToContent();

    //        while (reader.Read())
    //        {


    //            if (reader.NodeType == XmlNodeType.CDATA)
    //            {
    //                htmlWriter.Write(reader.Value);

    //                //currentWriter.Write(reader.ReadString());

    //            }
    //            else if (reader.NodeType == XmlNodeType.Element)
    //            {

    //                switch (reader.Name)
    //                {
    //                    case "TEXT":

    //                        if (isAlign)
    //                        {
    //                            htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign);
    //                        }

    //                        htmlWriter.RenderBeginTag(HtmlTextWriterTag.P);

    //                        break;
    //                    case "IMGS":
    //                        break;
    //                    case "IMG":
    //                        break;

    //                    case "WIDTH":
    //                        reader.Read();
    //                        htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
    //                        break;
    //                    case "HEIGHT":
    //                        reader.Read();
    //                        htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
    //                        break;
    //                    case "CAPTION":
    //                        reader.Read();
    //                        isCaption = true;
    //                        strCaption = reader.Value;
    //                        break;
    //                    case "DESCRIPTION":
    //                        break;
    //                    case "FILENAME":
    //                        reader.Read();
    //                        htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, "http://hyonga.iptime.org:15080/UploadFile/EntryImage/" + reader.Value);

    //                        break;
    //                }

    //            }
    //            else if (reader.NodeType == XmlNodeType.EndElement)
    //            {

    //                switch (reader.Name)
    //                {
    //                    case "TEXT":
    //                        htmlWriter.RenderEndTag();
    //                        break;
    //                    case "IMGS":
    //                        break;
    //                    case "IMG":
    //                        if (isAlign)
    //                        {
    //                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Align, strAlign);
    //                        }

    //                        htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);

    //                        if (isCaption)
    //                        {
    //                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Caption);
    //                            htmlWriter.Write(strCaption);
    //                            htmlWriter.RenderEndTag();
    //                            isCaption = false;
    //                        }

    //                        htmlWriter.RenderEndTag();
    //                        break;
    //                    case "ALIGN":
    //                        break;
    //                    case "WIDTH":

    //                        break;
    //                    case "HEIGHT":

    //                        break;
    //                    case "CAPTION":
    //                        break;
    //                    case "DESCRIPTION":
    //                        break;
    //                }
    //            }
    //        }
    //    }

    //    return stringWriter.ToString();
    //}

    public string convertXmlToHtml(string strXml, string recType)
    {

        //align이 있는지 확인한다.
        bool isAlign = false;
        string strAlign = String.Empty;
        int startAlignIndex = strXml.IndexOf("<ALIGN>");
        string alignEndTag = "</ALIGN>";
        int endAlignIndex = strXml.LastIndexOf(alignEndTag);
        if (startAlignIndex > -1)
        {
            isAlign = true;
            string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
            strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
            strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
        }


        if (recType.Equals("IMAGE"))
        {

            if (strXml.Contains("<IMGS>"))
            {
                if (!strXml.Contains("</IMGS>"))
                {
                    strXml += "</IMGS>";
                }
            }

            return parseXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("MATRIX_TABLE"))
        {
            return parseXmlToHtml(strXml, isAlign, strAlign);
        }
        else if (recType.Equals("SOUND") || recType.Equals("PAIRSENTENCE") || recType.Equals("VIDEO"))
        {
            strXml = strXml.Replace("<FILEANME>", "");
            strXml = strXml.Replace("</FILEANME>", "");
            return strXml;
        }
        else if (recType.Equals("LEVEL"))
        {
            return strXml;
        }
        else
        {

            strXml = strXml.Replace("<img src=\"", "<img src=\"" + imgURL);
            strXml = strXml.Replace("<br>", "<br/>");

            string result = string.Empty;
            strXml = "<edu>" + strXml + "</edu>";
            using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
            {
                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.CDATA:
                            if (isAlign)
                            {
                                StringWriter stringWriter = new StringWriter();
                                HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                                htmlWriter.Write(reader.Value);
                                htmlWriter.RenderEndTag();
                                result += stringWriter.ToString();
                            }
                            else
                            {
                                result += reader.Value;
                            }
                            break;
                        case XmlNodeType.Text:
                            result += reader.Value;
                            break;

                    }
                }
            }

            return result;

        }

    }

    private static string parseXmlToHtml(string strXml, bool isAlign, string strAlign)
    {
        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.CDATA)
                {
                    htmlWriter.Write(reader.Value);

                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMGS":
                            if (isAlign)
                            {
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);
                            break;
                        case "IMG":
                            break;
                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, imgURL + reader.Value);

                            break;
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMGS":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMG":
                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);
                            htmlWriter.RenderEndTag();
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }

}
