﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Drawing;

public partial class main_List : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private int paramCodeType=1;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  


        //if (!IsPostBack)
        //{

        //}
        
        // 파라미터 받기
        if (Request.Params["CodeType"] != null)
            paramCodeType = int.Parse(Request.Params["CodeType"].ToString());

        if (!IsPostBack)
        {
            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand("SELECT CodeType, CodeTypeName FROM TCodeType WHERE DelFlag=0 ORDER BY CodeTypeName ASC", Conn);
            Conn.Open();
            SqlDataReader Reader = Cmd.ExecuteReader();

            //Set up the data binding.
            ddlCodeType.DataSource = Reader;
            ddlCodeType.DataTextField = "CodeTypeName";
            ddlCodeType.DataValueField = "CodeType";
            ddlCodeType.DataBind();

            //Close the connection.
            Conn.Close();
            Reader.Close();

            ddlCodeType.SelectedValue = paramCodeType.ToString();

            Listing();
        }
    }
    protected void Listing()
    {
        // DB Connection String, File Path 설정
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandText = "USP_Code_SELECT";
        Cmd.Parameters.Add("@CodeType", SqlDbType.Int);
        Cmd.Parameters["@CodeType"].Value = paramCodeType;
        Cmd.CommandType = CommandType.StoredProcedure;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "status_list");

        CodeTableRepeater.DataSource = ds;
        CodeTableRepeater.DataBind();

        //TableRow tr;
        //TableCell td;

        //tr = new TableRow();

        //td = new TableCell();
        //td.Text = "";
        //td.Width = 50;
        //tr.Cells.Add(td);

        //td = new TableCell();
        //td.Text = "코드";
        //tr.Cells.Add(td);

        //td = new TableCell();
        //td.Text = "코드명";
        //tr.Cells.Add(td);

        //td = new TableCell();
        //td.Text = "";
        //tr.Cells.Add(td);

        //tr.BackColor = Color.FromName("#ccccff");

        //CodeTable.Rows.Add(tr);

        //for (int i = 0; i < ds.Tables["status_list"].Rows.Count; i++)
        //{
        //    string Code, CodeName;
        //    Code = ds.Tables["status_list"].Rows[i]["Code"].ToString();
        //    CodeName = ds.Tables["status_list"].Rows[i]["CodeName"].ToString();
        //    tr = new TableRow();
        //    td = new TableCell();
        //    tr.Cells.Add(td);
        //    // 
        //    td = new TableCell();
        //    Label tb0 = new Label();
        //    tb0.ID = "CodeID" + i;
        //    tb0.Text = Code;
        //    td.Controls.Add(tb0);
        //    tr.Cells.Add(td);
        //    //
        //    td = new TableCell();
        //    Label tb1 = new Label();
        //    tb1.ID = "CodeName" + i;
        //    tb1.Text = CodeName;
        //    td.Controls.Add(tb1);
        //    tr.Cells.Add(td);
        //    // 
        //    td = new TableCell();
        //    Button bt = new Button();
        //    bt.CommandName = "DELETE";
        //    bt.BorderWidth = 0;
        //    bt.CssClass = "btn-code-del";
        //    //bt.Text = "X";
        //    bt.ID = Code;
        //    bt.OnClientClick = "javascript:return confirm('삭제하시겠습니까?')";
        //    bt.Click += new EventHandler(this.DelButton_Click);
        //    td.Controls.Add(bt); 

        //    tr.Cells.Add(td);
        //    CodeTable.Rows.Add(tr);
        //}
    }

    protected void CodeTableRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //string delID;
        //Button clickedButton = (Button)sender;
        //delID = clickedButton.ID;

        string delID;
        RepeaterItem repeaterItem = CodeTableRepeater.Items[e.Item.ItemIndex];
        HiddenField hfID = repeaterItem.FindControl("CodeID") as HiddenField;
        delID = hfID.Value;


        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString); 
        SqlCommand Cmd = new SqlCommand("UPDATE TCode SET DelFlag=1	WHERE Code = '" + delID +"'", Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        string script = "<script>alert('삭제되었습니다');location.href='CodeView.aspx?CodeType=" + paramCodeType + "'</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

        Listing();
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        if (txtCode.Text.Trim().Length > 0)  //기관명을 입력했는지 체크한다.
        {
            DBFileInfo();
            SqlConnection Con0 = new SqlConnection(connectionString);
            SqlCommand Cmd0 = new SqlCommand("SELECT Code FROM TCode WHERE CodeType=" + ddlCodeType.SelectedValue + " AND Code='" + txtCode.Text.Trim() + "' AND DelFlag=0 ", Con0);
            Cmd0.CommandType = CommandType.Text;
            Con0.Open();

            SqlDataReader reader = Cmd0.ExecuteReader(CommandBehavior.CloseConnection);

            if (reader.Read())
            {
                msgIDCheck.InnerText = "* " + txtCode.Text.Trim() + " 코드는 이미 등록되어 있습니다. 다른 코드를 입력하세요.";
                string script = "<script>alert('" + txtCode.Text.Trim() + " 코드는 이미 등록되어 있습니다.');showPopup();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                Con0.Close();
                Con0 = null;
                Cmd0 = null;
            }
            else
            {
                SqlConnection Con = new SqlConnection(connectionString);

                SqlCommand Cmd = new SqlCommand("USP_Code_INSERT", Con);
                Cmd.CommandType = CommandType.StoredProcedure;

                Cmd.Parameters.Add("@Code", SqlDbType.VarChar, 255);
                Cmd.Parameters.Add("@CodeName", SqlDbType.VarChar, 255);
                Cmd.Parameters.Add("@CodeType", SqlDbType.Int);

                Cmd.Parameters["@Code"].Value = txtCode.Text.Trim();
                Cmd.Parameters["@CodeName"].Value = txtCodeName.Text;
                Cmd.Parameters["@CodeType"].Value = ddlCodeType.SelectedValue;

                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();

                Cmd = null;
                Con = null;

                //string script = "<script>alert('저장되었습니다');location.href='CodeView.aspx?CodeType="+paramCodeType+"'</script>";
                //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                //Response.Redirect("CodeView.aspx?CodeType=" + paramCodeType + "");
                Listing();
            }
        }
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

}