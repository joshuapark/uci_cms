﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadMultipleImage.aspx.cs" Inherits="main_UploadMultipleImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
<link href="../css/syaku.file.css" rel="stylesheet"/>
</head>
<script src="../js/ckeditor/ckeditor.js"></script>
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/common/json2.js"></script>
<script src="../js/common/xml2json.js"></script>
<script src="../js/swfupload/swfupload.js"></script>
<script src="../js/swfupload/swfupload.queue.js"></script>
<script src="../js/syaku/syaku.ckeditor.handlers.js"></script>
<script src="../js/syaku/syaku.file.handlers.js"></script>
<script src="../js/syaku/syaku.file.js"></script>
<body>
    <form id="form1" runat="server">
    <div>
        <textarea title="내용" cols="80" rows="10" id="content" name="content">안녕하세요.</textarea>
        <script>
            CKEDITOR.inline('content' , {
                filebrowserUploadUrl: './Upload.ashx',
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                    { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                ]
            });
        </script>
    </div>            <div class='file_upload' style='padding-bottom:5px;'>
  <div class='file_head'>
    <span id='swfu_button'></span>

    <button type="button" onclick="jQuery.syakuFileUpload.deleteSwfupload(swfu,objEditor);">삭제</button>
    <button type="button" onclick="jQuery.syakuFileUpload.editor_file_input(swfu,objEditor);">선택삽입</button>
    <button type="button" onclick="jQuery.syakuFileUpload.editor_file_remove(swfu,objEditor);">선택모두제거</button>
  </div>
  <div class='file_content'>
    <div class='file_preview' id='file_preview'></div>
    <div class='file_field'>
      <select class='file_view' id='file' name='file' multiple='multiple' onclick='jQuery.syakuFileUpload.preview(swfu);'>

      <!--
      <option value="{ 
      file_orl : '파일 번호' , 
      file : '파일명' , 
      re_file : '변경된 파일 명' , 
      folder : '폴더경로' , 
      file_size : '파일사이즈' ,
      extension : '파일 확장자명' , 
      type : '파일 형식' 
      }">파일명 (파일용량)</option>
      -->

      </select>
    </div>
    <div class='file_text'>
      <p>총 용량 : <span id='file_size_text'>0 KB</span> / 무제한</p>
      <p>개당 용량 : 무제한</p>
      <p>파일 형식 : *.jpg;*.png;*.gif;</p>
      <p>파일 제한 수 : 무제한</p>
    </div>
    <div class="clear"></div>
  </div>
  <div id='swfu_progress'></div>
</div>

<div>
<button type="button" onclick="save();">전송하기</button>
</div>

</form>

    <script>

        // SWFUpload
        swfu = jQuery.syakuFileUpload.swfupload({
            ele_file: '#file',
            ele_file_orl: '#file_orl',
            ele_file_size: '#file_size_text',
            ele_preview: '#file_preview',
            file_size_limit: 0,
            file_types: '*.jpg;*.png;*.gif;',
            file_types_description: '사용자 파일',
            file_upload_multi: true,
            file_upload_limit: 0, // 파일 첨부수 (0 = 무제한)
            file_upload_unlimited: true, // file_upload_limit 가 0인 경우 true , 아닌 경우 false

            post_params: { // 그외 전송될 파라메터
                /*
                file_orl : '',
                target_orl : '',
                mid : '',
                sid : '',
                seq : '',
                member_orl : ''
                */
            },


            button_placeholder_id: 'swfu_button'
        });

    </script>

</body>
</html>
