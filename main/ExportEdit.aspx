﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExportEdit.aspx.cs" Inherits="main_TemplateAdd" validateRequest="false" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>       <html class="no-js"> <![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/jquery.js"></script>
</head>

<body>

<header id="header"><!-- header -->
    <div class="container">
	    <h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

	    <div id="utility"><!-- utility -->
		    <asp:Label ID="lblLogIn" runat="server"></asp:Label>
		    <a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
	    </div><!-- // utility -->
    </div>
</header><!-- header -->
<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li class="active"><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="addForm" runat="server">
    <div id="contents">
        <div class="title"><!-- title -->
			<h2 class="title">Export 조회</h2>
		</div><!-- // title -->	

		<div class="pull-right">
            <a class="btn btn-sm btn-info " id="btnList" href="ExportList.aspx">Export목록</a>
		</div>
        <br /><br />
        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered"><!-- table-a -->
		    <colgroup>
			    <col style="width: 120px;">
			    <col style="width: auto;">
			    <col style="width: 100px;">
		    </colgroup>
            <asp:Repeater ID="EntryList" runat="server" >
                <HeaderTemplate>
                    <tr>
				        <th>검색분류</th>
				        <th>조회조건</th>
				        <th>엔트리수</th> 
                    </tr>
<%
    if (EntryList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="3" style="text-align:center">등록된 검색조건이 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblType" Text='<%#DataBinder.Eval(Container.DataItem , "Type")%>' runat="server" />
                            <asp:HiddenField ID="txtIDX" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>' runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblCondition" Text='<%#DataBinder.Eval(Container.DataItem , "Content")%>' runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblEntryCnt" Text='<%#DataBinder.Eval(Container.DataItem , "EntryCount")%>'  runat="server" />
                            <asp:HiddenField ID="txtContent" Value='<%#DataBinder.Eval(Container.DataItem , "Content")%>' runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered"><!-- table-a -->
		    <colgroup>
			    <col style="width: 120px;">
			    <col style="width: auto;">
			    <col style="width: 120px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>제공채널</th>
				    <td>
				        <asp:DropDownList ID="ddlType" runat="server" Enabled="false"></asp:DropDownList>                            
				    </td>
				    <th>템플릿명</th>
                    <td>
				        <asp:DropDownList ID="ddlTemplate" runat="server" Enabled="false"></asp:DropDownList>                            
                        <asp:HiddenField ID="txtExportIdx" runat="server"></asp:HiddenField>                            
                    </td>
			    </tr>
			    <tr>
				    <th>Export작업명</th>
				    <td colspan="3">
				        <asp:TextBox ID="txtTitle" runat="server" CssClass="large"  Enabled="false" ></asp:TextBox>                            
                    <!--//    <asp:RequiredFieldValidator ID="rfvTxtTitle" runat="server" ControlToValidate="txtTitle" ForeColor="OrangeRed" ErrorMessage="* Export작업명을 입력해주세요." /> -->
				    </td>
			    </tr>
		    </tbody>
		</table><!-- // table-a -->
        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered"><!-- table-a -->
		    <colgroup>
			    <col style="width: 120px;">
			    <col style="width: auto;">
			    <col style="width: 120px;">
			    <col style="width: auto;">
		    </colgroup>
            <tr>
                <th>엔트리 합계</th>
                <td><asp:Label ID="txtSumEntry" runat="server" Enabled="false"></asp:Label></td>
                <th>이미지 합계</th>
                <td><asp:Label ID="txtSumImage" runat="server" Enabled="false"></asp:Label></td>
            </tr>
        </table>

        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered"><!-- table-a -->
		    <colgroup>
			    <col style="width: 120px;">
			    <col style="width: auto;">
			    <col style="width: 120px;">
			    <col style="width: auto;">
		    </colgroup>
            <tr>
                <th>Export File</th>
                <td style="text-align:center">
                    <asp:Label ID="lblXMLFile" runat="server"></asp:Label><br />
                    <asp:Label ID="lblImageFile" runat="server"></asp:Label>                    
                </td>
                <th>최초생성일</th>
                <td style="text-align:center"><asp:Label ID="lblFirstDate" runat="server"></asp:Label></td>
            </tr>
        </table>
	</div><!-- // contents -->
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>

    </form>

</div>
</body>
</html>
