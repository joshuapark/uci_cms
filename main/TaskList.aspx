﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="TaskList.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

	  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1 active"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">테마관리</a></li>
				<li class="nth-child-3"><a href="EntryAddList.aspx">엔트리관리</a></li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5"><a href="UserList.aspx">사용자관리</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">엔트리생성</h2>
		</div><!-- // title -->	
   
		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <asp:Button ID="AddButton" cssClass="btn btn-sm btn-primary" runat="server" text="신규등록" onclick="AddButton_Click"></asp:Button> 
			</div>
		</div><!-- // section-button -->

        <asp:Label ID="strTotalCnt" runat="server"></asp:Label>
		<table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <asp:ListView ID="LV_Notice" runat="server" ItemPlaceholderID="phItemList" 
                    OnItemDataBound="ListView_ItemDataBound" >
                    <LayoutTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>                            
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>브랜드명</th>
				            <th>작업명</th>
				            <th>배포수량</th>
				            <th>작업수량</th>
				            <th>발급일시</th>
				            <th>작업자</th>
				            <th>검수자</th>
				            <th>파일</th>
				            <th>배포</th>
                        </tr>
                        <asp:PlaceHolder ID="phItemList" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblBrand" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblTaskTitle" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblPublishCnt" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEditCnt" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblPublishDate" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblUser" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblChecker" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblList" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/> 
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>브랜드명</th>
				            <th>작업명</th>
				            <th>배포수량</th>
				            <th>작업수량</th>
				            <th>발급일시</th>
				            <th>작업자</th>
				            <th>검수자</th>
				            <th>파일</th>
				            <th>배포</th>
                        </tr>
                        <tr>
                            <td colspan="10">등록된 작업이 없습니다.</td>
                        </tr>
                    </EmptyDataTemplate>
            </asp:ListView>
        </table>

		<div class="pagination-wrap">
            <ul class="pagination">
                <asp:HyperLink ID="hlPagePrev" runat="server" />
                    
                <%=lblPage %>
                    
                <asp:HyperLink ID="hlPageNext" runat="server" />
            </ul>
        </div> 

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>