﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class main_opencontents : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "1000";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;
    private string strType = string.Empty;
    public string strTitle = "대분류";
    private string strEntryIdx = string.Empty;
    private string entryIdx = string.Empty;

    private DataTable categoryTable = new DataTable();
    private DataTable entryTable = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');window.opener=self;self.close();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 5)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝   

        if (!IsPostBack)
        {
            if (Request.Params["Idx"] != null)
            {
                entryIdx = Request.Params["Idx"].ToString();
                txtEntryIdx.Value = entryIdx;
            }
            BindCategory();
            GetEntryCategory(entryIdx);
        }
        else
        {
            entryIdx = txtEntryIdx.Value;
        }
    }
    private void GetEntryCategory(string EntryIdx)
    {
        if (EntryIdx.Length > 0)
        {
            try
            {
                // 엔트리상세 데이터(TEntryData) 가져오기

                txtEntryIdx.Value = EntryIdx;
                SqlConnection Conn = new SqlConnection(connectionString);
                Conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + EntryIdx, Conn);

                this.entryTable = new DataTable();
                sda.Fill(entryTable);

                Conn.Close();
                // 엔트리기본(TEntry) 정보 가져오기
                //DBFileInfo();
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Connection = Con;
                //Cmd.CommandText = "SELECT E.*, U.UserName FROM TEntry E join TUser U on E.UserIdx=U.Idx WHERE E.Idx=" + idx + "";
                Cmd.Parameters.Add("@Idx", SqlDbType.Int);
                Cmd.Parameters["@Idx"].Value = EntryIdx;
                Cmd.CommandText = "USP_Entry_SELECT";
                Cmd.CommandType = CommandType.StoredProcedure;
                Con.Open();
                SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                txtEntryIdx.Value = EntryIdx;
                if (reader.Read())
                {
                    ddlRevision.SelectedValue = reader["RevisionIdx"].ToString();
                    //ddlSubjectGroup.SelectedValue = reader["SubjectGroupIdx"].ToString();
                    ddlSubject.SelectedValue = reader["SubjectIdx"].ToString();
                    //ddlSchool.SelectedValue = reader["SchoolIdx"].ToString();
                    ddlBrand.SelectedValue = reader["BrandIdx"].ToString();
                    ddlGrade.SelectedValue = reader["GradeIdx"].ToString();
                    ddlSemester.SelectedValue = reader["SemesterIdx"].ToString();
                    LUnitListing();
                    ddlLUnit.SelectedValue = reader["LUnitIdx"].ToString();
                    MUnitListing();
                    ddlMUnit.SelectedValue = reader["MUnitIdx"].ToString();
                }
                Con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("엔트리조회 중 오류가 발생하였습니다.");
            }
        }
        else
        {
            EntryIdx = "0";

            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + EntryIdx, Conn);

            this.entryTable = new DataTable();
            sda.Fill(entryTable);

            Conn.Close();
        }
    }


    private void BindCategory()
    {
        DBFileInfo();

        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = "SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC";
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);

        //ListItem li = new ListItem("선택", "0");
        //ddlRevision.Items.Add(li);
        //ddlSubjectGroup.Items.Add(li);
        //ddlSubject.Items.Add(li);
        //ddlSchool.Items.Add(li);
        //ddlBrand.Items.Add(li);
        //ddlGrade.Items.Add(li);
        //ddlSemester.Items.Add(li);
        //ddlLUnit.Items.Add(li);
        //ddlMUnit.Items.Add(li);

        //ddlRevision.AppendDataBoundItems = true;
        //ddlSubjectGroup.AppendDataBoundItems = true;
        //ddlSubject.AppendDataBoundItems = true;
        //ddlSchool.AppendDataBoundItems = true;
        //ddlBrand.AppendDataBoundItems = true;
        //ddlGrade.AppendDataBoundItems = true;
        //ddlSemester.AppendDataBoundItems = true;
        //ddlLUnit.AppendDataBoundItems = true;
        //ddlMUnit.AppendDataBoundItems = true;
        
        //SqlDataReader Reader = Cmd.ExecuteReader();
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        ddlRevision.DataSource = dtRevision;
        ddlRevision.DataTextField = "CodeName";
        ddlRevision.DataValueField = "Idx";
        ddlRevision.DataBind();

        //DataTable dtSubjectGroup = ds.Tables["TCode"];
        //dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        //ddlSubjectGroup.DataSource = dtSubjectGroup;
        //ddlSubjectGroup.DataTextField = "CodeName";
        //ddlSubjectGroup.DataValueField = "Idx";
        //ddlSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        dtSubject.DefaultView.Sort = "CodeName ASC";
        ddlSubject.DataSource = dtSubject;
        ddlSubject.DataTextField = "CodeName";
        ddlSubject.DataValueField = "Idx";
        ddlSubject.DataBind();

        //DataTable dtSchool = ds.Tables["TCode"];
        //dtSchool.DefaultView.RowFilter = "CodeType = 4";
        //ddlSchool.DataSource = dtSchool;
        //ddlSchool.DataTextField = "CodeName";
        //ddlSchool.DataValueField = "Idx";
        //ddlSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        dtBrand.DefaultView.Sort = "CodeName ASC";
        ddlBrand.DataSource = dtBrand;
        ddlBrand.DataTextField = "CodeName";
        ddlBrand.DataValueField = "Idx";
        ddlBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        dtGrade.DefaultView.Sort = "Idx ASC";
        ddlGrade.DataSource = dtGrade;
        ddlGrade.DataTextField = "CodeName";
        ddlGrade.DataValueField = "Idx";
        ddlGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        ddlSemester.DataSource = dtSemester;
        ddlSemester.DataTextField = "CodeName";
        ddlSemester.DataValueField = "Idx";
        ddlSemester.DataBind();

        //Close the connection.
        dtRevision = null;
        //dtSubjectGroup = null;
        dtSubject = null;
        //dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;

        //ddlRevision.AppendDataBoundItems = false;
        //ddlSubjectGroup.AppendDataBoundItems = false;
        //ddlSubject.AppendDataBoundItems = false;
        //ddlSchool.AppendDataBoundItems = false;
        //ddlBrand.AppendDataBoundItems = false;
        //ddlGrade.AppendDataBoundItems = false;
        //ddlSemester.AppendDataBoundItems = false;
        //ddlLUnit.AppendDataBoundItems = false;
        //ddlMUnit.AppendDataBoundItems = false;
    }

    private void LUnitListing()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Conn;
        Cmd.CommandText = "USP_LUnit_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        //Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);

        Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
        //Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
        //Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TLUnit");
        //Set up the data binding. 
        Conn.Close();
        //ListItem li = new ListItem("선택", "0");
        //ddlLUnit.Items.Clear();
        //ddlLUnit.Items.Add(li);
        //ddlLUnit.AppendDataBoundItems = true;
        DataTable dtLUnit = ds.Tables["TLUnit"];
        ddlLUnit.DataSource = dtLUnit;
        ddlLUnit.DataTextField = "LUnitName";
        ddlLUnit.DataValueField = "Idx";
        ddlLUnit.DataBind();

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
        //ddlLUnit.AppendDataBoundItems = false;
    }
    private void MUnitListing()
    {
        try
        {
            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Conn;
            Cmd.CommandText = "USP_MUnit_List_SELECT";
            Cmd.CommandType = CommandType.StoredProcedure;

            Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
            Cmd.Parameters["@LUnitIdx"].Value = ddlLUnit.SelectedValue;

            SqlDataAdapter sda = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();
            sda.Fill(ds, "TMUnit");
            //Set up the data binding. 
            Conn.Close();
            //ListItem li = new ListItem("선택", "0");
            //ddlMUnit.Items.Clear();
            //ddlMUnit.Items.Add(li);
            //ddlMUnit.AppendDataBoundItems = true;
            DataTable dtLUnit = ds.Tables["TMUnit"];
            ddlMUnit.DataSource = dtLUnit;
            ddlMUnit.DataTextField = "MUnitName";
            ddlMUnit.DataValueField = "Idx";
            ddlMUnit.DataBind();

            if (Conn.State == ConnectionState.Open)
                Conn.Close();

            sda = null;
            Conn = null;

            //ddlMUnit.AppendDataBoundItems = false;
        }
        catch(Exception ex)
        {
            Response.Write("중단원 조회 중 오류가 발생했습니다.");

        }
    }
    
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버  
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void MUnitSearch_Click(object sender, EventArgs e)
    {
        MUnitListing();
    }
    protected void LUnitSearch_Click(object sender, EventArgs e)
    {
        LUnitListing();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if(ddlLUnit.SelectedValue.Length>0 && ddlMUnit.SelectedValue.Length>0)
        {
            try
            {
                //DBFileInfo();
                //SqlConnection Con = new SqlConnection(connectionString);
                //SqlCommand Cmd = new SqlCommand();
                //Cmd.Connection = Con;

                //Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
                ////Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
                ////Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@LUnitIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@MUnitIdx", SqlDbType.Int);
                //Cmd.Parameters.Add("@EntryIdx", SqlDbType.Int);

                //Cmd.Parameters["@RevisionIdx"].Value = ddlRevision.SelectedValue;
                ////Cmd.Parameters["@SubjectGroupIdx"].Value = ddlSubjectGroup.SelectedValue;
                //Cmd.Parameters["@SubjectIdx"].Value = ddlSubject.SelectedValue;
                ////Cmd.Parameters["@SchoolIdx"].Value = ddlSchool.SelectedValue;
                //Cmd.Parameters["@BrandIdx"].Value = ddlBrand.SelectedValue;
                //Cmd.Parameters["@GradeIdx"].Value = ddlGrade.SelectedValue;
                //Cmd.Parameters["@SemesterIdx"].Value = ddlSemester.SelectedValue;
                //Cmd.Parameters["@LUnitIdx"].Value = ddlLUnit.SelectedValue;
                //Cmd.Parameters["@MUnitIdx"].Value = ddlMUnit.SelectedValue;
                //Cmd.Parameters["@EntryIdx"].Value = txtEntryIdx.Value;
                //Cmd.CommandText = "USP_Category_UPDATE";
                //Cmd.CommandType = CommandType.StoredProcedure;
                //Con.Open();
                //Cmd.ExecuteNonQuery();
                //Con.Close();
                //Text 설정하기
                string strRevision = string.Empty, strSubjectGroup = string.Empty, strSubject = string.Empty,
                    strSchool = string.Empty, strBrand = string.Empty, strGrade = string.Empty, strSemester = string.Empty,
                    strLUnit = string.Empty, strMUnit = string.Empty;
                if (ddlRevision.SelectedValue != "0")
                    strRevision = ddlRevision.SelectedItem.Text + ">";
                //if (ddlSubjectGroup.SelectedValue != "0")
                //    strSubjectGroup = ddlSubjectGroup.SelectedItem.Text + ">";
                if (ddlSubject.SelectedValue != "0")
                    strSubject = ddlSubject.SelectedItem.Text + ">";
                //if (ddlSchool.SelectedValue != "0")
                //    strSchool = ddlSchool.SelectedItem.Text + ">";
                if (ddlBrand.SelectedValue != "0")
                    strBrand = ddlBrand.SelectedItem.Text + ">";
                if (ddlGrade.SelectedValue != "0")
                    strGrade = ddlGrade.SelectedItem.Text + ">";
                if (ddlSemester.SelectedValue != "0")
                    strSemester = ddlSemester.SelectedItem.Text + ">";
                if (ddlLUnit.SelectedValue != "0")
                    strLUnit = ddlLUnit.SelectedItem.Text + ">";
                if (ddlMUnit.SelectedValue != "0")
                    strMUnit = ddlMUnit.SelectedItem.Text + ">";
                string strSelectEntryTitle = strRevision + strSubjectGroup + strSubject + strSchool + strBrand + strGrade + strSemester + strLUnit + strMUnit;
                strSelectEntryTitle = strSelectEntryTitle.Substring(0, strSelectEntryTitle.Length - 1);
                //Value 설정하기
                if (ddlRevision.SelectedValue != "0")
                    strRevision = ddlRevision.SelectedValue + ">";
                //if (ddlSubjectGroup.SelectedValue != "0")
                //    strSubjectGroup = ddlSubjectGroup.SelectedItem.Text + ">";
                if (ddlSubject.SelectedValue != "0")
                    strSubject = ddlSubject.SelectedValue + ">";
                //if (ddlSchool.SelectedValue != "0")
                //    strSchool = ddlSchool.SelectedValue + ">";
                if (ddlBrand.SelectedValue != "0")
                    strBrand = ddlBrand.SelectedValue + ">";
                if (ddlGrade.SelectedValue != "0")
                    strGrade = ddlGrade.SelectedValue + ">";
                if (ddlSemester.SelectedValue != "0")
                    strSemester = ddlSemester.SelectedValue + ">";
                if (ddlLUnit.SelectedValue != "0")
                    strLUnit = ddlLUnit.SelectedValue + ">";
                if (ddlMUnit.SelectedValue != "0")
                    strMUnit = ddlMUnit.SelectedValue + ">";
                string strSelectEntryIdx = strRevision + strSubjectGroup + strSubject + strSchool + strBrand + strGrade + strSemester + strLUnit + strMUnit;
                strSelectEntryIdx = strSelectEntryIdx.Substring(0, strSelectEntryIdx.Length - 1);

                //Response.Write("<script>opener.location.href='EntryEdit.aspx?exportIdx=" + exportIdx + "';window.opener=self;self.close();</script>");
                Response.Write("<script>opener.document.getElementById('txtCategory').innerHTML ='" + strSelectEntryTitle + "';opener.document.getElementById('hfCategoryIdx').value ='" + strSelectEntryIdx + "';window.opener=self;self.close();</script>");
            }
            catch (Exception ex)
            {
                Response.Write("카테고리 수정 중 오류가 발생했습니다.<br />오류내용:" + ex.ToString());
            }
            //Response.Redirect("ExportAdd.aspx?exportIdx=" + txtExportIdx.Value + "");
            //Response.Write("<script>opener.EntSum();self.close();</script>");
        }
        else
        {
            Response.Write("<script>alert('대단원과 중단원까지 선택하셔야 합니다.');</script>");
            return;
        }

    }
}
