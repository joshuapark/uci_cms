﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutlineList.aspx.cs" Inherits="main_List" EnableEventValidation="true" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
	    showAddOutlineType = function () {
	        $("#addOutlineType").show();
	        $("#addOutlineType").css("position", "absolute");
	        $("#addOutlineType").css("top", "0px");
	        $("#addOutlineType").css("left", "0px");
	    }
	    hideAddOutlineType = function () {
	        $("#addOutlineType").hide();
	    }
	    showAddOutline = function () {
	        $("#addOutline").show();
	        $("#addOutline").css("position", "absolute");
	        $("#addOutline").css("top", "200px");
	        $("#addOutline").css("left", "-300px");
	    }
	    hideAddOutline = function () {
	        $("#addOutline").hide();
	    }
	    jQuery.fn.center = function () {
	        this.css("position", "static");
	        this.css("top", "300px");
	        this.css("left", "300px");
	        //this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
	        //this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
	        return this;
	    }

	    var img_L = 0;
	    var img_T = 0;
	    var targetObj;

	    function getLeft(o) {
	        return parseInt(o.style.left.replace('px', ''));
	    }
	    function getTop(o) {
	        return parseInt(o.style.top.replace('px', ''));
	    }

	    // Div 움직이기
	    function moveDrag(e) {
	        var e_obj = window.event ? window.event : e;
	        var dmvx = parseInt(e_obj.clientX + img_L);
	        var dmvy = parseInt(e_obj.clientY + img_T);
	        targetObj.style.left = dmvx + "px";
	        targetObj.style.top = dmvy + "px";
	        return false;
	    }

	    // 드래그 시작
	    function startDrag(e, obj) {
	        targetObj = obj;
	        var e_obj = window.event ? window.event : e;
	        img_L = getLeft(obj) - e_obj.clientX;
	        img_T = getTop(obj) - e_obj.clientY;

	        document.onmousemove = moveDrag;
	        document.onmouseup = stopDrag;
	        if (e_obj.preventDefault) e_obj.preventDefault();
	    }

	    // 드래그 멈추기
	    function stopDrag() {
	        document.onmousemove = null;
	        document.onmouseup = null;
	    }
	</script>
    <style type="text/css">
        .popLayer {display:none; position:absolute; width:520px; z-index:10; padding:30px 30px 35px; margin-left:15px; background-color:#fff; border:1px solid #000;}
    </style>
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>
		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li class="active"><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="editForm" runat="server">
	
    <div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">개요부관리</h2>
			<div class="action">
				<!--<button type="button" class="btn btn-primary">전체보기</button>-->
			</div>
		</div><!-- // title -->

		<div class="row">

			<div class="col-md-6 code-manage" style="border: 1px groove;"><!-- col -->

				<div class="title"><!-- title -->
					<h3 class="title">개요부타입</h3>
					<div class="action">
                        <button type="button" class="btn btn-sm btn-add" onclick="javascript:showAddOutlineType()"></button>
                        <asp:Button type="button" class="btn btn-sm btn-success" runat="server" Text="저장" ID="btnSaveOutlineType" OnClick="btnSaveOutlineType_Click"></asp:Button>
					</div>
				</div><!-- // title -->

				<div class="code-list"><!-- list -->
                    <%--<asp:table class="table" ID="OutlineTypeTable" runat="server"></asp:table>--%>
                    <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                        <asp:Repeater ID="OutlineTypeListRepeater" runat="server" OnItemCommand="OutlineTypeListRepeater_ItemCommand">
                            <HeaderTemplate>
                                <tr>
                                    <th>개요부타입명</th>
                                    <th>조회</th>
                                    <th>삭제</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HiddenField ID="OutlineID" runat="server" Value=<%#DataBinder.Eval(Container.DataItem , "OutlineType")%> />
                                        <asp:TextBox runat="server" class="large" Height="30px" BorderWidth="0" ID="txtOutlineTypeName" Value='<%# DataBinder.Eval(Container.DataItem , "OutlineTypeName") %>' />
                                    </td>
                                    <td><asp:Button runat="server" cssClass="btn-modify" BorderWidth="0" CommandName="RptEvent" CommandArgument="G" /></td>
                                    <td><asp:Button runat="server" cssClass="btn-code-del" BorderWidth="0" OnClientClick="javascript:return confirm('삭제하시겠습니까?');" CommandName="RptEvent" CommandArgument="D" /></td>
                                </tr> 
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
				</div><!-- // list -->
			</div><!-- col -->

			<div class="col-md-6 code-manage" style="border: 1px groove"><!-- col -->

			    <div class="title"><!-- title -->
				    <h3 class="title">
                        <asp:Label ID="selectedOutlineTypeName" runat="server"></asp:Label> 상세항목
                        <asp:HiddenField ID="selectedOutlineType" runat="server" />
				    </h3>
				    <div class="action">
					    <button type="button" class="btn btn-sm btn-add" onclick="javascript:showAddOutline()"></button>
                        <asp:Button type="button" class="btn btn-sm btn-success" runat="server" Text="저장" ID="btnSaveOutline" OnClick="btnSaveOutline_Click"></asp:Button>
				    </div>
			    </div><!-- // title -->

			    <div class="code-list row"><!-- list -->
                    <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                        <asp:Repeater ID="OutlineTableRepeater" runat="server" OnItemCommand="OutlineTableRepeater_ItemCommand" >
                            <HeaderTemplate>
                                <tr>
                                    <th></th>
                                    <!--<th>항목코드</th>-->
                                    <th>항목명</th>
                                    <th>삭제</th>
                                </tr>
                            <% if (OutlineTableRepeater.Items.Count == 0) { %>
                                <tr>
                                    <td colspan="3" style="text-align:center">등록된 항목이 없습니다.</td>
                                </tr>                    
                            <%  }  %>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><asp:HiddenField ID="OutlineIdx" runat="server" Value='<%#DataBinder.Eval(Container.DataItem , "Idx")%>' /></td>
                                    <!--<td><asp:Label ID="OutlineID" runat="server" Text=<%#DataBinder.Eval(Container.DataItem , "OutlineName")%> /></td>-->
                                    <td><asp:TextBox ID="OutlineName" class="large" Height="30px" BorderWidth="0" Text=<%#DataBinder.Eval(Container.DataItem , "OutlineName")%> runat="server"/></td>
                                    <td align="center"><asp:Button runat="server" cssClass="btn-code-del" BorderWidth="0" OnClientClick="javascript:return confirm('삭제하시겠습니까?')"/></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>

                <!--div addOutlineType start -->
                <div id="addOutlineType" class="popLayer" onmousedown="startDrag(event, addOutlineType);"  draggable="true" style="cursor:pointer; cursor:hand" border="0">
                    <div class="title"><!-- title -->
					    <h3 class="title">개요부타입추가</h3>
				        <div class="action">
					        <button type="button" class="btn btn-sm btn-close" onclick="javascript:hideAddOutlineType()"></button>
				        </div>
				    </div><!-- // title -->
                    <div class="table-responsive">
		                <table class="table">
			                <tbody>
				                <tr>
				                    <td>개요부타입</td>
				                    <td>
				                        <div class="input-group" onmousedown="txtOutlineType.focus();">
    					                    <asp:TextBox Runat="server" Width="200" ID="txtOutlineType" class="form-control" aria-describedby="basic-addon2"></asp:TextBox>
					                    </div>
				                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label id="msgIDCheck" runat="server"></label></td>                                    
                                </tr>
                            </tbody>
                        </table>
                        <asp:Button ID="AddButton" class="btn btn-success" style="float: left; padding: 10px 90px;" runat="server" text="추가" onclick="AddButton_Click"></asp:Button> 
                    </div>
                </div>
                <!--- div addOutlineType end  -->

                    <!--div addOutline start -->
                    <div id="addOutline" class="popLayer" draggable="true" style="position:absolute; top:50px; right:50px; cursor:pointer; cursor:hand" border="0">
                        <div class="title" onmousedown="startDrag(event, addOutline);"><!-- title -->
				            <h3 class="title">항목추가</h3>
				            <div class="action">
					            <button type="button" class="btn btn-sm btn-close" onclick="javascript:hideAddOutline()"></button>
				            </div>
			            </div><!-- // title -->
                        <div class="table-responsive">
		                    <table class="table">
			                    <tbody>
				                    <tr>
				                        <td>개요부타입</td>
				                        <td>
				                            <div class="input-group" >
                                                <asp:DropDownList class="form-control" ID="ddlOutlineType" runat="server" aria-describedby="basic-addon2">
                						            </asp:DropDownList>
					                        </div>
				                        </td>
                                    </tr>
				                    <tr>
				                        <td>항목명</td>
				                        <td>
				                            <div class="input-group" onmousedown="txtOutlineName.focus();">
    					                        <asp:TextBox Runat="server" Width="100" ID="txtOutlineName" class="form-control" aria-describedby="basic-addon2" ></asp:TextBox>
					                        </div>
				                        </td>
                                    </tr>
                                    <!--
				                    <tr>
				                        <td>항목코드</td>
				                        <td>
				                            <div class="input-group" onmousedown="txtOutline.focus();">
    					                        <asp:TextBox Runat="server" Width="100" ID="txtOutline" class="form-control" aria-describedby="basic-addon2"></asp:TextBox>
                                                <br /><label id="msgIDCheck2" runat="server"></label>
					                        </div>
				                        </td>
                                    </tr>
                                    //-->
                                </tbody>
                            </table>
                            <asp:Button ID="btnOutlineAdd" class="btn btn-success" style="float: left; padding: 10px 90px;" runat="server" text="추가" OnClick="btnOutlineAdd_Click"></asp:Button> 
                        </div>
                    </div>
                    <!--- div addOutline end  -->


			    </div><!-- // list -->

			</div><!-- col -->

		</div>

	</div><!-- // contents -->

    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>




</div><!-- // container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>


 
