﻿using System;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.IO;

public partial class notice_Add : System.Web.UI.Page
{
    //
    // 공지사항 등록
    // 필수 기본값 
    //
    private string connectionString = string.Empty;
    
    private string pageNo = string.Empty;
    private string pageSize = string.Empty;

    private bool _refreshState;
    private bool _isRefresh;

    public bool IsRefresh
    {
        get { return _isRefresh; }
    }
    protected override void LoadViewState(object savedState)
    {
        object[] allStates = (object[])savedState;
        base.LoadViewState(allStates[0]);
        _refreshState = (bool)allStates[1];
        _isRefresh = _refreshState == (bool)Session["__ISREFRESH"];
    }
    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] allStates = new object[2];
        allStates[0] = base.SaveViewState();
        allStates[1] = !_refreshState;
        return allStates;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  

        if (Request.Params["pageNo"] != null)
            pageNo = Request.Params["pageNo"].ToString();
        if (Request.Params["pageSize"] != null)
            pageSize = Request.Params["pageSize"].ToString();

    }
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        if (!IsRefresh)
        {
            string strFileName = string.Empty;
            string strFilePath = string.Empty;

            //0.파일이 있는지 확인
            if ((null == fuFile.PostedFile) || (0 >= fuFile.PostedFile.ContentLength))
            {
                //파일이 선택되지 않았다.
                //Response.Write("파일을 선택해 주세요");
                //return;
            }
            else
            {
                //1.파일을 서버에 업로드 한다.
                
                //1-1.업로드
                //업로드될 dir경로를 만든다.
                //string sFileUri = HttpContext.Current.Server.MapPath("~/")
                ////    fileName = "Notice_" + DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(fuFile.FileName);
                ////    filePath += fileName;

                FileInfo fi = new FileInfo(fuFile.PostedFile.FileName);
                strFileName = "Notice_" + DateTime.Now.ToString("yyyyMMddhhmmss") + fi.Extension;
                strFilePath = HttpContext.Current.Server.MapPath("~/cms100data/Notice/") + strFileName;
                                  //+ string.Format(@"\{0}", fuFile.PostedFile.FileName);
                //이미 같은 이름의 파일이 있으면 지워준다.
                File.Delete(strFilePath);

                try
                {
                    //파일 업로드
                    fuFile.PostedFile.SaveAs(strFilePath);
                }
                catch (Exception ex)
                {
                    //오류다!
                    Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "
                                    + ex.ToString());
                    return;
                }
            }
            
            
            DBFileInfo(); 
            SqlConnection Con = new SqlConnection(connectionString);

            SqlCommand Cmd = new SqlCommand("USP_NOTICE_INSERT", Con);
            Cmd.CommandType = CommandType.StoredProcedure;

            Cmd.Parameters.Add("@title", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@content", SqlDbType.Text);
            Cmd.Parameters.Add("@fileName", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@fileLink", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@writerIdx", SqlDbType.Int);
            
            Cmd.Parameters["@title"].Value = Server.HtmlEncode(txtTitle.Text);
            Cmd.Parameters["@content"].Value = Server.HtmlEncode(txtContent.Text);
            Cmd.Parameters["@fileName"].Value = Server.HtmlEncode(strFileName);
            Cmd.Parameters["@fileLink"].Value = "";
            if (fuFile.PostedFile.FileName.Length > 2)
            {
                Cmd.Parameters["@fileLink"].Value = "/cms100data/Notice/" +strFileName;
            }
            Cmd.Parameters["@writerIdx"].Value = Session["uidx"];
            
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            Cmd = null;
            Con = null;

            if (Request.Params["pageNo"] != null)
                pageNo = Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();

            string script = "<script>alert('저장되었습니다');location.href='NoticeList.aspx?pageNo=" + pageNo + "&pageSize=" + pageSize + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP=="::1")  //-- 테스트 서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else            //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
}
