﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Net;
using System.IO;

public partial class notice_View : System.Web.UI.Page
{
    //
    // 공지사항 조회
    // 필수값 
    // 옵션값 
    //
    private string connectionString = string.Empty;

    private string idx = string.Empty;
    private string pageNo = string.Empty;
    private string pageSize = string.Empty;

    private bool _refreshState;
    private bool _isRefresh;

    public bool IsRefresh
    {
        get { return _isRefresh; }
    }
    protected override void LoadViewState(object savedState)
    {
        object[] allStates = (object[])savedState;
        base.LoadViewState(allStates[0]);
        _refreshState = (bool)allStates[1];
        _isRefresh = _refreshState == (bool)Session["__ISREFRESH"];
    }
    protected override object SaveViewState()
    {
        Session["__ISREFRESH"] = _refreshState;
        object[] allStates = new object[2];
        allStates[0] = base.SaveViewState();
        allStates[1] = !_refreshState;
        return allStates;
    }    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  
        
        // 초기화
        //if (!IsPostBack)
        //{
            if (Request.Params["idx"] != null)
                idx = Request.Params["idx"].ToString();
            if (Request.Params["pageNo"] != null)
                pageNo = Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();
            if (idx == string.Empty)
            {
                Response.Redirect("NoticeList.aspx");
                Response.End();
            }
            getContent();
        //}
    }
    private void getContent()
    {
        // DB Connection String, File Path 설정
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@idx", SqlDbType.Int);
        Cmd.Parameters["@idx"].Value = idx;

        try
        {
            Con.Open();

            //레코드를 읽어오는 프로시저로 지정
            Cmd.CommandText = "USP_Notice_SELECT";

            SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (reader.Read())
            {
                txtTitle.Text = Server.HtmlDecode(reader["Title"].ToString());
                txtContent.Text = Server.HtmlDecode(reader["Contents"].ToString());
                lblFileName.Text = "<a target=_blank href=" + reader["FileLink"].ToString() + ">" + reader["FileName"].ToString() + "</a>";
            }
            else
            {
                string script = "<script>alert('글이 존재하지 않습니다'); history.back();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script); 
            }

            reader.Close();
        }
        catch (Exception ex)
        {
            lblError.Text = "ERROR : " + ex.Source + " - " + ex.Message;
            lblError.Visible = true;
        }

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

    }
    
    protected void SaveButton_Click(object sender, EventArgs e)
    {
        if (!IsRefresh)
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);

            SqlCommand Cmd = new SqlCommand("USP_NOTICE_UPDATE", Con);
            Cmd.CommandType = CommandType.StoredProcedure;

            Cmd.Parameters.Add("@idx", SqlDbType.Int);
            Cmd.Parameters.Add("@title", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@content", SqlDbType.Text);
            Cmd.Parameters.Add("@fileName", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@fileLink", SqlDbType.VarChar, 255);
            Cmd.Parameters.Add("@writerIdx", SqlDbType.Int);

            idx = Request.Params["idx"].ToString();
            Cmd.Parameters["@idx"].Value = idx;
            Cmd.Parameters["@title"].Value = Request.Form["txtTitle"].ToString();
            Cmd.Parameters["@content"].Value = Request.Form["txtContent"].ToString();
            Cmd.Parameters["@fileName"].Value = lblFileName.Text;
            Cmd.Parameters["@fileLink"].Value = "/cms100data/Notice/"+ lblFileName.Text;
            if (fuFile.HasFile)
            {
                string uploadPath = @"/cms100data/Notice/";
                //string partPath = @"" + DateTime.Now.ToString("yyyyMM") + "/";
                //string filePath = uploadPath + partPath;
                ////해당 Sub 디렉토리가 있는지 확인하고 없으면 생성한다.
                //var mainDirectory = new DirectoryInfo(Server.MapPath(uploadPath));
                //var subDirectory = new DirectoryInfo(Server.MapPath(filePath));
                //if (!Directory.Exists(Server.MapPath(filePath)))
                //{
                //    mainDirectory.CreateSubdirectory(DateTime.Now.ToString("yyyyMM"));
                //}

                string fileName = string.Empty;
                string fileArr = string.Empty;

                fileName = "Notice_" + DateTime.Now.ToString("yyyyMMddhhmmss") + Path.GetExtension(fuFile.FileName);
                string filePath = uploadPath + fileName;

                fuFile.SaveAs(Server.MapPath(filePath)); 
                
                Cmd.Parameters["@fileName"].Value = Server.HtmlEncode(fileName);
                Cmd.Parameters["@fileLink"].Value = uploadPath + fileName;
            }
            Cmd.Parameters["@writerIdx"].Value = Session["uidx"];

            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

            Cmd = null;
            Con = null;

            string script = "<script>alert('저장되었습니다');location.href='NoticeView.aspx?Idx="+ idx +"&pageNo="+ pageNo +"&pageSize="+ pageSize +"';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        }
    }
    protected void DelButton_Click(object sender, EventArgs e)
    {
        DBFileInfo(); 
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand("USP_Notice_DELETE", Con);
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@idx", SqlDbType.Int);

        idx = Request.Params["idx"].ToString();
        Cmd.Parameters["@idx"].Value = idx;

        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;
        
        if (Request.Params["pageNo"] != null)
            pageNo = Request.Params["pageNo"].ToString();
        if (Request.Params["pageSize"] != null)
            pageSize = Request.Params["pageSize"].ToString();

        string script = "<script>alert('삭제되었습니다');location.href='NoticeList.aspx?pageNo=" + pageNo + "&pageSize=" + pageSize + "';</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else  //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void ListButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("NoticeList.aspx?pageNo=" + pageNo + "&pageSize=" + pageSize + "");
    }
}
