﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strCategory = string.Empty;    

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }
        
        if (!IsPostBack)
        {
            if (Request.Params["pageNo"] != null)
                pageNo =Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();
            
            Listing();
            BindUser();

        }

    }
    private void BindUser()
    {
        string strQuery = string.Empty;
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth == 1)
        {
            strQuery = "SELECT Idx, UserId, UserName FROM TUser WHERE Idx="+ Session["uidx"].ToString() +" AND DelFlag=0 ORDER BY UserName ASC";
        }
        else
        {
            strQuery = "SELECT Idx, UserId, UserName FROM TUser WHERE UserTypeIdx=1 AND DelFlag=0 ORDER BY UserName ASC";
            ListItem li = new ListItem("선택", "0");
            ddlUser.Items.Add(li);
            ddlUser.AppendDataBoundItems = true;
        }
        //##### 권한 처리 끝  

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "TUser");
        //Set up the data binding. 
        Con.Close();
        DataTable dtUser = ds.Tables["TUser"];
        ddlUser.DataSource = dtUser;
        ddlUser.DataTextField = "UserName";
        ddlUser.DataValueField = "Idx";
        ddlUser.DataBind();
        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtUser = null;

        ddlUser.AppendDataBoundItems = false;
    }
    private void Listing()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_EntryAdd_LIST_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@dateType", SqlDbType.Char);
        Cmd.Parameters.Add("@startDate", SqlDbType.Date);
        Cmd.Parameters.Add("@endDate", SqlDbType.Date);
        Cmd.Parameters.Add("@userIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@taskIDX", SqlDbType.Int);
        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        string dateType = ddlDateType.SelectedValue;
        if (txtStartDate.Text.Length > 0 )
            Cmd.Parameters["@startDate"].Value = Convert.ToDateTime(txtStartDate.Text);
        if (txtEndDate.Text.Length > 0) 
            Cmd.Parameters["@endDate"].Value = Convert.ToDateTime(txtEndDate.Text);
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth == 1)   // 작업자이면 자신에게 할당된 목록만 조회
        {
            Cmd.Parameters["@userIdx"].Value = Session["uidx"].ToString();
        }
        else   // 검수자, 관리자는 모두 조회 가능
        {
            if (ddlUser.SelectedValue.ToString().Length > 0)
                Cmd.Parameters["@userIdx"].Value = ddlUser.SelectedValue;
            else
                Cmd.Parameters["@userIdx"].Value = 0;
        }
        //##### 권한 처리 끝  
        if (ddlTask.SelectedValue.ToString().Length > 0 )
            Cmd.Parameters["@taskIdx"].Value = Convert.ToInt32(ddlTask.SelectedValue);
        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);
        if ( txtStartDate.Text.Length==0 || txtEndDate.Text.Length==0)
            dateType = "A";
        Cmd.Parameters["@dateType"].Value = dateType;

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        Response.Write("총페이지:" + totalPage + "총갯수" + pageNo);
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);

        lblTotalCnt.Text = totalCnt + "건의 검색결과가 있습니다.";
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        { 
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 작업자
            Label lblUser = item.FindControl("lblUser") as Label;
            lblUser.Text = DataBinder.Eval(item.DataItem, "UserName").ToString();
            // 검수자
            Label lblChecker = item.FindControl("lblChecker") as Label;
            lblChecker.Text = DataBinder.Eval(item.DataItem, "CheckerName").ToString();
            // 관리자
            Label lblManager = item.FindControl("lblManager") as Label;
            lblManager.Text = DataBinder.Eval(item.DataItem, "ManagerName").ToString();
            // 엔트리수
            Label lblEntryCount = item.FindControl("lblEntryCount") as Label;
            lblEntryCount.Text = DataBinder.Eval(item.DataItem, "EntryCount").ToString();
            // 작업배포일
            Label lblPublishDate = item.FindControl("lblPublishDate") as Label;
            lblPublishDate.Text = DataBinder.Eval(item.DataItem, "PublishDate").ToString();
            // 작업완료일
            Label lblCheckDate = item.FindControl("lblCheckDate") as Label;
            lblCheckDate.Text = DataBinder.Eval(item.DataItem, "CheckDate").ToString();
            // 상태
            Label lblStatus = item.FindControl("lblStatus") as Label;
            int intStatus = Convert.ToInt32(DataBinder.Eval(item.DataItem, "Status").ToString());
            string strStatus = string.Empty;
            if (intStatus == 1 || intStatus == 2)
            {
                strStatus = "작업중";
            }
            else if (intStatus == 3)
            {
                strStatus = "검수요청";
            }
            else if (intStatus > 4)
            {
                strStatus = "검수완료";
            }

            lblStatus.Text = strStatus;
            // 작업명과 상세보기 링크를 설정한다.
            HyperLink lblTitle = item.FindControl("lblTitle") as HyperLink;
            lblTitle.Text = DataBinder.Eval(item.DataItem, "TaskTitle").ToString();
            lblTitle.NavigateUrl = string.Format("EntryAdd.aspx?Idx={0}&pageNo={1}&pageSize={2}", DataBinder.Eval(item.DataItem, "Idx"), pageNo, pageSize);

            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
        }
    }

    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void SearchButton_Click(object sender, EventArgs e)
    {
        Listing();
    }

    protected void TaskSearch_Click(object sender, EventArgs e)
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        string strQuery = string.Empty;
        if (ddlUser.SelectedValue=="0" || ddlUser.SelectedValue.Length == 0)
        {
            strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE DelFlag=0 AND Status>0 ORDER BY TaskTitle ASC";
        }
        else
        {
            strQuery = "SELECT Idx, TaskTitle FROM TTaskID WHERE UserIdx=" + ddlUser.SelectedValue
                    + " AND DelFlag=0 AND Status>0  ORDER BY TaskTitle ASC";
        }
        
        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TTaskID");
        //Set up the data binding. 
        Conn.Close();

        ListItem li = new ListItem("선택", "0");
        ddlTask.Items.Clear();
        ddlTask.Items.Add(li);
        ddlTask.AppendDataBoundItems = true;
        
        DataTable dtTask = ds.Tables["TTaskID"];
        ddlTask.DataSource = dtTask;
        ddlTask.DataTextField = "TaskTitle";
        ddlTask.DataValueField = "Idx";
        ddlTask.DataBind();

        ddlTask.AppendDataBoundItems = false;

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        sda = null;
        Conn = null;
    }
}
