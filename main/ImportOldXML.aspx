﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImportOldXML.aspx.cs" Inherits="XmlImport2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>


<body>

<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title title-success">Import</h2>
		</div><!-- // title -->
        <asp:Label id="lblError" runat="server" CssClass="form-control" ForeColor="Red" Visible="false"></asp:Label>
		<div class="title"><!-- title -->
			<h3 class="title title-success">파일</h3>
		</div><!-- // title -->
		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>작업명</th>
				    <td>
                        <asp:Label ID="lblTaskTitle" runat="server" ></asp:Label>
				    </td>
			    </tr>
			    <tr>
				    <th>XML파일</th>
				    <td>
                        <asp:FileUpload ID="XMLFileUpload" runat="server" /><asp:Button ID="btnUploadExcel" Text="Upload" runat="server" OnClick="btnUploadExcel_Click" />
				    </td>
			    </tr>
			    <tr>
				    <th>유효성검증</th>
				    <td>
                        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
				    </td>
			    </tr>
			    <tr>
				    <th>이미지파일</th>
				    <td>
                        <asp:FileUpload ID="ImageFileUpload" runat="server" /><asp:Button ID="btnUploadImage" Text="Upload" runat="server" OnClick="btnUploadImage_Click" />
				    </td>
			    </tr>
		    </tbody>
		</table><!-- // table-a -->
        
		<div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="btnImport" Text="Data Import" cssClass="btn btn-lg btn-success" OnClick="btnImport_Click"></asp:Button>
		</div><!-- // section-button -->

		<div class="title"><!-- title -->
			<h3 class="title title-primary">Import Result</h3>
		</div><!-- // title -->
        <div id="divResult">
            <table border="0" cellpadding="0" cellspacing="0" class="table table-list"><!-- table-a -->
                <asp:Repeater ID="EntryList" runat="server" >
                    <HeaderTemplate>
			            <tr>
				            <th>No</th>
				            <th>Entry No</th>
				            <th>Title</th>
			            </tr>
    <%
        if (EntryList.Items.Count == 0)
        {
    %>
                        <tr>
                            <td colspan="3" style="text-align:center">엔트리가 등록되었습니다.</td>
                        </tr>                    
                        <tr>
                            <td colspan="3" style="text-align:center">
		                        <div class="section-button"><!-- section-button -->
			                        <a class="btn btn-lg btn-success" onclick="javascipt:window.opener.location.reload();window.close();">확인</a>
		                        </div><!-- // section-button -->
                            </td>
                        </tr>                    
    <%
        }
    %>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" Text='<%#DataBinder.Eval(Container.DataItem , "No")%>' runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEntryIdx" Text='<%#DataBinder.Eval(Container.DataItem , "Idx")%>' runat="server" />
                            </td>
                            <td><asp:Label ID="lblTitle" Text='<%#DataBinder.Eval(Container.DataItem , "EntryTitle")%>'  runat="server" /></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
		    </table><!-- // table-a -->
    <%
        if (EntryList.Items.Count > 0)
        {
    %>
		    <div class="section-button"><!-- section-button -->
			    <a class="btn btn-lg btn-success" onclick="javascipt:window.opener.location.reload();window.close();">확인</a>
		    </div><!-- // section-button -->
    <%
        }
    %>
        </div>
	</div><!-- // contents -->
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>
    </form>

</div><!-- // container -->

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/jquery-1.4.1.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/jquery.js"></script>

</body>
</html>


