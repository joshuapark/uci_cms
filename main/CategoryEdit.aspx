﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="CategoryEdit.aspx.cs" Inherits="main_opencontents" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>
<div class="container">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">엔트리 분류 수정</h3>
		</div><!-- // title -->

    <asp:HiddenField ID="recType" runat="server" />


		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
			<tr>
				<th>개정</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
					</div>
				</td>
<%--				<th>과목군</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSubjectGroup" runat="server"></asp:DropDownList>					
					</div>
				</td>				--%>
                <th>과목</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>					
					</div>
				</td>
                <th>브랜드</th>
				<td colspan="3">
					<div class="de-select" id="divBrand">
                        <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
					</div>
				</td>
            </tr>
            <tr>
<%--                <th>학교군</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSchool" runat="server"></asp:DropDownList>					
					</div>
				</td>--%>
                <th onclick="$('#divGrade').toggle();">학년</th>
				<td>
					<div class="de-select" id="divGrade">
                        <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
					</div>			
				</td>
				<th onclick="$('#divSemester').toggle();">학기</th>
				<td>
					<div class="de-select" id="divSemester">
                        <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
					</div>		
				</td>
				<th onclick="$('#divLUnit').toggle();">대단원</th>
                <td>
					<asp:Button runat="server" ID="LUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="LUnitSearch_Click" />
                    <div class="de-select" id="divLUnit">
                        <asp:DropDownList class="de-select" ID="ddlLUnit" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th onclick="$('#divMUnit').toggle();">중단원</th>
                <td>
					<asp:Button runat="server" ID="MUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="MUnitSearch_Click" />
                    <div class="de-select" id="divMUnit">
                        <asp:DropDownList class="de-select" ID="ddlMUnit" runat="server"></asp:DropDownList>					
					</div>	
                </td>
			</tr>
		</tbody>
		</table><!-- // table-a -->
        <asp:HiddenField ID="txtEntryIdx" runat="server" />
		<div class="section-button"><!-- section-button -->
            <asp:Button ID="btnSave" cssClass="btn btn-lg btn-success" runat="server" OnClientClick="if (!confirm('수정하시겠습니까?')) return false;" OnClick="btnSave_Click" Text ="저장"></asp:Button>
		</div><!-- // section-button -->

	</div><!-- // contents -->
    
    </form>

</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>  
</body>
</html>