﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="XmlImport2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>


<body>

<div class="container" style="width: 700px; height: 400px; margin-left: 0px; margin-right: 0px;">
	
    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">작업보고</h3>
		</div><!-- // title -->
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: 150px;">
			<col style="width: auto;">
			<col style="width: 150px;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
            <tr>
				<th>작업명</th>
                <td colspan="3">
                    <div class="de-select" id="divTask">
                        <asp:DropDownList class="de-select" ID="ddlTask" runat="server"></asp:DropDownList>					
                        <asp:RequiredFieldValidator ID="rfvDdlTask" InitialValue="0" runat="server" ForeColor="OrangeRed" ErrorMessage="* 작업을 선택하세요." ControlToValidate="ddlTask"></asp:RequiredFieldValidator>
					</div>	
                </td>
            </tr>
            <tr>
				<th>Date</th>
				<td>
                    <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox><br />(예:2015-07-31)
                    <asp:RequiredFieldValidator ID="rfvTxtDate" runat="server" ForeColor="OrangeRed" ErrorMessage="* 날짜를 입력하세요." ControlToValidate="txtDate"></asp:RequiredFieldValidator>
				</td>
				<th>작업수량</th>
                <td>
                    <asp:TextBox ID="txtCount" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvTxtCount" runat="server" ForeColor="OrangeRed" ErrorMessage="* 작업수량을 입력하세요." ControlToValidate="txtDate"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revTxtCount" runat="server" ControlToValidate="txtCount" ForeColor="OrangeRed"
                        ErrorMessage="숫자만 입력해주세요" ValidationExpression="[0-9]{1,20}" SetFocusOnError="True"></asp:RegularExpressionValidator>

                </td>
            </tr>
            <tr>
				<th colspan="4">작업내용 및 특이사항</th>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:TextBox ID="txtContents" TextMode="MultiLine" runat="server" Columns="80" Rows="4"></asp:TextBox>
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

        <div class="section-button"><!-- section-button -->
			<asp:Button ID="btnReport" cssClass="btn btn-lg btn-success" runat="server" OnClick="btnReport_Click" Text="보고완료"></asp:Button>
		</div><!-- // section-button -->
	</div><!-- // contents -->
    </form>

</div><!-- // container -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/jquery-1.4.1.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.js"></script>
<link rel="stylesheet" href="../css/jquery-ui.css" type="text/css" media="all" />
<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui-1.8.18.min.js" type="text/javascript"></script>
<script>
    $(function () {
        var dates = $("#<%=txtDate.ClientID%>").datepicker({
            prevText: '이전 달',
            nextText: '다음 달',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
            dateFormat: 'yy-mm-dd',
            showMonthAfterYear: true,
            yearSuffix: '년',
        });
    });
</script>
</body>
</html>


