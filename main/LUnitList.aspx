﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="LUnitList.aspx.cs" Inherits="notice_List"%>
<%@ Import Namespace="System.Data" %>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">  
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">대단원관리</h2>
		</div><!-- // title -->	

		<table border="0" id="LUnitTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
		<tbody>
			<tr>
				<th>개정</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvDdlRevision" runat="server" ForeColor="Red" ControlToValidate="ddlRevision" InitialValue="0" ErrorMessage="* 선택" ></asp:RequiredFieldValidator>
					</div>
				</td>
				<th>과목</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>					
			            <asp:RequiredFieldValidator ID="rfvDdlSubject" runat="server" ForeColor="Red" ControlToValidate="ddlSubject" InitialValue="0" ErrorMessage="* 선택" ></asp:RequiredFieldValidator>
					</div>
				</td>
                <th>브랜드</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
			            <asp:RequiredFieldValidator ID="rfvDdlBrand" runat="server" ForeColor="Red" ControlToValidate="ddlBrand" InitialValue="0" ErrorMessage="* 선택" ></asp:RequiredFieldValidator>
					</div>
				</td>
            </tr>
            <tr>
                <th>학년</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
	    	            <asp:RequiredFieldValidator ID="rfvDdlGrade" runat="server" ForeColor="Red" ControlToValidate="ddlGrade" InitialValue="0" ErrorMessage="* 선택" ></asp:RequiredFieldValidator>
				    </div>			
				</td>
				<th>학기</th>
				<td colspan="3">
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
		 	            <asp:RequiredFieldValidator ID="rfvDdlSemester" runat="server" ForeColor="Red" ControlToValidate="ddlSemester" InitialValue="0" ErrorMessage="* 선택" ></asp:RequiredFieldValidator>
					</div>		
				</td>
			</tr>
		</tbody>
		</table><!-- // table-a -->

		<div class="section-button"><!-- section-button -->
            <asp:Button runat="server" ID="searchButton" class="btn btn-lg btn-success" text="조회" OnClick="searchButton_Click" />
		</div><!-- // section-button -->

		<div class="section-button" id="sectionBtn" runat="server"><!-- section-button -->
			<div class="pull-right">
                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalUserSave">대단원추가</button>
                <asp:Button runat="server" ID="SaveButton" class="btn btn-sm btn-danger" text="수정사항저장" OnClick="SaveButton_Click" />
			</div>
		</div><!-- // section-button -->

        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
            <asp:Repeater ID="LUnitList" runat="server" OnItemCommand="LUnit_ItemCommand">
                <HeaderTemplate>
                    <tr>
                        <th>No</th>
                        <th>대단원명</th>
                        <th>최종수정일</th>
                        <th>
<%
    if (usrAuth=="9")
    { 
%> 
                            삭제
<%
    }
%>
                        </th>
                    </tr>
<%
    if (LUnitList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="4" style="text-align:center">등록된 대단원이 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center"><asp:Label ID="LUnitID" Text=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat="server" /></td>
                        <td><asp:TextBox cssClass="large" ID="LUnitName" Text=<%#DataBinder.Eval(Container.DataItem , "LUnitName")%>  runat="server" /></td>
                        <td class="text-center"><asp:Label ID="EditDate" Text=<%#DataBinder.Eval(Container.DataItem , "EditDate")%> runat="server"/></td>
                        <td class="text-center">
<%
    if (usrAuth=="9")
    { 
%> 
                            <asp:Button runat="server" cssClass="btn btn-sm btn-code-del" />
<%
    }
%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

        <div class="modal fade" id="modalUserSave" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><!-- modal -->
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="reset();"><!-- close --></button>
						<h4 class="modal-title" id="myModalLabel">대단원추가</h4>
					</div>
					<div class="modal-body">

						<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
						<colgroup>
							<col style="width: 110px;">
							<col style="width: auto;">
						</colgroup>
						<tbody>
							<tr>
								<th>대단원명</th>
								<td>
									<asp:TextBox Runat="server" Width="200" ID="txtLUnitName" class="large" aria-describedby="basic-addon2"></asp:TextBox>
								</td>
							</tr>
                            <tr>
                                <td colspan="2"><label id="msgIDCheck" runat="server"></label></td>                                    
                            </tr>						</tbody>
						</table><!-- // table-a -->

						<div class="section-button mt20"><!-- section-button -->
							<asp:Button ID="AddButton" class="btn btn-lg btn-danger" style="float: left; padding: 10px 90px;" runat="server" text="추가" onclick="AddButton_Click"></asp:Button> 
						</div><!-- // section-button -->

					</div>
				</div>
			</div>
		</div><!-- // modal -->

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>