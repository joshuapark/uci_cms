﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderList.aspx.cs" Inherits="main_List" EnableEventValidation="true" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/bootstrap.css" rel="stylesheet" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript">
	    showAddBinder = function () {
	        $("#addBinder").show();
	        $("#addBinder").css("position", "absolute");
	        $("#addBinder").css("top", "130px");
	        $("#addBinder").css("left", "220px");
	    }
	    hideAddBinder = function () {
	        $("#addBinder").hide();
	    }

	    jQuery.fn.center = function () {
	        this.css("position", "static");
	        this.css("top", "300px");
	        this.css("left", "300px");
	        //this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
	        //this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
	        return this;
	    }

	    var img_L = 0;
	    var img_T = 0;
	    var targetObj;

	    function getLeft(o) {
	        return parseInt(o.style.left.replace('px', ''));
	    }
	    function getTop(o) {
	        return parseInt(o.style.top.replace('px', ''));
	    }

	    // Div 움직이기
	    function moveDrag(e) {
	        var e_obj = window.event ? window.event : e;
	        var dmvx = parseInt(e_obj.clientX + img_L);
	        var dmvy = parseInt(e_obj.clientY + img_T);
	        targetObj.style.left = dmvx + "px";
	        targetObj.style.top = dmvy + "px";
	        return false;
	    }

	    // 드래그 시작
	    function startDrag(e, obj) {
	        targetObj = obj;
	        var e_obj = window.event ? window.event : e;
	        img_L = getLeft(obj) - e_obj.clientX;
	        img_T = getTop(obj) - e_obj.clientY;

	        document.onmousemove = moveDrag;
	        document.onmouseup = stopDrag;
	        if (e_obj.preventDefault) e_obj.preventDefault();
	    }

	    // 드래그 멈추기
	    function stopDrag() {
	        document.onmousemove = null;
	        document.onmouseup = null;
	    }
	</script>
    <style type="text/css">
        .popLayer {display:none; position:absolute; width:600px; z-index:10; padding:30px 30px 35px; margin-left:15px; background-color:#fff; border:1px solid #000;}
    </style>
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>
		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2 active"><a href="BinderList.aspx">테마관리</a></li>
				<li class="nth-child-3"><a href="EntryAddList.aspx">엔트리관리</a></li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5"><a href="UserList.aspx">사용자관리</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="editForm" runat="server">
	
    <div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">테마관리</h2>
		</div><!-- // title -->
		<div class="row">
            <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <tbody>
			        <tr>
                        <td><asp:TextBox ID="txtKeyword" runat="server" CssClass="large"></asp:TextBox></td>
                        <td><asp:Button runat="server" ID="SearchButton" class="btn btn-sm btn-success" text="조회" OnClick="SearchButton_Click"/></td>
                    </tr>
		    </tbody>
		    </table><!-- // table-a -->
        </div>
		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <button type="button" class="btn btn-sm btn-danger" onclick="javascript:showAddBinder()">테마추가</button>
			</div>
		</div><!-- // section-button -->

		<div class="row">
            <asp:Label ID="lblTotalCount" runat="server"></asp:Label>
            <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                <asp:Repeater ID="BinderListRepeater" runat="server" OnItemCommand="BinderListRepeater_ItemCommand">
                    <HeaderTemplate>
                        <tr>
                            <th>No</th>
                            <th>테마명</th>
                            <th>테마설명</th>
                            <th>엔트리수</th>
                            <th>최종수정일</th>
                            <th>테마관리</th>
                            <th>저장</th>
                            <th>삭제</th>
                        </tr>
<% if(BinderListRepeater.Items.Count == 0)
   {
%>
                        <tr>
                            <td colspan="8" style="text-align:center">
                                등록된 테마가 없습니다.
                            </td>
                        </tr> 
<% } %>
                        </HeaderTemplate>
                    <ItemTemplate>
            <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem , "BinderIdx")%>' />
                            </td>
                            <td>
                                <asp:TextBox runat="server" class="large" BorderWidth="0" ID="txtBinderTitle" Value='<%# DataBinder.Eval(Container.DataItem , "BinderTitle") %>' />
                            </td>
                            <td>
                                <asp:TextBox runat="server" class="large" BorderWidth="0" ID="txtBinderMemo" Value='<%# DataBinder.Eval(Container.DataItem , "BinderMemo") %>' />
                            </td>
                            <td style="text-align:center;">
                                <asp:Label runat="server" ID="lblEntryCount" Text='<%# DataBinder.Eval(Container.DataItem , "EntryCount") %>' />
                            </td>
                            <td style="text-align:center;">
                                <asp:Label runat="server" ID="lblEditDate" Text='<%# DataBinder.Eval(Container.DataItem , "EditDate") %>' />
                            </td>
                            <td style="text-align:center;"><asp:Button runat="server" cssClass="btn-setup" BorderWidth="0" CommandName="RptEvent" CommandArgument="G" /></td>
                            <td style="text-align:center;"><asp:Button runat="server" cssClass="btn btn-sm btn-success" BorderWidth="0" CommandName="RptEvent" CommandArgument="S" Text="저장"/></td>
                            <td style="text-align:center;"><asp:Button runat="server" cssClass="btn-code-del" BorderWidth="0" OnClientClick="javascript:return confirm('테마에 저장된 관계정보와 엔트리정보가 모두 삭제됩니다.\n정말 삭제하시겠습니까?');" CommandName="RptEvent" CommandArgument="D" /></td>
                        </tr> 
                    </ItemTemplate>
                </asp:Repeater>
            </table>

                <!--div addBinder start -->
                <div id="addBinder" class="popLayer" onmousedown="startDrag(event, addBinder);"  draggable="true" style="cursor:pointer; cursor:hand" border="0">
                    <div class="title"><!-- title -->
					    <h3 class="title">테마추가</h3>
				        <div class="action">
					        <button type="button" class="btn btn-sm btn-close" onclick="javascript:hideAddBinder()"></button>
				        </div>
				    </div><!-- // title -->
                    <div class="table-responsive">
		                <table class="table">
			                <tbody>
				                <tr>
				                    <td>테마명</td>
				                    <td>
				                        <div class="input-group" onmousedown="txtAddBinderTitle.focus();">
    					                    <asp:TextBox Runat="server" Width="200" ID="txtAddBinderTitle" class="form-control" aria-describedby="basic-addon2"></asp:TextBox>
					                    </div>
				                    </td>
                                </tr>
				                <tr>
				                    <td>테마설명</td>
				                    <td>
				                        <div class="input-group" onmousedown="txtAddBinderMemo.focus();">
    					                    <asp:TextBox Runat="server" Width="400" ID="txtAddBinderMemo" class="form-control" aria-describedby="basic-addon2"></asp:TextBox>
					                    </div>
				                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"><label id="msgIDCheck" runat="server"></label></td>                                    
                                </tr>
                            </tbody>
                        </table>
                        <asp:Button ID="AddButton" class="btn btn-success" style="float: left; padding: 10px 90px;" runat="server" text="추가" onclick="AddButton_Click"></asp:Button> 
                    </div>
                </div>
                <!--- div addCodeType end  -->

	</div><!-- // contents -->

    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>




</div><!-- // container -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>


 
