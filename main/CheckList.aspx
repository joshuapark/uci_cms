﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="CheckList.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">테마관리</a></li>
				<li class="nth-child-3"><a href="EntryAddList.aspx">엔트리관리</a></li>
				<li class="nth-child-4 active"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5"><a href="UserList.aspx">사용자관리</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">검수목록</h2>
		</div><!-- // title -->	

		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: 110px;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
			<tr>
				<th>일자검색</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlDateType" runat="server">
                            <asp:ListItem Text="선택" Value="0" />
                            <asp:ListItem Text="검수요청일(작업완료일)" Value="P" />
                            <asp:ListItem Text="검수완료일" Value="E" />
                        </asp:DropDownList>
					</div>
				</td>
                <td>&nbsp;</td>
				<td colspan="2">
                    <asp:TextBox ID="txtStartDate" runat="server" ></asp:TextBox>~
                    <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
				</td>
				<th>상태</th>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlStatus" runat="server">
                            <asp:ListItem Text="선택" Value="0" />
                            <asp:ListItem Text="검수요청" Value="3" />
                            <asp:ListItem Text="검수완료" Value="7" />
                        </asp:DropDownList>
					</div>
				</td>
            </tr>
            <tr>
				<th onclick="$('#divUser').toggle();">작업자</th>
                <td>
                    <div class="de-select" id="divUser">
                        <asp:DropDownList class="de-select" ID="ddlUser" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th onclick="$('#divTask').toggle();">작업명</th>
                <td colspan="4">
                    <asp:Button runat="server" ID="TaskSearch" class="btn btn-sm btn-success" text="조회" OnClick="TaskSearch_Click" />
                    <div class="de-select" id="divTask">
                        <asp:DropDownList class="de-select" ID="ddlTask" runat="server"></asp:DropDownList>					
					</div>	
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

		<div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="SearchButton" class="btn btn-lg btn-success" text="조회" OnClick="SearchButton_Click"/>
		</div><!-- // section-button -->


		<table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <asp:ListView ID="LV_Notice" runat="server" ItemPlaceholderID="phItemList" 
                    OnItemDataBound="ListView_ItemDataBound" >
                    <LayoutTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>                            
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>작업명</th>
				            <th>작업자</th>
				            <th>검수자</th>
				            <th>관리자</th>
				            <th>배정엔트리수</th>
				            <th>최종엔트리수</th>
				            <th>검수요청일</th>
				            <th>검수완료일</th>
				            <th>상태</th>
                        </tr>
                        <asp:PlaceHolder ID="phItemList" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="lblTitle" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblUser" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblChecker" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblManager" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEntryCount" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblFinishCount" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblCheckDate" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblCompleteDate" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>   
                            <col/>                                                     
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>작업명</th>
				            <th>작업자</th>
				            <th>검수자</th>
				            <th>관리자</th>
				            <th>배정엔트리수</th>
				            <th>최종엔트리수</th>
				            <th>작업배포일</th>
				            <th>작업완료일</th>
				            <th>상태</th>
                        </tr>
                        <tr>
                            <td colspan="10">등록된 작업이 없습니다.</td>
                        </tr>
                    </EmptyDataTemplate>
            </asp:ListView>
        </table>

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<link rel="stylesheet" href="../css/jquery-ui.css" type="text/css" media="all" />
<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui-1.8.18.min.js" type="text/javascript"></script>
<script>
    $(function () {
        var dates = $("#<%=txtStartDate.ClientID%>, #<%=txtEndDate.ClientID%> ").datepicker({
            prevText: '이전 달',
            nextText: '다음 달',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
            dateFormat: 'yy-mm-dd',
            showMonthAfterYear: true,
            yearSuffix: '년',
            onSelect: function (selectedDate) {
                var option = this.id == "<%=txtStartDate.ClientID%>" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(
                    instance.settings.dateFormat ||
                    $.datepicker._defaults.dateFormat,
                    selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });
</script>
</body>
</html>