﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QueryAdd.aspx.cs" Inherits="main_NewQuery" %>


<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
      
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li class="active"><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="addForm" runat="server" method="post">
    <asp:HiddenField ID="idx" runat="server" Value="-1"/>
	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">Query등록</h2>
		</div><!-- // title -->	
        <div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a class="btn btn-sm btn-primary" href="QueryList.aspx" >Query관리</a> 
			</div>
		</div><!-- // section-button -->
		<div class="title"><!-- title -->
			<h3 class="title title-success">Query</h3>
		</div><!-- // title -->

		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		<tbody>
			<tr>
				<th>SELECT</th>
				<td>
                    <asp:TextBox TextMode="MultiLine" Rows="3" Columns="70" ID="txtSelect" runat="server" ></asp:TextBox>
				</td>
			</tr>
			<tr>
				<th>FROM</th>
				<td>
                    <asp:TextBox TextMode="MultiLine" Rows="3" Columns="70" ID="txtFrom" runat="server" ></asp:TextBox>
				</td>
			</tr>
            <tr>
				<th>Where</th>
				<td>
                    <asp:TextBox TextMode="MultiLine" Rows="3" Columns="70" ID="txtWhere" runat="server" ></asp:TextBox>
				</td>
			</tr>
          
		</tbody>
		</table><!-- // table-a -->

	<div class="section-button"><!-- section-button -->
        <asp:Button cssClass="btn btn-lg btn-success" id="btnExecuteQuery" OnClick="btnExecuteQuery_Click" runat="server" Text="Query 실행"></asp:Button>
        <a class="btn btn-lg btn-default" id="btnReset" onclick="addForm.reset();">초기화</a>
    </div><!-- section-button -->

    <div class="title"><!-- title -->
	    <h3 class="title title-success">결과</h3>
    </div><!-- // title -->
    <table border="0" cellpadding="0" cellspacing="0" class="table">
		<colgroup>
			<col style="width: 110px;">
			<col style="width: auto;">
		</colgroup>
		<tr>
			<th>실행쿼리</th>
			<td>
				<asp:Label runat="server" id="txtQueryText"   />
			</td>
		</tr>  
        <tr>
            <th>조회건수</th>
            <td>
                <asp:Label ID="lblResultCount" runat ="server"></asp:Label>
                <asp:Button ID="btnExportExcel" runat="server" Text="엑셀 다운로드" OnClick="btnExportExcel_Click" />
            </td>
        </tr>
        <tr>
            <th>Query명</th>
            <td>
                <asp:TextBox ID="txtQueryName" runat="server" Columns="70" TextMode="SingleLine" ></asp:TextBox>
            </td>
        </tr>
    </table>
    <div id="queryList" style="height:200px; overflow-y: scroll;">
        <asp:DataGrid id="ItemsGrid"
           BorderColor="darkgray"
           BorderWidth="1"
           CellPadding="5"
           AutoGenerateColumns="true"
           runat="server">

         <HeaderStyle BackColor="#c0c0c0">
         </HeaderStyle>

         <Columns>
         </Columns> 

      </asp:DataGrid>
    </div>
    
    <div class="section-button"><!-- section-button -->
        <asp:Button class="btn btn-lg btn-danger" ID="btnSave" runat="server" text="저장" onclick="btnSave_Click"/>
    </div><!-- section-button -->

	</div><!-- // contents -->

    </form>
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>