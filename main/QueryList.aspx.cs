﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;

public partial class main_QueryList : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    public string lblPage = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
            Session.Timeout = 120;
        }  
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 끝        

        //if (!IsPostBack)
        //{
        if (Request.Params["pageNo"] != null)
            pageNo = Request.Params["pageNo"].ToString();
        if (Request.Params["pageSize"] != null)
            pageSize = Request.Params["pageSize"].ToString();

        Listing();
        //}
    }

    private void Listing()
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Query_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@pageNo", SqlDbType.Int);
        Cmd.Parameters.Add("@pageSize", SqlDbType.Int);

        Cmd.Parameters["@pageNo"].Value = Convert.ToInt32(pageNo);
        Cmd.Parameters["@pageSize"].Value = Convert.ToInt32(pageSize);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "bbs_notice");

        LV_Notice.DataSource = ds;
        LV_Notice.DataBind();

        int totalPage = ((int)totalCnt - 1) / Convert.ToInt32(pageSize) + 1;
        MakePage(totalCnt, Convert.ToInt32(pageNo), totalPage);

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    protected void ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem item = (ListViewDataItem)e.Item;
        // 아이템의 타입이 DataItem일 경우
        if (item.ItemType == ListViewItemType.DataItem)
        {
            //Image imgGoods = item.FindControl("imgGoods") as Image;
            //imgGoods.ImageUrl = string.Format("~/files/images/{0}", DataBinder.Eval(item.DataItem, "ImageUrl"));

            // 글 번호를 설정한다.
            Label lblNo = item.FindControl("lblNo") as Label;
            lblNo.Text = DataBinder.Eval(item.DataItem, "Idx").ToString();

            // 쿼리명을 설정한다.
            Label lblQueryName = item.FindControl("lblQueryName") as Label;
            lblQueryName.Text = DataBinder.Eval(item.DataItem, "QueryName").ToString();

            // 엔트리수와 엑셀 다운로드 링크를 설정한다.
            Label lblCount = item.FindControl("lblCount") as Label;
            lblCount.Text = DataBinder.Eval(item.DataItem, "ResultCount").ToString();

            Button btnExecuteQuery = item.FindControl("btnExecuteQuery") as Button;
            
            btnExecuteQuery.CommandName = "EXECUTE";
            btnExecuteQuery.Text = "엑셀Down";
            btnExecuteQuery.ID = DataBinder.Eval(item.DataItem, "QueryText").ToString();
            btnExecuteQuery.OnClientClick = "javascript:return confirm('쿼리 결과를 엑셀로 다운 받으시겠습니까?')";
            btnExecuteQuery.Click += new EventHandler(this.ExecuteButton_Click);
            //hlCount.NavigateUrl = string.Format("UserView.aspx?Idx={0}&pageNo={1}&pageSize={2}", DataBinder.Eval(item.DataItem, "Idx"), pageNo, pageSize);

            // 쿼리를 설정한다.
            Label lblQuery = item.FindControl("lblQuery") as Label;
            lblQuery.Text = DataBinder.Eval(item.DataItem, "QueryText").ToString();

            // 등록일을 설정한다.
            Label lblLastDate = item.FindControl("lblLastDate") as Label;
            lblLastDate.Text = DataBinder.Eval(item.DataItem, "LastDate").ToString();

            totalCnt = Convert.ToInt32(DataBinder.Eval(item.DataItem, "totalCnt"));
            strTotalCnt.Text = totalCnt + "건의 검색결과가 있습니다.";
        }
    }

    protected void ExecuteButton_Click(object sender, EventArgs e)
    {
        try
        {
            string strQuery;
            Button clickedButton = (Button)sender;
            ListViewDataItem item = (ListViewDataItem)clickedButton.Parent;
            strQuery = clickedButton.ID;
            Label lblNo = (Label)item.FindControl("lblNo");
            Label lblCount = (Label)item.FindControl("lblCount");
            DataTable dt = executeQueryText(strQuery, lblNo.Text);
            String excelFileName = "Query_Export_" + System.DateTime.Now.ToString("yyyyMMddhhmm") + ".xls";
            this.exportExcel(dt, excelFileName);
            lblCount.Text = dt.Rows.Count.ToString();
            //Response.Redirect("QueryList.aspx");
            //Listing();
        }
        catch(Exception ex)
        {
            Response.Write(ex.ToString());
        }
        Response.Redirect("QueryList.aspx");
    }

    protected void AddButton_Click(object sender, EventArgs e)
    {
        Response.Redirect(string.Format("QueryAdd.aspx?pageNo={0}&pageSize={1}", pageNo, pageSize));
    }

    private void MakePage(int TotalRecord, int CurPage, int TotalPage)
    {
        //링크 문자열
        string Path = Request.ServerVariables["PATH_INFO"].ToString() + "?pageNo=";
        string addParam = "&pageSize=" + pageSize;
        //FromPage 페이지 네비게이션 시작 페이지 번호
        //Curpage 페이지 네비게이션 마지막 페이지 번호
        int FromPage, ToPage;
        FromPage = (int)((CurPage - 1) / 10) * 10 + 1;
        if (TotalPage > FromPage + 9)
        {
            ToPage = FromPage + 9;
        }
        else ToPage = TotalPage;
        string Pager = "";
        int i;

        //이전 10개 표시
        if ((int)((CurPage - 1) / 10) > 0)
        {
            Pager = Pager + "<li><a href='" + Path + (FromPage - 1).ToString() + addParam + "'><font size='2'>이전페이지</font></a></li>";
        }

        //페이지 네비게이션 표시
        for (i = FromPage; i <= ToPage; i++)
        {
            if (i == CurPage)
            {
                Pager += "<li class='active'><a>" + i.ToString() + "</a></li>";
            }
            else
            {
                Pager = Pager + "<li><a href='" + Path + i.ToString() + addParam + "'>" + i.ToString() + "</a></li>";
            }
        }

        //다음 10개 표시
        if (ToPage < TotalPage)
        {
            Pager = Pager + "<li><a href='" + Path + (ToPage + 1).ToString() + addParam + "'><font size='2'>다음페이지</font></a></li>";
        }

        //페이지 네비게이션 출력하기
        lblPage = Pager;
        //Prev, Next 버튼의 링크 구성하기
        //if (CurPage > 1)
        //    hlPagePrev.NavigateUrl = Path + (CurPage - 1).ToString();
        //if (CurPage < ToPage)
        //    hlPageNext.NavigateUrl = Path + (CurPage + 1).ToString();
    }


    private DataTable executeQueryText(String query, String queryIdx)
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(query, Con);
        Con.Open();

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);

        if (Con.State == ConnectionState.Open)
            Con.Close();
        Cmd = new SqlCommand("UPDATE TQuery SET ResultCount=" + dataTable.Rows.Count + " WHERE Idx=" + queryIdx + "", Con);
        Con.Open();
        Cmd.ExecuteNonQuery();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }

    private void exportExcel(DataTable dataTable, String excelFileName)
    {
        string attachment = "attachment; filename=" + excelFileName;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }           
}