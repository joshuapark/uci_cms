﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckEdit.aspx.cs" Inherits="main_EntryEdit2" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <script src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.2.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<link href="../css/editor.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	<link href="../css/jquery.simplecolorpicker.css" rel="stylesheet">
    <link href="css/preview_style.css" rel="stylesheet">
    
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script src="../js/ckeditor/ckeditor.js"></script>
</head>

<script>
    /** jquery.nestedSortable.js 내의 소스 참고 **/
   
    $(document).ready(function(){

        window.onload = function () {

            delaySecond();
            
        }

        $(".editor").nestedSortable({
            forcePlaceholderSize: true,
            handle: "div",
            placeholder:"editor_placeholder",
            items: "li",
            revert: 250,
            tabSize: 10,
            toleranceElement: "> div",
            maxLevels:2,						/* 움직이는 레벨 영역 지정 */
            protectRoot:"true",
            cancel: ".editor_textarea",

            isAllowed: function(item,parent){
                /** DRAG & DROP 허용 조건에 대해 넣어 줌: false이면 못들어감 **/

                return true;
            }
        });

        $('#toHIER').click(function(e){
            //컨텐츠를 어레이로 뽑아 냅니다. --> jquery.nestedSortable.js 안의 toHierarchy 참고

            hiered = $('ol.editor').nestedSortable('toHierarchy', {startDepthCount: 0});
            hiered = dump(hiered);

            d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(hiered);
        })

        $('#toArr').click(function(e){
            arraied = $('ol.editor').nestedSortable('toArray', {startDepthCount: 0});
            arraied = dump(arraied);

            d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(arraied);
        })


        $('#save').click(function (e) {
            
            var con = confirm('저장하시겠습니까?');
            if (!con) return;

            var edu = getSaveString();

            $('#saveString').val(JSON.stringify(edu));
            $('#addForm').submit();
        });


        $("#preView").click(function (e) {

            hiered = preview();

            d = window.open("about:blank", "_BLANK", "width=1000,height=1000,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(hiered);
        });

        //마크업 내에 
        // 컬러 피커 구동 ../js/jquery.simplecolorpicker.js
        $('select[name="colorpicker"]').simplecolorpicker({ picker: true });


        $('#addLUnit').click(function(){
            var IndexContentNumber = Number($("#indexContentNumber").val());
            addIndexContent(IndexContentNumber);

            var strIndexContentNumber = String(IndexContentNumber + 1);
            $("#indexContentNumber").val(strIndexContentNumber);
            $("#spanIndexContentNumber").text(strIndexContentNumber);
    });

        $('#addQuiz').click(function(){
            var QuizNumber = Number($("#QuizNumber").val());
            addQuiz(QuizNumber);

            var strQuizNumber = String(QuizNumber + 1);
            $("#QuizNumber").val(strQuizNumber);
            $("#spanQuizNumber").text(strQuizNumber);
        })

    });

    function getSaveString() {

        var edu = new Object();

        //대분류 만들기
        var lunitArray = new Array();
        $("ol[id='olLUnit']").children("li[id^='list']").each(function () {
            lunit = new Object();
            lunit.idx = $(this).val();
            lunit.rectype = $(this).children('#rectype').val();

            var secondArray = new Array();
            $(this).children().children("li[id^='list']").each(function () {
                second = new Object();
                second.idx = $(this).val();
                var rectype = $(this).children('#rectype').val();
                second.rectype = rectype;
                if (rectype == 'IMAGE' || rectype == 'MATRIX_TABLE' || rectype == 'SOUND' || rectype == 'PAIRSENTENCE' || rectype == 'VIDEO') {
                    var thirdArray = new Array();
                    $(this).children().children().children("li[id^='list']").each(function () {
                        var third = new Object();
                        third.idx = $(this).val();
                        third.rectype = $(this).children('#rectype').val();
                        third.content = $(this).children(".editor_textarea").html();
                        thirdArray.push(third);
                    });
                    second.content = JSON.stringify(thirdArray);
                } else {
                    second.content = $(this).children(".editor_textarea").html();
                }

                secondArray.push(second);
            });
            lunit.list = secondArray;
            lunitArray.push(lunit);
        });
        edu.lunit = lunitArray;

        //퀴즈 만들기
        var quizArray = new Array();
        $("ol[id='olQuiz']").children("li[id^='list']").each(function () {
            quiz = new Object();
            quiz.idx = $(this).val();
            quiz.rectype = $(this).children('#rectype').val();

            var secondArray = new Array();
            $(this).children().children().children("li[id^='list']").each(function () {
                second = new Object();
                second.idx = $(this).val();
                var rectype = $(this).children('#rectype').val();
                second.rectype = rectype;

                if (rectype == 'SOUND' || rectype == 'PAIRSENTENCE' || rectype == 'VIDEO') {
                    var thirdArray = new Array();
                    $(this).children().children().children("li[id^='list']").each(function () {
                        var third = new Object();
                        third.idx = $(this).val();
                        third.rectype = $(this).children('#rectype').val();
                        third.content = $(this).children(".editor_textarea").html();
                        thirdArray.push(third);
                    });
                    second.content = JSON.stringify(thirdArray);
                } else {
                    second.content = $(this).children(".editor_textarea").html();
                }

                secondArray.push(second);
            });

            quiz.list = secondArray;
            quizArray.push(quiz);
        });
        edu.quiz = quizArray;

        //태그 만들기
        $("ol[id='olTag']").children("li[id^='list']").each(function () {
            tag = new Object();
            tag.idx = $(this).val();
            tag.rectype = $(this).children('#rectype').val();
            tag.content = $(this).children(".editor_textarea").html();
            edu.tag = tag;
        });

        return edu;

    }


    function addIndexContent(IndexContentNumber){

        var recTypeSize = Number($("#allRectTypeCount").val());

        var html = "";
        html += '<li id="list_'+ recTypeSize +'" value="-1" >';
        html += '<input type="hidden" id="rectype" value="INDEXCONTENT" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#resultContent_INDEXCONTENT_' + IndexContentNumber + '\').toggle();">분류</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a>';
        html += '<span class="editor_control_container">';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'MAINTITLE\');" onmouseover="" style="cursor: pointer;">대제목 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'SUBTITLE\');" onmouseover="" style="cursor: pointer;">중제목 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BASIC\');" onmouseover="" style="cursor: pointer;">기본1 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BASIC2\');" onmouseover="" style="cursor: pointer;">기본2 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BOX\');" onmouseover="" style="cursor: pointer;">블럭 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'MATRIX_TABLE\');" onmouseover="" style="cursor: pointer;">표 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'IMAGE\');" onmouseover="" style="cursor: pointer;">이미지 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'ANNOTATION\');" onmouseover="" style="cursor: pointer;">주석 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'CHDIVIDE\');" onmouseover="" style="cursor: pointer;">한자분해 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'SOUND\');" onmouseover="" style="cursor: pointer;">사운드 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'PAIRSENTENCE\');" onmouseover="" style="cursor: pointer;">Pair Sentence </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'VIDEO\');" onmouseover="" style="cursor: pointer;">동영상 </span>';
        html += '</span>';
        html += '</div>';
        html += '<ol id="resultContent_INDEXCONTENT_' + IndexContentNumber + '"></ol></li>';

        $("#olLUnit").append(html);

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        
    }

    function addQuiz(QuizNumber){

        var recTypeSize = Number($("#allRectTypeCount").val());

        var html = "";
        html += '<li id="list_'+ recTypeSize +'" value="-1" >';
        html += '<input type="hidden" id="rectype" value="QUIZ" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#resultContent_QUIZ_' + QuizNumber + '\').toggle();">문제시작</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a>';
        html += '<span class="editor_control_container">';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'QUESTION\');" onmouseover="" style="cursor: pointer;">문제 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'EXAMPLE\');" onmouseover="" style="cursor: pointer;">보기 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'DISTRACTOR\');" onmouseover="" style="cursor: pointer;">선택지 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'ANSWER\');" onmouseover="" style="cursor: pointer;">정답 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'EXPLANATION\');" onmouseover="" style="cursor: pointer;">해설 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'LEVEL\');" onmouseover="" style="cursor: pointer;">난이도 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'SOUND\');" onmouseover="" style="cursor: pointer;">사운드 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'PAIRSENTENCE\');" onmouseover="" style="cursor: pointer;">Pair Sentence </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'VIDEO\');" onmouseover="" style="cursor: pointer;">동영상 </span>';
        html += '</span>';
        html += '</div>';
        html += '<ol id="resultContent_QUIZ_' + QuizNumber + '"></ol></li>';

        $("#olQuiz").append(html);

        $("#allRectTypeCount").val(String(recTypeSize + 1));
    }


    CKEDITOR.on('dialogDefinition', function (ev) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        // Check if the definition is from the dialog we're
        // interested in (the 'link' dialog).
        if (dialogName == 'link') {

            dialogDefinition.removeContents('upload');
            dialogDefinition.removeContents('advanced');
            dialogDefinition.removeContents('target');


            //dialogDefinition.getContents('info').get('protocol')['items'].splice(4, 1);
            //dialogDefinition.getContents('info').get('protocol')['items'].push("entry,entry@");

        } else if (dialogName == 'image') {

            dialogDefinition.removeContents('Link');
            dialogDefinition.removeContents('advanced');

            var infoTab = dialogDefinition.getContents('info');
            infoTab.remove('txtAlt');
            infoTab.remove('txtBorder');
            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            //infoTab.remove('cmbAlign');
            //infoTab.remove('txtUrl');

            //var txtUrl = infoTab.get('txtUrl');
            //txtUrl['width'] = '10px';
            //dialogDefinition.onShow = function () {
                // This code will open the Advanced tab.
            //    this.selectPage('Upload');
            //};

        }

    });

    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function preview() {

        var html="";

        //대분류 만들기
        $("ol[id='olLUnit']").children("li[id^='list']").each(function () {

            $(this).children().children("li[id^='list']").each(function () {
                var rectype = $(this).children('#rectype').val();
                if (rectype == 'IMAGE' || rectype == 'MATRIX_TABLE' || rectype == 'PAIRSENTENCE' || rectype == 'SOUND' || rectype == 'VIDEO')
                {

                    var extraInfo = "";
                    var rectypeContent = "";

                    $(this).children().children().children("li[id^='list']").each(function () {
                        var content = $(this).children(".editor_textarea").html();
                        var rectype = $(this).children('#rectype').val();
                        if (typeof content != "undefined") {
                            if (content != '<p><br></p>') {
                                if ( rectype == "") {
                                    rectypeContent = content;
                                } else {
                                    extraInfo += content;
                                }
                            }
                        }
                    });

                    html += "<div id=\"aaa\" style=\"width:100%;text-align:center\">";
                    html += "<div id=\"bbb\" style=\"margin:0 auto;\">";
                    html += rectypeContent;
                    if (extraInfo != "") {
                        html += extraInfo;
                    }
                    html += "</div></div></br>";


                } else {
                    html += $(this).children(".editor_textarea").html();
                    html += "<br/>";
                }


            });
        });

        //퀴즈 만들기
        $("ol[id='olQuiz']").children("li[id^='list']").each(function () {
            $(this).children().children("li[id^='list']").each(function () {
                var rectype = $(this).children('#rectype').val();
                if (rectype == 'IMAGE' || rectype == 'MATRIX_TABLE' || rectype=='PAIRSENTENCE' || rectype=='SOUND' || rectype == 'VIDEO') {

                    $(this).children().children().children("li[id^='list']").each(function () {
                        var content = $(this).children(".editor_textarea").html()
                        if (content != '<p><br></p>') {
                            //html += $(this).children(".editor_textarea").html() + "</br>";
                            html += content;
                            html += "<br/>";
                        }
                    });
                } else {
                    //html += $(this).children(".editor_textarea").html() + "</br>";
                    html += $(this).children(".editor_textarea").html();
                    html += "<br/>";
                }
            });
        });

        //태그 만들기
        $("ol[id='olTag']").children("li[id^='list']").each(function () {
            html += $(this).children(".editor_textarea").html() + "</br>";
        });

        return html;
    }

    function getRecTypeName(recType){
        var result = "";
        switch (recType) {

            case "INDEXCONTENT":
                result = "대분류";
                break;
            case "MAINTITLE":
                result = "대제목";
                break;
            case "SUBTITLE":
                result = "중제목";
                break;
            case "BOX":
                result="블럭";
                break;
            case "BASIC":
                result = "기본";
                break;
            case "BASIC2":
                result = "기본2";
                break;
            case "RELATEDSEARCH":
                result = "연관검색어";
                break;
            case "MATRIX_TABLE":
                result="표";
                break;
            case "ANNOTATION":
                result = "주석";
                break;
            case "IMAGE":
                result="이미지";
                break;
            case "TAG":
                result = "태그";
                break;
            case "QUIZ":
                result = "문제";
                break;
            case "INFOTABLE":
                result = "정보";
                break;
            case "QUESTION":
                result = "문제";
                break;
            case "EXAMPLE":
                result = "보기";
                break;
            case "DISTRACTOR":
                result = "선택지";
                break;
            case "EXPLANATION":
                result = "설명";
                break;
            case "ANSWER":
                result = "정답";
                break;
            case "LEVEL":
                result = "난이도";
                break;
            case "CHDIVIDE":
                result = "한자분해";
                break;
            case "SOUND":
                result = "사운드";
                break;
            case "PAIRSENTENCE":
                result = "Pair Sentence";
                break;
            case "VIDEO":
                result = "동영상";
                break;
        }

        return result;
    }

    function addContent(panelName, recType, content) {

        if (recType == "IMAGE") {
            addImageContent(panelName, recType, content);
            return;
        } else if (recType == "MATRIX_TABLE") {
            addTableContent(panelName, recType, content);
            return;
        } else if (recType == "SOUND") {
            addSoundContent(panelName, recType, content);
            return;
        } else if (recType == "PAIRSENTENCE") {
            addPairSentenceContent(panelName, recType, content);
            return;
        } else if (recType == "VIDEO") {
            addVideoContent(panelName, recType, content);
            return;
        }

        if (typeof content == "undefined") {
            content = "";
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move">' + getRecTypeName(recType) + '</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="editor_' + recTypeSize + '" name="' + recType + '" value="0" contenteditable="true">' + content + '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_' + recTypeSize + '\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        if (recType == 'MAINTITLE' || recType == 'SUBTITLE') {
            html += 'toolbar: [';
            html += '{ name: \'document\', items: [\'Image\'] },[\'CJFormula\'],[\'Sourcedialog\']]';
        } else if (recType == 'BASIC' || recType == "BASIC2" || recType == 'BOX' || recType == 'QUESTION' || recType == 'EXAMPLE' || recType == 'DISTRACTOR' || recType == 'EXPLANATION') {
            html += 'toolbar: [';
            html += '{ name: \'document\', items: [\'Table\', \'Image\',\'HorizontalRule\'] },';
            html += '[\'JustifyLeft\', \'JustifyCenter\', \'JustifyRight\'],';
            html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
            html += '[\'Footnote\'[,[\'CJFormula\'],]\'Sourcedialog\']]';
        } else if (recType == 'ANSWER') {
            html += 'toolbar: [';
            html += '{ name: \'document\', items: [\'Table\', \'Image\'] },';
            html += '[\'JustifyLeft\', \'JustifyCenter\', \'JustifyRight\'],';
            html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
            html += '[\'Footnote\'[,[\'CJFormula\'],]\'Sourcedialog\']]';
        } else if (recType == 'RELATEDSEARCH' || recType=='ANNOTATION') {
            html += 'toolbar: [[\'CJFormula\'],[\'Sourcedialog\']]';
        } else {
            html += 'toolbar: [[\'Sourcedialog\']]';
        }
        html += '});';
        html += '</';
        html += 'script>';
        html += '</li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_" + recTypeSize).trigger('focus');

    }

    function addPairSentenceContent(panelName, recType, content) {
        
        var pairSentenceObject;
        if (typeof content == "undefined") {
            content = "";
        }
        else {
            pairSentenceObject = $.parseJSON(content);
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#div_' + recTypeSize + '\').toggle();">Pair Sentence</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="div_' + recTypeSize + '"><ol><li id="list_SENTENCE" value="-2" draggable="false">';
        html += '<input type="hidden" id="rectype" value="SENTENCE" />';
        html += '<div><span class="glyphicon glyphicon-move">문장</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_PAIRSENTENCE' + recTypeSize + '0" contenteditable="true" >';
        if (typeof pairSentenceObject != "undefined") {
            if (typeof pairSentenceObject.sentence != "undefined") {
                html += pairSentenceObject.sentence;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_PAIRSENTENCE' + recTypeSize + '0\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';



        html += '<li id="list_TRANSLATION" value="-2">';
        html += '<input type="hidden" id="rectype" value="TRANSLATION" />';
        html += '<div><span class="glyphicon glyphicon-move">해석</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_PAIRSENTENCE' + recTypeSize + '1" contenteditable="true" >';
        if (typeof pairSentenceObject != "undefined") {
            if (typeof pairSentenceObject.translation != "undefined") {
                html += pairSentenceObject.translation;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_PAIRSENTENCE' + recTypeSize + '1\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_GRAMMAR" value="-2">';
        html += '<input type="hidden" id="rectype" value="GRAMMAR" />';
        html += '<div><span class="glyphicon glyphicon-move">문법</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_PAIRSENTENCE' + recTypeSize + '2" contenteditable="true" >';
        if (typeof pairSentenceObject != "undefined") {
            if (typeof pairSentenceObject.grammar != "undefined") {
                html += pairSentenceObject.grammar;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_PAIRSENTENCE' + recTypeSize + '2\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';


        html += '<li id="list_PAIRSENTENCE" value="-2">';
        html += '<input type="hidden" id="rectype" value="" />';
        html += '<div><span class="glyphicon glyphicon-move">오디오 파일</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_PAIRSENTENCE' + recTypeSize + '3" contenteditable="true" >';
        if (typeof pairSentenceObject != "undefined") {
            if (typeof pairSentenceObject.node != "undefined") {
                html += pairSentenceObject.node;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_PAIRSENTENCE' + recTypeSize + '3\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'sourcedialog,cjmedia\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'CJMedia\'] },';
        html += '[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li></ol></div></li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_PAIRSENTENCE" + recTypeSize + "3").trigger('focus');


    }

    function addSoundContent(panelName, recType, content) {

        var soundObejct;
        if (typeof content == "undefined") {
            content = "";
        }
        else {
            soundObejct = $.parseJSON(content);
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#div_' + recTypeSize + '\').toggle();">' + getRecTypeName(recType) + '</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="div_' + recTypeSize + '"><ol><li id="list_SENTENCE" value="-2" draggable="false">';
        html += '<input type="hidden" id="rectype" value="SENTENCE" />';
        html += '<div><span class="glyphicon glyphicon-move">문장</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_SOUND' + recTypeSize + '0" contenteditable="true" >';
        if (typeof soundObejct != "undefined") {
            if (typeof soundObejct.sentence != "undefined") {
                html += tableObject.sentence;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_SOUND' + recTypeSize + '0\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_IMAGE" value="-2">';
        html += '<input type="hidden" id="rectype" value="" />';
        html += '<div><span class="glyphicon glyphicon-move">오디오파일</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_SOUND' + recTypeSize + '1" contenteditable="true" >';
        if (typeof soundObejct != "undefined") {
            if (typeof soundObejct.node != "undefined") {
                html += soundObejct.node;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_SOUND' + recTypeSize + '1\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'sourcedialog,cjmedia\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'CJMedia\'] },';
        html += '[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li></ol></div></li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_SOUND" + recTypeSize + "1").trigger('focus');
    }

    function addVideoContent(panelName, recType, content) {

        var videoObejct;
        if (typeof content == "undefined") {
            content = "";
        }
        else {
            videoObejct = $.parseJSON(content);
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#div_' + recTypeSize + '\').toggle();">' + getRecTypeName(recType) + '</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="div_' + recTypeSize + '"><ol><li id="list_SENTENCE" value="-2" draggable="false">';
        html += '<input type="hidden" id="rectype" value="SENTENCE" />';
        html += '<div><span class="glyphicon glyphicon-move">문장</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_VIDEO' + recTypeSize + '0" contenteditable="true" >';
        if (typeof videoObejct != "undefined") {
            if (typeof videoObejct.sentence != "undefined") {
                html += videoObejct.sentence;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_VIDEO' + recTypeSize + '0\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_IMAGE" value="-2">';
        html += '<input type="hidden" id="rectype" value="" />';
        html += '<div><span class="glyphicon glyphicon-move">동영상 파일</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_VIDEO' + recTypeSize + '1" contenteditable="true" >';
        if (typeof videoObejct != "undefined") {
            if (typeof videoObejct.node != "undefined") {
                html += videoObejct.node;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_VIDEO' + recTypeSize + '1\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'sourcedialog,cjmedia\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'CJMedia\'] },';
        html += '[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li></ol></div></li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_VIDEO" + recTypeSize + "1").trigger('focus');
    }


    function addImageContent(panelName, recType, content) {

        var imageObject;

        if (typeof content == "undefined") {
            content = "";
        }
        else
        {
            imageObject = $.parseJSON(content);
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#div_' + recTypeSize + '\').toggle();">이미지</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="div_' + recTypeSize + '"><ol><li id="list_CAPTION" value="-2" draggable="false">';
        html += '<input type="hidden" id="rectype" value="CAPTION" />';
        html += '<div><span class="glyphicon glyphicon-move">캡션</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_IMAGE' + recTypeSize + '0" contenteditable="true" >';
        if (typeof imageObject != "undefined") {
            if (typeof imageObject.caption != "undefined") {
                html += imageObject.caption;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_IMAGE' + recTypeSize + '0\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';



        html += '<li id="list_DESCRIPTION" value="-2">';
        html += '<input type="hidden" id="rectype" value="DESCRIPTION" />';
        html += '<div><span class="glyphicon glyphicon-move">설명</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_IMAGE' + recTypeSize + '1" contenteditable="true" >';
        if (typeof imageObject != "undefined") {
            if (typeof imageObject.description != "undefined") {
                html += imageObject.description;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_IMAGE' + recTypeSize + '1\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_LINK" value="-2">';
        html += '<input type="hidden" id="rectype" value="LINK" />';
        html += '<div><span class="glyphicon glyphicon-move">링크</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_IMAGE' + recTypeSize + '2" contenteditable="true" >';
        if (typeof imageObject != "undefined") {
            if (typeof imageObject.link != "undefined") {
                html += imageObject.link;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_IMAGE' + recTypeSize + '2\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'sourcedialog,cjformula\',';
        html += 'toolbar: [[\'CJFormula\'],[\'Sourcedialog\']]';
        html += '});';
        html += '</';
        html += 'script>';
        html += '</li>';


        html += '<li id="list_IMAGE" value="-2">';
        html += '<input type="hidden" id="rectype" value="" />';
        html += '<div><span class="glyphicon glyphicon-move">이미지</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_IMAGE' + recTypeSize + '3" contenteditable="true" >';
        if (typeof imageObject != "undefined") {
            if (typeof imageObject.node != "undefined") {
                html += imageObject.node;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_IMAGE' + recTypeSize + '3\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'image_previewText: \' \',';
        html += 'extraPlugins: \'sourcedialog,cjformula\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'Image\'] },';
        html += '[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li></ol></div></li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_IMAGE" + recTypeSize + "3").trigger('focus');

    }

    function addTableContent(panelName, recType, content) {

        var tableObject;
        if (typeof content == "undefined") {
            content = "";
        }
        else
        {
            tableObject = $.parseJSON(content);
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move" onclick="$(\'#div_' + recTypeSize + '\').toggle();">' + getRecTypeName(recType) + '</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="div_' + recTypeSize + '"><ol><li id="list_CAPTION" value="-2" draggable="false">';
        html += '<input type="hidden" id="rectype" value="CAPTION" />';
        html += '<div><span class="glyphicon glyphicon-move">캡션</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_MATRIX_TABLE' + recTypeSize + '0" contenteditable="true" >';
        if(typeof tableObject != "undefined")
        if (typeof tableObject.caption != "undefined") {
            html += tableObject.caption;
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_MATRIX_TABLE' + recTypeSize + '0\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_DESCRIPTION" value="-2">';
        html += '<input type="hidden" id="rectype" value="DESCRIPTION" />';
        html += '<div><span class="glyphicon glyphicon-move">설명</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_MATRIX_TABLE' + recTypeSize + '1" contenteditable="true" >';
        if (typeof tableObject != "undefined") {
            if (typeof tableObject.description != "undefined") {
                html += tableObject.description;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_MATRIX_TABLE' + recTypeSize + '1\', {';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        html += 'toolbar: [';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        html += '<li id="list_IMAGE" value="-2">';
        html += '<input type="hidden" id="rectype" value="" />';
        html += '<div><span class="glyphicon glyphicon-move">이미지</span><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea"  id ="editor_MATRIX_TABLE' + recTypeSize + '3" contenteditable="true" >';
        if (typeof tableObject != "undefined") {
            if (typeof tableObject.node != "undefined") {
                html += tableObject.node;
            }
        }
        html += '</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_MATRIX_TABLE' + recTypeSize + '3\', {';
        html += 'extraPlugins: \'footnote,cjlist,sourcedialog,cjformula\',';
        html += 'image_previewText: \' \',';
        html += 'enterMode: CKEDITOR.ENTER_BR,';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'Table\', \'Image\' , \'HorizontalRule\'] },';
        html += '{ name: \'basicstyles\', items: [\'CJList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\' ] },';
        html += '[\'Footnote\'],[\'CJFormula\'],[\'Sourcedialog\']';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li></ol></div></li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        $("#editor_MATRIX_TABLE" + recTypeSize + "3").trigger('focus');
    }

    function getContentsFromServer( idxList ){

        var obj = {};
        obj.idxList = idxList;

        $.ajax({
            type: "POST",
            url: "EntryEdit.aspx/getContents",
            data: JSON.stringify(obj) ,
            contentType: "application/json; charset=utf-8",
            dataType:"json",
            success: function( contents ) {
                pasteContents(contents.d);
            }
        });
	    
    }

    function pasteContents( contents ){

        var parentMode = "";
        var parentNumber = 0;
        $.each($.parseJSON(contents) , function(i ,object){
            if( object.RecType == 'INDEXCONTENT' ){

                parentMode = object.RecType;
                var IndexContentNumber = Number($("#indexContentNumber").val());
                parentNumber = IndexContentNumber;
                addIndexContent(IndexContentNumber);
                var strIndexContentNumber = String(IndexContentNumber + 1);
                $("#indexContentNumber").val(strIndexContentNumber);
                $("#spanIndexContentNumber").text(strIndexContentNumber);

            }else if( object.RecType == 'QUIZ' ){

                parentMode = object.RecType;
                var QuizNumber = Number($("#QuizNumber").val());
                parentNumber = QuizNumber;
                addQuiz(QuizNumber);

                var strQuizNumber = String(QuizNumber + 1);
                $("#QuizNumber").val(strQuizNumber);
                $("#spanQuizNumber").text(strQuizNumber);

            }else if( object.RecType == 'EXAMPLE' ){
                //현재 추가된 Parent에 추가한다.
                
            }else{
                var panelName = parentMode + '_' + parentNumber;
                addContent(panelName , object.RecType , object.Content);
            }
        });
    }

    function deleteContent( deleteID ){
        var deleteIDString = $("#deleteIDString").val();

        if (deleteID != "") {
            deleteIDString += $("#" + deleteID).val() + ',';
            $("#" + deleteID).remove();
            $("#deleteIDString").val(deleteIDString);
        }
    }

    function delaySecond() {
        var timer = setInterval(function () {
            
            previewtext = preview();

            $("#viewArea").html(previewtext);
            $("#editArea").hide();

            clearInterval(timer);
        }, 1500);
    }

    function resetSaveString() {
        var edu = getSaveString();

        $('#saveString').val(JSON.stringify(edu));

    }

    function deleteEntry(deleteEntryIdx) {
        if (confirm('삭제하시겠습니까?')) {
            $("#deleteEntryString").val(deleteEntryIdx);
            $("#addForm").submit();
        }
    }

    function confirmAllCheck()
    {
        return confirm('최종완료 하시겠습니까?');
    }

</script>
<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">테마관리</a></li>
				<li class="nth-child-3"><a href="EntryAddList.aspx">엔트리관리</a></li>
				<li class="nth-child-4 active"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5"><a href="UserList.aspx">사용자관리</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">검수</h2>
		</div><!-- // title -->	
        <div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a class="btn btn-sm btn-primary" href="CheckList.aspx" >검수목록</a> 
			</div>
		</div><!-- // section-button -->
		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>작업명</th>
				    <td>
                        <asp:Label runat="server" ID="txtTaskTitle" class="large" />
				    </td>
				    <th>엔트리수</th>
				    <td>
                        <asp:Label runat="server" ID="txtEntryCount" class="medium" />
				    </td>
				    <th>관리자</th>
				    <td>
                        <asp:Label runat="server" ID="txtManager" class="medium" />
				    </td>
				    <th>작업자</th>
				    <td>
                        <asp:Label runat="server" ID="txtUser" class="medium" />
				    </td>
			    </tr>
			    <tr>
				    <th>지시내용</th>
				    <td colspan="3">
                        <asp:Label runat="server" ID="txtTaskContent" class="large" />
				    </td>
				    <th>작업배포일</th>
				    <td>
                        <asp:Label runat="server" ID="txtPublishDate" class="medium" />
				    </td>
				    <th>작업완료일</th>
				    <td>
                        <asp:Label runat="server" ID="txtFinishDate" class="medium" />
				    </td>
			    </tr>
		    </tbody>
        </table>

        <!-- column start -->
        <div class="col-md-3 box" style="width: 25%; overflow-y: scroll; max-height: 1200px;"> 
        <form name="addForm" runat="server" id="addForm">
            <asp:HiddenField id="saveString" runat="server"/>
            <asp:HiddenField ID="deleteIDString" runat="server" />
            <asp:HiddenField id="deleteEntryString" runat="server" />
		    <div class="title"><!-- title -->
                <asp:Button id="btnAllCheck" cssClass="btn btn-sm btn-danger" runat="server" Text="최종완료" OnClientClick="if (!confirmAllCheck()) return false;" OnClick  ="btnAllCheck_Click"></asp:Button>
			    <h4 class="title">타이틀 <asp:Label ID="txtCount" Forecolor="DarkGray" Font-Size="Small" runat="server"></asp:Label></h4>
		    </div><!-- // title -->	
			<div class="table-responsive">
                <table border="0" class="table table-list" style="padding:0; border-spacing:0"><!-- table-a -->
                    <asp:Repeater ID="EntryList" runat="server" >
                        <HeaderTemplate>
<%

    if (EntryList.Items.Count == 0)
    {
%>
                            <tr>
                                <td colspan="3" style="text-align:center">등록된 엔트리가 없습니다.</td>
                            </tr>                    
<%
    }
%>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="txtIDX" Value=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat="server" />
                                    <asp:HiddenField ID="txtStatus" Value=<%#DataBinder.Eval(Container.DataItem , "Status")%> runat="server" />
                                    <a href='CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%#DataBinder.Eval(Container.DataItem , "Idx")%>'>
                                        <asp:Label ID="lblTitle" Text=<%#DataBinder.Eval(Container.DataItem, "EntryTitle")%>  runat="server" />
                                    </a>
                                </td>
                                <td><asp:Label ID="lblStatus" Text=<%#DataBinder.Eval(Container.DataItem , "Data")%>  runat="server" /></td>
                                <td>
<% 
    if (flagEdit)
    { 
%>
                                    <!-- <asp:Button ID="btnDel" runat="server" cssClass="btn-code-del" OnClientClick="if (!confirm('삭제하시겠습니까?')) return false;" BorderWidth="0" /> -->
                                    <input type="button" class="btn-code-del" onclick='<%#"deleteEntry(" + DataBinder.Eval(Container.DataItem , "Idx") + ");"%>' style="border:0 none;"/>
<% 
    }
%>
                               </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
			</div>
        </div>
        <!-- column end -->

        <div class="col-md-9 box" style="width: 75%;">
            <div class="btn_both">
                <div id="divPreEntry" class="leftSide" runat="server">
                    <a href="CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%=preIdx %>">
                        <img src="../images/btn_left.gif" style="border:0px"> <%=preTitle %>
                    </a>
                </div>
                <div id="divNextEntry"  class="rightSide" runat="server">
                    <a href="CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%=nextIdx %>">
                        <%=nextTitle %> <img src="../images/btn_right.gif" style="border:0px">
                    </a>
                </div>
            </div>
            <a class="btn btn-success" onclick="selectMode('view');">미리보기</a>
            <a class="btn btn-success" onclick="selectMode('edit');">편집보기</a>
            
            <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>
                    <tr >
				        <th>엔트리코드</th>
				        <td>
				            <div class="input-group">
					            <asp:Label ID="txtEntryNo" CssClass="large" runat="server" />
				            </div>
				        </td>
				        <th>UCI코드</th>
				        <td>
				        <div class="input-group">
					        <asp:Label ID="txtUCICode" CssClass="large" runat="server" />
				        </div>
				        </td>
			        </tr>
			        <tr>
				        <th>분 류</th>
				        <td>
                            <asp:Label ID="txtCategory" CssClass="medium" runat="server" />
                            <asp:HiddenField ID="hfCategoryIdx" runat="server" />
				        </td>
				        <th>최종수정일</th>
				        <td>
					        <asp:Label ID="txtEditDate" CssClass="medium" runat="server" />
				        </td>
			        </tr>
		        </tbody>
		        </table><!-- // table-a -->
		        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>		
			        <tr>
				        <th>타이틀</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle" CssClass="large" runat="server" />
				        </td>
			        </tr>
                    <tr>
				        <th>소제목1</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_Sub1" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>소제목2</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_Sub2" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>국 문</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_K" CssClass="large" runat="server" />
				        </td>
			        </tr>
                    <tr>
				        <th>원어명</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_E" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>한 자</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_C" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>요 약</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtSummary" CssClass="large" TextMode="MultiLine" Rows="3" Width="95%" runat="server" />
				        </td>
			        </tr>
                 	<tr>
				        <th>동의어</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtSynonym" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>개요부</th>
				        <td colspan=4>
                            <asp:Label ID="lblOutline" CssClass="medium" runat="server" />
					        <a id="btnOutline" class="btn btn-success" onclick="javascript:window.open('OutlineEdit.aspx?Idx=<%=EntryIdx %>', 'cate', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=960,height=650,left=0, top=0, scrollbars=yes');return false">개요부관리</a>
                            <asp:HiddenField ID="txtOutline" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>단원정보</th>
				        <td colspan="3">
                            <asp:TextBox ID="txtChapterData" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>연관엔트리</th>
				        <td colspan="3">
                            <asp:TextBox ID="txtRelationEntryData" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>용어엔트리</th>
				        <td colspan="3">
                            <asp:TextBox ID="txtTermsData" CssClass="large" runat="server" />
				        </td>
			        </tr>
		        </tbody>
            </table><!-- // table-a -->
        <div id="viewArea" style="width:100%" >
        </div>
        <div id="editArea">    
        <% 
            int cntRecType = 0;
            if (entryTable != null) { 
            DataRow[] indexContentRowList = entryTable.Select("ParentIdx=0 AND RecType='INDEXCONTENT'", "SortNo ASC");
        %>
        <input type="hidden" id="allRectTypeCount" value="<%=entryTable.Rows.Count + 1 %>" />
        <input type="hidden" id="indexContentNumber" value="<%=indexContentRowList.Length %>" />
        <div class="panel panel-gray" id="panelIndexContent"><!-- 대분류 panel 시작-->
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head" onclick="$('#olLUnit').toggle();"></span>			
				
				<span class="panel-title editor_head_title">대분류<span class="badge" id="spanIndexContentNumber"><%=indexContentRowList.Length %> </span></span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelIndexContent');"></a>
				
				<span class="editor_head_container">
				<button type="button" class="btn btn-sm btn-primary" id="addLUnit">대분류+</button>
				<a class="btn btn-sm btn-primary" onclick="javascript:window.open('OpenContents.aspx?type=cont', 'openCont', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=1440,height=980,left=0, top=0, scrollbars=yes');return false">불러오기</a>  
				</span>
            </div>
            <div class="panel-body" >
            <ol class='editor vertical' id="olLUnit">
            
        <!-- 대분류 가져오기-->
        <% 
           
            for(int i = 0 ; i < indexContentRowList.Length ;i++) {
                DataRow indexContentRow = indexContentRowList[i];
        %>
            
                    <li id="list_<%=cntRecType%>" value="<%=Convert.ToString(indexContentRow["Idx"])%>" >
                    <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentRow["RecType"])%>" />
                    <div>
                        <span class="glyphicon glyphicon-move" onclick="$('#resultContent_INDEXCONTENT_<%=i %>').toggle();">분류</span> 
				        <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<%=cntRecType%>');"></a>
				        <span class="editor_control_container">
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','MAINTITLE');" onmouseover="" style="cursor: pointer;">대제목 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','SUBTITLE');" onmouseover="" style="cursor: pointer;">중제목 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BASIC');" onmouseover="" style="cursor: pointer;">기본1 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BASIC2');" onmouseover="" style="cursor: pointer;">기본2 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BOX');" onmouseover="" style="cursor: pointer;">블럭 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','MATRIX_TABLE');" onmouseover="" style="cursor: pointer;">표 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','IMAGE');" onmouseover="" style="cursor: pointer;">이미지 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','ANNOTATION');" onmouseover="" style="cursor: pointer;">주석 </span>			  
                            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','CHDIVIDE');" onmouseover="" style="cursor: pointer;">한자분해 </span>
                            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','SOUND');" onmouseover="" style="cursor: pointer;">사운드 </span>
                            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','PAIRSENTENCE');" onmouseover="" style="cursor: pointer;">PairSentence </span>
                            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','VIDEO');" onmouseover="" style="cursor: pointer;">동영상 </span>
                        </span>     
                    </div>
                    <ol id="resultContent_INDEXCONTENT_<%=i%>">
            
        <%  
                cntRecType++;
                int parentIdx = Convert.ToInt32(indexContentRow["Idx"]);
                DataRow[] childRowList = entryTable.Select("ParentIdx=" + parentIdx , "SortNo ASC");
                for (int j = 0; j < childRowList.Length; j++) {
                    DataRow indexContentChildRow = childRowList[j];

                    if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") || Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ) {
                        string content = Convert.ToString(indexContentChildRow["Content"]);
        %>
                        <li id="list_<%=cntRecType%>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<%=cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_CAPTION" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="CAPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">캡션</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<CAPTION>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</CAPTION>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "CAPTION"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'],['CJFormula'],['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_DESCRIPTION" value="-2">
                                    <input type="hidden" id="rectype" value="DESCRIPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">설명</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) +  cntRecType%>1" contenteditable="true" >
                                        <%
                                            
                                            int descriptionStartIndex = content.IndexOf("<DESCRIPTION>");
                                            if (descriptionStartIndex != -1)
                                            {
                                                string descriptionEndTag = "</DESCRIPTION>";
                                                int descriptionEndIndex = content.IndexOf(descriptionEndTag);
                                                string caption = content.Substring(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                                                content = content.Remove(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption, "DESCRIPTION"));
                                            }
                                        %>
                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['CJFormula'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <% if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE")){ %>
                                <li id="list_LINK" value="-2">
                                    <input type="hidden" id="rectype" value="LINK" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">링크</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>2" contenteditable="true" >
                                        <%
                                            
                                           int linkStartIndex = content.IndexOf("<LINK>");
                                           if (linkStartIndex != -1)
                                           {
                                               string linkEndTag = "</LINK>";
                                               int linkEndIndex = content.IndexOf(linkEndTag);
                                               string link = content.Substring(linkStartIndex, linkEndIndex - linkStartIndex + linkEndTag.Length);
                                               content = content.Remove(linkStartIndex, linkEndIndex - linkStartIndex + linkEndTag.Length);
                                               Response.Write(convertXmlToHtml(link, "LINK"));
                                           }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>2', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            toolbar: [['CJFormula'], ['Sourcedialog']]
                                        });
                                    </script>
                                </li>
                                <% } %>

                                <li id="list_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3" value="-2">
                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3" contenteditable="true" >
                                        <%
                                            if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") ){

                                                Response.Write(convertXmlToHtml(content, Convert.ToString(indexContentChildRow["RecType"])));
                                                
                                            }else if(Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ) {

                                                Response.Write(convertXmlToHtml(content, Convert.ToString(indexContentChildRow["RecType"])));
                                                
                                            }
                                        %>

                                    </div>
                                    <% if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") ){ %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            toolbar: [
                                                { name: 'document', items: ['Image'] }, ['CJFormula'], ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                    <% }else if (Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ){ %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            image_previewText: ' ',
                                            toolbar: [
                                                { name: 'document', items: ['Table', 'Image','HorizontalRule'] },
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['CJFormula'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>
                                    <% }%>

                                </li>
                            </ol>
                            </div>
                        </li>
        <% 
                    }else if (Convert.ToString(indexContentChildRow["RecType"]).Equals("SOUND") ) {
                        string content = Convert.ToString(indexContentChildRow["Content"]);
        %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<SENTENCE>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'] , ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">오디오파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" contenteditable="true" >
                                        <%
                                            Response.Write(convertXmlToHtml(content, Convert.ToString(indexContentChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>
        <% 
                    }else if (Convert.ToString(indexContentChildRow["RecType"]).Equals("VIDEO") ) {
                        string content = Convert.ToString(indexContentChildRow["Content"]);
        %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<SENTENCE>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">동영상 파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" contenteditable="true" >
                                        <%
                                            Response.Write(convertXmlToHtml(content, Convert.ToString(indexContentChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>
        <% 
                    }else if (Convert.ToString(indexContentChildRow["RecType"]).Equals("PAIRSENTENCE") ) {
                        string content = Convert.ToString(indexContentChildRow["Content"]);
        %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int sentenceStartIndex = content.IndexOf("<SENTENCE>");
                                            if (sentenceStartIndex != -1)
                                            {
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(sentenceStartIndex, captionEndIndex - sentenceStartIndex + captionEndTag.Length);
                                                content = content.Remove(sentenceStartIndex, captionEndIndex - sentenceStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'] , ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_TRANSLATION" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="TRANSLATION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">해석</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" contenteditable="true" >
                                        <%
                                            int translationStartIndex = content.IndexOf("<TRANSLATION>");
                                            if (translationStartIndex != -1)
                                            {
                                                string captionEndTag = "</TRANSLATION>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(translationStartIndex, captionEndIndex - translationStartIndex + captionEndTag.Length);
                                                content = content.Remove(translationStartIndex, captionEndIndex - translationStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption, "TRANSLATION"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_GRAMMAR" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="GRAMMAR" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문법</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>2" contenteditable="true" >
                                        <%
                                            int grammarStartIndex = content.IndexOf("<GRAMMAR>");
                                            if (grammarStartIndex != -1)
                                            {
                                                string captionEndTag = "</GRAMMAR>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(grammarStartIndex, captionEndIndex - grammarStartIndex + captionEndTag.Length);
                                                content = content.Remove(grammarStartIndex, captionEndIndex - grammarStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "GRAMMAR"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>2', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">오디오파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>1" contenteditable="true" >
                                        <%
                                            Response.Write(convertXmlToHtml(content, Convert.ToString(indexContentChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + cntRecType%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>

        <%            
                    }else{
        %>
                            <li id="list_<% =cntRecType%>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                                <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"])%>" />
                                <div>
				                    <span class="glyphicon glyphicon-move"> <%=getRecTypeName(Convert.ToString( indexContentChildRow["RecType"])) %></span>
				
				                    <a href="#!" class="btn-close" onclick="deleteContent('list_<% =cntRecType%>');" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea"  id ="editor_<% =cntRecType%>" contenteditable="true" >
                                    
                                    
                                    <%=convertXmlToHtml(Convert.ToString(indexContentChildRow["Content"]) , Convert.ToString(indexContentChildRow["RecType"])) %>
           

                                </div>
                                <script>
                                <% String recType = Convert.ToString(indexContentChildRow["RecType"]);
                                    if(recType.Equals("MAINTITLE") || recType.Equals("SUBTITLE")) { %>
                                        CKEDITOR.inline('editor_<% =cntRecType%>', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                            image_previewText: ' ',
                                            toolbar: [
                                                { name: 'document', items: ['Image'] }, ['CJFormula'], ['Sourcedialog']
                                            ]
                                        });
                                    <%}else if( recType.Equals("BASIC") || recType.Equals("BASIC2") || recType.Equals("BOX") || recType.Equals("QUESTION") || recType.Equals("EXAMPLE") || recType.Equals("DISTRACTOR") || recType.Equals("EXPLANATION") ){%>
                                    CKEDITOR.inline('editor_<% =cntRecType%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        image_previewText: ' ',
                                        enterMode: CKEDITOR.ENTER_BR,
                                        extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                        image_previewText: ' ',
                                        toolbar: [
                                                { name: 'document', items: ['Table', 'Image' , 'HorizontalRule'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['CJFormula'], ['Sourcedialog']
                                        ]
                                    });
                                    <%}else if( recType.Equals("ANSWER") ){%>
                                    CKEDITOR.inline('editor_<% =cntRecType%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        image_previewText: ' ',
                                        enterMode: CKEDITOR.ENTER_BR,
                                        extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                        image_previewText: ' ',
                                        toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['CJFormula'], ['Sourcedialog']
                                        ]
                                    });
                                    <%}else if( recType.Equals("RELATEDSEARCH") || recType.Equals("ANNOTATION") ){%>
                                    CKEDITOR.inline('editor_<% =cntRecType%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        image_previewText: ' ',
                                        enterMode: CKEDITOR.ENTER_BR,
                                        extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                        image_previewText: ' ',
                                        toolbar: [['CJFormula'], ['Sourcedialog']]
                                    });
                                    <%}else{%>
                                    CKEDITOR.inline('editor_<% =cntRecType%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        image_previewText: ' ',
                                        enterMode: CKEDITOR.ENTER_BR,
                                        extraPlugins: 'footnote,cjlist,sourcedialog',
                                        image_previewText: ' ',
                                        toolbar: [['Sourcedialog']]
                                    });     
                                   <%}%>
                                </script>
                            </li>
         <%           
                    }
                    cntRecType++;      
                }//end for
         %>
                    </ol>     
               </li>
               
            
        <%  
                
            }//end for
        %>
                </ol><!-- olLunit -->
            </div><!-- panel body -->
            </div><!-- 대분류 panel 끝 --> 

        <!-- 퀴즈 가져오기 -->
        <% 
            DataRow[] quizRowList = entryTable.Select("ParentIdx=0 AND RecType='QUIZ'", "SortNo ASC");
            
            %>
        <input type="hidden" id="QuizNumber" value="<%=quizRowList.Length %>" />
        <div class="panel panel-gray" id="panelQuiz"><!-- 퀴즈 panel 시작-->    
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head" onclick="$('#olQuiz').toggle();"></span>
				
				<span class="panel-title editor_head_title">문제<span class="badge" id="spanQuizNumber"><%=quizRowList.Length%></span></span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelQuiz');"><!-- close --></a>
				
				<span class="editor_head_container">
				<button type="button" class="btn btn-sm btn-primary" id="addQuiz">퀴즈+</button>
				<a class="btn btn-sm btn-primary" onclick="javascript:window.open('OpenContents.aspx?type=quiz', 'openCont', 'location=no, directories=no,resizable=no,status=no,toolbar=no,menubar=no, width=1440,height=980,left=0, top=0, scrollbars=yes');return false">불러오기</a>  
				</span>
            </div>
            <div class="panel-body" > <!-- 퀴즈 패널 바디 시작 -->
            <ol class='editor vertical' id="olQuiz">
        <% 
            
            for(int i = 0 ; i < quizRowList.Length ;i++) {
                DataRow quizRow = quizRowList[i];
        %>
                    <li id="list_<% =cntRecType%>" value="<%=Convert.ToString(quizRow["Idx"])%>" >
                    <input type="hidden" id="rectype" value="<%=Convert.ToString(quizRow["RecType"])%>" />
                    <div>
				        <span class="glyphicon glyphicon-move" onclick="$('#resultContent_QUIZ_<%=i%>').toggle();">문제 시작</span> 
				        <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
				        <span class="editor_control_container">
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','QUESTION');" onmouseover="" style="cursor: pointer;">문제 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','EXAMPLE');" onmouseover="" style="cursor: pointer;">보기 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','DISTRACTOR');" onmouseover="" style="cursor: pointer;">선택지 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','ANSWER');" onmouseover="" style="cursor: pointer;">정답 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','EXPLANATION');" onmouseover="" style="cursor: pointer;">해설 </span>
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','LEVEL');" onmouseover="" style="cursor: pointer;">난이도 </span>
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','SOUND');" onmouseover="" style="cursor: pointer;">사운드 </span>
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','PAIRSENTENCE');" onmouseover="" style="cursor: pointer;">PairSentence </span>
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','VIDEO');" onmouseover="" style="cursor: pointer;">동영상 </span>
                        </span>     
                    </div>
                    <ol id="resultContent_QUIZ_<%=i%>">
            
        <%  
                cntRecType++;
                int parentIdx = Convert.ToInt32(quizRow["Idx"]);
                DataRow[] quizChildRowList = entryTable.Select("ParentIdx=" + parentIdx , "SortNo ASC");
                for (int j = 0; j < quizChildRowList.Length; j++) {
                    DataRow quizChildRow = quizChildRowList[j];
                    if (Convert.ToString(quizChildRow["RecType"]).Equals("SOUND"))
                    {
                        string content = Convert.ToString(quizChildRow["Content"]);
         %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(quizChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(quizChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<SENTENCE>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">오디오파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>3" contenteditable="true" >
                                        <%
                                               Response.Write(convertXmlToHtml(content, Convert.ToString(quizChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>
        <%
                    }else if (Convert.ToString(quizChildRow["RecType"]).Equals("VIDEO")) {
                        string content = Convert.ToString(quizChildRow["Content"]);
         %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(quizChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(quizChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<SENTENCE>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">동영상 파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>3" contenteditable="true" >
                                        <%
                                               Response.Write(convertXmlToHtml(content, Convert.ToString(quizChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>
        <% 
                    }
                    else if (Convert.ToString(quizChildRow["RecType"]).Equals("PAIRSENTENCE"))
                    {
                        string content = Convert.ToString(quizChildRow["Content"]);
        %>
                        <li id="list_<%=cntRecType %>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(quizChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move" onclick="$('#div_<%=cntRecType %>').toggle();"><%=getRecTypeName(Convert.ToString(quizChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
                            </div>
                            <div class="editor_textarea" id="div_<%=cntRecType %>">
                            <ol>
                                <li id="list_SENTENCE" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="SENTENCE" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문장</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0" contenteditable="true" >
                                        <%
                                            int sentenceStartIndex = content.IndexOf("<SENTENCE>");
                                            if (sentenceStartIndex != -1)
                                            {
                                                string captionEndTag = "</SENTENCE>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(sentenceStartIndex, captionEndIndex - sentenceStartIndex + captionEndTag.Length);
                                                content = content.Remove(sentenceStartIndex, captionEndIndex - sentenceStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "SENTENCE"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_TRANSLATION" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="TRANSLATION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">해석</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1" contenteditable="true" >
                                        <%
                                            int transitionStartIndex = content.IndexOf("<TRANSLATION>");
                                            if (transitionStartIndex != -1)
                                            {
                                                string captionEndTag = "</TRANSLATION>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(transitionStartIndex, captionEndIndex - transitionStartIndex + captionEndTag.Length);
                                                content = content.Remove(transitionStartIndex, captionEndIndex - transitionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption, "TRANSLATION"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_GRAMMAR" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="GRAMMAR" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">문법</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>2" contenteditable="true" >
                                        <%
                                            int grammarStartIndex = content.IndexOf("<GRAMMAR>");
                                            if (grammarStartIndex != -1)
                                            {
                                                string captionEndTag = "</GRAMMAR>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(grammarStartIndex, captionEndIndex - grammarStartIndex + captionEndTag.Length);
                                                content = content.Remove(grammarStartIndex, captionEndIndex - grammarStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption , "GRAMMAR"));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>2', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            image_previewText: ' ',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'footnote,cjlist,sourcedialog',
                                            toolbar: [
                                                { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                ['Footnote'], ['Sourcedialog']
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>3" value="-2">

                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">오디오파일</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>3" contenteditable="true" >
                                        <%
                        Response.Write(convertXmlToHtml(content, Convert.ToString(quizChildRow["RecType"])));
                                        %>

                                    </div>

                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + cntRecType%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            enterMode: CKEDITOR.ENTER_BR,
                                            extraPlugins: 'sourcedialog,cjmedia',
                                            toolbar: [
                                                { name: 'document', items: ['CJMedia'] },
                                                ['Sourcedialog']
                                            ]

                                        });
                                    </script>
                                </li>
                            </ol>
                            </div>
                        </li>

        <%
                    }else { 
                    
        %>

                        <li id="list_<% =cntRecType%>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(quizChildRow["RecType"])%>" />
                            <div>
				                <span class="glyphicon glyphicon-move"> <%=getRecTypeName(Convert.ToString( quizChildRow["RecType"])) %></span>
				
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =cntRecType%>');"></a>
				                <span class="editor_control_container">
				                </span>
		                    </div>
                            <div class="editor_textarea"  id ="editor_<% =Convert.ToString( quizChildRow["SortNo"])%>" contenteditable="true" ><%=convertXmlToHtml( Convert.ToString(quizChildRow["Content"]) , Convert.ToString( quizChildRow["RecType"]) ) %></div>
                                                            <script>
                            <% String recType = Convert.ToString(quizChildRow["RecType"]);
                                if(recType.Equals("MAINTITLE") || recType.Equals("SUBTITLE")) { %>
                                                                CKEDITOR.inline('editor_<% =cntRecType%>', {
                                                                    filebrowserUploadUrl: 'Upload.ashx',
                                                                    enterMode: CKEDITOR.ENTER_BR,
                                                                    image_previewText: ' ',
                                                                    extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                                                    image_previewText: ' ',
                                                                    toolbar: [
                                                                        { name: 'document', items: ['Image'] }, ['CJFormula'],['Sourcedialog']
                                                                    ]
                                                                });
                                <%}else if( recType.Equals("BASIC") || recType.Equals("BASIC2") || recType.Equals("BOX") || recType.Equals("QUESTION") || recType.Equals("EXAMPLE") || recType.Equals("DISTRACTOR") || recType.Equals("EXPLANATION") ){%>
                                                                CKEDITOR.inline('editor_<% =cntRecType%>', {
                                                                    filebrowserUploadUrl: 'Upload.ashx',
                                                                    enterMode: CKEDITOR.ENTER_BR,
                                                                    image_previewText: ' ',
                                                                    extraPlugins: 'footnote,cjlist,sourcedialog',
                                                                    image_previewText: ' ',
                                                                    toolbar: [
                                                                            { name: 'document', items: ['Table', 'Image','HorizontalRule'] },
                                                                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                                            { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                                            ['Footnote'],['Sourcedialog']
                                                                    ]
                                                                });
                                <%}else if( recType.Equals("ANSWER")){%>
                                                                CKEDITOR.inline('editor_<% =cntRecType%>', {
                                                                    filebrowserUploadUrl: 'Upload.ashx',
                                                                    enterMode: CKEDITOR.ENTER_BR,
                                                                    image_previewText: ' ',
                                                                    extraPlugins: 'footnote,cjlist,sourcedialog',
                                                                    image_previewText: ' ',
                                                                    toolbar: [
                                                                            { name: 'document', items: ['Table', 'Image'] },
                                                                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                                            { name: 'basicstyles', items: ['CJList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] },
                                                                            ['Footnote'], ['Sourcedialog']
                                                                    ]
                                                                });
                                <%}else if (recType.Equals("ANNOTATION") || recType.Equals("RELATEDSEARCH")){%>
                                                                CKEDITOR.inline('editor_<% =cntRecType%>', {
                                                                    filebrowserUploadUrl: 'Upload.ashx',
                                                                    enterMode: CKEDITOR.ENTER_BR,
                                                                    image_previewText: ' ',
                                                                    extraPlugins: 'footnote,cjlist,sourcedialog,cjformula',
                                                                    image_previewText: ' ',
                                                                    toolbar: [['CJFormula'],['Sourcedialog']]
                                                                });
                                <%}else{%>
                                                                CKEDITOR.inline('editor_<% =cntRecType%>', {
                                                                    filebrowserUploadUrl: 'Upload.ashx',
                                                                    enterMode: CKEDITOR.ENTER_BR,
                                                                    image_previewText: ' ',
                                                                    extraPlugins: 'footnote,cjlist,sourcedialog',
                                                                    image_previewText: ' ',
                                                                    toolbar: [['Sourcedialog']]
                                                                });
                                <%}%>
                            </script>

                        </li>
         <%     
         }
                    cntRecType++;      
          }//end for
         %>
                    </ol>     
               </li>
               
            
            
        <%        
            }//end for
        %>
        </ol>  
            </div><!-- 퀴즈 패널 바디 끝 -->
        </div><!-- 퀴즈 panel 끝--> 

       
        <!-- 태그 가져오기 -->
        <div class="panel panel-gray" id="panelTag"><!-- 태그 panel 시작-->    
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head"></span>			
				
				<span class="panel-title editor_head_title">태그</span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelTag');"><!-- close --></a>
				<span class="editor_head_container">
				</span>
            </div>
            <div class="panel-body" > <!-- 태그 패널 바디 시작 -->
                <ol class="editor vertical" id="olTag">
                    <li id="list_tag" value="<%=txtEntryNo.Text%>">
                        <input type="hidden" id="rectype" value="TAG" />
                        <div>
				            <span class="editor_control_container"></span>
		                </div>
                        <div class="editor_textarea" contenteditable="true" ><%=entryTag %></div>
                    </li>
                </ol>
            </div><!-- 태그 패널 바디 끝 -->
        </div><!-- 태그 panel 끝--> 


        <%
        }//Null 체크 IF
             %>		 
	    </div>
		<div class="section-button" ><!-- section-button -->
			<div class="pull-center">
                <!--<button id="preView" type="button" class="btn btn-sm btn-primary">미리보기</button>-->
                <span id="btnSection" runat="server">
                    <asp:Button id="Button1" cssClass="btn btn-sm btn-primary" OnClientClick="resetSaveString();" runat="server" Text="저장" OnClick="btnCheck_Click"></asp:Button>
                </span>
			</div>
		</div>
        <div class="btn_both">
            <div id="preEntry" class="leftSide" runat="server">
                <a href="CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%=preIdx %>">
                    <img src="../images/btn_left.gif" style="border:0px"> <%=preTitle %>
                </a>
            </div>
            <div id="nextEntry"  class="rightSide" runat="server">
                <a href="CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%=nextIdx %>">
                    <%=nextTitle %> <img src="../images/btn_right.gif" style="border:0px">
                </a>
            </div>
        </div>		
    </div><!-- Contents -->

	    <footer id="footer">
		    <img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	    </footer>

        <div id="markup_dialog" title="Basic dialog">
        </div>

    </form>

</div><!-- // container -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/jquery.ui/jquery.ui.js"></script>
<script src="../js/jquery.nestedSortable.js"></script>
<script src="../js/jquery.simplecolorpicker.js"></script>
<script type="text/javascript">
    function selectMode(typeVal) {
        if (typeVal == 'view') {
            previewtext = preview();
            $("#viewArea").html(previewtext);

            $('#viewArea').show();
            $('#editArea').hide();
        }
        else {
            $('#editArea').show();
            $('#viewArea').hide();
        }
    }
</script>
</body>

</html>
