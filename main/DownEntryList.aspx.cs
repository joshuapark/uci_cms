﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class main_NewQuery : System.Web.UI.Page
{

    private string connectionString = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }        
        
        int TaskIdx = Convert.ToInt32(Request.Params["TaskIdx"]);
        //####### 권한 체크 시작 
        string strQuery = "SELECT T.UserIdx, A.Status FROM TEntry A, TTaskID T WHERE A.TaskIdx=T.Idx and TaskIdx=" + TaskIdx + "";
        
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = strQuery;
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        int intStatus = 1; string userIdx = string.Empty;
        if (reader.Read())
        {
            intStatus = Convert.ToInt32(reader["Status"].ToString());
            userIdx = reader["UserIdx"].ToString();
        }
        Con.Close();
        
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0  )
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        //####### 권한 체크 끝 
        strQuery = "SELECT A.Idx as EntryNo, LUnitName as 대단원, MUnitName as 중단원, EntryTitle as 타이틀 FROM TEntry A, TLUnit B, TMUnit C WHERE A.LUnitIdx=B.Idx AND A.MUnitIdx=C.Idx AND TaskIdx=" + TaskIdx + " Order BY A.Idx ASC";
        DataTable dt = executeQueryText(strQuery);
        String excelFileName = "EntryList_" + TaskIdx.ToString() + "_" + DateTime.Now.ToString("yyyyMMdd") + ".xls";
        
        if ((uAuth == 1) && (intStatus > 0) && (userIdx == Session["uidx"].ToString()))
        {
            this.exportExcel(dt, excelFileName);
            Response.Write("<script>window.opener=self;self.close();");
            return;
        }
        else if ((uAuth == 3) && (intStatus > 0))
        {
            this.exportExcel(dt, excelFileName);
            Response.Write("<script>window.opener=self;self.close();");
            return;
        }
        else if (uAuth == 9)
        {
            this.exportExcel(dt, excelFileName);
            Response.Write("<script>window.opener=self;self.close();");
            return;
        }
        else
        {
            Response.Write("<script>alert('권한이 없습니다.');window.opener=self;self.close();");
            return;
        }

    }


    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];

        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    private void showMessage(String message){

        string script = "alert(\""+ message +"\");";
        ScriptManager.RegisterStartupScript(this, GetType() , "ServerControlScript", script, true);

    }

    private DataTable executeQueryText( String query )
    {
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(query, Con);
        Con.Open();

        SqlDataReader dataReader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        DataTable dataTable = new DataTable();
        dataTable.Load(dataReader);
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

        return dataTable;
    }

    private void exportExcel( DataTable dataTable , String excelFileName ) {
        string attachment = "attachment; filename=" + excelFileName ;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.ContentEncoding = System.Text.Encoding.Default;
        string tab = "";
        foreach (DataColumn dc in dataTable.Columns)
        {
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");

        int i;
        foreach (DataRow dr in dataTable.Rows)
        {
            tab = "";
            for (i = 0; i < dataTable.Columns.Count; i++)
            {
                Response.Write(tab + dr[i].ToString().TrimEnd());
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }

}