﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Xml.Serialization;

public partial class XmlImport2 : System.Web.UI.Page
{
    private int intSortNo = 0;
    private string connectionString = string.Empty;
    private string strTaskIdx = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["taskIdx"] != null)
            strTaskIdx = Request.Params["taskIdx"].ToString();
        strTaskIdx = "2";

    }

    protected void btnUploadXML_Click(object sender, EventArgs e)
    {
        lblMessage.Text = ""; //메시지 창 초기화
        DBFileInfo();
        string strQuery = string.Empty;
        //0.파일이 있는지 확인
        if ((null == XMLFileUpload.PostedFile)
            || (0 >= XMLFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.
            lblMessage.Text = "파일을 선택해 주세요";
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        FileInfo fi = new FileInfo(XMLFileUpload.PostedFile.FileName);
        string FileNewName = "XML_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;
        string sFileUri = HttpContext.Current.Server.MapPath("~/UploadFile/") + FileNewName;
        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);
        
        try
        {
            //파일 업로드
            XMLFileUpload.PostedFile.SaveAs(sFileUri);
            //1-3. DB에 TaskIdx 기준으로 데이터를 저장한다.            
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@FileType", SqlDbType.Char);
            Cmd.Parameters.Add("@XMLFileName", SqlDbType.VarChar, 255);
            Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
            Cmd.Parameters["@FileType"].Value = "X";
            Cmd.Parameters["@XMLFileName"].Value = FileNewName;
            Cmd.CommandText = "USP_ImportFile_INSERT_UPDATE";
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Connection = Con;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
        }
        catch (Exception ex)
        {
            //오류다!
            //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString();
            return;
        }
        //1-4. XML 파싱, 작업에 해당하는 EntryIdx 및 중복 EntryIdx 파악을 위해 EntryIdx 저장.
        XmlSerializer serializer = new XmlSerializer(typeof(edu));
        serializer.UnknownNode += new XmlNodeEventHandler(Serializer_UnknownNode);
        serializer.UnknownAttribute += new XmlAttributeEventHandler(Serializer_UnknownAttribute);
        
        FileStream fs = new FileStream(sFileUri, FileMode.Open);

        edu eduroot = (edu)serializer.Deserialize(fs);
        int intEntryCount = eduroot.ENTRY.Count();
        string strEntryIdx = string.Empty;
        string strEntryCode = string.Empty;
        string strENTRYTITLE = string.Empty;
        for (int i = 0; i < intEntryCount; i++) //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 시작
        {
            try
            {
                strEntryCode = eduroot.ENTRY[i].entryCode;
                strENTRYTITLE = eduroot.ENTRY[i].ENTRYTITLE;
                strQuery = "INSERT INTO TEntry (EntryCode, EntryTitle, TaskIdx) "
                    + " VALUES ('" + strEntryCode + "', '" + strENTRYTITLE + "',"+ strTaskIdx + ") ";
                DBFileInfo();
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //오류다!
                //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
                lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                                + ex.ToString().Substring(0, 130) + "...";
                return;
            }
        }    //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 끝
        
        fs.Close();


        lblMessage.Text = "XML 업로드가 완료되었습니다.";
        lblXMLFileName.Text = FileNewName;
        btnImport.Enabled = true;
        Console.WriteLine("No rows found.");
    }

    //protected void ValidationEventHandler(object sender, ValidationEventArgs e)
    //{
    //    switch (e.Severity)
    //    {
    //        case XmlSeverityType.Error:
    //            Console.WriteLine("Error: {0}", e.Message);
    //            break;
    //        case XmlSeverityType.Warning:
    //            Console.WriteLine("Warning {0}", e.Message);
    //            break;
    //    }

    //}
    protected void btnUploadImage_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //0.파일이 있는지 확인
        if ((null == ImageFileUpload.PostedFile)
            || (0 >= ImageFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.

            Response.Write("파일을 선택해 주세요");
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        FileInfo fi = new FileInfo(ImageFileUpload.PostedFile.FileName);
        string FileNewName = "IMG_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;
        string sFileUri = HttpContext.Current.Server.MapPath("~/UploadFile/") + FileNewName;
        //               + string.Format(@"\{0}", ImageFileUpload.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try
        {
            //### 3-1  UploadFile/ 폴더에 파일 업로드
            ImageFileUpload.PostedFile.SaveAs(sFileUri);
            string zipPath = sFileUri;
            //### 3-2 UploadFile/Extract 폴더에 압축해제
            string extractPath = HttpContext.Current.Server.MapPath("~/UploadFile/Extract");
            System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
            //### 3-3 UploadFile/Extract 폴더에서  압축해제
            string copyPath = HttpContext.Current.Server.MapPath("~/UploadFile/EntryImage/");
            string[] fileList = Directory.GetFiles(@extractPath);
            foreach (string fileURI in fileList)
            {
                string fileName = Path.GetFileName(fileURI);
                string newFileURI = copyPath + fileName;
                // Delete a file by using File class static method... 
                if (System.IO.File.Exists(@newFileURI))
                {
                    // 삭제할것인가?
                }
                else
                {
                    try
                    {
                        File.Copy(fileURI, newFileURI);
                    }
                    catch (Exception ex)
                    {
                        lblMessage.Text = "파일 복사중 에러가 발생하였습니다.";
                    }
                }
                File.Delete(fileURI);
            }

            DBFileInfo();
            string strQuery = string.Empty;
            try
            {
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@FileType", SqlDbType.Char);
                Cmd.Parameters.Add("@XMLFileName", SqlDbType.VarChar, 255);
                Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
                Cmd.Parameters["@FileType"].Value = "I";
                Cmd.Parameters["@ImgFileName"].Value = FileNewName;
                Cmd.CommandText = "USP_ImportFile_INSERT_UPDATE";
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = Con;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                + ex.ToString();
            }
            finally
            {
                lblMessage.Text = "Image 파일을 업로드하였습니다.";
                lblImgFileName.Text = FileNewName;
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = "파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //1-1.XML 파싱, Table로 Push: TEntry
        string strEntryIdx= string.Empty;
        string strEntryCode = string.Empty;
        string strENTRYTITLE = string.Empty;
        string strENTRYTITLE_E = string.Empty;
        string strENTRYTITLE_K = string.Empty;
        string strENTRYTITLE_C = string.Empty;
        string strATTRIBUTES = string.Empty;
        string strINDEXCONTENT = string.Empty;
        string strRELATEDSEARCH = string.Empty;
        string strQUIZ = string.Empty;
        string strTAG = string.Empty;

        string strEntType = string.Empty;
        string strRecType = string.Empty;
        string strQuery = string.Empty;
        string strICIdx = string.Empty;
        string strContent = string.Empty;
        string strExIdx = string.Empty;

        DBFileInfo();
        //strQuery = "Select XMLFileName FROM TImportFile WHERE Idx=" + strTaskIdx + " "; 
        //SqlConnection Con = new SqlConnection(connectionString);
        //SqlCommand Cmd = new SqlCommand(strQuery, Con);
        //Cmd.CommandType = CommandType.Text;
        //Con.Open();
        //SqlDataReader reader;
        //reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //string XMLFileName = string.Empty;
        //if (reader.Read())
        //{
        //    XMLFileName = reader["XMLFileName"].ToString();
        //}
        //reader.Close();
        //Con.Close();
        string XMLFileName = lblXMLFileName.Text;
        XmlDocument xmlDoc = new XmlDocument();
        string pathFile = HttpContext.Current.Server.MapPath("~/UploadFile/") + XMLFileName;
        try
        {
            xmlDoc.Load(pathFile);
        }   
        catch (Exception ex)
        {
            //오류다!
            //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString().Substring(0, 130) + "...";
            return;
        }

        int intEntryCount = xmlDoc["edu"].ChildNodes.Count;
        int EntryChildCount = 0;
        int cntIContentChild = 0;
        int cntExampleChild = 0;

        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();
        SqlDataReader reader;

        //참조 URL = https://msdn.microsoft.com/en-us/library/System.Xml.XmlDocument(v=vs.110).aspx
        try
        {
            for (int i = 0; i < intEntryCount; i++) //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 시작
            {
                EntryChildCount = xmlDoc["edu"].ChildNodes[i].ChildNodes.Count;
                intSortNo = 0;
                strEntryCode = xmlDoc["edu"].ChildNodes[i].Attributes["entryCode"].Value;

                for (int j = 0; j < EntryChildCount; j++)  //##### Entry 자식노드 갯수만큼 돌면서 파싱 시작
                {
                    strEntType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].Name;
                    strQuery = "SELECT Top 1 Idx FROM TEntry WHERE EntryCode='" + strEntryCode + "' ORDER BY Idx Desc";

                    Con = new SqlConnection(connectionString);
                    Cmd = new SqlCommand(strQuery, Con);
                    Cmd.CommandType = CommandType.Text;
                    Cmd.Connection = Con;
                    Con.Open();
                    reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    if (reader.Read())
                    {
                        strEntryIdx = reader["Idx"].ToString();
                    }
                    Con.Close();

                    switch (strEntType)  //-- 엔트리 1dept type구분. EntryTitle, IndexContent, Qize, Tag, Attribute
                    {

                        case "ENTRYTITLE":
                            strENTRYTITLE = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE = strENTRYTITLE.Replace("'", "''");
                            Console.WriteLine("Case 1");
                            break;
                        case "ENTRYTITLE_E":
                            strENTRYTITLE_E = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_E = strENTRYTITLE_E.Replace("'", "''");
                            Console.WriteLine("Case 2");
                            break;
                        case "ENTRYTITLE_C":
                            strENTRYTITLE_C = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_C = strENTRYTITLE_C.Replace("'", "''");
                            Console.WriteLine("Case 3");
                            break;
                        case "ENTRYTITLE_K":
                            strENTRYTITLE_K = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_K = strENTRYTITLE_K.Replace("'", "''");
                            Console.WriteLine("Case 4");
                            break;
                        case "ATTRIBUTES":
                            strATTRIBUTES = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strATTRIBUTES = strATTRIBUTES.Replace("'", "''");
                            //##### ATTRIBUTES 입력
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strATTRIBUTES + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 5");
                            break;
                        case "INDEXCONTENT":
                            strINDEXCONTENT = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strINDEXCONTENT = strINDEXCONTENT.Replace("'", "''");
                            cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;
                            //##### 대분류 입력 시작
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strINDEXCONTENT + "', " + cntIContentChild + "," + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();
                            //##### 대분류 입력 끝
                            //##### 대분류 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 시작
                            strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                                + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Connection = Con;
                            Con.Open();
                            reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            if (reader.Read())
                            {
                                strICIdx = reader["Idx"].ToString();
                            }
                            Con.Close();
                            //##### 대분류 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 끝                        
                            intSortNo++;
                            Console.WriteLine("Case 6");

                            for (int k = 0; k < cntIContentChild; k++)
                            {
                                strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                                strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                                strContent = strContent.Replace("'", "''");
                                strQuery = "INSERT INTO TEntryData "
                                    + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx ) VALUES ("
                                    + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                    + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();

                                intSortNo++;
                                Console.WriteLine("Case 6-" + k);
                            }

                            break;
                        case "QUIZ":
                            strQUIZ = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strQUIZ = strQUIZ.Replace("'", "''");
                            cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;
                            //##### 퀴즈 입력 시작
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strQUIZ + "', " + cntIContentChild + "," + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();
                            //##### 퀴즈 입력 끝
                            //##### 퀴즈 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 시작
                            strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                                + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Connection = Con;
                            Con.Open();
                            reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            if (reader.Read())
                            {
                                strICIdx = reader["Idx"].ToString();
                            }
                            Con.Close();
                            //##### 퀴즈 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 끝
                            intSortNo++;
                            Console.WriteLine("Case 7");

                            for (int k = 0; k < cntIContentChild; k++)
                            {
                                strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                                strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                                strContent = strContent.Replace("'", "''");
                                strQuery = "INSERT INTO TEntryData "
                                    + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                    + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                    + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();

                                intSortNo++;
                                Console.WriteLine("Case 7-" + k);

                                if (strRecType == "EXAMPLE")
                                {
                                    cntExampleChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes.Count;

                                    strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=" + Convert.ToInt32(strICIdx) + " "
                                        + "AND RecType='" + strRecType + "'  ORDER BY Idx Desc";

                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand(strQuery, Con);
                                    Cmd.CommandType = CommandType.Text;
                                    Cmd.Connection = Con;
                                    Con.Open();

                                    reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                                    if (reader.Read())
                                    {
                                        strExIdx = reader["Idx"].ToString();
                                    }
                                    Con.Close();

                                    strQuery = "UPDATE TEntryData SET ChildCount=" + cntExampleChild + " WHERE Idx=" + strExIdx + " ";
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand(strQuery, Con);
                                    Cmd.CommandType = CommandType.Text;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                    string strExType = string.Empty;
                                    for (int l = 0; l < cntExampleChild; l++)
                                    {
                                        strExType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].Name;
                                        strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].InnerXml;
                                        strContent = strContent.Replace("'", "''");

                                        strQuery = "INSERT INTO TEntryData "
                                            + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                            + strEntryIdx + ", " + Convert.ToInt32(strExIdx) + ", '" + strExType + "', "
                                            + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                        Con = new SqlConnection(connectionString);
                                        Cmd = new SqlCommand(strQuery, Con);
                                        Cmd.CommandType = CommandType.Text;
                                        Con.Open();
                                        Cmd.ExecuteNonQuery();
                                        Con.Close();

                                        intSortNo++;
                                        Console.WriteLine("Case 7-Ex-" + k);

                                    }
                                }
                            }

                            break;
                        case "RELATEDSEARCH":
                            strRELATEDSEARCH = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strRELATEDSEARCH = strRELATEDSEARCH.Replace("'", "''");
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strRELATEDSEARCH + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 8");
                            break;

                        case "TAG":
                            strTAG = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strTAG = strTAG.Replace("'", "''");
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strTAG + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 9");
                            break;
                    }  //-- Switch End
                }  //##### Entry 자식노드 갯수만큼 돌면서 파싱 끝

                strQuery = "UPDATE TEntry SET EntryTitle='" + strENTRYTITLE + "', "
                    + " EntryTitleK='" + strENTRYTITLE_K + "', EntryTitleE='" + strENTRYTITLE_E + "', "
                    + " EntryTitleC='" + strENTRYTITLE_C + "', Tag='" + strTAG + "', ImportDate=getdate(), EditDate=getdate() "
                    + " Where Idx=" + strEntryIdx + " ";
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }    //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 끝
        }
        catch(Exception ex)
        {
            lblMessage.Text = "엔트리 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
        }
        finally
        {
            DBFileInfo();
            Con = new SqlConnection(connectionString);
            strQuery = "UPDATE TImportFile SET ImportFlag=1 WHERE TaskIdx=" + strTaskIdx + "; ";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            btnImport.Visible = false;
            Response.Write("<script>self.opener=self;window.close();</script>");
        }
    }

    protected void Serializer_UnknownNode(Object sender, XmlNodeEventArgs e)
    {

    }

    protected void Serializer_UnknownAttribute(Object sender, XmlAttributeEventArgs e)
    {

    }
}