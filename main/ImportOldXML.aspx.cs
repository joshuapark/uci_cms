﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;

public partial class XmlImport2 : System.Web.UI.Page
{
    private int intSortNo = 0;
    private string connectionString = string.Empty;
    private string strTaskIdx = string.Empty;
    private bool flgImport = false;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUploadExcel_Click(object sender, EventArgs e)
    {
        lblMessage.Text = ""; //메시지 창 초기화
        strTaskIdx = "2";
        //0.파일이 있는지 확인
        if ((null == XMLFileUpload.PostedFile)
            || (0 >= XMLFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.
            lblMessage.Text = "파일을 선택해 주세요";
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        string sFileUri = HttpContext.Current.Server.MapPath("~/Data")
                            + string.Format(@"\{0}", XMLFileUpload.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try
        {
            //파일 업로드
            XMLFileUpload.PostedFile.SaveAs(sFileUri);
        }
        catch (Exception ex)
        {
            //오류다!
            //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString();
            return;
        }

        //1-3.XML 파싱, Temp Table로 Push: TEntryTemp
        string strEntryIdx = string.Empty;
        string strEntryConde = string.Empty;
        string strENTRYTITLE = string.Empty;
        string strENTRYTITLE_E = string.Empty;
        string strENTRYTITLE_K = string.Empty;
        string strENTRYTITLE_C = string.Empty;
        string strATTRIBUTES = string.Empty;
        string strINDEXCONTENT = string.Empty;
        string strRELATEDSEARCH = string.Empty;
        string strQUIZ = string.Empty;
        string strTAG = string.Empty;

        string strEntType = string.Empty;
        string strRecType = string.Empty;
        string strQuery = string.Empty;
        string strICIdx = string.Empty;
        string strContent = string.Empty;
        string strExIdx = string.Empty;

        XmlDocument xmlDoc = new XmlDocument();
        try
        {
            xmlDoc.Load(sFileUri);
        }
        catch (Exception ex)
        {
            //오류다!
            //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString().Substring(0,130) + "...";
            return;
        }
        
        int cntEntry = xmlDoc["edu"].ChildNodes.Count;
        int cntEntryChild = 0;
        int cntIContentChild = 0;
        int cntExampleChild = 0;

        DBFileInfo();
        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();
        SqlDataReader reader;

        for (int i = 0; i < cntEntry; i++)
        {
            cntEntryChild = xmlDoc["edu"].ChildNodes[i].ChildNodes.Count;
            intSortNo = 0;
            strEntryIdx = xmlDoc["edu"].ChildNodes[i].Attributes["entryCode"].Value;

            for (int j = 0; j < cntEntryChild; j++)
            {
                strEntType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].Name;

                switch (strEntType)  //-- 엔트리 1dept type구분. EntryTitle, IndexContent, Qize, Tag, Attribute
                {
                    
                    case "ENTRYTITLE":
                        strENTRYTITLE = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                        Console.WriteLine("Case 1");
                        break;
                    case "ENTRYTITLE_E":
                        strENTRYTITLE_E = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                        Console.WriteLine("Case 2");
                        break;
                    case "ENTRYTITLE_C":
                        strENTRYTITLE_C = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                        Console.WriteLine("Case 3");
                        break;
                    case "ENTRYTITLE_K":
                        strENTRYTITLE_K = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                        Console.WriteLine("Case 4");
                        break;
                    case "ATTRIBUTES":
                        strATTRIBUTES = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                        strQuery = "INSERT INTO TEntryTempData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                            + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                            + strATTRIBUTES + "', "+ intSortNo + ", " + strTaskIdx +") ";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();

                        intSortNo++;
                        Console.WriteLine("Case 5");
                        break;
                    case "INDEXCONTENT":
                        strINDEXCONTENT = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                        cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;

                        strQuery = "INSERT INTO TEntryTempData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                            + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                            + strINDEXCONTENT + "', " + cntIContentChild + "," + intSortNo +", " + strTaskIdx + ") ";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();

                        strQuery = "SELECT Top 1 Idx FROM TEntryTempData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                            + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Cmd.Connection= Con;
                        Con.Open();
                        
                        reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);                        
                        if (reader.Read())
                        {
                            strICIdx = reader["Idx"].ToString();            
                        }
                        Con.Close();
                        
                        intSortNo++;
                        Console.WriteLine("Case 6");                        
                        
                        for (int k = 0; k < cntIContentChild; k++)
                        {
                            strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                            strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                            strQuery = "INSERT INTO TEntryTempData "
                                + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx ) VALUES ("
                                + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 6-" + k);
                        }
                        
                        break;
                    case "QUIZ":
                        strQUIZ = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                        cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;
                        
                        strQuery = "INSERT INTO TEntryTempData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                            + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                            + strQUIZ + "', " + cntIContentChild + "," + intSortNo + ", " + strTaskIdx + ") ";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();

                        strQuery = "SELECT Top 1 Idx FROM TEntryTempData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                            + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Cmd.Connection= Con;
                        Con.Open();
                        
                        reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);                        
                        if (reader.Read())
                        {
                            strICIdx = reader["Idx"].ToString();            
                        }
                        Con.Close();
                        
                        intSortNo++;
                        Console.WriteLine("Case 7");                        
                        
                        for (int k = 0; k < cntIContentChild; k++)
                        {
                            strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                            strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                            strQuery = "INSERT INTO TEntryTempData "
                                + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 7-" + k);

                            if (strRecType == "EXAMPLE")
                            {
                                cntExampleChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes.Count;

                                strQuery = "SELECT Top 1 Idx FROM TEntryTempData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                                    + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Cmd.Connection = Con;
                                Con.Open();

                                reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                                if (reader.Read())
                                {
                                    strExIdx = reader["Idx"].ToString();
                                }
                                Con.Close();

                                strQuery = "UPDATE TEntryTempData SET ChildCount=" + cntExampleChild + " WHERE Idx=" + strExIdx + " ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();

                                for (int l = 0; l < cntExampleChild; l++ )
                                {
                                    strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].Name;
                                    strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].InnerXml;
                                    strQuery = "INSERT INTO TEntryTempData "
                                        + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                        + strEntryIdx + ", " + Convert.ToInt32(strExIdx) + ", '" + strRecType + "', "
                                        + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand(strQuery, Con);
                                    Cmd.CommandType = CommandType.Text;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();

                                    intSortNo++;
                                    Console.WriteLine("Case 7-Ex-" + k);

                                }
                            }
                        }

                        break;
                    case "RELATEDSEARCH":
                        strRELATEDSEARCH = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;

                        strQuery = "INSERT INTO TEntryTempData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                            + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                            + strRELATEDSEARCH + "', " + intSortNo + ", " + strTaskIdx + ") ";

                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();

                        intSortNo++;
                        Console.WriteLine("Case 8");
                        break;

                    case "TAG":
                        strTAG = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                        Console.WriteLine("Case 9");
                        break;
                }  //-- Switch End
            }
            /**
            strQuery = "UPDATE TEntry SET EntryTitle='"+ strENTRYTITLE +"', "
                + "EntryTitleK='"+ strENTRYTITLE_K +"', EntryTitleE='"+ strENTRYTITLE_E +"', "
                + "EntryTitleC='"+ strENTRYTITLE_C +"', Tag='"+ strTAG +"' "
                + "Where Idx="+ strEntryIdx +" ";
            **/
            strQuery = "INSERT INTO TEntryTemp (Idx, TaskIdx, SeqNo, EntryTitle, EntryTitleK, EntryTItleE, EntryTitleC, Tag, ImportDate ) "
                + " VALUES ( " + strEntryIdx + ", " + strTaskIdx + ", " + i + ", '" + strENTRYTITLE + "', "
                + "'" + strENTRYTITLE_K + "', '" + strENTRYTITLE_E + "', '" + strENTRYTITLE_C + "', '" + strTAG + "', getdate()) ";

            Con = new SqlConnection(connectionString);
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();

        }
        //-- 1.5 엔트리 Temp Table 넣기완료 후 Valid Check// 중복 Entry 검사
        strQuery = "SELECT A.Idx, A.EntryTitle FROM TEntryTemp as A, (SELECT Idx FROM TEntryTemp GROUP BY Idx Having Count(Idx) > 1) as B "
            + " WHERE A.Idx = B.Idx Group By A.Idx, A.EntryTitle";
        Con = new SqlConnection(connectionString);
        Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        reader = Cmd.ExecuteReader();
        string strMessage = string.Empty;
        int m = 1;
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                strMessage += m + ". 중복오류:"+ reader.GetInt32(0)+" ("+ reader.GetString(1)+")<br/>";
                m++;    
            }
            lblMessage.Text = strMessage;
            strQuery = "Delete From TEntryTemp Where TaskIdx="+strTaskIdx +"; Delete From TEntryTemp Where TaskIdx= "+ strTaskIdx +";";
            Con = new SqlConnection(connectionString);
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
        }
        else
        {
            btnImport.Enabled = true;
            Console.WriteLine("No rows found.");

        }
        
        reader.Close();
        Con.Close();


        //참조 URL = https://msdn.microsoft.com/en-us/library/System.Xml.XmlDocument(v=vs.110).aspx
    }

    //protected void ValidationEventHandler(object sender, ValidationEventArgs e)
    //{
    //    switch (e.Severity)
    //    {
    //        case XmlSeverityType.Error:
    //            Console.WriteLine("Error: {0}", e.Message);
    //            break;
    //        case XmlSeverityType.Warning:
    //            Console.WriteLine("Warning {0}", e.Message);
    //            break;
    //    }

    //}

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
            //rootPath = "http://test-www2012.isherpa.co.kr/UploadFile/Mobile/Sibung/Notice/";
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
            //rootPath = "http://www.isherpa.co.kr/UploadFile/Mobile/Sibung/Notice/";
        }
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        string strQuery = "Select ROW_NUMBER() OVER (ORDER BY A.Idx) AS No, A.Idx, A.EntryTitle "
            + " From TEntryTemp A left outer join TEntry B on A.Idx=B.Idx AND A.TaskIdx=" + strTaskIdx + " Where B.Idx is Null";
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();

        try
        {
            strQuery = "Update TEntry SET (EntryTitle, EntryTitleK, EntryTitleE, EntryTitleC, ImportDate, EditDate, TitleFlag) "
                + "=(Select TEntryTemp.EntryTitle, TEntryTemp.EntryTitleK, TEntryTemp.EntryTitleE, TEntryTemp.EntryTitleC, 1 "
                + " From TEntryTemp  "
                + " Where TEntry.EntryTitle<>TEntryTemp.EntryTitle AND TEntry.Idx=TEntryTemp.Idx "
                + " AND TaskIdx=" + strTaskIdx + ")";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            
        }
        catch(Exception ex)
        {
            lblMessage.Text = "엔트리 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
        }
        finally
        {
            strQuery = "UPDATE TImportFile SET ImportFlag=1 WHERE TaskIdx="+ strTaskIdx +"; "
                + " Delete From TEntryTemp Where TaskIdx=" + strTaskIdx + "; Delete From TEntryTemp Where TaskIdx= " + strTaskIdx + ";";
            Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
        }
    }
    protected void btnUploadImage_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //0.파일이 있는지 확인
        if ((null == ImageFileUpload.PostedFile)
            || (0 >= ImageFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.

            Response.Write("파일을 선택해 주세요");
            return;
        }
                
        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        string sFileUri = HttpContext.Current.Server.MapPath("~/UploadFile")
                            + string.Format(@"\{0}", ImageFileUpload.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try
        {
            //파일 업로드
            ImageFileUpload.PostedFile.SaveAs(sFileUri);
            string startPath = @"C:\VisualStudio\Source\Repos\ch_cms\UploadFile";
            string zipPath = sFileUri;
            string extractPath = @"C:\VisualStudio\Source\Repos\ch_cms\UploadFile\Image";

            //System.IO.Compression.ZipFile.CreateFromDirectory(startPath, zipPath);
            System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            string strQuery = string.Empty;
            try
            {
                strQuery = "Insert Into TImportFile (TaskIdx, ImgFileName) Values (" + strTaskIdx + ",'" + ImageFileUpload.PostedFile.FileName + "')";
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                + ex.ToString();
            }
            finally
            {

            }

        }
        catch (Exception ex)
        {
            //오류다!
            //Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "                        + ex.ToString());
            lblMessage.Text = "파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }
    }
}