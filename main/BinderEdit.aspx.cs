﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Net;
using System.Xml;
using System.IO;
using System.Xml.Serialization;
using System.IO.Compression;

public partial class main_TemplateAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //

    private const int INITIAL_RECTYPE_ID = 1;
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    private DataTable dtRecType = new DataTable();
    private DataTable dtTemplate = new DataTable();
    private string strQuery = string.Empty;
    public string strTaskIdx = string.Empty;
    public string BinderIdx = string.Empty;
    // Set a variable to the /Data File path. 
    private string pathData = HttpContext.Current.Server.MapPath("~/DATA/"); 
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
        {
            lblLogIn.Text = Session["uname"] + "(" + Session["uid"] + ")님이 접속하셨습니다.";
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        ////##### 권한 처리 끝  
        if (!IsPostBack)
        {
            // 1. BinderIDX 받기
            if (Request.Params["idx"] != null)
            {
                BinderIdx = Request.Params["idx"].ToString();
                if (BinderIdx.Length==0)
                    Response.Redirect("BinderList.aspx");

                txtBinderIdx.Value = BinderIdx;
                GetBinderInfo();
                //ParentListing();
                //LinkListing();
                EntryListing();
            }
            else
            {
                Response.Redirect("BinderList.aspx");
            }
        }
    }

    protected void EntryListing()
    {
        DBFileInfo();
        BinderIdx = txtBinderIdx.Value;
        strQuery = "USP_Binder_Entry_List_SELECT " + BinderIdx + " ";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "data_list");

        EntryList.DataSource = ds;
        EntryList.DataBind();
        if (ds.Tables["data_list"].Rows.Count > 0)
        {
            //txtSumEntry.Text = ds.Tables["data_list"].Rows[0]["totalCount"].ToString();
        }
        Con.Close();

        Cmd = null;
        Con = null;
    }
    private void GetBinderInfo()
    {
        DBFileInfo();
        BinderIdx = txtBinderIdx.Value; 
        
        strQuery = "SELECT * FROM TBinder WHERE BinderIdx =" + BinderIdx + "";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Cmd.Connection = Con;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            lblBinderTitle.Text = reader["BinderTitle"].ToString();
            lblBinderMemo.Text = reader["BinderMemo"].ToString();
        }
        reader.Close();
        Con.Close();
    }

    protected void AddEntry_Click(object sender, EventArgs e)
    {
        BinderIdx = txtBinderIdx.Value;
        Response.Write("<script>window.open('BinderEntryAdd.aspx?BinderIdx=" + BinderIdx + "','popup','width=960,height=700, scrollbars=yes');</script>");        
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void EntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        RepeaterItem repeaterItem = EntryList.Items[e.Item.ItemIndex];
        HiddenField hfID = repeaterItem.FindControl("hfBinderEntryIdx") as HiddenField;

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand("UPDATE TBinderEntry SET DelFlag=1	WHERE Idx = " + hfID.Value, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        string script = "<script>alert('삭제되었습니다');</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);

        //ParentListing();
    }
}
