﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="UserEdit.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body onload="$('div.modal').modal();">


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">테마관리</a></li>
				<li class="nth-child-3"><a href="EntryAddList.aspx">엔트리관리</a></li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 active"><a href="UserList.aspx">사용자관리</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">사용자관리</h2>
		</div><!-- // title -->	

		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a href="UserAdd.aspx" class="btn btn-sm btn-primary">신규등록</a>
			</div>
		</div><!-- // section-button -->
        <asp:Label ID="strTotalCnt" runat="server"></asp:Label>
		<table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <asp:ListView ID="LV_Notice" runat="server" ItemPlaceholderID="phItemList" 
                    OnItemDataBound="ListView_ItemDataBound" >
                    <LayoutTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>                            
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>등급</th>
				            <th>사용자명</th>
				            <th>ID</th>
				            <th>부서</th>
				            <th>Email</th>
				            <th>연락처</th>
				            <th>사용여부</th>
				            <th>등록일</th>
                        </tr>
                        <asp:PlaceHolder ID="phItemList" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblType" runat="server" />
                            </td>
                            <td>
                                <asp:HyperLink ID="hlName" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblUserId" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblTeam" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblPhone" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblEnabled" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblInsertDate" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <EmptyDataTemplate>
                        <colgroup>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                            <col/>
                        </colgroup>
                        <tr>
				            <th>No</th>
				            <th>등급</th>
				            <th>사용자명</th>
				            <th>ID</th>
				            <th>부서</th>
				            <th>Email</th>
				            <th>연락처</th>
				            <th>사용여부</th>
				            <th>등록일</th>
                        </tr>
                        <tr>
                            <td colspan="9">등록된 사용자가 없습니다.</td>
                        </tr>
                    </EmptyDataTemplate>
            </asp:ListView>
        </table>

       <div class="modal fade" id="modalUserSave" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true"><!-- modal -->
			<div class="modal-dialog">
				<div class="modal-content">

	                <div class="modal-header" onclick="location.href='UserList.aspx';">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><!-- close --></button>
		                <h4 class="modal-title" id="myModalLabel">사용자 수정</h4>
	                </div>
	                <div class="modal-body">

		                <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		                <colgroup>
			                <col style="width: 110px;">
			                <col style="width: auto;">
		                </colgroup>
		                <tbody>
			                <tr>
				                <th>사용자 등급</th>
				                <td>
					                <div class="modal-select">
						                <asp:DropDownList id="ddlUserType" runat="server" cssClass="de-select">
							                <asp:ListItem value="0">선택</asp:ListItem>
                                            <asp:ListItem value="1">작업자</asp:ListItem>
                                            <asp:ListItem value="5">검수자</asp:ListItem>
                                            <asp:ListItem value="9">관리자</asp:ListItem>
						                </asp:DropDownList>	
                                        <asp:RequiredFieldValidator ID="rfvSelect" runat="server" InitialValue="0" ErrorMessage="* 사용자등급을 선택해주세요." ControlToValidate="ddlUserType" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
					                </div>
				                </td>
			                </tr>
			                <tr>
				                <th>이름</th>
				                <td>
					                <asp:TextBox ID="txtName" class="large" runat="server"/>
                                    <asp:RequiredFieldValidator ID="rfvTxtName" runat="server" ErrorMessage="* 이름을 입력하세요." ControlToValidate="txtName" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
                                    <asp:HiddenField ID="txtIdx" runat="server"/>
				                </td>
			                </tr>
			                <tr>
				                <th>부서</th>
				                <td>
					                <asp:TextBox ID="txtTeam" class="large" runat="server"/>
                                    <asp:RequiredFieldValidator ID="rfvTxtTeam" runat="server" ErrorMessage="* 부서를 입력하세요." ControlToValidate="txtTeam" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
				                </td>
			                </tr>
			                <tr>
				                <th>ID</th>
				                <td>
					                <asp:TextBox ID="txtID" class="large" runat="server" readonly="true"/>
                                    <asp:RequiredFieldValidator ID="rfvTxtID" runat="server" ControlToValidate="txtID" ForeColor="OrangeRed" ErrorMessage="* ID를 입력하세요."></asp:RequiredFieldValidator>
				                </td>
			                </tr>
			                <tr>
				                <th>PW</th>
				                <td>
					                <asp:TextBox ID="txtPwd1" TextMode="password" class="large" runat="server"/>
				                </td>
			                </tr>
			                <tr>
				                <th>PW 확인</th>
				                <td>
					                <asp:TextBox ID="txtPwd2" TextMode="password" class="large" runat="server"/>
                                    <asp:CompareValidator ID="ComparePwd" runat="server" ControlToValidate="txtPwd2" ControlToCompare="txtPwd1" ForeColor="OrangeRed" ErrorMessage="<br/>* 비밀번호 입력값이 다릅니다." />
				                </td>
			                </tr>
			                <tr>
				                <th>E-mail</th>
				                <td>
					                <asp:TextBox ID="txtEmail" class="large" runat="server"/>
                                    <asp:RequiredFieldValidator ID="rfvTxtEmail" runat="server" ErrorMessage="* E-mail을 입력하세요." ControlToValidate="txtEmail" ForeColor="OrangeRed"></asp:RequiredFieldValidator><br />
                                    <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="* E-mail 형식이 맞지 않습니다."></asp:RegularExpressionValidator>
				                </td>
			                </tr>
			                <tr>
				                <th>연락처</th>
				                <td>
					                <asp:TextBox ID="txtPhone" class="large" runat="server"/>
                                    <asp:RequiredFieldValidator ID="rfvTxtPhone" runat="server" ErrorMessage="* 연락처를 입력하세요." ControlToValidate="txtPhone" ForeColor="OrangeRed"></asp:RequiredFieldValidator>
				                </td>
			                </tr>
			                <tr>
				                <th>사용여부</th>
				                <td>
				                    <asp:RadioButtonList ID="radioEnabled" CssClass="de-radio" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem CssClass="de-radio" Text="사용" Value="1" style="margin-right:10px" />
                                        <asp:ListItem CssClass="de-radio" Text="미사용" Value="0" />
                                    </asp:RadioButtonList>
                                    <asp:RequiredFieldValidator ID="rfvRadio" runat="server" ErrorMessage="* 사용여부를 선택하세요." ControlToValidate="radioEnabled" ForeColor="OrangeRed"></asp:RequiredFieldValidator></font>
				                </td>
			                </tr>
		                </tbody>
		                </table><!-- // table-a -->

		                <div class="section-button mt20"><!-- section-button -->
			                <asp:Button cssClass="btn btn-lg btn-danger" ID="btnSave" OnClick="btnSave_Click" Text="저장" runat="server"></asp:Button>
		                </div><!-- // section-button -->

	                </div>

				</div>
			</div>
		</div><!-- // modal -->

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script type="text/javascript">
    $(function () {
        $("#btnOpen").click(function () {
            $('div.modal').modal();
        })
    })
</script>
</body>
</html>