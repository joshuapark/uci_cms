﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="StatTask.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>


<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li class="active"><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
                        <li><a href="OutlineList.aspx">개요부관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="searchForm" runat="server">


	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">작업자통계</h2>
		</div><!-- // title -->	

		<table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: 110px;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
  
		<tbody>
			<tr>
				<td>
                    <asp:TextBox ID="txtStartDate" runat="server" ></asp:TextBox>~
                    <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
				</td>
				<th onclick="$('#divUser').toggle();">작업자</th>
                <td>
                    <div class="de-select" id="divUser">
                        <asp:DropDownList class="de-select" ID="ddlUser" runat="server"></asp:DropDownList>					
					</div>	
                </td>
				<th onclick="$('#divTask').toggle();">작업명</th>
                <td>
                    <asp:Button runat="server" ID="TaskSearch" class="btn btn-sm btn-success" text="조회" OnClick="TaskSearch_Click" />
                    <div class="de-select" id="divTask">
                        <asp:DropDownList class="de-select" ID="ddlTask" runat="server">
                            <asp:ListItem Value="0">선택</asp:ListItem>
                        </asp:DropDownList>					
					</div>	
                </td>
                <td>
					<asp:Button runat="server" ID="btnSearch" Text="조회" CssClass="btn btn-success btn-lg" OnClick="SearchButton_Click" />
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

        <div runat="server" id="StatTable">
        </div>
        <table border="0" class="table table-list" style="padding:0; border-spacing:0">
            <tr>
			    <th rowspan="2">작업자</th>
			    <th rowspan="2">작업명</th>
			    <th rowspan="2">관리자</th>
			    <th rowspan="2">배정수</th>
			    <th rowspan="2">누적합계</th>
			    <th rowspan="2">기간합계</th>
			    <th rowspan="2">작업율</th>
			    <th><asp:Label ID="prevTerm" runat="server"></asp:Label></th>
			    <th colspan="5">Week</th>
			    <th><asp:Label ID="nextTerm" runat="server"></asp:Label></th>
            </tr>
            <tr>
                <th><asp:Label ID="SunDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="MonDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="TueDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="WedDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="ThrDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="FriDate" runat="server"></asp:Label></th>
                <th><asp:Label ID="SatDate" runat="server"></asp:Label></th>
            </tr>
<%
    string serverIP = Request.ServerVariables["LOCAL_ADDR"];
    string connectionString = string.Empty;
    if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
    {
        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
    }
    else //-- 서비스서버
    {
        connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
    }
    System.Data.SqlClient.SqlConnection Conn = new System.Data.SqlClient.SqlConnection(connectionString);
    Conn.Open();
    string strQuery = "SELECT UserIdx FROM TReport GROUP BY UserIdx";
    System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter(strQuery, Conn);
    System.Data.DataSet ds = new System.Data.DataSet();
    sda.Fill(ds, "TUser");
    //Set up the data binding. 
    Conn.Close();

    System.Data.DataTable dtUser = ds.Tables["TUser"];    
    foreach (System.Data.DataRow childRow in dtUser.Rows)
    {
        //string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        //string connectionString = string.Empty;
        //if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        //{
        //    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        //}
        //else //-- 서비스서버
        //{
        //    connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        //}
        System.Data.SqlClient.SqlConnection Conn1 = new System.Data.SqlClient.SqlConnection(connectionString);
        Conn1.Open();
        
        if (Request.Params["keydate"] != null)
            keydate = Convert.ToDateTime(Request.Params["keydate"].ToString());
        else
        {
            keydate = System.DateTime.Now;
        }
        
        strQuery = "USP_StatTask_SELECT 'A', '" + keydate.ToString("yyyy-MM-dd") + "','" + txtStartDate.Text + "','" + txtEndDate.Text + "'," + childRow["UserIdx"].ToString() + "," + ddlTask.SelectedValue;
        System.Data.SqlClient.SqlDataAdapter sda1 = new System.Data.SqlClient.SqlDataAdapter(strQuery, Conn1);
        System.Data.DataSet ds1 = new System.Data.DataSet();
        sda1.Fill(ds1, "TReport");
        System.Data.DataTable dt = new System.Data.DataTable();
        sda1.Fill(dt);        
        //Set up the data binding. 
        Conn1.Close();
        
        ReportList.DataSource = ds1;
        ReportList.DataBind();
%>
            <asp:Repeater ID="ReportList" runat="server" OnItemDataBound="ReportList_ItemDataBound" >
                <ItemTemplate>
                    <tr>
                        <td style="text-align:center"><asp:Label ID="lblUser" Text=<%#DataBinder.Eval(Container.DataItem , "UserName")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblTaskTitle" Text=<%#DataBinder.Eval(Container.DataItem , "TaskTitle")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblManager" Text=<%#DataBinder.Eval(Container.DataItem , "ManagerName")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="lblEntryCount" Text=<%#DataBinder.Eval(Container.DataItem , "EntryCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label1" Text=<%#DataBinder.Eval(Container.DataItem , "TaskCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label2" Text=<%#DataBinder.Eval(Container.DataItem , "TermCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label3" Text=<%#DataBinder.Eval(Container.DataItem , "Rate")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label4" Text=<%#DataBinder.Eval(Container.DataItem , "SunCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label5" Text=<%#DataBinder.Eval(Container.DataItem , "MonCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label6" Text=<%#DataBinder.Eval(Container.DataItem , "TueCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label7" Text=<%#DataBinder.Eval(Container.DataItem , "WedCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label8" Text=<%#DataBinder.Eval(Container.DataItem , "ThrCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label9" Text=<%#DataBinder.Eval(Container.DataItem , "FriCount")%> runat="server" /></td>
                        <td style="text-align:center"><asp:Label ID="Label10" Text=<%#DataBinder.Eval(Container.DataItem , "SatCount")%> runat="server" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
<%--                    <tr id="footerTemplate" runat="server">
				        <th colspan="3">합계</th>
				        <th><asp:Label ID="lblSumEntry" runat="server" /></th>
				        <th><asp:Label ID="lblSumTask" runat="server" /></th>
				        <th><asp:Label ID="lblSumTerm" runat="server" /></th>
				        <th><asp:Label ID="lblAvgRate" runat="server" /></th>
				        <th><asp:Label ID="lblSumSun" runat="server" /></th>
				        <th><asp:Label ID="lblSumMon" runat="server" /></th>
				        <th><asp:Label ID="lblSumTue" runat="server" /></th>
				        <th><asp:Label ID="lblSumWed" runat="server" /></th>
				        <th><asp:Label ID="lblSumThr" runat="server" /></th>
				        <th><asp:Label ID="lblSumFri" runat="server" /></th>
				        <th><asp:Label ID="lblSumSat" runat="server" /></th>
                    </tr>--%>
<%        
        //lblSumEntry.Text = dt.Compute("Sum(EntryCount)", "").ToString();
        //lblSumTask.Text = dt.Compute("Sum(TaskCount)", "").ToString();
        //lblSumTerm.Text = dt.Compute("Sum(TaskCount)", "").ToString();
        //lblAvgRate.Text = dt.Compute("Avg(Rate)", "").ToString();
        //lblSumSun.Text = dt.Compute("Sum(SunCount)", "").ToString();
        //lblSumMon.Text = dt.Compute("Sum(MonCount)", "").ToString();
        //lblSumTue.Text = dt.Compute("Sum(TueCount)", "").ToString();
        //lblSumWed.Text = dt.Compute("Sum(WedCount)", "").ToString();
        //lblSumThr.Text = dt.Compute("Sum(ThrCount)", "").ToString();
        //lblSumFri.Text = dt.Compute("Sum(FriCount)", "").ToString();
        //lblSumSat.Text = dt.Compute("Sum(SatCount)", "").ToString();
    }
%>        
        </table>

	</div><!-- // contents -->
    
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<link rel="stylesheet" href="../css/jquery-ui.css" type="text/css" media="all" />
<script src="../js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../js/jquery-ui-1.8.18.min.js" type="text/javascript"></script>
<script>
    $(function () {
        var dates = $("#<%=txtStartDate.ClientID%>, #<%=txtEndDate.ClientID%> ").datepicker({
            prevText: '이전 달',
            nextText: '다음 달',
            monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
            dayNames: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
            dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
            dateFormat: 'yy-mm-dd',
            showMonthAfterYear: true,
            yearSuffix: '년',
            onSelect: function (selectedDate) {
                var option = this.id == "<%=txtStartDate.ClientID%>" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(
                    instance.settings.dateFormat ||
                    $.datepicker._defaults.dateFormat,
                    selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });
</script>
</body>
</html>