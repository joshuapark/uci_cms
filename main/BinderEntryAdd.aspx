﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderEntryAdd.aspx.cs" Inherits="main_BinderEntryAdd" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script src="../js/jquery-2.1.3.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="title"><!-- title -->
		    <h3 class="title title-success">엔트리 추가</h3>
	    </div><!-- // title -->

        <form id="form1" runat="server">
            <asp:HiddenField ID="hdBinderIdx" runat="server" />
		    <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
			    <col style="width: auto;">
		    </colgroup>
  
		    <tbody>
			    <tr>
				    <th>개정</th>
				    <td>
					    <div class="de-select">
                            <asp:DropDownList class="de-select" ID="ddlRevision" runat="server"></asp:DropDownList>
					    </div>
				    </td>
				    <th>과목</th>
				    <td>
					    <div class="de-select">
                            <asp:DropDownList class="de-select" ID="ddlSubject" runat="server"></asp:DropDownList>					
					    </div>
				    </td>
                    <th>브랜드</th>
				    <td colspan="3">
					    <div class="de-select" id="divBrand">
                            <asp:DropDownList class="de-select" ID="ddlBrand" runat="server"></asp:DropDownList>					
					    </div>
				    </td>
                </tr>
                <tr>
                    <th onclick="$('#divGrade').toggle();">학년</th>
				    <td>
					    <div class="de-select" id="divGrade">
                            <asp:DropDownList class="de-select" ID="ddlGrade" runat="server"></asp:DropDownList>					
					    </div>			
				    </td>
				    <th onclick="$('#divSemester').toggle();">학기</th>
				    <td>
					    <div class="de-select" id="divSemester">
                            <asp:DropDownList class="de-select" ID="ddlSemester" runat="server"></asp:DropDownList>					
					    </div>		
				    </td>
				    <th onclick="$('#divLUnit').toggle();">대단원</th>
                    <td>
					    <asp:Button runat="server" ID="LUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="LUnitSearch_Click" />
                        <div class="de-select" id="divLUnit">
                            <asp:DropDownList class="de-select" ID="ddlLUnit" runat="server"></asp:DropDownList>					
					    </div>	
                    </td>
				    <th onclick="$('#divMUnit').toggle();">중단원</th>
                    <td>
					    <asp:Button runat="server" ID="MUnitSearch" class="btn btn-sm btn-success" text="조회" OnClick="MUnitSearch_Click" />
                        <div class="de-select" id="divMUnit">
                            <asp:DropDownList class="de-select" ID="ddlMUnit" runat="server"></asp:DropDownList>					
					    </div>	
                    </td>
			    </tr>
                <tr>
				    <th>키워드</th>
                    <td colspan="7">
					    <asp:TextBox runat="server" ID="txtKeyWord" class="large" />
                    </td>
                </tr>
		    </tbody>
		    </table><!-- // table-a -->

		    <div class="section-button"><!-- section-button -->
			    <asp:Button runat="server" ID="SearchButton" class="btn btn-lg btn-success" text="조회" OnClick="SearchButton_Click"/>
		    </div><!-- // section-button -->

            <asp:Label ID="lblTotalCnt" runat="server" />
            <div class="title"><!-- title -->
		        <h3 class="title title-success">
                    엔트리
		        </h3>
	        </div><!-- // title -->
            <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
                <asp:Repeater ID="rpEntryList" runat="server">
                <HeaderTemplate>
                    <colgroup>
			            <col style="width: 5%;">
                        <col style="width: 10%;">
			            <col style="width: 20%;">
			            <col style="width: 25%;">
			            <col style="width: 40%;">
		            </colgroup>
                    <tr>
                        <th></th>
				        <th>Entry No</th>
				        <th>대단원</th>
				        <th>중단원</th>
				        <th>타이틀</th>
                    </tr>
<%
    if (rpEntryList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="5" style="text-align:center">조회된 Entry가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox ID="cbSelect" runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblEntryNo" Text=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat ="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblLUnit" Text=<%#DataBinder.Eval(Container.DataItem , "LUnit") %> runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblMUnit" Text=<%#DataBinder.Eval(Container.DataItem , "MUnit") %> runat="server" />
                        </td>
                        <td>
                            <asp:Label ID="lblTitle" Text=<%#DataBinder.Eval(Container.DataItem , "Title") %> runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table><!-- // table-a -->
            <div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="SaveButton" class="btn btn-lg btn-success" text="저장" OnClick="SaveButton_Click"/>
		    </div><!-- // section-button -->
        </form>
    </div>
</body>
</html>
