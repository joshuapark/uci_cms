﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignIn_Agree.aspx.cs" Inherits="main_MyInfo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>iCareD 관리자 사이트</title>
    <link rel="shortcut icon" href="images/favicon.png" /> 
    <link href="css/bootstrap.css" rel="stylesheet" />

    <script type="text/javascript">
    <!--
        function checkToggle() {
            if (document.getElementById("allChk").checked) {
                document.getElementById("termAgree").checked = true;
                document.getElementById("privacyAgree").checked = true;
            }
            else {
                document.getElementById("termAgree").checked = false;
                document.getElementById("privacyAgree").checked = false;
            }
        }
        function formCheck() {
            if (document.getElementById("termAgree").checked && document.getElementById("privacyAgree").checked) {
                document.addform.submit();
            }
            else {

                if (document.getElementById("termAgree").checked) {
                    alert("개인정보 수집 및 이용에 동의해주세요.");
                    document.getElementById("privacyAgree").focus();
                }
                else {
                    alert("이용약관에 동의해주세요.");
                    document.getElementById("termAgree").focus();
                }
                return false;
            }
        } 
    //-->
    </script>
</head>
<body>
	<nav class="navbar navbar-inverse">
	    <div class="container">
    	    <div class="navbar-header">
			    <a class="navbar-brand">iCareD 회원가입 </a> 
		    </div>
		    <div class="collapse navbar-collapse">
			    <ul class="nav navbar-nav">
				    <li><a>환영합니다! </a></li>		
			    </ul>	
		    </div>		
        </div>
	</nav>
    <form name="addform" action="SignIn_Register.aspx">
	<div class="container">
		<div class="row" >
			<nav class="navbar navbar-default navbar-static-top" style="padding: 15px;">
				<ul class="nav nav-pills nav-justified">	
					<li class="active"><a>약관 및 정보이용 동의 </a></li>
					<li ><a>회원정보 입력 </a></li>
					<li ><a>가입 완료</a></li>
				</ul>				
			</nav>
		</div>

		<div class="row">
			<div class="panel panel-default" >
				<iframe src="terms.html" width="100%" height="200" frameborder="0">
				</iframe>
			</div>
			<div class="checkbox">
				<label>
				<input type="checkbox" id="termAgree" value="1" />
				이용약관 동의 (필수)
				</label>
			</div>
		</div>	
	    <br />
		<div class="row">
			<div class="panel panel-default">
				<iframe src="privacy.html" width="100%" height="200" frameborder="0">
				</iframe>
			</div>
			<div class="checkbox">
				<label>
				<input type="checkbox" id="privacyAgree" value="1" />
				개인정보 수집 및 이용에 대한 안내 (필수)
				</label>
			</div>
        </div>
		<div class="row">
			<div class="checkbox">
			    <label>
			        <input type="checkbox" id="allChk" value="" onclick="checkToggle();" />
			        이용약관, 개인정보 수집 및 이용에 모두 동의합니다.
			    </label>
			    <center>	
			        <button type="button" class="btn btn-primary btn-lg" onclick="formCheck();">동의 </button>
			        <button type="button" class="btn btn-default btn-lg" onclick="window.close();">비동의 </button>	
			    </center>
		    </div>	
	    </div>
    </div>
    </form>
</body>
</html>