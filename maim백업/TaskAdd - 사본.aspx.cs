﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Data.OleDb;
public partial class notice_List : System.Web.UI.Page 
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";
    private int totalCnt = 0;
    public string lblPage = string.Empty;
    private string strTaskId = string.Empty;
    private string strTaskIdx = string.Empty;
    public string strMUnitArray = string.Empty;
    protected DataView dvLUnit;
    private DataTable dtMUnit= new DataTable();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //권한 처리-- 관리자가 아니면 로그인 페이지로
        if (Session["uauth"].ToString() != "9")
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;

        //string strUIdx = Session["uidx"].ToString();
        if (!IsPostBack)
        {
            if (Request.Params["pageNo"] != null)
                pageNo =Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();

            BindCheckBox();

            BindUser();            
        }
        Page.MaintainScrollPositionOnPostBack = true;
        //else
        //{
        //    Listing();
        //    //BindMUnit();
        //}        
    }
    private void BindCheckBox()
    {
        DBFileInfo();
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, CodeType, Code, CodeName FROM TCode WHERE DelFlag=0 ORDER BY Idx ASC", Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TCode");
        //Set up the data binding. 
        Conn.Close();
        DataTable dtRevision = ds.Tables["TCode"];
        dtRevision.DefaultView.RowFilter = "CodeType = 1";
        rbtRevision.DataSource = dtRevision;
        rbtRevision.DataTextField = "CodeName";
        rbtRevision.DataValueField = "Idx";
        rbtRevision.CssClass = "de-radio";
        rbtRevision.DataBind();

        DataTable dtSubjectGroup = ds.Tables["TCode"];
        dtSubjectGroup.DefaultView.RowFilter = "CodeType = 2";
        rbtSubjectGroup.DataSource = dtSubjectGroup;
        rbtSubjectGroup.DataTextField = "CodeName";
        rbtSubjectGroup.DataValueField = "Idx";
        rbtSubjectGroup.CssClass = "de-radio";
        rbtSubjectGroup.DataBind();

        DataTable dtSubject = ds.Tables["TCode"];
        dtSubject.DefaultView.RowFilter = "CodeType = 3";
        rbtSubject.DataSource = dtSubject;
        rbtSubject.DataTextField = "CodeName";
        rbtSubject.DataValueField = "Idx";
        rbtSubject.CssClass = "de-radio";
        rbtSubject.DataBind();

        DataTable dtSchool = ds.Tables["TCode"];
        dtSchool.DefaultView.RowFilter = "CodeType = 4";
        rbtSchool.DataSource = dtSchool;
        rbtSchool.DataTextField = "CodeName";
        rbtSchool.DataValueField = "Idx";
        rbtSchool.CssClass = "de-radio";
        rbtSchool.DataBind();

        DataTable dtBrand = ds.Tables["TCode"];
        dtBrand.DefaultView.RowFilter = "CodeType = 5";
        rbtBrand.DataSource = dtBrand;
        rbtBrand.DataTextField = "CodeName";
        rbtBrand.DataValueField = "Idx";
        rbtBrand.CssClass = "de-radio";
        rbtBrand.DataBind();

        DataTable dtGrade = ds.Tables["TCode"];
        dtGrade.DefaultView.RowFilter = "CodeType = 6";
        rbtGrade.DataSource = dtGrade;
        rbtGrade.DataTextField = "CodeName";
        rbtGrade.DataValueField = "Idx";
        rbtGrade.CssClass = "de-radio";
        rbtGrade.DataBind();

        DataTable dtSemester = ds.Tables["TCode"];
        dtSemester.DefaultView.RowFilter = "CodeType = 7";
        rbtSemester.DataSource = dtSemester;
        rbtSemester.DataTextField = "CodeName";
        rbtSemester.DataValueField = "Idx";
        rbtSemester.CssClass = "de-radio";
        rbtSemester.DataBind();

        //Close the connection.
        dtRevision = null;
        dtSubjectGroup = null;
        dtSubject = null;
        dtSchool = null;
        dtBrand = null;
        dtGrade = null;
        dtSemester = null;
        sda = null;
    }
    private void BindUser()
    {
        ListItem li = new ListItem("선택", "0");
        ddlUser.Items.Add(li);
        ddlUser.AppendDataBoundItems = true;
        
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        Con.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, UserName FROM TUser WHERE UserType='작업자' AND DelFlag=0 ORDER BY UserName ASC", Con);

        DataSet ds = new DataSet();
        sda.Fill(ds, "TUser");
        //Set up the data binding. 
        Con.Close();
        DataTable dtUser = ds.Tables["TUser"];
        ddlUser.DataSource = dtUser;
        ddlUser.DataTextField = "UserName";
        ddlUser.DataValueField = "Idx";
        ddlUser.DataBind();



        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        dtUser = null;
    }
    protected void CreateTask()
    {
        int entryCnt = 0;
        if (txtAddEntryCnt.Text.Length > 0)
        {
            entryCnt = Convert.ToInt16(txtAddEntryCnt.Text.ToString());
        }

        strTaskId = txtTaskId.Text;
        if (strTaskId.Length > 0)
        {
            AddEntry(entryCnt);
        }
        else
        {
            strTaskId = ddlUser.SelectedValue + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            txtTaskId.Text = strTaskId;
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;

            Cmd.Parameters.Add("@TaskID", SqlDbType.VarChar, 50);
            Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@ManagerIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@TaskTitle", SqlDbType.NVarChar, 255);
            Cmd.Parameters.Add("@TaskContent", SqlDbType.NText);
            Cmd.Parameters.Add("@EntryCnt", SqlDbType.Int);

            Cmd.Parameters["@TaskID"].Value = strTaskId;
            Cmd.Parameters["@RevisionIdx"].Value = rbtRevision.SelectedValue;
            Cmd.Parameters["@SubjectGroupIdx"].Value = rbtSubjectGroup.SelectedValue;
            Cmd.Parameters["@SubjectIdx"].Value = rbtSubject.SelectedValue;
            Cmd.Parameters["@SchoolIdx"].Value = rbtSchool.SelectedValue;
            Cmd.Parameters["@BrandIdx"].Value = rbtBrand.SelectedValue;
            Cmd.Parameters["@GradeIdx"].Value = rbtGrade.SelectedValue;
            Cmd.Parameters["@SemesterIdx"].Value = rbtSemester.SelectedValue;
            Cmd.Parameters["@UserIdx"].Value = ddlUser.SelectedValue;
            Cmd.Parameters["@ManagerIdx"].Value = Convert.ToInt32(Session["uidx"].ToString());
            Cmd.Parameters["@TaskTitle"].Value = txtTaskTitle.Text.Trim();
            Cmd.Parameters["@TaskContent"].Value = txtTaskContent.Text.Trim();
            Cmd.Parameters["@EntryCnt"].Value = entryCnt;
            Cmd.CommandText = "USP_Task_INSERT";
            Cmd.CommandType = CommandType.StoredProcedure;
            Con.Open();
            SqlDataAdapter sda0 = new SqlDataAdapter(Cmd);
            DataSet ds0 = new DataSet();
            sda0.Fill(ds0);

            txtTaskIdx.Text = ds0.Tables[0].Rows[0]["taskIdx"].ToString();
            //Cmd.ExecuteNonQuery();
            Con.Close();

            Cmd = null;
            Con = null;

            Listing();

            //CreateTask.Visible = false;
        }
    }

    private void Listing()
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text;
        if (strTaskId.Length > 0)
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.CommandText = "SELECT * FROM TTaskTempData WHERE TaskIdx=" + strTaskIdx + " AND DelFlag=0 ORDER BY Idx ASC";
            Cmd.CommandType = CommandType.Text;

            SqlDataAdapter sda = new SqlDataAdapter(Cmd);
            DataSet ds = new DataSet();

            sda.Fill(ds);
            EntryList.DataSource = ds;
            EntryList.DataBind();

            if (Con.State == ConnectionState.Open)
                Con.Close();

            Cmd = null;
            Con = null;
        }
    }
    protected void EntryList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        DropDownList ddlLUnitID = e.Item.FindControl("ddlLUnitID") as DropDownList;
        HiddenField txtLUnitID = e.Item.FindControl("txtLUnitID") as HiddenField;
        if (ddlLUnitID != null)
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();
            string strQuery = string.Empty;
            strQuery = "SELECT Idx, LUnitName FROM TLUnit WHERE RevisionIdx=" + rbtRevision.SelectedValue
                + " AND SubjectGroupIdx=" + rbtSubjectGroup.SelectedValue + " AND SubjectIdx=" + rbtSubject.SelectedValue
                + " AND SchoolIdx=" + rbtSchool.SelectedValue + " AND BrandIdx=" + rbtBrand.SelectedValue
                + " AND GradeIdx=" + rbtGrade.SelectedValue + " AND SemesterIdx=" + rbtSemester.SelectedValue 
                + " AND DelFlag=0 ORDER BY LUnitName ASC";
            SqlDataAdapter sdaLUnit = new SqlDataAdapter(strQuery, Con);

            DataSet dsLUnit = new DataSet();
            sdaLUnit.Fill(dsLUnit, "TLUnit");
            //Set up the data binding. 
            Con.Close();

            ddlLUnitID.DataSource = dsLUnit; //your datasource
            ddlLUnitID.DataTextField = "LUnitName";
            ddlLUnitID.DataValueField = "Idx";
            //ddlLUnitID.SelectedIndexChanged += ddlLUnitID_OnSelectedIndexChanged;
            ddlLUnitID.DataBind();
        }
        if (txtLUnitID.Value.Length>0)
        {
            ddlLUnitID.SelectedValue = txtLUnitID.Value;
        }
        DropDownList ddlMUnitID = e.Item.FindControl("ddlMUnitID") as DropDownList;
        HiddenField txtMUnitID = e.Item.FindControl("txtMUnitID") as HiddenField;
        
        if (ddlMUnitID != null)
        {           
            if (ddlLUnitID.SelectedValue.Length > 0)
            {
                DBFileInfo(); 
                SqlConnection Con = new SqlConnection(connectionString);
                Con.Open();
                string strQuery = string.Empty;
                strQuery = "SELECT Idx, MUnitName FROM TMUnit Where "
                    + " LUnitIdx=" + ddlLUnitID.SelectedValue + " AND DelFlag=0 ORDER BY Idx ASC";
                SqlDataAdapter sda = new SqlDataAdapter(strQuery, Con);

                DataSet dsMUnit = new DataSet();
                sda.Fill(dsMUnit, "list_data");
                //Set up the data binding. 
                ddlMUnitID.DataSource = dsMUnit; //your datasource
                ddlMUnitID.DataTextField = "MUnitName";
                ddlMUnitID.DataValueField = "Idx";
                ddlMUnitID.DataBind();
                Con.Close();
            }            
        }
        if (txtMUnitID.Value.Length > 0)
        {
            ddlMUnitID.SelectedValue = txtMUnitID.Value;
        }
            
    }
    //private void BindMUnit(string LUnitID)
    //{
    //    //LUnit에 따라 MUnit 바인딩
    //    foreach (RepeaterItem item in EntryList.Items)
    //    {
    //        DropDownList ddlLUnitID = item.FindControl("ddlLUnitID") as DropDownList;
    //        DropDownList ddlMUnitID = item.FindControl("ddlMUnitID") as DropDownList;
    //        HiddenField txtLUnitID = item.FindControl("txtLUnitID") as HiddenField;
    //        HiddenField txtMUnitID = item.FindControl("txtMUnitID") as HiddenField;

    //        DBFileInfo();

    //        SqlConnection Con = new SqlConnection(connectionString);
    //        Con.Open();
    //        string strQuery = string.Empty;
    //        strQuery = "SELECT Idx, MUnitName FROM TMUnit Where "
    //            + " LUnitID=" + ddlLUnitID.SelectedValue + " AND DelFlag=0 ORDER BY Idx ASC";
    //        SqlDataAdapter sda = new SqlDataAdapter(strQuery, Con);

    //        DataSet dsMUnit = new DataSet();
    //        sda.Fill(dsMUnit, "list_data");
    //        //Set up the data binding. 
    //        ddlMUnitID.DataSource = dsMUnit; //your datasource
    //        ddlMUnitID.DataTextField = "MUnitName";
    //        ddlMUnitID.DataValueField = "Idx";
    //        ddlMUnitID.DataBind();
    //        Con.Close();

    //        if (txtLUnitID.Value.Length > 0)
    //            ddlLUnitID.SelectedValue = txtLUnitID.Value;
    //        if (txtMUnitID.Value.Length>0)
    //            ddlMUnitID.SelectedValue = txtMUnitID.Value;
            
    //    }

    //    //DBFileInfo();

    //    //SqlConnection Con = new SqlConnection(connectionString);
    //    //Con.Open();
    //    //string strMUnitCode = string.Empty;
    //    //strMUnitCode = rbtRevision.SelectedValue + "-" + rbtSubjectGroup.SelectedValue + "-" + rbtSubject.SelectedValue + rbtSchool.SelectedValue + rbtBrand.SelectedValue + "-" + rbtGrade.SelectedValue + rbtSemester.SelectedValue;
    //    //string strQuery = string.Empty;
    //    //strQuery = "SELECT MUnitID, MUnitName FROM TMUnit Where "
    //    //    + "Left(LUnitCode, 10)='" + strMUnitCode + "' AND DelFlag=0 ORDER BY Idx ASC";
    //    //SqlDataAdapter sda = new SqlDataAdapter(strQuery, Con);
    //    ////SqlDataAdapter sda = new SqlDataAdapter("SELECT Idx, Title, Markup FROM TRecType ORDER BY Idx ASC", Con);

    //    //DataSet ds = new DataSet();
    //    //sda.Fill(ds, "list_data");

    //    ////Set up the data binding. 
    //    //Con.Close();
    //    //dtMUnit = ds.Tables["list_data"];
    //}

    protected void btnAddTask_Click(object sender, EventArgs e)
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text; 
        if (strTaskIdx.Length > 0)
        {
            SaveEntry();
            int entryCnt = 0;
            if (txtAddEntryCnt.Text.Length > 0)
            {
                entryCnt = Convert.ToInt16(txtAddEntryCnt.Text.ToString());
                AddEntry(entryCnt);
            } 
        }
        else
        {
            CreateTask();
        }
    }
    protected void AddEntry(int entryCnt)
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@EntryCnt", SqlDbType.Int);
        int intTaskIdx=0;
        if (strTaskIdx.Length > 0)
            intTaskIdx = Convert.ToInt16(strTaskIdx);
        Cmd.Parameters["@TaskIdx"].Value = intTaskIdx;
        Cmd.Parameters["@EntryCnt"].Value = entryCnt;
        Cmd.CommandText = "USP_TaskAddEntry_INSERT";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;
        // 여기서 Listing을 할 것인가? Repeater(EntryList)에 Item을 추가하여 바인드 할 것인가?
        SaveInfo();
        Listing();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveInfo();
        SaveEntry();
        Response.Write("<script>alert('임시저장이 완료되었습니다.');</script>");
        //string script = "<script>alert('임시저장이 완료되었습니다.');</script>";
        //ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        //Listing();
        //BindMUnit();
    }
    protected void SaveEntry()
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text;
        if (EntryList.Items.Count == 0)
        {
            return;
        }

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();

        //엔트리 내용을 저장함. Repeater로부터 데이터를 가져와야함.
        foreach (RepeaterItem item in EntryList.Items)
        {
            HiddenField txtIDX = item.FindControl("txtIDX") as HiddenField;
            DropDownList ddlLUnitID = item.FindControl("ddlLUnitID") as DropDownList;
            DropDownList ddlMUnitID = item.FindControl("ddlMUnitID") as DropDownList;
            TextBox txtTitle = item.FindControl("txtTitle") as TextBox;

            Cmd = new SqlCommand();
            Cmd.Connection = Con;
            Cmd.CommandText = "UPDATE TTaskTempData SET LUnitIdx='" + ddlLUnitID.SelectedValue + "', MUnitIdx='" + ddlMUnitID.SelectedValue + "', EntryTitle='" + txtTitle.Text + "' WHERE Idx=" + txtIDX.Value + "";
            Cmd.CommandType = CommandType.Text;

            Con.Open();
            Cmd.ExecuteNonQuery();
            if (Con.State == ConnectionState.Open)
                Con.Close();
        }
        if (Con.State == ConnectionState.Open)
            Con.Close();
    }
    protected void btnPublish_Click(object sender, EventArgs e)
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text;
        SaveInfo();
        SaveEntry();
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        int intEntryCount=0;
        if (EntryList!=null)
            intEntryCount = EntryList.Items.Count;
        Cmd.Parameters.Add("@TaskIdx", SqlDbType.VarChar, 50);
        Cmd.Parameters.Add("@EntryCount", SqlDbType.Int);
        Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
        Cmd.Parameters["@EntryCount"].Value = intEntryCount;
        Cmd.CommandText = "USP_TaskPublish_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        string script = "<script>alert('최종배포되었습니다.');location.href='TaskList.aspx';</script>";
        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        return;
        //Response.Write("<script>alert('최종배포되었습니다.');</script>"); 
        //Response.Redirect("TaskList.aspx");

    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    //[System.Web.Services.WebMethod]
    //public static ArrayList GetMUnitList(string LUnitCode)
    //{
    //    //return "Hello " + name + Environment.NewLine + "The Current Time is: "
    //    //    + DateTime.Now.ToString();

    //    ArrayList list = new ArrayList();
    //    String strConnString = ConfigurationManager
    //        .ConnectionStrings["JConnectionString"].ConnectionString;
    //    String strQuery = "USP_MUnit_LIST_SELECT";

    //    using (SqlConnection con = new SqlConnection(strConnString))
    //    {
    //        using (SqlCommand cmd = new SqlCommand())
    //        {
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.Add("@LUnitCode", SqlDbType.NChar);

    //            cmd.Parameters["@LUnitCode"].Value = LUnitCode;

    //            cmd.CommandText = strQuery;
    //            cmd.Connection = con;
    //            con.Open();
    //            SqlDataReader sdr = cmd.ExecuteReader();
    //            while (sdr.Read())
    //            {
    //                list.Add(new ListItem(
    //               sdr["MUnitName"].ToString(),
    //               sdr["Idx"].ToString()
    //                ));
    //            }
    //            con.Close();
    //            return list;
    //        }
    //    }

    //}
    //private void PopulateDropDownList(ArrayList list, DropDownList ddl)
    //{
    //    ddl.DataSource = list;
    //    ddl.DataTextField = "Text";
    //    ddl.DataValueField = "Value";
    //    ddl.DataBind();
    //}


        //DropDownList ddlMUnitID = e.Item.FindControl("ddlMUnitID") as DropDownList;

        //if (ddlMUnitID != null)
        //{
        //    DBFileInfo();
        //    SqlConnection Con = new SqlConnection(connectionString);

        //    Con.Open();
        //    SqlDataAdapter sdaLUnit = new SqlDataAdapter("SELECT Idx, MUnitName FROM TMUnit WHERE LUnitID=" + ddlLUnitID.SelectedValue + " AND DelFlag=0 ORDER BY MUnitName ASC", Con);

        //    DataSet dsMUnit = new DataSet();
        //    sdaLUnit.Fill(dsMUnit, "TMUnit");
        //    //Set up the data binding. 
        //    Con.Close();

        //    ddlMUnitID.DataSource = dsMUnit; //your datasource
        //    ddlMUnitID.DataTextField = "MUnitName";
        //    ddlMUnitID.DataValueField = "Idx";
        //    ddlMUnitID.DataBind();

        //    if (DataBinder.Eval(e.Item.DataItem, "Idx") != null)
        //    {
        //        string selectDdlMUnitVal = DataBinder.Eval(e.Item.DataItem, "Idx").ToString();
        //        ddlMUnitID.SelectedValue = selectDdlMUnitVal;
        //    }
        //}
        //
   
    protected void ddlLUnitID_OnSelectedIndexChanged(Object sender, EventArgs e)
    {
        DropDownList ddlLUnitID = (DropDownList)sender;
        RepeaterItem item = (RepeaterItem)ddlLUnitID.Parent;
        HiddenField txtLUnitID = (HiddenField)item.FindControl("txtLUnitID");
        txtLUnitID.Value = ddlLUnitID.SelectedValue;
        //Listing();
        foreach (RepeaterItem ritem in EntryList.Items)
        {
            if (item.ItemIndex == ritem.ItemIndex)  //ddlMUnitID != null)
            {
                DropDownList ddlMUnitID = ritem.FindControl("ddlMUnitID") as DropDownList;

                DBFileInfo();

                SqlConnection Con = new SqlConnection(connectionString);
                Con.Open();
                string strQuery = string.Empty;
                strQuery = "SELECT Idx, MUnitName FROM TMUnit Where "
                    + "LUnitIdx=" + ddlLUnitID.SelectedValue + " AND DelFlag=0 ORDER BY Idx ASC";
                SqlDataAdapter sda = new SqlDataAdapter(strQuery, Con);
                DataSet dsLUnit = new DataSet();
                sda.Fill(dsLUnit, "list_data");
                //Set up the data binding. 

                ddlMUnitID.DataSource = dsLUnit; //your datasource
                ddlMUnitID.DataTextField = "MUnitName";
                ddlMUnitID.DataValueField = "Idx";
                ddlMUnitID.DataBind();
                Con.Close();
                ddlMUnitID.Focus();                                
            }
        }
        
    }
    protected void EntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string delID;
        RepeaterItem repeaterItem = EntryList.Items[e.Item.ItemIndex];
        HiddenField txtIDX = repeaterItem.FindControl("txtIDX") as HiddenField;
        delID = txtIDX.Value;

        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand("UPDATE TTaskTempData SET DelFlag=1 WHERE Idx = " + delID, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

        SaveInfo();
        SaveEntry(); 
        Listing();
    }
    protected void SaveInfo()
    {
        strTaskId = txtTaskId.Text;
        strTaskIdx = txtTaskIdx.Text;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@UserIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@ManagerIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@TaskTitle", SqlDbType.NVarChar, 255);
        Cmd.Parameters.Add("@TaskContent", SqlDbType.NText);
        Cmd.Parameters.Add("@EntryCnt", SqlDbType.Int);
        Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
        Cmd.Parameters["@RevisionIdx"].Value = rbtRevision.SelectedValue;
        Cmd.Parameters["@SubjectGroupIdx"].Value = rbtSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = rbtSubject.SelectedValue;
        Cmd.Parameters["@SchoolIdx"].Value = rbtSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = rbtBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = rbtGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = rbtSemester.SelectedValue;
        Cmd.Parameters["@UserIdx"].Value = ddlUser.SelectedValue;
        Cmd.Parameters["@ManagerIdx"].Value = Convert.ToInt32(Session["uidx"].ToString());
        Cmd.Parameters["@TaskTitle"].Value = txtTaskTitle.Text.Trim();
        Cmd.Parameters["@TaskContent"].Value = txtTaskContent.Text.Trim();
        Cmd.Parameters["@EntryCnt"].Value = 0;

        Cmd.CommandText = "USP_Task_UPDATE";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        Cmd.ExecuteNonQuery();
        Con.Close();

        Cmd = null;
        Con = null;

    }
    protected void btn_ExcelUp_Click(object sender, EventArgs e)
    {
        //0.파일이 있는지 확인
        if ((null == fuExcel.PostedFile) || (0 >= fuExcel.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.
            Response.Write("파일을 선택해 주세요");
            return;
        }

        //1.파일을 서버에 업로드 한다.
        //1-1.업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        string sFileUri = HttpContext.Current.Server.MapPath("~/Data")
                          + string.Format(@"\{0}", fuExcel.PostedFile.FileName);
        //1-2.이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try 
        {
            //1-3 파일 업로드
            fuExcel.PostedFile.SaveAs(sFileUri);
        }
        catch (Exception ex)
        {
            //1-3-1 오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다<br/>오류내용 : "
                            + ex.ToString());
            return;
        }
        //2. 엑셀에서 MUnit을 추출한다.
        try
        {
            DataTable dtExcel = new DataTable();
            //string SourceConstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + sFileUri + "';Extended Properties= 'Excel 8.0;HDR=Yes;IMEX=1'";
            string SourceConstr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + sFileUri + ";Extended Properties=Excel 12.0;";
            OleDbConnection con = new OleDbConnection(SourceConstr);
            string strQuery = "Select * from [Sheet1$]";
            OleDbDataAdapter data = new OleDbDataAdapter(strQuery, con);
            data.Fill(dtExcel);
            DBFileInfo();
            SqlConnection Con = new SqlConnection();
            SqlCommand Cmd = new SqlCommand();
            int count = 0;
            //#### 분류가 동일한지 체크한다.
            if (CheckCategory(dtExcel))
            {
                int taskIdx = MakeTaskIdx();
                
                for (int i = 0; i < dtExcel.Rows.Count; i++)
                {
                    try
                    {
                        //### 엑셀 행별로 Task Data(Entry)를 생성한다.
                        strQuery = "insert into TTaskTempData (MUnitIdx, EntryTitle, SeqNo, TaskIdx) values(" + dtExcel.Rows[i][0] + ",'" + dtExcel.Rows[i][1] + "'," + (i + 1) + ", " + taskIdx + ")";
                        DBFileInfo();
                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();

                        count++;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }
                }
                if (count == dtExcel.Rows.Count)
                {
                    try
                    {
                        //### 입력한 Task Data(Entry)의 MUnitIdx가 속한 LUnitIdx를 찾아서 업데이트 해준다.
                        strQuery = "UPDATE TTaskTempData SET LUnitIdx=(SELECT LUnitIdx FROM TMUnit WHERE Idx=TTaskTempData.MUnitIdx AND TTaskTempData.TaskIdx=" + taskIdx + ") ";
                        DBFileInfo();
                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    try
                    {
                        //### 입력한 TaskID의 엔트리카운트를 업데이트 해준다.
                        strQuery = "UPDATE TTaskId SET EntryCount=" + count + " WHERE Idx=" + taskIdx + " ";
                        DBFileInfo();
                        Con = new SqlConnection(connectionString);
                        Cmd = new SqlCommand(strQuery, Con);
                        Cmd.CommandType = CommandType.Text;
                        Con.Open();
                        Cmd.ExecuteNonQuery();
                        Con.Close();
                        string script = "<script>alert('등록이 완료되었습니다.');</script>";
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
                    }
                    catch (Exception ex)
                    {
                        throw ex; ;
                    }
                }
                else
                {
                    Response.Write("엑셀 행수와 입력한 엔트리 숫자가 다릅니다.");
                }
            }
            else
            {
                string script = "<script>alert('분류정보가 다른 중단원의 엔트리가 있습니다. 엑셀파일을 확인해 주세요.');</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            //conLinq.Dispose();
            Listing();
        }
    }
    private int MakeTaskIdx()
    {
        //DataClasses DataContext conLinq = new DataClassesDataContext("Data Source=server name;Initial Catalog=Database Name;Integrated Security=true");
        strTaskId = ddlUser.SelectedValue + "_" + DateTime.Now.ToString("yyyyMMddHHmmss");
        txtTaskId.Text = strTaskId;
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@TaskID", SqlDbType.VarChar, 50);
        Cmd.Parameters.Add("@RevisionIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectGroupIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SubjectIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SchoolIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@BrandIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@GradeIdx", SqlDbType.Int);
        Cmd.Parameters.Add("@SemesterIdx", SqlDbType.Int);
        Cmd.Parameters["@TaskID"].Value = strTaskId;
        Cmd.Parameters["@RevisionIdx"].Value = rbtRevision.SelectedValue;
        Cmd.Parameters["@SubjectGroupIdx"].Value = rbtSubjectGroup.SelectedValue;
        Cmd.Parameters["@SubjectIdx"].Value = rbtSubject.SelectedValue;
        Cmd.Parameters["@SchoolIdx"].Value = rbtSchool.SelectedValue;
        Cmd.Parameters["@BrandIdx"].Value = rbtBrand.SelectedValue;
        Cmd.Parameters["@GradeIdx"].Value = rbtGrade.SelectedValue;
        Cmd.Parameters["@SemesterIdx"].Value = rbtSemester.SelectedValue;

        Cmd.CommandText = "USP_Task_INSERT";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        SqlDataAdapter sda0 = new SqlDataAdapter(Cmd);
        DataSet ds0 = new DataSet();
        sda0.Fill(ds0);

        
        int taskIdx = Convert.ToInt32(ds0.Tables[0].Rows[0]["taskIdx"].ToString());
        txtTaskIdx.Text = taskIdx.ToString();
        
        if(Con.State==ConnectionState.Open)
            Con.Close();
        Cmd = null;
        Con = null;

        return taskIdx;
    }
    private bool CheckCategory(DataTable dtExcel)
    {
        bool rtnBool = true;

        string strQuery;
        DBFileInfo();
        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();
        try 
        {
            foreach(DataRow row in dtExcel.Rows)
            {
                strQuery = "SELECT RevisionIdx, SubjectGroupIdx, SubjectIdx, SchoolIdx, BrandIdx, GradeIdx, SemesterIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx "
                    + " AND RevisionIdx=(SELECT RevisionIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND SubjectGroupIdx=(SELECT SubjectGroupIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND SubjectIdx=(SELECT SubjectIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND SchoolIdx=(SELECT SchoolIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND BrandIdx=(SELECT BrandIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND GradeIdx=(SELECT GradeIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND SemesterIdx=(SELECT SemesterIdx FROM TMUnit M, TLUnit L WHERE L.Idx=M.LUnitIdx AND M.Idx="+ row[0] +" )"
                    + " AND M.Idx="+ dtExcel.Rows[0][0] +"";
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                //Cmd.CommandType = CommandType.Text;
                //Cmd.Connection = Con;
                Con.Open();
                SqlDataReader reader = Cmd.ExecuteReader();
                if (reader.Read())
                {
                    rbtRevision.SelectedValue = reader["RevisionIdx"].ToString();
                    rbtSubjectGroup.SelectedValue = reader["SubjectGroupIdx"].ToString();
                    rbtSubject.SelectedValue = reader["SubjectIdx"].ToString();
                    rbtSchool.SelectedValue = reader["SchoolIdx"].ToString();
                    rbtBrand.SelectedValue = reader["BrandIdx"].ToString();
                    rbtGrade.SelectedValue = reader["GradeIdx"].ToString();
                    rbtSemester.SelectedValue = reader["SemesterIdx"].ToString();
                    rbtRevision.Enabled = false;
                    rbtSubjectGroup.Enabled = false;
                    rbtSubject.Enabled = false;
                    rbtSchool.Enabled = false;
                    rbtBrand.Enabled = false;
                    rbtGrade.Enabled = false;
                    rbtSemester.Enabled = false;
                    if (Con.State == ConnectionState.Open)
                        Con.Close();

                    continue;
                }                    
                else
                {
                    rtnBool=false;
                    if (Con.State == ConnectionState.Open)
                        Con.Close();
                    break;
                }               
            }
        }
        catch (Exception ex)
        {
            //2-1 오류다!
            Response.Write("분류를 분석하던 중 다음과 같은 오류가 발생 하였습니다<br/>오류내용 : "
                            + ex.ToString());
            rtnBool=false;
        }        
        return rtnBool;
    }
}
