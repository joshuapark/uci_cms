﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class main_BinderDirectoryAdd : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝

        if (Request.Params["ParentIdx"] != null) {
            hdParentIdx.Value = Request.Params["ParentIdx"];
        }

        if (Request.Params["ParentLevel"] != null) {
            hdParentLevel.Value = Request.Params["ParentLevel"];
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void AddBinderButton_Click(object sender, EventArgs e)
    {
        if (txtBinderTitle.Text.Length == 0)
        {
            showMessage("바인더명을 입력하여 주십시오.");
            txtBinderTitle.Focus();
            return;
        }

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@parentid", SqlDbType.Int);
        Cmd.Parameters.Add("@title", SqlDbType.NVarChar, 50);
        Cmd.Parameters.Add("@memo", SqlDbType.NText);
        Cmd.Parameters.Add("@level", SqlDbType.Int);
        Cmd.Parameters.Add("@useridx", SqlDbType.Int);

        Cmd.Parameters["@parentid"].Value = Convert.ToInt32(hdParentIdx.Value );
        Cmd.Parameters["@title"].Value = txtBinderTitle.Text;
        Cmd.Parameters["@memo"].Value = txtBinderMemo.Text;
        Cmd.Parameters["@level"].Value = Convert.ToInt32(hdParentLevel.Value) + 1;
        Cmd.Parameters["@useridx"].Value = 1;

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_Binder_INSERT_2";
        Con.Open();

        Cmd.ExecuteNonQuery();
        
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;


        Response.Write("<script>opener.doTheSubmit();self.close();</script>");
    }

    private void showMessage(String message)
    {
        string script = "alert(\"" + message + "\");";
        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
    }

}