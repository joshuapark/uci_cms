﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;
using System.Xml.Serialization;

public partial class XmlImport2 : System.Web.UI.Page
{
    private int intSortNo = 0;
    private string connectionString = string.Empty;
    private string strTaskIdx = string.Empty;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Params["taskIdx"] != null)
            strTaskIdx = Request.Params["taskIdx"].ToString();

        //if(!IsPostBack)
        //{
            GetTaskTitle();  // 작업명을 가져온다.
        //}

        if (lblXMLFileName.Text.Length < 5)  
        {
            btnImport.Enabled = false;
            //btnImgAnalysis.Enabled = false;
        }
        else
        {
            btnImport.Enabled = true;
            //btnImgAnalysis.Enabled = true;
        }
    }

    protected void GetTaskTitle()
    {
        string strQuery = "Select TaskTitle FROM TTaskID WHERE Idx=" + strTaskIdx + " ";
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        SqlDataReader reader;
        reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            lblTaskTitle.Text = reader["TaskTitle"].ToString();
        }
        reader.Close();
        Con.Close();
    }

    protected void btnUploadXML_Click(object sender, EventArgs e)
    {
        lblMessage.Text = ""; //메시지 창 초기화
        DBFileInfo();
        string strQuery = string.Empty;
        //0.파일이 있는지 확인
        if ((null == XMLFileUpload.PostedFile)
            || (0 >= XMLFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.
            lblMessage.Text = "파일을 선택해 주세요";
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        FileInfo fi = new FileInfo(XMLFileUpload.PostedFile.FileName);
        string FileNewName = "XML_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;
        string sFileUri = HttpContext.Current.Server.MapPath("~/cms100data/XmlData/") + FileNewName;
        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);
        
        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();

        try
        {
            //파일 업로드
            XMLFileUpload.PostedFile.SaveAs(sFileUri);
            //1-3. DB에 TaskIdx 기준으로 데이터를 저장한다.            
            Con = new SqlConnection(connectionString);
            Cmd = new SqlCommand();
            Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
            Cmd.Parameters.Add("@FileType", SqlDbType.Char);
            Cmd.Parameters.Add("@XMLFileName", SqlDbType.VarChar, 255);
            Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
            Cmd.Parameters["@FileType"].Value = "X";
            Cmd.Parameters["@XMLFileName"].Value = FileNewName;
            Cmd.CommandText = "USP_ImportFile_INSERT_UPDATE";
            Cmd.CommandType = CommandType.StoredProcedure;
            Cmd.Connection = Con;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
        }
        catch (Exception ex)
        {
            //오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString();
            return;
        }
        //1-4. XML 파싱, 작업에 해당하는 EntryIdx 및 중복 EntryIdx 파악을 위해 EntryIdx 저장.
        //1-4-0 먼저 TempTable TImportDulpCheck를 삭제한다.
        DeleteTImportDuplCheck(strTaskIdx);
        //1-4- 파일을 읽어들인다.
        XmlSerializer serializer = new XmlSerializer(typeof(edu));
        serializer.UnknownNode += new XmlNodeEventHandler(Serializer_UnknownNode);
        serializer.UnknownAttribute += new XmlAttributeEventHandler(Serializer_UnknownAttribute);
        
        FileStream fs = new FileStream(sFileUri, FileMode.Open);

        edu eduroot = (edu)serializer.Deserialize(fs);
        int intEntryCount = eduroot.ENTRY.Count();
        string strEntryIdx = string.Empty;
        string strENTRYTITLE = string.Empty;
        for (int i = 0; i < intEntryCount; i++) //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 시작
        {
            try
            {
                strEntryIdx = eduroot.ENTRY[i].entryCode;
                strENTRYTITLE = eduroot.ENTRY[i].ENTRYTITLE;
                strENTRYTITLE = strENTRYTITLE.Replace("'", "''");

                strQuery = "INSERT INTO TImportDuplCheck (TaskIdx, EntryIdx, EntryTitle) VALUES (" + strTaskIdx + ", " + strEntryIdx + ", '"+ strENTRYTITLE +"')";
                //#####  TImportDuplCheck 테이블에 저장한다  끝
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                //오류다!
                Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
                lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                                + ex.ToString().Substring(0, 130) + "...";
                DeleteTImportDuplCheck(strTaskIdx);
                return;
            }
        }    //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 끝
        
        fs.Close();

        //-- 1.4-1 TImportDuplCheck 테이블에 넣기완료 후 Valid Check// Task내 EntryIdx 검사
        strQuery = "SELECT A.EntryIdx, A.EntryTitle FROM TImportDuplCheck as A left outer join (SELECT Idx FROM TEntry WHERE TaskIdx=" + strTaskIdx + ") as B "
            + " on A.EntryIdx=B.Idx "
            + " WHERE B.Idx is null";
        SqlConnection Conn = new SqlConnection(connectionString);
        SqlCommand Cmnd = new SqlCommand(strQuery, Conn);
        Cmnd.CommandType = CommandType.Text;
        Conn.Open();
        SqlDataReader reader = Cmnd.ExecuteReader();
        string strMessage = string.Empty;
        int m = 1;
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                strMessage += m.ToString() + ". EntryCode오류:" + reader.GetInt32(0).ToString() + " (" + reader.GetString(1).ToString() + ")<br/>";
                m++;
            }
            lblMessage.Text = strMessage;
            if (Conn.State == ConnectionState.Open)
                Conn.Close();

            strQuery = "Delete From TImportDuplCheck Where TaskIdx=" + strTaskIdx + " ";
            Conn = new SqlConnection(connectionString);
            Cmnd = new SqlCommand(strQuery, Conn);
            Cmnd.CommandType = CommandType.Text;
            Conn.Open();
            Cmnd.ExecuteNonQuery();
            reader.Close();
            if (Conn.State == ConnectionState.Open)
                Conn.Close();
            /// 디버그용 디버그 후 삭제
            lblXMLFileName.Text = FileNewName;
        }
        else
        {
            if(Conn.State== ConnectionState.Open)
                Conn.Close();
            //-- 1.5 TImportDuplCheck 테이블에 넣기완료 후 Valid Check// 중복 Entry 검사
            strQuery = "SELECT A.EntryIdx, A.EntryTitle FROM TImportDuplCheck as A, (SELECT EntryIdx FROM TImportDuplCheck GROUP BY EntryIdx Having Count(Idx) > 1) as B "
                + " WHERE A.EntryIdx = B.EntryIdx Group By A.EntryIdx, A.EntryTitle";
            Conn = new SqlConnection(connectionString);
            Cmnd = new SqlCommand(strQuery, Conn);
            Cmnd.CommandType = CommandType.Text;
            Conn.Open();
            reader = Cmnd.ExecuteReader();
            strMessage = string.Empty;
            m = 1;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    strMessage += m.ToString() + ". 중복오류:" + reader.GetInt32(0).ToString() + " (" + reader.GetString(1).ToString() + ")<br/>";
                    m++;
                }
                lblMessage.Text = strMessage;
            }
            else
            {
                lblMessage.Text = "XML 업로드가 완료되었습니다.";
                lblXMLFileName.Text = FileNewName;
                btnImport.Enabled = true;
                Console.WriteLine("No rows found.");
            }
            if (Conn.State == ConnectionState.Open)
                Conn.Close(); 
            
            strQuery = "Delete From TImportDuplCheck Where TaskIdx=" + strTaskIdx + " ";
            Conn = new SqlConnection(connectionString);
            Cmnd = new SqlCommand(strQuery, Conn);
            Cmnd.CommandType = CommandType.Text;
            Conn.Open();
            Cmnd.ExecuteNonQuery();
            reader.Close();
            if (Conn.State == ConnectionState.Open)
                Conn.Close(); 
        }
        if (Conn.State == ConnectionState.Open)
            Conn.Close();
    }
    protected void DeleteTImportDuplCheck(string paramTaskIdx)
    {
        DBFileInfo();
        string strQuery = "Delete From TImportDuplCheck Where TaskIdx=" + paramTaskIdx + " ";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        Cmd.ExecuteNonQuery();
        if (Con.State == ConnectionState.Open)
            Con.Close();
        Con = null;
        Cmd = null;
    }

    protected void btnUploadImage_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //0.파일이 있는지 확인
        if ((null == ImageFileUpload.PostedFile)
            || (0 >= ImageFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.

            Response.Write("파일을 선택해 주세요");
            return;
        }

        //1.파일을 서버에 업로드 한다.
        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        FileInfo fi = new FileInfo(ImageFileUpload.PostedFile.FileName);
        string FileNewName = "IMG_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;

        string uploadPath = @"/cms100data/EntryData/";

        string sFileUri = HttpContext.Current.Server.MapPath(uploadPath) + FileNewName;
        //               + string.Format(@"\{0}", ImageFileUpload.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        ////1-2.업로드
        ////업로드될 dir경로를 만든다.
        ////string sFileUri = HttpContext.Current.Server.MapPath("~/")
        //FileInfo fi = new FileInfo(ImageFileUpload.PostedFile.FileName);
        //string FileNewName = "IMG_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;
        //string sFileUri = HttpContext.Current.Server.MapPath("~/UploadFile/") + FileNewName;
        ////               + string.Format(@"\{0}", ImageFileUpload.PostedFile.FileName);

        ////이미 같은 이름의 파일이 있으면 지워준다.
        //File.Delete(sFileUri);

        try
        {
            //### 3-1  UploadFile/ 폴더에 파일 업로드
            ImageFileUpload.PostedFile.SaveAs(sFileUri);
            string zipPath = sFileUri;

            //### 3-2-1 UploadFile/Extract 폴더 내 파일 삭제
            string extractPath = HttpContext.Current.Server.MapPath(uploadPath + "Extract/");
            string[] filelist = Directory.GetFiles(extractPath);
            
            foreach(string delfile in filelist)
            {
                if(delfile.Length>3)
                {
                    //Response.Write("파일삭제:" + delfile + "<br />");
                    File.Delete(delfile);
                }
            }

            System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
            
            //Response.Write("압축해제 성공<br />");

            ////### 3-3 UploadFile/Extract 폴더에서 EntryImage폴더로 복사
            //string copyPath = HttpContext.Current.Server.MapPath("~/UploadFile/EntryImage/");
            //string[] fileList = Directory.GetFiles(@extractPath);
            //foreach (string fileURI in fileList)
            //{
            //    string fileName = Path.GetFileName(fileURI);
            //    string newFileURI = copyPath + fileName;
            //    // Delete a file by using File class static method... 
            //    if (System.IO.File.Exists(@newFileURI))
            //    {
            //        // 삭제할것인가?
            //    }
            //    else
            //    {
            //        try
            //        {
            //            File.Copy(fileURI, newFileURI);
            //        }
            //        catch (Exception ex)
            //        {
            //            lblMessage.Text = "파일 복사중 에러가 발생하였습니다.";
            //        }
            //    }
            //    File.Delete(fileURI);
            //}

            DBFileInfo();
            string strQuery = string.Empty;
            try
            {
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
                Cmd.Parameters.Add("@FileType", SqlDbType.Char);
                Cmd.Parameters.Add("@XMLFileName", SqlDbType.VarChar, 255);
                Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
                Cmd.Parameters["@FileType"].Value = "I";
                Cmd.Parameters["@ImgFileName"].Value = FileNewName;
                Cmd.CommandText = "USP_ImportFile_INSERT_UPDATE";
                Cmd.CommandType = CommandType.StoredProcedure;
                Cmd.Connection = Con;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
            }
            catch (Exception ex)
            {
                lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                + ex.ToString();
            }
            finally
            {
                lblMessage.Text = "Image 파일을 업로드하였습니다.";
                lblImgFileName.Text = FileNewName;
            }

        }
        catch (Exception ex)
        {
            lblMessage.Text = "파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void btnImport_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //1-1.XML 파싱, Table로 Push: TEntry
        string strEntryIdx;
        string strENTRYTITLE = string.Empty;
        string strENTRYTITLE_E = string.Empty;
        string strENTRYTITLE_K = string.Empty;
        string strENTRYTITLE_C = string.Empty;
        string strATTRIBUTES = string.Empty;
        string strINDEXCONTENT = string.Empty;
        string strRELATEDSEARCH = string.Empty;
        string strQUIZ = string.Empty;
        string strTAG = string.Empty;
        string strSummary = string.Empty;

        string strEntType = string.Empty;
        string strRecType = string.Empty;
        string strQuery = string.Empty;
        string strICIdx = string.Empty;
        string strContent = string.Empty;
        string strExIdx = string.Empty;

        DBFileInfo();
        //### 해당 TaskIdx에 기존 데이터가 있으면 "처리 완료" 후 삭제하기 위한 정보저장
        strQuery = "Select Top 1 Idx FROM TEntryData WHERE TaskIdx=" + strTaskIdx + " Order By Idx Desc "; 
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Cmd.CommandType = CommandType.Text;
        Con.Open();
        SqlDataReader reader;
        reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        string delEntryDataIdx = string.Empty;
        if (reader.Read())
        {
            delEntryDataIdx = reader["Idx"].ToString();
        }
        reader.Close();
        Con.Close();

        //strQuery = "Select XMLFileName FROM TImportFile WHERE Idx=" + strTaskIdx + " "; 
        //SqlConnection Con = new SqlConnection(connectionString);
        //SqlCommand Cmd = new SqlCommand(strQuery, Con);
        //Cmd.CommandType = CommandType.Text;
        //Con.Open();
        //SqlDataReader reader;
        //reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        //string XMLFileName = string.Empty;
        //if (reader.Read())
        //{
        //    XMLFileName = reader["XMLFileName"].ToString();
        //}
        //reader.Close();
        //Con.Close();
        string XMLFileName = lblXMLFileName.Text;
        XmlDocument xmlDoc = new XmlDocument();
        string pathFile = HttpContext.Current.Server.MapPath("~/cms100data/XmlData/") + XMLFileName;
        //string pathFile = HttpContext.Current.Server.MapPath("~/UploadFile/") + XMLFileName;
        try
        {
            xmlDoc.Load(pathFile);
        }   
        catch (Exception ex)
        {
            //오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString().Substring(0, 130) + "...";
            return;
        }

        int intEntryCount = xmlDoc["edu"].ChildNodes.Count;
        int EntryChildCount = 0;
        int cntIContentChild = 0;
        int cntExampleChild = 0;

        //SqlConnection Con = new SqlConnection();
        //SqlCommand Cmd = new SqlCommand();
        //SqlDataReader reader;

        //참조 URL = https://msdn.microsoft.com/en-us/library/System.Xml.XmlDocument(v=vs.110).aspx
        try
        {
            for (int i = 0; i < intEntryCount; i++) //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 시작
            {
                EntryChildCount = xmlDoc["edu"].ChildNodes[i].ChildNodes.Count;
                intSortNo = 0;
                strEntryIdx = xmlDoc["edu"].ChildNodes[i].Attributes["entryCode"].Value;
                int intImgNo = 1; //이미지 분석 키를 위한 숫자
                for (int j = 0; j < EntryChildCount; j++)  //##### Entry 자식노드 갯수만큼 돌면서 파싱 시작
                {
                    strEntType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].Name;

                    switch (strEntType)  //-- 엔트리 1dept type구분. EntryTitle, IndexContent, Qize, Tag, Attribute
                    {

                        case "ENTRYTITLE":
                            strENTRYTITLE = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE = strENTRYTITLE.Replace("'", "''");
                            //##### 엔트리 생성 시 Title 가져오기 시작
                            strQuery = "SELECT EntryTitle FROM TEntry WHERE Idx=" + strEntryIdx + " ";
                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Connection = Con;
                            Con.Open();
                            reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            string originEntryTitle = string.Empty; 
                            if (reader.Read())
                            {
                                originEntryTitle = reader["EntryTitle"].ToString();
                            }
                            Con.Close();
                            //##### 엔트리 생성 시 Title 가져오기 끝
                            if(strENTRYTITLE != originEntryTitle.Replace("'", "''"))
                            {
                                strQuery = "UPDATE TEntry SET TitleFlag=1 WHERE Idx="+ strEntryIdx +"";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();
                                strQuery = "INSERT INTO TImportTitleCheck (EntryIdx, OriginalTitle, NewTitle, InsertDate) "
                                    + " VALUES (" + strEntryIdx + ", '" + originEntryTitle.Replace("'", "''")  +"', "
                                    + " '" + strENTRYTITLE + "', getdate() ) ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();
                            }
                            Console.WriteLine("Case 1");
                            break;
                        case "ENTRYTITLE_E":
                            strENTRYTITLE_E = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_E = strENTRYTITLE_E.Replace("'", "''");
                            Console.WriteLine("Case 2");
                            break;
                        case "ENTRYTITLE_C":
                            strENTRYTITLE_C = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_C = strENTRYTITLE_C.Replace("'", "''");
                            Console.WriteLine("Case 3");
                            break;
                        case "ENTRYTITLE_K":
                            strENTRYTITLE_K = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strENTRYTITLE_K = strENTRYTITLE_K.Replace("'", "''");
                            Console.WriteLine("Case 4");
                            break;
                        case "ATTRIBUTES":
                            strATTRIBUTES = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strATTRIBUTES = strATTRIBUTES.Replace("'", "''");
                            //##### ATTRIBUTES 입력
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strATTRIBUTES + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 5");
                            break;
                        case "SUMMARY":
                            strSummary = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strSummary = strSummary.Replace("'", "''");
                            //##### ATTRIBUTES 입력
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strSummary + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case Summary");
                            break;
                        case "INDEXCONTENT":
                            strINDEXCONTENT = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strINDEXCONTENT = strINDEXCONTENT.Replace("'", "''");
                            cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;

                            // arrImgFile --> IMAGE 태그 추출 내부에 여러 이미지가 포함될 수 있음
                            string[] arrImgFile = strINDEXCONTENT.ToString().Split(new string[] { "<IMAGE>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                int imageEnd = imgFile.ToString().IndexOf("</IMAGE>");

                                if (imageEnd > 0)
                                {
                                    string strCaption = string.Empty;
                                    string strDesc = string.Empty;
                                    string strLink = string.Empty;
                                    string strAlign = string.Empty;
                                    try 
                                    {
                                        //캡션추출
                                        int captionStart = imgFile.ToString().IndexOf("<CAPTION>");
                                        int captionEnd = imgFile.ToString().IndexOf("</CAPTION>");
                                        strCaption = imgFile.ToString().Substring(captionStart + 9, captionEnd - (captionStart + 9));
                                        //Description추출
                                        int descStart = imgFile.ToString().IndexOf("<DESCRIPTION>");
                                        int descEnd = imgFile.ToString().IndexOf("</DESCRIPTION>");
                                        strDesc = imgFile.ToString().Substring(descStart + 13, descEnd - (descStart + 13));
                                        //LINK추출
                                        int linkStart = imgFile.ToString().IndexOf("<LINK>");
                                        int linkEnd = imgFile.ToString().IndexOf("</LINK>");
                                        strLink = imgFile.ToString().Substring(linkStart + 6, linkEnd - (linkStart + 6));
                                        //ALIGN추출
                                        int alignStart = imgFile.ToString().IndexOf("<ALIGN>");
                                        int alignEnd = imgFile.ToString().IndexOf("</ALIGN>");
                                        strAlign = imgFile.ToString().Substring(alignStart + 7, alignEnd - (alignStart + 7));                                    
                                    }
                                    catch (Exception ex)
                                    {
                                        lblMessage.Text = "Desc, Caption 추출 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                                        + ex.ToString();
                                        return;
                                    }
                                    try
                                    {
                                        //Response.Write("데이터"+ Server.HtmlEncode(imgFile.ToString()) + "<br/><br/>");
                                        // arrImg --> IMG 태그 추출 하나의 이미지 빼내기
                                        string[] arrImg = imgFile.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                                        foreach (string imgEach in arrImg)
                                        {
                                            int imgEnd = imgEach.ToString().IndexOf("</FILENAME>");
                                            string strImageName = string.Empty;
                                            if (imgEnd > 0)
                                            {
                                                try
                                                {
                                                    //이미지명 추출
                                                    strImageName = imgEach.ToString().Substring(0, imgEnd);
                                                    //WIDTH추출
                                                    int widthStart = imgEach.ToString().IndexOf("<WIDTH>");
                                                    int widthEnd = imgEach.ToString().IndexOf("</WIDTH>");
                                                    string strWidth = imgEach.ToString().Substring(widthStart + 7, widthEnd - (widthStart + 7));
                                                    //HEIGHT추출
                                                    int heightStart = imgEach.ToString().IndexOf("<HEIGHT>");
                                                    int heightEnd = imgEach.ToString().IndexOf("</HEIGHT>");
                                                    string strHeight = imgEach.ToString().Substring(heightStart + 8, heightEnd - (heightStart + 8));
                                                    //새로운 이미지코드명 생성
                                                    FileInfo fi = new FileInfo(strImageName);
                                                    string strImageCode = strTaskIdx + "_" + strEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + "_" + intImgNo + fi.Extension;
                                                    SqlConnection Conn = new SqlConnection(connectionString);
                                                    SqlCommand Cmdn = new SqlCommand();
                                                    Cmdn.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
                                                    Cmdn.Parameters.Add("@TaskIdx", SqlDbType.Int);
                                                    Cmdn.Parameters.Add("@EntryIdx", SqlDbType.Int);
                                                    Cmdn.Parameters.Add("@OriginName", SqlDbType.VarChar);
                                                    Cmdn.Parameters.Add("@Description", SqlDbType.VarChar);
                                                    Cmdn.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
                                                    Cmdn.Parameters.Add("@Width", SqlDbType.VarChar, 10);
                                                    Cmdn.Parameters.Add("@Height", SqlDbType.VarChar, 10);
                                                    Cmdn.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

                                                    Cmdn.Parameters["@FileName"].Value = strImageCode;
                                                    Cmdn.Parameters["@TaskIdx"].Value = strTaskIdx;
                                                    Cmdn.Parameters["@EntryIdx"].Value = strEntryIdx;
                                                    Cmdn.Parameters["@OriginName"].Value = strImageName;
                                                    Cmdn.Parameters["@Description"].Value = Server.HtmlEncode(strDesc);
                                                    Cmdn.Parameters["@Caption"].Value = Server.HtmlEncode(strCaption);
                                                    Cmdn.Parameters["@Width"].Value = strWidth;
                                                    Cmdn.Parameters["@Height"].Value = strHeight;
                                                    Cmdn.Parameters["@FileType"].Value = "IMG";

                                                    //Response.Write(">>" + strImageCode + " / " + strTaskIdx + " / " + strEntryIdx + " / " + strImageName + " / " + Server.HtmlEncode(strDesc) + " / " + strDesc + " / " + Server.HtmlEncode(strCaption) + " /Wid" + strWidth + " /Height" + strHeight + "<br />");

                                                    Cmdn.CommandText = "USP_File_INSERT";
                                                    Cmdn.CommandType = CommandType.StoredProcedure;
                                                    Cmdn.Connection = Conn;
                                                    Conn.Open();
                                                    Cmdn.ExecuteNonQuery();
                                                    Conn.Close();
                                                    intImgNo++;
                                                    strINDEXCONTENT = strINDEXCONTENT.Replace(@strImageName, @strImageCode);
                                                }
                                                catch (Exception ex)
                                                {
                                                    lblMessage.Text = "이미지 정보 저장 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                                                    + ex.ToString();
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        lblMessage.Text = "IMG 저장 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                                                        + ex.ToString();
                                        return;
                                    }
                                }
                            }

                            //##### 대분류 입력 시작
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strINDEXCONTENT + "', " + cntIContentChild + "," + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();
                            //##### 대분류 입력 끝
                            //##### 대분류 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 시작
                            strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                                + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Connection = Con;
                            Con.Open();
                            reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            if (reader.Read())
                            {
                                strICIdx = reader["Idx"].ToString();
                            }
                            Con.Close();
                            //##### 대분류 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 끝                        
                            intSortNo++;
                            Console.WriteLine("Case 6");

                            for (int k = 0; k < cntIContentChild; k++)
                            {
                                strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                                strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                                strContent = strContent.Replace("'", "''");
                                strContent = ReplaceImage(strContent);
                                strQuery = "INSERT INTO TEntryData "
                                    + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx ) VALUES ("
                                    + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                    + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();

                                intSortNo++;
                                Console.WriteLine("Case 6-" + k);
                            }

                            break;
                        case "QUIZ":
                            strQUIZ = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strQUIZ = strQUIZ.Replace("'", "''");
                            cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;

                            // arrImgFile --> IMAGE 태그 추출 내부에 여러 이미지가 포함될 수 있음
                            string[] arrImgFile1 = strQUIZ.ToString().Split(new string[] { "<IMAGE>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile1)
                            {
                                int imageEnd = imgFile.ToString().IndexOf("</IMAGE>");

                                if (imageEnd > 0)
                                {
                                    //캡션추출
                                    int captionStart = imgFile.ToString().IndexOf("<CAPTION>");
                                    int captionEnd = imgFile.ToString().IndexOf("</CAPTION>");
                                    string strCaption = imgFile.ToString().Substring(captionStart + 9, captionEnd);
                                    //Description추출
                                    int descStart = imgFile.ToString().IndexOf("<DESCRIPTION>");
                                    int descEnd = imgFile.ToString().IndexOf("</DESCRIPTION>");
                                    string strDesc = imgFile.ToString().Substring(descStart + 22, descEnd - 3);
                                    //LINK추출
                                    int linkStart = imgFile.ToString().IndexOf("<LINK>");
                                    int linkEnd = imgFile.ToString().IndexOf("</LINK>");
                                    string strLink = imgFile.ToString().Substring(linkStart + 6, linkEnd);
                                    //ALIGN추출
                                    int alignStart = imgFile.ToString().IndexOf("<ALIGN>");
                                    int alignEnd = imgFile.ToString().IndexOf("</ALIGN>");
                                    string strAlign = imgFile.ToString().Substring(alignStart + 7, alignEnd);
                                    // arrImg --> IMG 태그 추출 하나의 이미지 빼내기
                                    string[] arrImg = imgFile.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                                    foreach (string imgEach in arrImg)
                                    {
                                        int imgEnd = imgEach.ToString().IndexOf("</FILENAME>");
                                        string strImageName = string.Empty;
                                        if (imgEnd > 0)
                                        {
                                            //이미지명 추출
                                            strImageName = imgEach.ToString().Substring(0, imgEnd);
                                            //WIDTH추출
                                            int widthStart = imgEach.ToString().IndexOf("<WIDTH>");
                                            int widthEnd = imgEach.ToString().IndexOf("</WIDTH>");
                                            string strWidth = imgEach.ToString().Substring(widthStart + 7, widthEnd);
                                            //HEIGHT추출
                                            int heightStart = imgEach.ToString().IndexOf("<HEIGHT>");
                                            int heightEnd = imgEach.ToString().IndexOf("</HEIGHT>");
                                            string strHeight = imgEach.ToString().Substring(heightStart + 8, heightEnd);
                                            //새로운 이미지코드명 생성
                                            FileInfo fi = new FileInfo(strImageName);
                                            string strImageCode = strTaskIdx + "_" + strEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + fi.Extension;
                                            Response.Write("strImageCode-->" + strImageCode +"<br />");
                                            SqlConnection Conn = new SqlConnection(connectionString);
                                            SqlCommand Cmdn = new SqlCommand();
                                            Cmdn.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
                                            Cmdn.Parameters.Add("@TaskIdx", SqlDbType.Int);
                                            Cmdn.Parameters.Add("@EntryIdx", SqlDbType.Int);
                                            Cmdn.Parameters.Add("@OriginName", SqlDbType.VarChar);
                                            Cmdn.Parameters.Add("@Description", SqlDbType.VarChar);
                                            Cmdn.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
                                            Cmdn.Parameters.Add("@Width", SqlDbType.VarChar, 10);
                                            Cmdn.Parameters.Add("@Height", SqlDbType.VarChar, 10);
                                            Cmdn.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

                                            Cmdn.Parameters["@FileName"].Value = strImageCode;
                                            Cmdn.Parameters["@TaskIdx"].Value = strTaskIdx;
                                            Cmdn.Parameters["@EntryIdx"].Value = strEntryIdx;
                                            Cmdn.Parameters["@OriginName"].Value = strImageName;
                                            Cmdn.Parameters["@Description"].Value = strDesc;
                                            Cmdn.Parameters["@Caption"].Value = strCaption;
                                            Cmdn.Parameters["@Width"].Value = strWidth;
                                            Cmdn.Parameters["@Height"].Value = strHeight;
                                            Cmdn.Parameters["@FileType"].Value = "IMG";
                                            Cmdn.CommandText = "USP_File_INSERT";
                                            Cmdn.CommandType = CommandType.StoredProcedure;
                                            Cmdn.Connection = Conn;
                                            Conn.Open();
                                            Cmdn.ExecuteNonQuery();
                                            Conn.Close();

                                            strQUIZ = strQUIZ.Replace(strImageName, strImageCode);
                                        }
                                    }
                                }
                            }

                            //##### 퀴즈 입력 시작
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, ChildCount, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strQUIZ + "', " + cntIContentChild + "," + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();
                            //##### 퀴즈 입력 끝
                            //##### 퀴즈 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 시작
                            strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=0 "
                                + "AND RecType='" + strEntType + "' AND SortNo=" + intSortNo + " ORDER BY Idx Desc";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Cmd.Connection = Con;
                            Con.Open();
                            reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                            if (reader.Read())
                            {
                                strICIdx = reader["Idx"].ToString();
                            }
                            Con.Close();
                            //##### 퀴즈 Idx 가져오기 strICIdx -- 자식노드의 ParentIdx로 사용 끝
                            intSortNo++;
                            Console.WriteLine("Case 7");

                            for (int k = 0; k < cntIContentChild; k++)
                            {
                                strRecType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].Name;
                                strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].InnerXml;
                                strContent = strContent.Replace("'", "''");
                                strContent = ReplaceImage(strContent);
                                strQuery = "INSERT INTO TEntryData "
                                    + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                    + strEntryIdx + ", " + Convert.ToInt32(strICIdx) + ", '" + strRecType + "', "
                                    + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                Con = new SqlConnection(connectionString);
                                Cmd = new SqlCommand(strQuery, Con);
                                Cmd.CommandType = CommandType.Text;
                                Con.Open();
                                Cmd.ExecuteNonQuery();
                                Con.Close();

                                intSortNo++;
                                Console.WriteLine("Case 7-" + k);

                                if (strRecType == "EXAMPLE")
                                {
                                    cntExampleChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes.Count;

                                    strQuery = "SELECT Top 1 Idx FROM TEntryData WHERE EntryIdx=" + strEntryIdx + " AND ParentIdx=" + Convert.ToInt32(strICIdx) + " "
                                        + "AND RecType='" + strRecType + "'  ORDER BY Idx Desc";

                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand(strQuery, Con);
                                    Cmd.CommandType = CommandType.Text;
                                    Cmd.Connection = Con;
                                    Con.Open();

                                    reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                                    if (reader.Read())
                                    {
                                        strExIdx = reader["Idx"].ToString();
                                    }
                                    Con.Close();

                                    strQuery = "UPDATE TEntryData SET ChildCount=" + cntExampleChild + " WHERE Idx=" + strExIdx + " ";
                                    Con = new SqlConnection(connectionString);
                                    Cmd = new SqlCommand(strQuery, Con);
                                    Cmd.CommandType = CommandType.Text;
                                    Con.Open();
                                    Cmd.ExecuteNonQuery();
                                    Con.Close();
                                    string strExType = string.Empty;
                                    for (int l = 0; l < cntExampleChild; l++)
                                    {
                                        strExType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].Name;
                                        strContent = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes[k].ChildNodes[l].InnerXml;
                                        strContent = strContent.Replace("'", "''");

                                        strQuery = "INSERT INTO TEntryData "
                                            + " (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) VALUES ("
                                            + strEntryIdx + ", " + Convert.ToInt32(strExIdx) + ", '" + strExType + "', "
                                            + "'" + strContent + "', " + intSortNo + ", " + strTaskIdx + ") ";
                                        Con = new SqlConnection(connectionString);
                                        Cmd = new SqlCommand(strQuery, Con);
                                        Cmd.CommandType = CommandType.Text;
                                        Con.Open();
                                        Cmd.ExecuteNonQuery();
                                        Con.Close();

                                        intSortNo++;
                                        Console.WriteLine("Case 7-Ex-" + k);

                                    }
                                }
                            }

                            break;
                        case "RELATEDSEARCH":
                            strRELATEDSEARCH = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strRELATEDSEARCH = strRELATEDSEARCH.Replace("'", "''");
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strRELATEDSEARCH + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 8");
                            break;

                        case "TAG":
                            strTAG = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerText;
                            strTAG = strTAG.Replace("'", "''");
                            strQuery = "INSERT INTO TEntryData (EntryIdx, ParentIdx, RecType, Content, SortNo, TaskIdx) "
                                + " VALUES (" + strEntryIdx + ", 0, '" + strEntType + "', '"
                                + strTAG + "', " + intSortNo + ", " + strTaskIdx + ") ";

                            Con = new SqlConnection(connectionString);
                            Cmd = new SqlCommand(strQuery, Con);
                            Cmd.CommandType = CommandType.Text;
                            Con.Open();
                            Cmd.ExecuteNonQuery();
                            Con.Close();

                            intSortNo++;
                            Console.WriteLine("Case 9");
                            break;
                    }  //-- Switch End
                }  //##### Entry 자식노드 갯수만큼 돌면서 파싱 끝

                strQuery = "UPDATE TEntry SET EntryTitle='" + strENTRYTITLE + "', "
                    + " EntryTitleK='" + strENTRYTITLE_K + "', EntryTitleE='" + strENTRYTITLE_E + "', "
                    + " EntryTitleC='" + strENTRYTITLE_C + "', Summary='"+ strSummary +"', "
                    + " Tag='" + strTAG + "', ImportDate=getdate(), EditDate=getdate() "
                    + " Where Idx=" + strEntryIdx + " ";
                Con = new SqlConnection(connectionString);
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();


                //##### 이미지 처리하기 
                //Response.Write("이미지처리<br/>");
                ImageCopy();
                //##### 예전 데이터 지우기
                //Response.Write("예전데이터삭제<br/>");
                if (delEntryDataIdx.Length > 0)
                {
                    Con = new SqlConnection(connectionString);
                    strQuery = "DELETE FROM TEntryData WHERE TaskIdx=" + strTaskIdx + " AND Idx <=" + delEntryDataIdx;
                    Cmd = new SqlCommand(strQuery, Con);
                    Cmd.CommandType = CommandType.Text;
                    Con.Open();
                    Cmd.ExecuteNonQuery();
                    Con.Close();
                }

                //##### DB저장하기
                //DBFileInfo();
                Con = new SqlConnection(connectionString);
                strQuery = "UPDATE TImportFile SET ImportFlag=1 WHERE TaskIdx=" + strTaskIdx + "; ";
                Cmd = new SqlCommand(strQuery, Con);
                Cmd.CommandType = CommandType.Text;
                Con.Open();
                Cmd.ExecuteNonQuery();
                Con.Close();
                btnImport.Visible = false;
                Response.Write("<script>alert('Import가 완료되었습니다.');self.opener=self;window.close();</script>");
            }    //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 끝

        }
        catch(Exception ex)
        {
            lblMessage.Text = "엔트리 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }

    }
    private string ReplaceImage(string content)
    {
        DBFileInfo();
        string strQuery = "SELECT FileName, OriginName FROM TFileData WHERE TaskIdx=" + strTaskIdx + " order by Idx ASC";
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand(strQuery, Con);
        Con.Open();
        DataTable dt = new DataTable();
        SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
        adapter.Fill(dt);
        Con.Close();
        foreach (DataRow row2 in dt.Rows)  //---- 각 항목별로 원 이미지 이름을 코드 이미지이름으로 변경하여 복사한다.
        {
            content = content.Replace(row2["OriginName"].ToString(), row2["FileName"].ToString());
        }

        return content;
    }
    protected void Serializer_UnknownNode(Object sender, XmlNodeEventArgs e)
    {

    }

    protected void Serializer_UnknownAttribute(Object sender, XmlAttributeEventArgs e)
    {

    }

    protected string makeDirectory()
    {
        string uploadPath = @"/cms100data/EntryData/";
        string partPath = @"" + strTaskIdx + "";
        string filePath = uploadPath + partPath + "/";
        
        //Response.Write("Path"+filePath);
        //해당 Sub 디렉토리가 있는지 확인하고 없으면 생성한다.
        var mainDirectory = new DirectoryInfo(Server.MapPath(uploadPath));
        //Response.Write("<br/>Main ToSting"+mainDirectory.ToString());
        //Response.Write("<br/>Main Exists" + mainDirectory.Exists);
        //Response.Write("<br/>Main root" + mainDirectory.Root);
        var subDirectory = new DirectoryInfo(Server.MapPath(filePath));
        //Response.Write("<br/>Sub ToSting" + subDirectory.ToString());
        //Response.Write("<br/>Sub Exists" + subDirectory.Exists);
        //Response.Write("<br/>Sub root" + subDirectory.Root);
        if (!Directory.Exists(Server.MapPath(filePath)))
        {
            //Response.Write("<br/>만들기");
            mainDirectory.CreateSubdirectory(partPath);
        }
        return HttpContext.Current.Server.MapPath(@filePath);
    }
    protected void ImageUpTest_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //0.파일이 있는지 확인
        if ((null == TestFileUpload.PostedFile)
            || (0 >= TestFileUpload.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.

            Response.Write("파일을 선택해 주세요");
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        FileInfo fi = new FileInfo(TestFileUpload.PostedFile.FileName);
        string FileNewName = "IMG_Upload_" + DateTime.Now.ToString("yyyyMMddHHmmss") + fi.Extension;

        string uploadPath = @"/cms100data/EntryData/";

        string sFileUri = HttpContext.Current.Server.MapPath(uploadPath) + FileNewName;
        //               + string.Format(@"\{0}", ImageFileUpload.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try
        {
            //### 3-1  UploadFile/ 폴더에 파일 업로드
            TestFileUpload.PostedFile.SaveAs(sFileUri);
            //Response.Write("파일업로드 성공<br />");
            string zipPath = sFileUri;
            //### 3-2 UploadFile/Extract 폴더에 압축해제
            string extractPath = HttpContext.Current.Server.MapPath(uploadPath+"Extract/");
            System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
            //Response.Write("압축해제 성공<br />");
            //### 3-3 UploadFile/Extract 폴더에서  압축해제
            
            string copyPath = makeDirectory();  //HttpContext.Current.Server.MapPath("~/UploadFile/EntryImage/");
            
            //string[] fileList = Directory.GetFiles(@extractPath);
            //Response.Write("카피패스" + copyPath + "<br />");

            DBFileInfo();
            string strQuery = "SELECT FileName, OriginName FROM TFileData WHERE TaskIdx=" + strTaskIdx + " order by Idx ASC";
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            Con.Close();
            foreach (DataRow row2 in dt.Rows)  //---- 각 항목별로 원 이미지 이름을 코드 이미지이름으로 변경하여 복사한다.
            {
                string originPath = extractPath + row2["OriginName"].ToString();
                if (File.Exists(originPath))
                {
                    //
                    string newPath = copyPath + row2["FileName"].ToString();
                    // Delete a file by using File class static method... 
                    if (System.IO.File.Exists(newPath))
                    {
                        // 삭제할것인가?
                    }
                    else
                    {
                        try
                        {
                            //Response.Write("원소스" + originPath + "<br />");
                            //Response.Write("타겟링크" + newPath + "<br />");
                            File.Copy(originPath, newPath, true);
                            //Response.Write("복사완료" + newPath + "<br />");
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "파일 복사중 에러가 발생하였습니다." + ex.ToString(); ;
                        }
                    }
                    File.Delete(originPath);
                }
            }
            lblTaskTitle.Text = "<img src='" + uploadPath + FileNewName + "' />";

            //try
            //{
            //    SqlConnection Con = new SqlConnection(connectionString);
            //    SqlCommand Cmd = new SqlCommand();
            //    Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
            //    Cmd.Parameters.Add("@FileType", SqlDbType.Char);
            //    Cmd.Parameters.Add("@XMLFileName", SqlDbType.VarChar, 255);
            //    Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
            //    Cmd.Parameters["@FileType"].Value = "I";
            //    Cmd.Parameters["@ImgFileName"].Value = FileNewName;
            //    Cmd.CommandText = "USP_ImportFile_INSERT_UPDATE";
            //    Cmd.CommandType = CommandType.StoredProcedure;
            //    Cmd.Connection = Con;
            //    Con.Open();
            //    Cmd.ExecuteNonQuery();
            //    Con.Close();
            //}
            //catch (Exception ex)
            //{
            //    lblMessage.Text = "파일 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
            //                    + ex.ToString();
            //}
            //finally
            //{
            //    lblMessage.Text = "Image 파일을 업로드하였습니다.";
            //    lblImgFileName.Text = FileNewName;
            //}

        }
        catch (Exception ex)
        {
            lblMessage.Text = "파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }
    }

    protected void ImageCopy()
    {
        lblMessage.Text = "";
        string uploadPath = @"/cms100data/EntryData/";
        string sFileUri = HttpContext.Current.Server.MapPath(uploadPath) + lblImgFileName.Text;

        try
        {
            string zipPath = sFileUri;
            ////### 3-2-1 UploadFile/Extract 폴더 내 파일 삭제
            string extractPath = HttpContext.Current.Server.MapPath(uploadPath + "Extract/");
            //string[] filelist = Directory.GetFiles(extractPath);
            
            //foreach(string delfile in filelist)
            //{
            //    if(delfile.Length>3)
            //    {
            //        //Response.Write("파일삭제:" + delfile + "<br />");
            //        File.Delete(delfile);
            //    }
            //}

            ////### 3-2-2 UploadFile/Extract 폴더에 압축해제

            //System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
            ////Response.Write("압축해제 성공<br />");
            //### 3-3 UploadFile/Extract 폴더에서  압축해제

            string copyPath = makeDirectory();  //HttpContext.Current.Server.MapPath("~/UploadFile/EntryImage/");

            //string[] fileList = Directory.GetFiles(@extractPath);
            //Response.Write("카피패스" + copyPath + "<br />");

            DBFileInfo();
            string strQuery = "SELECT FileName, OriginName FROM TFileData WHERE TaskIdx=" + strTaskIdx + " order by Idx ASC";
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Con.Open();
            DataTable dt = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(Cmd);
            adapter.Fill(dt);
            Con.Close();
            foreach (DataRow row2 in dt.Rows)  //---- 각 항목별로 원 이미지 이름을 코드 이미지이름으로 변경하여 복사한다.
            {
                string originPath = extractPath + row2["OriginName"].ToString();
                //Response.Write("파일존재여부:" + File.Exists(originPath) + "<br />" + originPath+"<br />");
                if (File.Exists(originPath))
                {
                    //
                    string newPath = copyPath + row2["FileName"].ToString();
                    // Delete a file by using File class static method... 
                    //if (System.IO.File.Exists(newPath))
                    //{
                    //    // 삭제할것인가?
                    //}
                    //else
                    //{
                        try
                        {
                            //Response.Write("원소스" + originPath + "<br />");
                            //Response.Write("타겟링크" + newPath + "<br />");
                            File.Copy(originPath, newPath, true);
                            //Response.Write("복사완료" + newPath + "<br />");
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = "파일 복사 중 에러가 발생하였습니다." + ex.ToString(); ;
                        }
                    //}
                    //File.Delete(originPath);
                }
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = "압축해제하고 복사 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
            return;
        }
    }

    protected void btnImgAnalysis_Click(object sender, EventArgs e)
    {
        lblMessage.Text = "";
        //1-1.XML 파싱, Table로 Push: TEntry
        string strEntryIdx;
        string strENTRYTITLE = string.Empty;
        string strENTRYTITLE_E = string.Empty;
        string strENTRYTITLE_K = string.Empty;
        string strENTRYTITLE_C = string.Empty;
        string strATTRIBUTES = string.Empty;
        string strINDEXCONTENT = string.Empty;
        string strRELATEDSEARCH = string.Empty;
        string strQUIZ = string.Empty;
        string strTAG = string.Empty;

        string strEntType = string.Empty;
        string strRecType = string.Empty;
        string strQuery = string.Empty;
        string strICIdx = string.Empty;
        string strContent = string.Empty;
        string strExIdx = string.Empty;

        string XMLFileName = lblXMLFileName.Text;
        XmlDocument xmlDoc = new XmlDocument();
        string pathFile = HttpContext.Current.Server.MapPath("~/cms100data/XmlData/") + XMLFileName;
        try
        {
            xmlDoc.Load(pathFile);
        }
        catch (Exception ex)
        {
            //오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "+ ex.ToString());
            lblMessage.Text = "XML 파싱 중 다음과 같은 오류가 발생 하였습니다.<br/>내용 : "
                            + ex.ToString().Substring(0, 130) + "...";
            return;
        }

        int intEntryCount = xmlDoc["edu"].ChildNodes.Count;
        int EntryChildCount = 0;
        int cntIContentChild = 0;

        DBFileInfo();
        SqlConnection Con = new SqlConnection();
        SqlCommand Cmd = new SqlCommand();

        //참조 URL = https://msdn.microsoft.com/en-us/library/System.Xml.XmlDocument(v=vs.110).aspx
        try
        {
            for (int i = 0; i < intEntryCount; i++) //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 시작
            {
                EntryChildCount = xmlDoc["edu"].ChildNodes[i].ChildNodes.Count;
                intSortNo = 0;
                strEntryIdx = xmlDoc["edu"].ChildNodes[i].Attributes["entryCode"].Value;

                for (int j = 0; j < EntryChildCount; j++)  //##### Entry 자식노드 갯수만큼 돌면서 파싱 시작
                {
                    strEntType = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].Name;

                    switch (strEntType)  //-- 엔트리 1dept type구분. EntryTitle, IndexContent, Qize, Tag, Attribute
                    {

                        case "ENTRYTITLE":
                            Console.WriteLine("Case 1");
                            break;
                        case "ENTRYTITLE_E":
                            Console.WriteLine("Case 2");
                            break;
                        case "ENTRYTITLE_C":
                            Console.WriteLine("Case 3");
                            break;
                        case "ENTRYTITLE_K":
                            Console.WriteLine("Case 4");
                            break;
                        case "ATTRIBUTES":
                            Console.WriteLine("Case 5");
                            break;
                        case "INDEXCONTENT":
                            strINDEXCONTENT = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            // arrImgFile --> IMAGE 태그 추출 내부에 여러 이미지가 포함될 수 있음
                            string[] arrImgFile = strINDEXCONTENT.ToString().Split(new string[] { "<IMAGE>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile)
                            {
                                int imageEnd = imgFile.ToString().IndexOf("</IMAGE>");

                                if (imageEnd > 0)
                                {
                                    //캡션추출
                                    int captionStart = imgFile.ToString().IndexOf("<CAPTION>");
                                    int captionEnd = imgFile.ToString().IndexOf("</CAPTION>");
                                    string strCaption = imgFile.ToString().Substring(captionStart + 9, captionEnd - (captionStart + 9));
                                    //Description추출
                                    int descStart = imgFile.ToString().IndexOf("<DESCRIPTION>");
                                    int descEnd = imgFile.ToString().IndexOf("</DESCRIPTION>");
                                    string strDesc = imgFile.ToString().Substring(descStart + 22, descEnd - 3 - (descStart + 22));
                                    //LINK추출
                                    int linkStart = imgFile.ToString().IndexOf("<LINK>");
                                    int linkEnd = imgFile.ToString().IndexOf("</LINK>");
                                    string strLink = imgFile.ToString().Substring(linkStart + 6, linkEnd - (linkStart + 6));
                                    //ALIGN추출
                                    int alignStart = imgFile.ToString().IndexOf("<ALIGN>");
                                    int alignEnd = imgFile.ToString().IndexOf("</ALIGN>");
                                    string strAlign = imgFile.ToString().Substring(alignStart + 7, alignEnd -(alignStart + 7));
                                    // arrImg --> IMG 태그 추출 하나의 이미지 빼내기
                                    string[] arrImg = imgFile.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                                    foreach (string imgEach in arrImg)
                                    {
                                        int imgEnd = imgEach.ToString().IndexOf("</FILENAME>");
                                        string strImageName = string.Empty;
                                        if (imgEnd > 0)
                                        {
                                            //이미지명 추출
                                            strImageName = imgEach.ToString().Substring(0, imgEnd);
                                            //WIDTH추출
                                            int widthStart = imgEach.ToString().IndexOf("<WIDTH>");
                                            int widthEnd = imgEach.ToString().IndexOf("</WIDTH>");
                                            string strWidth = imgEach.ToString().Substring(widthStart + 7, widthEnd-(widthStart + 7));
                                            //HEIGHT추출
                                            int heightStart = imgEach.ToString().IndexOf("<HEIGHT>");
                                            int heightEnd = imgEach.ToString().IndexOf("</HEIGHT>");
                                            string strHeight = imgEach.ToString().Substring(heightStart + 8, heightEnd-(heightStart + 8));
                                            //새로운 이미지코드명 생성
                                            FileInfo fi = new FileInfo(strImageName); 
                                            string strImageCode = strTaskIdx + "_" + strEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + fi.Extension;
                                            Con = new SqlConnection(connectionString);
                                            Cmd = new SqlCommand();
                                            Cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
                                            Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
                                            Cmd.Parameters.Add("@EntryIdx", SqlDbType.Int);
                                            Cmd.Parameters.Add("@OriginName", SqlDbType.VarChar);
                                            Cmd.Parameters.Add("@Description", SqlDbType.VarChar);
                                            Cmd.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
                                            Cmd.Parameters.Add("@Width", SqlDbType.VarChar, 10);
                                            Cmd.Parameters.Add("@Height", SqlDbType.VarChar, 10);
                                            Cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

                                            Cmd.Parameters["@FileName"].Value = strImageCode;
                                            Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
                                            Cmd.Parameters["@EntryIdx"].Value = strEntryIdx;
                                            Cmd.Parameters["@OriginName"].Value = strImageName;
                                            Cmd.Parameters["@Description"].Value = strDesc;
                                            Cmd.Parameters["@Caption"].Value = strCaption;
                                            Cmd.Parameters["@Width"].Value = strWidth;
                                            Cmd.Parameters["@Height"].Value = strHeight;
                                            Cmd.Parameters["@FileType"].Value = "IMG";
                                            Cmd.CommandText = "USP_File_INSERT";
                                            Cmd.CommandType = CommandType.StoredProcedure;
                                            Cmd.Connection = Con;
                                            Con.Open();
                                            Cmd.ExecuteNonQuery();
                                            Con.Close();
                                        }
                                    }
                                }
                            }

                            Console.WriteLine("Case 6");
                            break;
                        case "QUIZ":
                            strQUIZ = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].InnerXml;
                            strQUIZ = strQUIZ.Replace("'", "''");
                            cntIContentChild = xmlDoc["edu"].ChildNodes[i].ChildNodes[j].ChildNodes.Count;
                            // arrImgFile --> IMAGE 태그 추출 내부에 여러 이미지가 포함될 수 있음
                            string[] arrImgFile1 = strQUIZ.ToString().Split(new string[] { "<IMAGE>" }, StringSplitOptions.None);
                            foreach (string imgFile in arrImgFile1)
                            {
                                int imageEnd = imgFile.ToString().IndexOf("</IMAGE>");

                                if (imageEnd > 0)
                                {
                                    //캡션추출
                                    int captionStart = imgFile.ToString().IndexOf("<CAPTION>");
                                    int captionEnd = imgFile.ToString().IndexOf("</CAPTION>");
                                    string strCaption = imgFile.ToString().Substring(captionStart + 9, captionEnd);
                                    //Description추출
                                    int descStart = imgFile.ToString().IndexOf("<DESCRIPTION>");
                                    int descEnd = imgFile.ToString().IndexOf("</DESCRIPTION>");
                                    string strDesc = imgFile.ToString().Substring(descStart + 22, descEnd - 3);
                                    //LINK추출
                                    int linkStart = imgFile.ToString().IndexOf("<LINK>");
                                    int linkEnd = imgFile.ToString().IndexOf("</LINK>");
                                    string strLink = imgFile.ToString().Substring(linkStart + 6, linkEnd);
                                    //ALIGN추출
                                    int alignStart = imgFile.ToString().IndexOf("<ALIGN>");
                                    int alignEnd = imgFile.ToString().IndexOf("</ALIGN>");
                                    string strAlign = imgFile.ToString().Substring(alignStart + 7, alignEnd);
                                    // arrImg --> IMG 태그 추출 하나의 이미지 빼내기
                                    string[] arrImg = imgFile.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);
                                    foreach (string imgEach in arrImg)
                                    {
                                        int imgEnd = imgEach.ToString().IndexOf("</FILENAME>");
                                        string strImageName = string.Empty;
                                        if (imgEnd > 0)
                                        {
                                            //이미지명 추출
                                            strImageName = imgEach.ToString().Substring(0, imgEnd);
                                            //WIDTH추출
                                            int widthStart = imgEach.ToString().IndexOf("<WIDTH>");
                                            int widthEnd = imgEach.ToString().IndexOf("</WIDTH>");
                                            string strWidth = imgEach.ToString().Substring(widthStart + 7, widthEnd);
                                            //HEIGHT추출
                                            int heightStart = imgEach.ToString().IndexOf("<HEIGHT>");
                                            int heightEnd = imgEach.ToString().IndexOf("</HEIGHT>");
                                            string strHeight = imgEach.ToString().Substring(heightStart + 8, heightEnd);
                                            //새로운 이미지코드명 생성
                                            FileInfo fi = new FileInfo(strImageName);
                                            string strImageCode = strTaskIdx + "_" + strEntryIdx + "_" + DateTime.Now.ToString("HHmmss") + fi.Extension;
                                            Con = new SqlConnection(connectionString);
                                            Cmd = new SqlCommand();
                                            Cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255);
                                            Cmd.Parameters.Add("@TaskIdx", SqlDbType.Int);
                                            Cmd.Parameters.Add("@EntryIdx", SqlDbType.Int);
                                            Cmd.Parameters.Add("@OriginName", SqlDbType.VarChar);
                                            Cmd.Parameters.Add("@Description", SqlDbType.VarChar);
                                            Cmd.Parameters.Add("@Caption", SqlDbType.VarChar, 255);
                                            Cmd.Parameters.Add("@Width", SqlDbType.VarChar, 10);
                                            Cmd.Parameters.Add("@Height", SqlDbType.VarChar, 10);
                                            Cmd.Parameters.Add("@FileType", SqlDbType.VarChar, 10);

                                            Cmd.Parameters["@FileName"].Value = strImageCode;
                                            Cmd.Parameters["@TaskIdx"].Value = strTaskIdx;
                                            Cmd.Parameters["@EntryIdx"].Value = strEntryIdx;
                                            Cmd.Parameters["@OriginName"].Value = strImageName;
                                            Cmd.Parameters["@Description"].Value = strDesc;
                                            Cmd.Parameters["@Caption"].Value = strCaption;
                                            Cmd.Parameters["@Width"].Value = strWidth;
                                            Cmd.Parameters["@Height"].Value = strHeight;
                                            Cmd.Parameters["@FileType"].Value = "IMG";
                                            Cmd.CommandText = "USP_File_INSERT";
                                            Cmd.CommandType = CommandType.StoredProcedure;
                                            Cmd.Connection = Con;
                                            Con.Open();
                                            Cmd.ExecuteNonQuery();
                                            Con.Close();
                                        }
                                    }
                                }
                            }
                            Console.WriteLine("Case 7");
                            break;
                        case "RELATEDSEARCH":
                            Console.WriteLine("Case 8");
                            break;

                        case "TAG":
                            Console.WriteLine("Case 9");
                            break;
                    }  //-- Switch End
                }  //##### Entry 자식노드 갯수만큼 돌면서 파싱 끝
            }    //##### Entry가 데이터의 기준. Entry 갯수만큼 돌면서 파싱 끝
        }
        catch (Exception ex)
        {
            lblMessage.Text = "엔트리 정보 업데이트 중 다음과 같은 오류가 발생 하였습니다.<br/>오류내용 : "
                            + ex.ToString();
        }
    }
}