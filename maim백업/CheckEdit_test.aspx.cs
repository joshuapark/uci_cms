﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.IO;
using Newtonsoft.Json;

public partial class main_EntryEdit2 : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;
    
    public DataTable entryTable  = null;
    public string strTaskIdx = string.Empty;
    private string EntryIdx = string.Empty;
    public int EntryStatus = 0;
    public string entryTag = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 5)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝  
       
        strTaskIdx = Request.Params["Idx"].ToString();
        strTaskIdx = "1";
        if (!IsPostBack)
        {
            //############ 작업정보 가져오기 시작 #######################
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            Con.Open();

            string jsonSaveString = saveString.Value;
            if (jsonSaveString.Length != 0)
            {

                int sortNo = 0;

                Edu edu = JsonConvert.DeserializeObject<Edu>(jsonSaveString);

                List<LUnit> lunitList = edu.LUnit;

                foreach (LUnit lunit in lunitList)
                {
                    //대분류 DB에 저장하기
                    SqlCommand lunitCommand = null;
                    if (Convert.ToInt32(lunit.idx) == -1)
                    {
                        lunitCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount) VALUES(" + EntryIdx + ", 0 , " + sortNo + " , '" + lunit.rectype + "' , '' , " + lunit.list.Count + ")", Con);
                        lunitCommand.ExecuteNonQuery();
                        sortNo++;

                        //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                        lunitCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData ORDER BY idx DESC", Con);
                        SqlDataReader sdr = lunitCommand.ExecuteReader(CommandBehavior.SingleResult);
                        sdr.Read();
                        lunit.idx = Convert.ToString(sdr["idx"]);
                        sdr.Close();

                    }
                    else
                    {
                        lunitCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + lunit.list.Count + " , SortNo=" + sortNo + " WHERE idx =" + lunit.idx, Con);
                        lunitCommand.ExecuteNonQuery();
                        sortNo++;
                    }

                    string lunitContent = string.Empty;
                    List<SecondLevel> lunitChildList = lunit.list;
                    foreach (SecondLevel secondItem in lunitChildList)
                    {

                        if (secondItem.content != null)
                        {
                            string secondContent = convertHtmlToXml(secondItem.content);
                            lunitContent += secondContent;
                            SqlCommand secondCommand = null;
                            if (Convert.ToInt32(secondItem.idx) == -1)
                            {
                                secondCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount) VALUES(" + EntryIdx + "," + lunit.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent + "' , 0)", Con);
                                sortNo++;
                            }
                            else
                            {
                                secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent + "' WHERE idx=" + secondItem.idx, Con);
                                sortNo++;
                            }

                            secondCommand.ExecuteNonQuery();
                        }
                    }

                    lunitCommand = new SqlCommand("UPDATE TEntryData SET Content='" + lunitContent + "' WHERE idx =" + lunit.idx, Con);
                    lunitCommand.ExecuteNonQuery();
                }

                //퀴즈 DB에 넣기
                List<Quiz> quizList = edu.Quiz;
                if (quizList != null)
                {
                    foreach (Quiz quiz in quizList)
                    {
                        SqlCommand quizCommand = null;

                        if (Convert.ToInt32(quiz.idx) == -1)
                        {
                            quizCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount) VALUES(" + EntryIdx + ", 0 , " + sortNo + " , '" + quiz.rectype + "' , '' , " + quiz.list.Count + ")", Con);
                            quizCommand.ExecuteNonQuery();
                            sortNo++;

                            //제일 마지막에 넣은 것을 가져와서 idx로 설정을 해야 한다.
                            quizCommand = new SqlCommand("SELECT TOP 1 * FROM TEntryData ORDER BY idx DESC", Con);
                            SqlDataReader sdr = quizCommand.ExecuteReader(CommandBehavior.SingleResult);
                            sdr.Read();
                            quiz.idx = Convert.ToString(sdr["idx"]);
                            sdr.Close();

                        }
                        else
                        {
                            quizCommand = new SqlCommand("UPDATE TEntryData SET ChildCount=" + quiz.list.Count + " , SortNo=" + sortNo + " WHERE idx =" + quiz.idx, Con);
                            quizCommand.ExecuteNonQuery();
                            sortNo++;
                        }

                        string quizContent = string.Empty;
                        List<SecondLevel> quizChildList = quiz.list;
                        foreach (SecondLevel secondItem in quizChildList)
                        {

                            if (secondItem.content != null)
                            {
                                string secondContent = convertHtmlToXml(secondItem.content);
                                quizContent += secondContent;
                                SqlCommand secondCommand = null;
                                if (Convert.ToInt32(secondItem.idx) == -1)
                                {
                                    secondCommand = new SqlCommand("INSERT INTO TEntryData(EntryIdx , ParentIdx , SortNo , RecType , Content , ChildCount) VALUES(" + EntryIdx + "," + quiz.idx + " , " + sortNo + " , '" + secondItem.rectype + "' , '" + secondContent + "' , 0)", Con);
                                    sortNo++;
                                }
                                else
                                {
                                    secondCommand = new SqlCommand("UPDATE TEntryData SET SortNo=" + sortNo + ", Content='" + secondContent + "' WHERE idx=" + secondItem.idx, Con);
                                    sortNo++;
                                }

                                secondCommand.ExecuteNonQuery();
                            }
                        }

                        quizCommand = new SqlCommand("UPDATE TEntryData SET Content='" + quizContent + "' WHERE idx =" + quiz.idx, Con);
                        quizCommand.ExecuteNonQuery();
                    }
                }

                //태그에 DB에 넣기
                Tag tag = edu.Tag;
                if (tag != null)
                {
                    SqlCommand tagCommand = new SqlCommand("Update TEntry SET Tag='" + tag.content + "' Where idx =" + tag.idx, Con);
                }
            }

            GetTaskInfo();
            GetEntryList();
        }
    }
   
    private void GetTaskInfo()
    {
        DBFileInfo();
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        //Cmd.CommandText = "SELECT * FROM TTaskID WHERE Idx=" + strTaskIdx + " AND DelFlag=0";
        Cmd.Parameters.Add("@Idx", SqlDbType.Int);
        Cmd.Parameters["@Idx"].Value = strTaskIdx;
        Cmd.CommandText = "USP_Task_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;
        Con.Open();
        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
        if (reader.Read())
        {
            txtTaskTitle.Text = reader["TaskTitle"].ToString();
            txtTaskContent.Text = reader["TaskContent"].ToString();
            txtEntryCount.Text = reader["EntryCount"].ToString();
            txtManager.Text = reader["ManagerName"].ToString();
            txtUser.Text = reader["UserName"].ToString();
            txtPublishDate.Text = reader["PublishDate"].ToString();
            txtFinishDate.Text = reader["CompleteDate"].ToString();
            //-- 검수완료가 된 상태면 검수완료 버튼 숨기기 KJH 저장버튼도 숨기기
            if (Convert.ToInt32(reader["Status"].ToString()) > 6)
            {
                btnAllCheck.Visible = false;
            }
        }
        Con.Close();
        //############ 작업정보 가져오기 끝 #######################
    }
    private void GetEntryList()
    {
        //############ 작업 내 엔트리목록 가져오기 끝 #######################
        //############ 엔트리를 클릭하였으면 엔트리Idx로 편집화면 가져오기 시작 #######################
        EntryIdx = string.Empty;

        if (Request.Params["EntryIdx"] != null)
        {
            if (Request.Params["EntryIdx"].ToString().Length > 0)
            {
                EntryIdx = Request.Params["EntryIdx"].ToString();
                GetEntryInfo(EntryIdx);
            }
        }
        else
        {
            EntryIdx = "23458";
            GetEntryInfo(EntryIdx);
        }

    }
    private void GetEntryInfo(string EntryIdx)
    {
        if (EntryIdx.Length > 0)
        {
            try
            {
                // 엔트리상세 데이터(TEntryData) 가져오기

                txtEntryNo.Text = EntryIdx;
                SqlConnection Conn = new SqlConnection(connectionString);
                Conn.Open();
                SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + EntryIdx, Conn);

                this.entryTable = new DataTable();
                sda.Fill(entryTable);

                Conn.Close();
                // 엔트리기본(TEntry) 정보 가져오기
                //DBFileInfo();
                SqlConnection Con = new SqlConnection(connectionString);
                SqlCommand Cmd = new SqlCommand();
                Cmd.Connection = Con;
                //Cmd.CommandText = "SELECT E.*, U.UserName FROM TEntry E join TUser U on E.UserIdx=U.Idx WHERE E.Idx=" + idx + "";
                Cmd.Parameters.Add("@Idx", SqlDbType.Int);
                Cmd.Parameters["@Idx"].Value = EntryIdx;
                Cmd.CommandText = "USP_Entry_SELECT";
                Cmd.CommandType = CommandType.StoredProcedure;
                Con.Open();
                SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
                txtEntryNo.Text = EntryIdx;
                if (reader.Read())
                {
                    txtTitle_K.Text = reader["EntryTitleK"].ToString();
                    txtTitle.Text = reader["EntryTitle"].ToString();
                    txtTitle_K.Text = reader["EntryTitleK"].ToString();
                    txtTitle_E.Text = reader["EntryTitleE"].ToString();
                    txtTitle_C.Text = reader["EntryTitleC"].ToString();
                    txtUser.Text = reader["UserName"].ToString();
                    txtCategory.Text = reader["Category"].ToString();
                    txtEditDate.Text = reader["EditDate"].ToString();
                    EntryStatus = Convert.ToInt32(reader["Status"].ToString());
                    if (EntryStatus>4)
                    {
                        btnSection.Visible = false;
                    }
                    
                    entryTag = reader["Tag"].ToString();

                }
                Con.Close();
            }
            catch (Exception ex)
            {
                Response.Write("엔트리조회 중 오류가 발생하였습니다.");
            }
        }
        else
        {
            EntryIdx = "0";

            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TEntryData WHERE EntryIdx = " + EntryIdx, Conn);

            this.entryTable = new DataTable();
            sda.Fill(entryTable);

            Conn.Close();
        }
    }
    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    public string getRecTypeName(string recType)
    {
        string result = String.Empty;
        switch (recType)
        {

            case "INDEXCONTENT":
                result = "대분류";
                break;
            case "MAINTITLE":
                result = "대제목";
                break;
            case "SUBTITLE":
                result = "중제목";
                break;
            case "BOX":
                result = "블럭";
                break;
            case "BASIC":
                result = "기본";
                break;
            case "BASIC2":
                result = "기본2";
                break;
            case "RELATEDSEARCH":
                result = "연관검색어";
                break;
            case "MATRIX_TABLE":
                result = "표";
                break;
            case "ANNOTATION":
                result = "주석";
                break;
            case "IMAGE":
                result = "이미지";
                break;
            case "TAG":
                result = "태그";
                break;
            case "QUIZ":
                result = "문제";
                break;
            case "INFOTABLE":
                result = "정보";
                break;
            case "QUESTION":
                result = "문제";
                break;
            case "EXAMPLE":
                result = "보기";
                break;
            case "DISTRACTOR":
                result = "선택지";
                break;
            case "EXPLANATION":
                result = "설명";
                break;
            case "ANSWER":
                result = "정답";
                break;
        }

        return result;

    }

    public static string convertXmlToHtml(string strXml)
    {

        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter htmlWriter = new HtmlTextWriter(stringWriter);

        //루트태그를 붙여줌
        strXml = "<edu>" + strXml + "</edu>";

        //align이 있는지 확인한다.
        Boolean isAlign = false;
        string strAlign = String.Empty;
        int startAlignIndex = strXml.IndexOf("<ALIGN>");
        string alignEndTag = "</ALIGN>";
        int endAlignIndex = strXml.LastIndexOf(alignEndTag);

        if (startAlignIndex > -1)
        {
            isAlign = true;
            string alignTag = strXml.Substring(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
            strAlign = alignTag.Replace("<ALIGN>", "").Replace("</ALIGN>", "");
            strXml = strXml.Remove(startAlignIndex, endAlignIndex - startAlignIndex + alignEndTag.Length);
        }


        //caption이 있는 확인한다.
        Boolean isCaption = false;
        string strCaption = string.Empty;

        strXml = strXml.Replace("<EMPHASIS>", "<strong>");
        strXml = strXml.Replace("</EMPHASIS>", "</strong>");



        using (XmlReader reader = XmlReader.Create(new StringReader(strXml)))
        {

            while (reader.Read())
            {


                if (reader.NodeType == XmlNodeType.CDATA)
                {
                    htmlWriter.Write(reader.Value);

                }
                else if (reader.NodeType == XmlNodeType.Element)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":

                            if (isAlign)
                            {
                                //htmlWriter.AddAttribute(HtmlTextWriterAttribute.Align, strAlign);
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.P);
                            break;
                        case "IMGS":
                            if (isAlign)
                            {
                                //htmlWriter.AddAttribute(HtmlTextWriterAttribute.Align, strAlign.ToLower());
                                htmlWriter.AddStyleAttribute(HtmlTextWriterStyle.TextAlign, strAlign.ToLower());
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.P);
                            break;
                        case "IMG":
                            break;

                        case "WIDTH":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Width, reader.Value);
                            break;
                        case "HEIGHT":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Height, reader.Value);
                            break;
                        case "CAPTION":
                            reader.Read();
                            isCaption = true;
                            strCaption = reader.Value;
                            break;
                        case "DESCRIPTION":
                            break;
                        case "FILENAME":
                            reader.Read();
                            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Src, "http://hyonga.iptime.org:15080/UploadFile/EntryImage/" + reader.Value);

                            break;
                    }

                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {

                    switch (reader.Name)
                    {
                        case "TEXT":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMGS":
                            htmlWriter.RenderEndTag();
                            break;
                        case "IMG":

                            if (isCaption)
                            {
                                htmlWriter.RenderBeginTag(HtmlTextWriterTag.Caption);
                                htmlWriter.Write(strCaption);
                                htmlWriter.RenderEndTag();
                                isCaption = false;
                            }

                            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Img);

                            htmlWriter.RenderEndTag();
                            break;
                        case "ALIGN":
                            break;
                        case "WIDTH":
                            break;
                        case "HEIGHT":

                            break;
                        case "CAPTION":
                            break;
                        case "DESCRIPTION":
                            break;
                    }
                }
            }
        }

        return stringWriter.ToString();
    }

    private string convertHtmlToXml(string strHtml)
    {

        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);

        //img 태그와 br 태그의 닫는 태그를 넣는다.
        strHtml = strHtml.Replace("<br>", "<br/>");
        int imgStartIndex = 0;
        int imgEndIndex = 0;

        imgStartIndex = strHtml.IndexOf("<img", 0);
        while (imgStartIndex > -1)
        {

            imgEndIndex = strHtml.IndexOf(">", imgStartIndex) + 1;
            strHtml = strHtml.Insert(imgEndIndex, "</img>");
            imgStartIndex = strHtml.IndexOf("<img", imgEndIndex);

        }

        strHtml = strHtml.Replace("<strong>", "<EMPHASIS>").Replace("</strong>", "</EMPHASIS>");

        string strAlign = string.Empty;
        strHtml = "<edu>" + strHtml + "</edu>";
        XmlDocument xml = new XmlDocument();

        xml.LoadXml(strHtml);

        XmlNodeList pNodeList = xml.GetElementsByTagName("p");
        foreach (XmlNode pNode in pNodeList)
        {
            XmlAttributeCollection attributes = pNode.Attributes;
            foreach (XmlAttribute attribute in attributes)
            {
                if (attribute.Value.Contains("text-align"))
                {
                    strAlign = "<ALIGN>" + attribute.Value.Replace("text-align:", "").Trim() + "</ALIGN>";
                }
            }
        }

        strHtml = strHtml.Trim();

        XmlNodeList eduList = xml.GetElementsByTagName("edu");
        foreach (XmlNode eduNode in eduList)
        {
            XmlNodeList eduChildList = eduNode.ChildNodes;

            foreach (XmlNode eduChildNode in eduChildList)
            {
                XmlNodeType eduChildNodeType = eduChildNode.NodeType;
                if (eduChildNodeType.Equals(XmlNodeType.Element))
                {
                    if (eduChildNode.Name.Equals("p"))
                    {
                        //xmlWriter.WriteString( parsePTagContent(eduChildNode.InnerXml) );
                        xmlWriter.WriteRaw(parsePTagContent(eduChildNode.InnerXml));
                    }// END IF p 태그 확인
                    else if (eduChildNode.Name.Equals("table"))
                    {

                        xmlWriter.WriteStartElement("CONTENTS");
                        xmlWriter.WriteCData(eduChildNode.OuterXml);
                        xmlWriter.WriteEndElement();

                    }// END IF table 태그 확인
                }
            }
        }

        return stringWriter.ToString() + strAlign;
    }

    private string parsePTagContent(string strPTagContent)
    {

        StringWriter stringWriter = new StringWriter();
        XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);

        strPTagContent = "<edu>" + strPTagContent + "</edu>";

        string strText = string.Empty;
        Boolean startedImageTage = false;

        using (XmlReader reader = XmlReader.Create(new StringReader(strPTagContent)))
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Text:
                        if (startedImageTage)
                        {
                            xmlWriter.WriteEndElement();
                            startedImageTage = false;
                        }

                        strText += reader.Value;
                        break;
                    case XmlNodeType.Element:

                        switch (reader.Name)
                        {
                            case "table":
                                if (startedImageTage)
                                {
                                    xmlWriter.WriteEndElement();
                                    startedImageTage = false;
                                }

                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }


                                string tableHeight = reader.GetAttribute("height");
                                string tableWidth = reader.GetAttribute("width");

                                xmlWriter.WriteStartElement("CONTENTS");
                                xmlWriter.WriteCData(reader.ReadInnerXml());
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("HEIGHT");
                                xmlWriter.WriteValue(tableHeight);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("WIDTH");
                                xmlWriter.WriteValue(tableWidth);
                                xmlWriter.WriteEndElement();
                                break;
                            case "img":
                                if (strText != string.Empty)
                                {
                                    xmlWriter.WriteStartElement("TEXT");
                                    xmlWriter.WriteCData(strText);
                                    xmlWriter.WriteEndElement();
                                    strText = string.Empty;
                                }

                                if (!startedImageTage)
                                {
                                    xmlWriter.WriteStartElement("IMGS");
                                    startedImageTage = true;
                                }

                                xmlWriter.WriteStartElement("IMG");

                                string imgHeight = string.Empty;
                                string imgWidth = string.Empty;
                                string imgStyle = reader.GetAttribute("style");
                                string fileName = reader.GetAttribute("src");

                                if (imgStyle != null)
                                {
                                    string[] splitedImgStyle = imgStyle.Split(new char[] { ' ' });

                                    foreach (string attribute in splitedImgStyle)
                                    {
                                        if (attribute.Contains("width"))
                                        {
                                            imgWidth = attribute.Replace("width:", string.Empty).Replace("px", string.Empty);
                                        }
                                        else if (attribute.Contains("height"))
                                        {
                                            imgHeight = attribute.Replace("width:", string.Empty).Replace("px", string.Empty);
                                        }
                                    }
                                }

                                xmlWriter.WriteStartElement("FILENAME");
                                string[] splitedFileName = fileName.Split(new char[] { '/' });
                                xmlWriter.WriteValue(splitedFileName[splitedFileName.Length - 1]);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("HEIGHT");
                                xmlWriter.WriteValue(imgHeight);
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("WIDTH");
                                xmlWriter.WriteValue(imgWidth);
                                xmlWriter.WriteEndElement();

                                break;
                            case "EMPHASIS":
                                if (startedImageTage)
                                {
                                    xmlWriter.WriteEndElement();
                                    startedImageTage = false;
                                }
                                strText += "<EMPHASIS>";
                                break;
                        }


                        break;
                    case XmlNodeType.EndElement:

                        switch (reader.Name)
                        {
                            case "table":
                                break;
                            case "img":
                                xmlWriter.WriteEndElement();
                                break;
                            case "EMPHASIS":
                                strText += "</EMPHASIS>";
                                break;
                        }

                        break;
                }
            }
        }

        if (strText != string.Empty)
        {
            xmlWriter.WriteStartElement("TEXT");
            xmlWriter.WriteCData(strText);
            xmlWriter.WriteEndElement();
        }
        if (startedImageTage) xmlWriter.WriteEndElement();

        return stringWriter.ToString();
    }

    [System.Web.Services.WebMethod]
    public static string getContents(string idxList)
    {
        string[] arrIdxList = idxList.Split(new char[] { ',' });
        DataTable entryTable = new DataTable();

        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        SqlConnection Con = new SqlConnection(connectionString);


        foreach (string unitIdx in arrIdxList)
        {
            if (unitIdx.Trim().Length > 0)
            {
                Con.Open();

                string strQuery = "SELECT * FROM TEntryData Where idx=" + unitIdx;
                SqlCommand cmd = new SqlCommand(strQuery, Con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);

                sda.Fill(entryTable);

                DataRow[] entryRowList = entryTable.Select("idx=" + unitIdx);
                int childCount = Convert.ToInt32(entryRowList[0]["childcount"]);

                if (childCount > 0)
                {
                    strQuery = "SELECT * FROM TEntryData Where parentidx=" + unitIdx;
                    cmd = new SqlCommand(strQuery, Con);
                    sda = new SqlDataAdapter(cmd);
                    sda.Fill(entryTable);
                }

                Con.Close();
            }
        }

        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

        Dictionary<string, object> row;
        foreach (DataRow dr in entryTable.Rows)
        {
            Boolean isParse = true;
            String recType = Convert.ToString(dr["RecType"]);

            if (recType.Equals("INDEXCONTENT") || recType.Equals("QUIZ") || recType.Equals("EXAMPLE"))
            {
                isParse = false;
            }


            row = new Dictionary<string, object>();
            foreach (DataColumn col in entryTable.Columns)
            {
                if (col.ColumnName.Equals("Content"))
                {
                    if (isParse)
                    {
                        string parseContent = convertXmlToHtml(Convert.ToString(dr[col]));
                        row.Add(col.ColumnName, parseContent);
                    }
                }
                else
                {
                    row.Add(col.ColumnName, dr[col]);
                }

            }
            rows.Add(row);
        }

        return serializer.Serialize(rows);
    }


    public class Edu
    {
        [JsonProperty("lunit")]
        public List<LUnit> LUnit { get; set; }
        [JsonProperty("quiz")]
        public List<Quiz> Quiz { get; set; }
        [JsonProperty("tag")]
        public Tag Tag { get; set; }
    }


    public class Tag
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

    public class LUnit
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<SecondLevel> list { get; set; }
    }

    public class Quiz
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("list")]
        public List<SecondLevel> list { get; set; }
    }



    public class SecondLevel
    {
        [JsonProperty("idx")]
        public string idx { get; set; }
        [JsonProperty("rectype")]
        public string rectype { get; set; }
        [JsonProperty("content")]
        public string content { get; set; }
    }

    protected void btnCheck_Click(object sender, EventArgs e)
    {
        try
        {
            // 상세 수정내용을 엔트리 테이블에 저장해야 함 2015.07.01 KJH 

            EntryIdx = txtEntryNo.Text;
            // 상태값 업데이트 1:엔트리배포 3:작업완료/검수요청 5:검수완료
            string strQuery = "UPDATE TEntry SET Status=5, CompleteDate=getdate(), EditDate=getDate() WHERE Idx=" + EntryIdx + "; "
                + " UPDATE TEntryData SET Status=5, EditDate=getDate() Where EntryIdx=" + EntryIdx + ";";            

            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            GetEntryList();
            btnSection.Visible = false;
        }
        catch(Exception ex)
        {
            Response.Write("검수완료 중에 오류가 발생했습니다.<br/> 오류내용:"+ ex.ToString());
        }
    }        
    protected void btnAllCheck_Click(object sender, EventArgs e)
    {
        EntryIdx = txtEntryNo.Text;
        // 상태값 업데이트 1:엔트리배포 3:작업완료/검수요청 5:검수완료
        string strQuery = "UPDATE TEntry SET Status=7, CompleteDate=getdate(), EditDate=getdate() WHERE TaskIdx=" + strTaskIdx + "; "
            + " UPDATE TTaskID SET Status=7, CompleteDate=getdate()  Where Idx=" + strTaskIdx + "; "
            + " UPDATE TEntryData SET Status=7, EditDate=getdate()  Where TaskIdx=" + strTaskIdx + "; "
            + " UPDATE TTaskID SET FinishCount=(SELECT Count(Idx) FROM TEntry WHERE DelFlag=0 AND Status=7 AND TaskIdx=" + strTaskIdx + ") Where Idx=" + strTaskIdx + "; ";
        try
        {
            DBFileInfo();
            SqlConnection Con = new SqlConnection(connectionString);
            SqlCommand Cmd = new SqlCommand(strQuery, Con);
            Cmd.CommandType = CommandType.Text;
            Con.Open();
            Cmd.ExecuteNonQuery();
            Con.Close();
            GetTaskInfo();
            GetEntryList();            
            // 최종 완료 버튼 숨기기
            btnAllCheck.Visible = false;
        }
        catch(Exception ex)
        {
            Response.Write("최종검수처리 중 오류가 발생했습니다." + ex.ToString() + strQuery);
        }

    }
    protected void btnView_Click(object sender, EventArgs e)
    {
        Response.Redirect("CheckView.aspx?Idx=" + strTaskIdx + "&EntryIdx=" + EntryIdx + "");
    }
}