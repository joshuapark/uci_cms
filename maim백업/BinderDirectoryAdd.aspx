﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderDirectoryAdd.aspx.cs" Inherits="main_BinderDirectoryAdd" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->

</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdParentLevel" runat="server" />
    <asp:HiddenField ID="hdParentIdx" runat="server" />
    <div>
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: 20%;">
			<col style="width: 80%;">
		</colgroup>
		<tbody>	
            <tr>
				<th>바인더명</th>
                <td>
					<asp:TextBox runat="server" ID="txtBinderTitle" class="large" />
                </td>
            </tr>
            <tr>
				<th>설명</th>
                <td>
					<asp:TextBox runat="server" ID="txtBinderMemo" class="large" />
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->
        <div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="AddButton" class="btn btn-lg btn-success" text="저장" OnClick="AddBinderButton_Click"/>
		</div><!-- // section-button -->
    </div>
    </form>
</body>
</html>
