﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="TaskEdit - 사본.aspx.cs" Inherits="notice_List" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
	  	  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1 active"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

    <form id="addForm" runat="server">

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">엔트리생성</h2>
		</div><!-- // title -->	

		<div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a class="btn btn-sm btn-primary" href="TaskList.aspx" >작업목록</a> 
			</div>
		</div><!-- // section-button -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">CATEGORY</h3>
		</div><!-- // title -->

		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: 110px;">
			<col style="width: auto;">
		</colgroup>
		<tbody>
			<tr>
				<th>개정</th>
				<td>
                    <asp:radiobuttonlist ID="rbtRevision" CssClass="de-radio" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtRevision" ControlToValidate="rbtRevision" ErrorMessage="* 개정 항목을 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>과목군</th>
				<td>
                    <asp:radiobuttonlist ID="rbtSubjectGroup" CssClass="de-radio" runat="server" RepeatColumns="10" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtSubjectGroup" ControlToValidate="rbtSubjectGroup" ErrorMessage="* 과목군을 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>과목</th>
				<td>
					<asp:radiobuttonlist ID="rbtSubject" CssClass="de-radio" runat="server" RepeatColumns="7" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtSubject" ControlToValidate="rbtSubject" ErrorMessage="* 과목을 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>학교군</th>
				<td>
					<asp:radiobuttonlist ID="rbtSchool" CssClass="de-radio" runat="server" RepeatColumns="8" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtSchool" ControlToValidate="rbtSchool" ErrorMessage="* 학교군을 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>브랜드</th>
				<td>
					<asp:radiobuttonlist ID="rbtBrand" CssClass="de-radio" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtBrand" ControlToValidate="rbtBrand" ErrorMessage="* 브랜드를 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>학년</th>
				<td>
					<asp:radiobuttonlist ID="rbtGrade" CssClass="de-radio" runat="server" RepeatColumns="8" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtGrade" ControlToValidate="rbtGrade" ErrorMessage="* 학년을 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>학기</th>
				<td>
					<asp:radiobuttonlist ID="rbtSemester" CssClass="de-radio" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table"></asp:radiobuttonlist>
                    <asp:RequiredFieldValidator ID="rfvRbtSemester" ControlToValidate="rbtSemester" ErrorMessage="* 학기를 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
		</tbody>
		</table><!-- // table-a -->
        
		<div class="title"><!-- title -->
			<h3 class="title title-success">작업정보</h3>
		</div><!-- // title -->

		<table border="0" cellpadding="0" cellspacing="0" class="table entry"><!-- table-a -->
			<tr>
				<th>작업자</th>
				<td colspan="2">
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlUser" runat="server"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvDdlUser" InitialValue="0" ControlToValidate="ddlUser" ErrorMessage="* 작업자를 선택해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
					</div>
				</td>
			</tr>
			<tr>
				<th>작업명</th>
				<td colspan="2">
					<asp:TextBox runat="server" ID="txtTaskTitle" class="large" />
                    <asp:RequiredFieldValidator ID="rfvTaskTitle" ControlToValidate="txtTaskTitle" ErrorMessage="* 작업명을 입력해 주세요." runat="server" ForeColor="red"></asp:RequiredFieldValidator>
				</td>
			</tr>
			<tr>
				<th>지시내용</th>
				<td colspan="2">
					<asp:TextBox runat="server" ID="txtTaskContent" class="large" />
                    <asp:HiddenField ID="txtEntryCount" runat="server" />
				</td>
			</tr>
        </table>

		<div class="title"><!-- title -->
			<h3 class="title title-primary">
                단원 (엔트리)
			</h3>            
		</div><!-- // title -->
        

        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
            <asp:Repeater ID="EntryList" runat="server" OnItemDataBound="EntryList_ItemDataBound" OnItemCommand="EntryList_ItemCommand">
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:Label ID="lblNo" Text=<%#DataBinder.Eval(Container.DataItem , "SeqNo")%> runat="server" />
                            <asp:HiddenField ID="txtIDX" Value=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat="server" />
                            <asp:HiddenField ID="txtLUnitID" Value=<%#DataBinder.Eval(Container.DataItem , "LUnitIdx")%> runat="server" />
                            <asp:HiddenField ID="txtMUnitID" Value=<%#DataBinder.Eval(Container.DataItem , "MUnitIdx")%> runat="server" />                            
                        </td>
                        <td>
                            <asp:DropDownList cssClass="de-select" ID="ddlLUnitID" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLUnitID_OnSelectedIndexChanged" />
                        </td>
                        <td><asp:DropDownList cssClass="de-select" ID="ddlMUnitID" runat="server" /></td>
                        <td><asp:TextBox cssClass="large" ID="txtTitle" Text=<%#DataBinder.Eval(Container.DataItem , "EntryTitle")%>  runat="server" /></td>
                        <td><asp:Button runat="server" ID="btnDelete" border="0" cssClass="btn-code-del" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
<%
    if (Session["uauth"].ToString() == "9")
    { 
%>        
		<div class="section-button"><!-- section-button -->
			<asp:TextBox runat="server" Visible="false" ID="txtTaskId" />
            <asp:TextBox runat="server" Visible="false" ID="txtTaskIdx" />
            <asp:TextBox runat="server" ID="txtAddEntryCnt" cssClass="xsmall" /> 개 엔트리 추가
            <asp:Button runat="server" ID="btnAddTask" cssClass="btn-add" style="border:0" OnClick="btnAddTask_Click"></asp:Button>			                
        </div>
		<div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="btnSave" Text="임시저장" cssClass="btn btn-lg btn-success" OnClick="btnSave_Click"></asp:Button>
			<asp:Button runat="server" ID="btnPublish" Text="최종배포" cssClass="btn btn-lg btn-danger" OnClick="btnPublish_Click"></asp:Button>
		</div><!-- // section-button -->
<%
    }
%>
       

	</div><!-- // contents -->
        
	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>
    </form>

</div><!-- // container -->

<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>

</body>
</html>