﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

public partial class main_XMLImport : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void Upload_Button_Click(object sender, EventArgs e)
    {
        //동영상 업로드
        //0.파일이 있는지 확인
        if ((null == FileUpload1.PostedFile)
            || (0 >= FileUpload1.PostedFile.ContentLength))
        {
            //파일이 선택되지 않았다.
            Response.Write("파일을 선택해 주세요");
            return;
        }

        //1.파일을 서버에 업로드 한다.

        //1-2.업로드
        //업로드될 dir경로를 만든다.
        //string sFileUri = HttpContext.Current.Server.MapPath("~/")
        string sFileUri = HttpContext.Current.Server.MapPath("~/Data")
                            + string.Format(@"\{0}", FileUpload1.PostedFile.FileName);

        //이미 같은 이름의 파일이 있으면 지워준다.
        File.Delete(sFileUri);

        try
        {
            //파일 업로드
            FileUpload1.PostedFile.SaveAs(sFileUri);
        }
        catch (Exception ex)
        {
            //오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "
                            + ex.ToString());
            return;
        }

        //xml을 업로드 하고 여기서 마샬링인가? 언마샬링인가 그걸 여기서 한다.

        XmlSerializer serializer = new XmlSerializer(typeof(edu));
        serializer.UnknownNode += new XmlNodeEventHandler(Serializer_UnknownNode);
        serializer.UnknownAttribute += new XmlAttributeEventHandler(Serializer_UnknownAttribute);
        
        // A FileStream is needed to read the XML document.
        
        // Declare an object variable of the type to be deserialized.
        using (StreamReader sr = new StreamReader(sFileUri))
        {
            String line = sr.ReadToEnd();
            string[] kkk = line.Split(new string[] { "<FILENAME>" }, StringSplitOptions.None);

            foreach (string unitKKK in kkk)
            {
                int preVal = unitKKK.ToString().IndexOf("<FILENAME>");
                int nextVal = unitKKK.ToString().IndexOf("</FILENAME>");
                string imgName = string.Empty;
                if (nextVal > 0)
                    imgName = unitKKK.ToString().Substring(0, nextVal);
            }
            Console.WriteLine(line);
        }
        


        FileStream fs = new FileStream(sFileUri, FileMode.Open);
        edu eduroot;
        try
        {
            
            eduroot = (edu)serializer.Deserialize(fs);
            //데이터 베이스에 넣어야 함
            //파라미터 정의

            string entryCode, entryTitle, entryTitle_c, entryTitle_e, entryTitle_k;

            int intIDXCONT = eduroot.ENTRY[0].INDEXCONTENT.Count();

            entryCode = eduroot.ENTRY[0].entryCode;
            entryTitle = eduroot.ENTRY[0].ENTRYTITLE;
            entryTitle_c = eduroot.ENTRY[0].ENTRYTITLE_C;
            entryTitle_e = eduroot.ENTRY[0].ENTRYTITLE_E;
            entryTitle_k = eduroot.ENTRY[0].ENTRYTITLE_K;

            for (int i = 0; i < intIDXCONT; i++)
            {
                int intBasicCount = eduroot.ENTRY[0].INDEXCONTENT[0].BASIC.Count();

                for (int j=0;j<intBasicCount;j++ )
                {
                    string basicText = eduroot.ENTRY[0].INDEXCONTENT[i].BASIC[j].TEXT;
                }

            }


        }
        catch (Exception ex)
        {
            //오류다!
            Response.Write("파일을 업로드하던중 다음과 같은 오류가 발생 하였습니다\n오류내용 : "
                            + ex.ToString());
            return;
        }
        
        
       




    }

    protected void Serializer_UnknownNode(Object sender, XmlNodeEventArgs e) { 

    }

    protected void Serializer_UnknownAttribute(Object sender, XmlAttributeEventArgs e) { 

    }

}