﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EntryEdit3.aspx.cs" Inherits="main_EntryEdit" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <script src="../js/ckeditor/ckeditor.js"></script>
    <script src="../js/jquery-2.1.3.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.jqtransform.js"></script>
    <script src="../js/ui.js"></script>
    <script src="../js/cjeditor.js"></script>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
    <script>
        
        var LClassNodeList = new Array();

        function addLClassContent() {

            var containerName = "DivLClassContainer";
            var contentCotainerName = "DivLClassContentContainer";
            var hearText = '<div style="border:1px solid red;border-width:2px 1px"><div ID="' + containerName + '">';
            var tailText = '</div></div>';

            var lClassNode = new ContainerHtmlNode( hearText , tailText );
            LClassNodeList.push(lClassNode);

            var lCassNodeIndex = LClassNodeList.length;
            lClassNode.setID( "LClass" + lCassNodeIndex );

            var html = '<div style="border:1px solid red;border-width:2px 1px">';
            html += '<div>'
            html += '<input type="button" value="대제목" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'대제목\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="중제목" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'중제목\' , \'' + contentCotainerName + '\');"/>';
            html += '<input type="button" value="기본 1" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'기본1\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="기본 2" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'기본2\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="블럭" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'블럭\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="표" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'표\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="이미지" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'이미지\' , \'' + contentCotainerName + '\' );"/>';
            html += '<input type="button" value="주석" onclick="addLClassNode(\'LClass' + lCassNodeIndex + '\' ,' + lCassNodeIndex + ' , \'주석\' , \'' + contentCotainerName + '\' );"/>';
            html += '</div >';
            html += '<div ID="' + contentCotainerName + '">';
            html += '</div></div>'

            $("#" + containerName).append(html);
        }

        function addLClassNode(parentID, parentIndex, rectypeName, containerName) {

            //심플 노드를 만든다.
            var simpleNode = new SimpleHtmlNode();

            //컨테이너 노드에 더한다.
            var containerNode = LClassNodeList[parentIndex - 1];
            var addIndex = containerNode.addNode(simpleNode);

            //심플 노드에 아이디를 설정한다.
            var simpleNodeID = parentID + addIndex;
            simpleNode.setID(simpleNodeID);

            //html을 작성한다.
            //var headerText = "<div>" + rectypeName + "<textarea cols='50' id='" + simpleNodeID + "' name='" + simpleNodeID + "' rows='3' >";
            var headerText = "<div>" + rectypeName + "<textarea id='" + simpleNodeID + "' name='" + simpleNodeID + "'>";
            var tailText = "</textarea></div>";

            simpleNode.setHeaderText("<div>" + rectypeName);
            simpleNode.setTailText("</div>");

            // html을 더한다.
            $("#" + containerName).append(headerText + tailText);


            CKEDITOR.replace(simpleNodeID, {
                enterMode: CKEDITOR.ENTER_BR ,
                height: '200px',
                width: '600px' ,
                toolbar: [
                    { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },	// Defines toolbar group with name (used to create voice label) and items in 3 subgroups.
                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],			// Defines toolbar group without name.
                    { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                ]
            });
        }

        function addQuizContent() {
            var html = '<div>';
            html += '<input type="button" value="문제" onclick="addQuizRow();"/>';
            html += '<input type="button" value="보기" onclick="addExampleRow()"/>';
            html += '<input type="button" value="선택지" onclick="addBasic1Row()"/>';
            html += '<input type="button" value="정답" onclick="addBasic2Row()"/>';
            html += '<input type="button" value="해설" onclick="addBasic2Row()"/>';
            html += '</div >';
            html += '<div id="DivLTitle">문제</div>';

            $("#DivQuizContent").append(html);
        }

        function preview() {
            
            var content="";

            for( i = 0 ; i < LClassNodeList.length ;i++ ){
                content += LClassNodeList[i].toHtml();
            }

            d = window.open("about:blank", "_BLANK", "width=500,height=500,scrollbars=1");

            //d.document.write("<pre>");
            d.document.write(content);
        }

</script>
</head>
<body>
    <header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
    </header><!-- header -->
    
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown active">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li class="active"><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

	<div id="contents"><!-- contents -->
        <form id="editform" runat="server">
	        <div class="title"><!-- title -->
			    <h2 class="title">엔트리 등록/편집</h2>
		    </div><!-- // title -->	

		    <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>
			        <tr >
				        <th>엔트리코드</th>
				        <td colspan=3>
				            <div class="input-group">
					            <asp:Label ID="lblEntryNo" runat="server" />
				            </div>
				        </td>
			        </tr>
			        <tr>
				        <th>분 류</th>
				        <td colspan=3>
					        <asp:Label ID="lblCategory" cssClass="medium" runat="server" /><asp:Button ID="SaveCategory" class="btn btn-success" runat="server" Text="수정" />
				        </td>
			        </tr>
			        <tr>
				        <th>작업자</th>
				        <td>
					        <asp:Label ID="lblTasker" runat="server" />
				        </td>
				        <th>최종수정일</th>
				        <td>
					        <asp:Label ID="lblEditDate" runat="server" />
				        </td>
			        </tr>
		        </tbody>
		    </table><!-- // table-a -->
		    <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>		
			        <tr>
				        <th>타이틀</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtEntryTitle" runat="server"/>
				        </td>
			        </tr>
			        <tr>
				        <th>국문</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtEntryTitle_K" runat="server"/>
				        </td>
			        </tr>
			        <tr>
				        <th>영문</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtEntryTitle_E" runat="server"/>
				        </td>
			        </tr>
                    <tr>
				        <th>한자</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtEntryTitle_C" runat="server"/>
				        </td>
			        </tr>
		        </tbody>
		    </table><!-- // table-a -->
        
            <!--에디터 시작-->
            <div ID="DivLClassContainer" style="border:1px solid black;border-width:2px 1px">
                <div>
                    대분류(1)
                    <input type="button" value="대분류+" onclick="addLClassContent();" />
                    <asp:Button ID="bntGetLClass" runat="server" Text="대분류 불러오기"/>
                    <input type="button"value="미리보기" onclick="preview();" />
                </div>

            </div>
            <p/>
            <div id="DivQuizContainer" style="border:1px solid black;border-width:2px 1px">
                <div>
                    퀴즈(2)
                    <%--<input type="button" value="퀴즈+" onclick="addQuizContent();" />
                    <asp:Button ID="btnGetQuiz" runat="server" Text="퀴즈 불러오기"/>--%>
                </div>
                <div ID="DivQuizContent" style="border:1px solid orange;border-width:2px 1px" >
                    <div>
                        <input type="button" value="문제" onclick="addQuestionRow();"/>
                        <input type="button" value="보기" onclick="addExampleRow();"/>
                        <input type="button" value="선택지" onclick="addDistractorRow();"/>
                        <input type="button" value="정답" onclick="addAnswerRow();"/>
                        <input type="button" value="해설" onclick="addExplanationRow();"/>
                        <input type="button" value="해설" onclick="addLClassNode('LClass1', 1, '대제목', 'DivLClassContentContainer');"/>
                    </div >
                    <div id="DivQuestionContainer"></div>
                    <div id="DivExampleContainer"></div>
                    <div id="DivDistractorContainer"></div>
                    <div id="DivAnswerContainer"></div>
                    <div id="DivExplanationContainer"></div>
                </div>
            </div>
        </form>
    </div> 
</body>
</html>
