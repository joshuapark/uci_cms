﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class main_BinderDirectoryDelete : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝
        
        if (Request.Params["BinderIdx"] != null)
        {
            int binderIdx = Convert.ToInt32( Request.Params["BinderIdx"] );
            DBFileInfo();

            DataTable allBinderTable = this.getAllBinder();
            deleteBinderTree( allBinderTable ,  binderIdx);

            Response.Write("<script>opener.doTheSubmit();self.close();</script>");
        }
    }

    private void deleteBinderTree(DataTable allBinderTable ,  int binderIdx ) { 

        DataRow[] childTableList = allBinderTable.Select("ParentIdx=" +  binderIdx);
        foreach (DataRow childRow in childTableList) {
            deleteBinderTree(allBinderTable, Convert.ToInt32(childRow["BinderIdx"]));
        }

        deleteBinder(binderIdx);
    }



    private DataTable getAllBinder() {
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();
        SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TBinder WHERE DelFlag=0 ORDER BY BinderIdx ASC", Conn);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        if (Conn.State == ConnectionState.Open)
            Conn.Close();

        Conn = null;

        return dt;
    }

    private void deleteBinder( int binderIdx ) {
        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@BinderIdx", SqlDbType.Int);
        Cmd.Parameters["@BinderIdx"].Value = binderIdx;

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_Binder_DELETE";
        Con.Open();

        Cmd.ExecuteNonQuery();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;

    }


}