﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.Collections;

public partial class main_BinderAdd : System.Web.UI.Page
{
    //
    // 공지사항 목록 반환 
    // 필수 기본값 pageNo=1, pageSize=30
    //
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";

    private ListItemCollection relationItems = new ListItemCollection
    {
        new ListItem("없음" , "0"),
        new ListItem("이전" , "1"),
        new ListItem("다음" , "2"),
        new ListItem("속함" , "3"),
        new ListItem("반대" , "4"),
        new ListItem("유사" , "5"),
    };


    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝

        if (!IsPostBack)
        {
            if (Request.Params["pageNo"] != null)
                pageNo = Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();

            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter("SELECT BinderIdx, BinderTitle, BinderLevel FROM TBinder WHERE DelFlag=0 ORDER BY BinderIdx ASC", Conn);
            DataSet ds = new DataSet();

            sda.Fill(ds, "TBinder");
            DataTable dtBinder = ds.Tables["TBinder"];
            dtBinder.DefaultView.RowFilter = "BinderLevel = 1";
            ddlCategory1.DataSource = dtBinder;
            ddlCategory1.DataTextField = "BinderTitle";
            ddlCategory1.DataValueField = "BinderIdx";
            ddlCategory1.CssClass = "de-radio";
            ddlCategory1.DataBind();
            //ddlCategory1.SelectedIndexChanged += ddlCategory1_SelectedIndexChanged;
            ListItem li = new ListItem("선택","0");
            ddlCategory1.Items.Add(li);
            ddlCategory1.SelectedValue = "0";
            Conn.Close();
            sda = null;
            ds = null;

            ddlCategory2.Visible = false;
            ddlCategory3.Visible = false;
            ddlCategory4.Visible = false;
            ddlCategory5.Visible = false;
            ddlCategory6.Visible = false;

            AddReationButton.Visible = false;
            AddEntryButton.Visible = false;
        }

        string binderIdx = hdBinderIdx.Value;
        if (!binderIdx.Equals(string.Empty) || binderIdx.Length > 0) {
            this.getBinderRelationList();
            this.getBinderEntryList();
        }
        
        Page.MaintainScrollPositionOnPostBack = true;
    }

    private void getBinderRelationList() {
        //바인더 관계 가져오기
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Binder_Relation_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@BinderIdx", SqlDbType.Int);

        Cmd.Parameters["@BinderIdx"].Value = Convert.ToInt32(hdBinderIdx.Value);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "bbs_relation");

        rpRelationList.DataSource = ds;
        rpRelationList.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    private void getBinderEntryList() {
        //바인더 관계 가져오기
        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);

        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "USP_Binder_Entry_List_SELECT";
        Cmd.CommandType = CommandType.StoredProcedure;

        Cmd.Parameters.Add("@BinderIdx", SqlDbType.Int);

        Cmd.Parameters["@BinderIdx"].Value = Convert.ToInt32(hdBinderIdx.Value);

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataSet ds = new DataSet();

        sda.Fill(ds, "bbs_entry");

        rpEntryList.DataSource = ds;
        rpEntryList.DataBind();

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
    protected void ddlCategory1_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdParentID.Value = ddlCategory1.SelectedValue;
        hdLevel.Value = "1";
        BindCategory(2);

    }

    protected void BindCategory(int level)
    {
        DBFileInfo();

        if (level == 2)
        {
            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            int intCate1 = Convert.ToInt32(ddlCategory1.SelectedValue);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT BinderIdx, BinderTitle FROM TBinder WHERE BinderLevel=2 AND DelFlag=0 ORDER BY BinderIdx ASC", Conn);
            DataSet ds = new DataSet();
            sda.Fill(ds, "TBinder");
            DataTable dtBinder = ds.Tables["TBinder"];
            //dtBinder.DefaultView.RowFilter = "BinderLevel = 2";
            ddlCategory2.DataSource = dtBinder;
            ddlCategory2.DataTextField = "BinderTitle";
            ddlCategory2.DataValueField = "BinderIdx";
            ddlCategory2.CssClass = "de-radio";
            ddlCategory2.DataBind();
            ListItem li = new ListItem("선택", "0");
            ddlCategory2.Items.Add(li);
            ddlCategory2.SelectedValue = "0";

            if ( ddlCategory2.Items.Count > 1)
                ddlCategory2.Visible = true;                
            else
            {
                ddlCategory3.Visible = false;
                ddlCategory3.SelectedValue = "0";
            }
                
        }
        if (level == 3)
        {
            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();

            int intCate1 = Convert.ToInt32(ddlCategory2.SelectedValue);

            SqlDataAdapter sda = new SqlDataAdapter("SELECT BinderIdx, BinderTitle FROM TBinder WHERE BinderLevel = 3 AND DelFlag=0 ORDER BY BinderIdx ASC", Conn);
            DataSet ds = new DataSet();
            sda.Fill(ds, "TBinder");
            DataTable dtBinder = ds.Tables["TBinder"];
            //DataTable dtSubject = ds.Tables["TBinder"];
            //dtBinder.DefaultView.RowFilter = "BinderLevel = 3";
            ddlCategory3.DataSource = dtBinder;
            ddlCategory3.DataTextField = "BinderTitle";
            ddlCategory3.DataValueField = "BinderIdx";
            ddlCategory3.CssClass = "de-radio";
            ddlCategory3.DataBind();
            ListItem li = new ListItem("선택", "0");
            ddlCategory3.Items.Add(li);
            ddlCategory3.SelectedValue = "0";

            if (ddlCategory3.Items.Count > 1)
                ddlCategory3.Visible = true;
            else
            {
                ddlCategory4.Visible = false;
                ddlCategory4.SelectedValue = "0";
            }
        }
        if (level == 4)
        {
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();

        int intCate1 = Convert.ToInt32(ddlCategory3.SelectedValue);
        SqlDataAdapter sda = new SqlDataAdapter("SELECT BinderIdx, BinderTitle FROM TBinder WHERE BinderLevel=4 AND DelFlag=0 ORDER BY BinderIdx ASC", Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TBinder");
        DataTable dtBinder = ds.Tables["TBinder"];
            //DataTable dtSubject = ds.Tables["TBinder"];
            //dtBinder.DefaultView.RowFilter = "BinderLevel = 4";
            ddlCategory4.DataSource = dtBinder;
            ddlCategory4.DataTextField = "BinderTitle";
            ddlCategory4.DataValueField = "BinderIdx";
            ddlCategory4.CssClass = "de-radio";
            ddlCategory4.DataBind();
            ListItem li = new ListItem("선택", "0");
            ddlCategory4.Items.Add(li);
            ddlCategory4.SelectedValue = "0";

            if (ddlCategory4.Items.Count > 1)
                ddlCategory4.Visible = true;
            else
            {
                ddlCategory5.Visible = false;
                ddlCategory5.SelectedValue = "0";
            }
        }
        if (level == 5)
        {
        SqlConnection Conn = new SqlConnection(connectionString);
        Conn.Open();

        int intCate1 = Convert.ToInt32(ddlCategory4.SelectedValue);
        SqlDataAdapter sda =  new SqlDataAdapter("SELECT BinderIdx, BinderTitle, BinderLevel FROM TBinder WHERE DelFlag=0 ORDER BY BinderIdx ASC", Conn);
        DataSet ds = new DataSet();
        sda.Fill(ds, "TBinder");
        DataTable dtBinder = ds.Tables["TBinder"];
            //DataTable dtSubject = ds.Tables["TBinder"];
            dtBinder.DefaultView.RowFilter = "BinderLevel = 5";
            ddlCategory5.DataSource = dtBinder;
            ddlCategory5.DataTextField = "BinderTitle";
            ddlCategory5.DataValueField = "BinderIdx";
            ddlCategory5.CssClass = "de-radio";
            ddlCategory5.DataBind();
            ListItem li = new ListItem("선택", "0");
            ddlCategory5.Items.Add(li);
            ddlCategory5.SelectedValue = "0";

            if (ddlCategory5.Items.Count > 1)
                ddlCategory5.Visible = false;
            else
            {
                ddlCategory6.Visible = false;
                ddlCategory6.SelectedValue = "0";
            }
        }

    }
    protected void ddlCategory2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory2.SelectedValue == "0")
        {
            ddlCategory3.Visible = false;
            ddlCategory3.SelectedValue = "0";
            ddlCategory4.Visible = false;
            ddlCategory4.SelectedValue = "0";
            ddlCategory5.Visible = false;
            ddlCategory5.SelectedValue = "0";
            ddlCategory6.Visible = false;
            ddlCategory6.SelectedValue = "0";
        }
        else
        {
            hdParentID.Value = ddlCategory2.SelectedValue;
            hdLevel.Value = "2";
            BindCategory(3);
        }
    }
    protected void ddlCategory3_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory3.SelectedValue == "0")
        {
            ddlCategory4.Visible = false;
            ddlCategory4.SelectedValue = "0";
            ddlCategory5.Visible = false;
            ddlCategory5.SelectedValue = "0";
            ddlCategory6.Visible = false;
            ddlCategory6.SelectedValue = "0";
        }
        else
        {
            hdParentID.Value = ddlCategory3.SelectedValue;
            hdLevel.Value = "3";
            BindCategory(4);
        }

    }
    protected void ddlCategory4_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory4.SelectedValue == "0")
        {
            ddlCategory5.Visible = false;
            ddlCategory5.SelectedValue = "0";
            ddlCategory6.Visible = false;
            ddlCategory6.SelectedValue = "0";
        }
        else
        {
            hdParentID.Value = ddlCategory4.SelectedValue;
            hdLevel.Value = "4";
            BindCategory(5);
            
        }

    }
    protected void ddlCategory5_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory5.SelectedValue == "0")
        {
            ddlCategory6.Visible = false;
            ddlCategory6.SelectedValue = "0";
        }
        else
        {
            hdParentID.Value = ddlCategory5.SelectedValue;
            hdLevel.Value = "5";
            BindCategory(6);
            

        }

    }

    protected void ddlCategory6_SelectedIndexChanged(object sender, EventArgs e)
    {
        hdParentID.Value = ddlCategory6.SelectedValue;
        hdLevel.Value = "6";
    }


    protected void AddBinderButton_Click(object sender, EventArgs e)
    {
        if (txtBinderTitle.Text.Length == 0)
        {
            showMessage("바인더명을 입력하여 주십시오.");
            txtBinderTitle.Focus();
            return;
        }

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@parentid", SqlDbType.Int);
        Cmd.Parameters.Add("@title", SqlDbType.NVarChar, 50);
        Cmd.Parameters.Add("@memo", SqlDbType.NText);
        Cmd.Parameters.Add("@level", SqlDbType.Int);
        Cmd.Parameters.Add("@useridx", SqlDbType.Int);

        Cmd.Parameters["@parentid"].Value = Convert.ToInt32( hdParentID.Value ) ;
        Cmd.Parameters["@title"].Value = txtBinderTitle.Text;
        Cmd.Parameters["@memo"].Value = txtBinderMemo.Text;
        int binderLevel = Convert.ToInt32( hdLevel.Value );
        if (binderLevel < 6) 
        {
            binderLevel++;
        }
        Cmd.Parameters["@level"].Value = binderLevel;
        Cmd.Parameters["@useridx"].Value = 1;

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_Binder_INSERT_2";
        Con.Open();

        SqlDataAdapter sda = new SqlDataAdapter(Cmd);
        DataTable dt = new DataTable();

        sda.Fill(dt);
        DataRow binderRow = dt.Rows[0];
        string binderIdx = Convert.ToString( binderRow["BinderIdx"] );
        hdBinderIdx.Value = binderIdx;

        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
        showMessage("저장되었습니다.");

        AddReationButton.Visible = true;
        AddReationButton.PostBackUrl = "~/main/BinderRelationAdd.aspx?binderidx=" + binderIdx;
        AddEntryButton.Visible = true;
        AddEntryButton.PostBackUrl = "~/main/BinderEntryAdd.aspx?binderidx=" + binderIdx;
        //SaveButton.Visible = true;
    }

    private void showMessage(String message)
    {
        string script = "alert(\"" + message + "\");";
        ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", script, true);
    }

    protected void rpRelationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DropDownList ddlRelation = e.Item.FindControl("ddlRelation") as DropDownList;

            ddlRelation.DataSource = relationItems;
            ddlRelation.DataBind();

            int relation = Convert.ToInt16(DataBinder.Eval(e.Item.DataItem, "Relation"));
            ddlRelation.SelectedValue = Convert.ToString ( relation );
        }
    }
    

    protected void rpEntryList_ItemCommand(object source, RepeaterCommandEventArgs e) {

        RepeaterItem repeaterItem = rpRelationList.Items[e.Item.ItemIndex];
        Label lblEntryIdx = repeaterItem.FindControl("lblNo") as Label;
        int binderIdx = Convert.ToInt32(hdBinderIdx.Value);
        int entryIdx = Convert.ToInt32(lblEntryIdx.Text);

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();

        Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "DELETE FROM TBinderEntry WHERE BinderIdx =" + binderIdx + " AND EntryIdx=" + entryIdx;
        Cmd.CommandType = CommandType.Text;

        Con.Open();
        Cmd.ExecuteNonQuery();
        if (Con.State == ConnectionState.Open)
            Con.Close();

        this.getBinderEntryList();
    }

    protected void reRelationList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        int relationBinderIdx;
        RepeaterItem repeaterItem = rpRelationList.Items[e.Item.ItemIndex];
        HiddenField hdRelationBinderIdx = repeaterItem.FindControl("hdRelationBinderIdx") as HiddenField;
        relationBinderIdx = Convert.ToInt32(hdRelationBinderIdx.Value);
        relationBinderIdx = Convert.ToInt32(hdBinderIdx.Value);
        
        int binderIdx = Convert.ToInt32(hdBinderIdx.Value);

        DBFileInfo();

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();

        Cmd = new SqlCommand();
        Cmd.Connection = Con;
        Cmd.CommandText = "DELETE FROM TBinderRelation WHERE BinderIdx =" + binderIdx + " AND RelationBinderIdx=" + relationBinderIdx;
        Cmd.CommandType = CommandType.Text;

        Con.Open();
        Cmd.ExecuteNonQuery();
        if (Con.State == ConnectionState.Open)
            Con.Close();

        this.getBinderRelationList();
    }



}