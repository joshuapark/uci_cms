﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckEdit_test.aspx.cs" Inherits="main_EntryEdit2" %>
<%@ Import Namespace="System.Data" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <script src="http://ajax.microsoft.com/ajax/jquery/jquery-1.4.2.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<link href="../css/editor.css" rel="stylesheet">
	<link href="../css/jquery-ui.css" rel="stylesheet">
	<link href="../css/jquery.simplecolorpicker.css" rel="stylesheet">
    
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
<script src="../js/ckeditor/ckeditor.js"></script>
<script src="../js/jquery.ui/jquery.ui.js"></script>
<script src="../js/jquery.nestedSortable.js"></script>
<script src="../js/jquery.simplecolorpicker.js"></script>
    
<script>
    /** jquery.nestedSortable.js 내의 소스 참고 **/
   
    $(document).ready(function(){

        window.onload = function () {
            hiered = preview();
        }

        $(".editor").nestedSortable({
            forcePlaceholderSize: true,
            handle: "div",
            placeholder:"editor_placeholder",
            items: "li",
            revert: 250,
            tabSize: 10,
            toleranceElement: "> div",
            maxLevels:2,						/* 움직이는 레벨 영역 지정 */
            protectRoot:"true",
            cancel: ".editor_textarea",

            isAllowed: function(item,parent){
                /** DRAG & DROP 허용 조건에 대해 넣어 줌: false이면 못들어감 **/

                return true;
            }
        });

        $('#toHIER').click(function(e){

            //컨텐츠를 어레이로 뽑아 냅니다. --> jquery.nestedSortable.js 안의 toHierarchy 참고

            hiered = $('ol.editor').nestedSortable('toHierarchy', {startDepthCount: 0});
            hiered = dump(hiered);

            d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(hiered);
        })

        $('#toArr').click(function(e){
            arraied = $('ol.editor').nestedSortable('toArray', {startDepthCount: 0});
            arraied = dump(arraied);

            d=window.open("about:blank","_BLANK","width=500,height=500,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(arraied);
        })


        $('#save').click(function (e) {

            var edu = new Object();

            //대분류 만들기
            var lunitArray = new Array();
            $("ol[id='olLUnit']").children("li[id^='list']").each(function () {
                lunit = new Object();
                lunit.idx = $(this).val();
                lunit.rectype = $(this).children('#rectype').val();

                var secondArray = new Array();
                $(this).children().children("li[id^='list']").each(function () {
                    second = new Object();
                    second.idx = $(this).val();
                    var rectype = $(this).children('#rectype').val();
                    second.rectype = rectype;

                    if (rectype == 'IMAGE' || rectype == 'MATRIX_TABLE') {
                        var thirdArray = new Array();
                        $(this).children().children("li[id^='list']").each(function () {
                            var third = new Object();
                            third.idx = $(this).val();
                            third.rectype = $(this).children('#rectype').val();
                            third.content = $(this).children(".editor_textarea").html();
                            thirdArray.push(third);
                        });
                        second.content = JSON.stringify(thirdArray);
                    } else {
                        second.content = $(this).children(".editor_textarea").html();
                    }

                    secondArray.push(second);
                });
                lunit.list = secondArray;
                lunitArray.push(lunit);
            });
            edu.lunit = lunitArray;

            //퀴즈 만들기
            var quizArray = new Array();
            $("ol[id='olQuiz']").children("li[id^='list']").each(function () {
                quiz = new Object();
                quiz.idx = $(this).val();
                var rectype = $(this).children('#rectype').val();
                second.rectype = rectype;
                var secondArray = new Array();
                $(this).children().children("li[id^='list']").each(function () {
                    second = new Object();
                    second.idx = $(this).val();
                    second.rectype = $(this).children('#rectype').val();

                    if (rectype == 'IMAGE' || rectype == 'MATRIX_TABLE') {
                        var thirdArray = new Array();
                        $(this).children().children("li[id^='list']").each(function () {
                            var third = new Object();
                            third.idx = $(this).val();
                            third.rectype = $(this).children('#rectype').val();
                            third.content = $(this).children(".editor_textarea").html();
                            thirdArray.push(third);
                        });
                        second.content = JSON.stringify(thirsArray);
                    } else {
                        second.content = $(this).children(".editor_textarea").html();
                    }

                    secondArray.push(second);
                });

                quiz.list = secondArray;
                quizArray.push(quiz);
            });
            edu.quiz = quizArray;

            //태그 만들기
            $("ol[id='olTag']").children("li[id^='list']").each(function () {
                tag = new Object();
                tag.idx = $(this).val();
                tag.rectype = $(this).children('#rectype').val();
                tag.content = $(this).children(".editor_textarea").html();
                edu.tag = tag;
            });


            //alert(JSON.stringify(firstArray));
            $('#saveString').val(JSON.stringify(edu));
            $('#addForm').submit();
        });


        $("#preView").click(function (e) {

            hiered = preview();

            d = window.open("about:blank", "_BLANK", "width=1000,height=1000,scrollbars=1");

            d.document.write("<pre>");
            d.document.write(hiered);
        });
        //마크업 내에 
        // 컬러 피커 구동 ../js/jquery.simplecolorpicker.js
        $('select[name="colorpicker"]').simplecolorpicker({ picker: true });


        $('#addLUnit').click(function(){
            var IndexContentNumber = Number($("#indexContentNumber").val());
            addIndexContent(IndexContentNumber);

            var strIndexContentNumber = String(IndexContentNumber + 1);
            $("#indexContentNumber").val(strIndexContentNumber);
            $("#spanIndexContentNumber").text(strIndexContentNumber);
    });

        $('#addQuiz').click(function(){
            var QuizNumber = Number($("#QuizNumber").val());
            addQuiz(QuizNumber);

            var strQuizNumber = String(QuizNumber + 1);
            $("#QuizNumber").val(strQuizNumber);
            $("#spanQuizNumber").text(strQuizNumber);
        })

    });

    function addIndexContent(IndexContentNumber){

        var recTypeSize = Number($("#allRectTypeCount").val());

        var html = "";
        html += '<li id="list_'+ recTypeSize +'" value="-1" >';
        html += '<input type="hidden" id="rectype" value="INDEXCONTENT" /><div>';
        html += '<span class="glyphicon glyphicon-move">분류</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a>';
        html += '<span class="editor_control_container">';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'MAINTITLE\');" onmouseover="" style="cursor: pointer;">대제목 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'SUBTITLE\');" onmouseover="" style="cursor: pointer;">중제목 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BASIC\');" onmouseover="" style="cursor: pointer;">기본1 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BASIC2\');" onmouseover="" style="cursor: pointer;">기본2 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'BOX\');" onmouseover="" style="cursor: pointer;">블럭 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'MATRIX_TABLE\');" onmouseover="" style="cursor: pointer;">표 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'IMAGE\');" onmouseover="" style="cursor: pointer;">이미지 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'INDEXCONTENT_' + IndexContentNumber + '\',\'ANNOTATION\');" onmouseover="" style="cursor: pointer;">주석 </span>';
        html += '</span>';
        html += '</div>';
        html += '<ol id="resultContent_INDEXCONTENT_' + IndexContentNumber + '"></ol></li>';

        $("#olLUnit").append(html);

        $("#allRectTypeCount").val(String(recTypeSize + 1));
        
    }

    function addQuiz(QuizNumber){

        var recTypeSize = Number($("#allRectTypeCount").val());

        var html = "";
        html += '<li id="list_'+ recTypeSize +'" value="-1" >';
        html += '<input type="hidden" id="rectype" value="QUIZ" /><div>';
        html += '<span class="glyphicon glyphicon-move">문제시작</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a>';
        html += '<span class="editor_control_container">';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'QUESTION\');" onmouseover="" style="cursor: pointer;">문제 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'EXAMPLE\');" onmouseover="" style="cursor: pointer;">보기 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'DISTRACTOR\');" onmouseover="" style="cursor: pointer;">선택지 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'ANSWER\');" onmouseover="" style="cursor: pointer;">정답 </span>';
        html += '<span class="label label-info editor_controls" onclick="addContent(\'QUIZ_' + QuizNumber + '\',\'EXPLANATION\');" onmouseover="" style="cursor: pointer;">해설 </span>';
        html += '</span>';
        html += '</div>';
        html += '<ol id="resultContent_QUIZ_' + QuizNumber + '"></ol></li>';

        $("#olQuiz").append(html);

        $("#allRectTypeCount").val(String(recTypeSize + 1));
    }


    function dump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        //The padding given at the beginning of the line.
        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') { //Array/Hashes/Objects
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') { //If it is an array,
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += dump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else { //Strings/Chars/Numbers etc.
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function preview(arr, level) {

        var html="";

        $("ol[id^='resultContent']").children().children(".editor_textarea").each(function () {

            html += $(this).html() + "</br>";

        });

        return html;
    }

    function getRecTypeName(recType){
        var result = "";
        switch (recType) {

            case "INDEXCONTENT":
                result = "대분류";
                break;
            case "MAINTITLE":
                result = "대제목";
                break;
            case "SUBTITLE":
                result = "중제목";
                break;
            case "BOX":
                result="블럭";
                break;
            case "BASIC":
                result = "기본";
                break;
            case "BASIC2":
                result = "기본2";
                break;
            case "RELATEDSEARCH":
                result = "연관검색어";
                break;
            case "MATRIX_TABLE":
                result="표";
                break;
            case "ANNOTATION":
                result = "주석";
                break;
            case "IMAGE":
                result="이미지";
                break;
            case "TAG":
                result = "태그";
                break;
            case "QUIZ":
                result = "문제";
                break;
            case "INFOTABLE":
                result = "정보";
                break;
            case "QUESTION":
                result = "문제";
                break;
            case "EXAMPLE":
                result = "보기";
                break;
            case "DISTRACTOR":
                result = "선택지";
                break;
            case "EXPLANATION":
                result = "설명";
                break;
            case "ANSWER":
                result = "정답";
                break;
        }

        return result;
    }

    function addContent(panelName , recType , content) {

        if ( typeof content == "undefined" ){
            content = "";
        }

        var html = "";
        var recTypeSize = Number($("#allRectTypeCount").val());

        html += '<li id="list_' + recTypeSize + '" value="-1" >';
        html += '<input type="hidden" id="rectype" value="' + recType + '" /><div>';
        html += '<span class="glyphicon glyphicon-move">' + getRecTypeName(recType) + '</span>';
        html += '<a href="#!" class="btn-close" onclick="deleteContent(\'list_' + recTypeSize + '\');" style=\'float:right\'></a><span class="editor_control_container"></span></div>';
        html += '<div class="editor_textarea" id="editor_'+ recTypeSize +'" name="'+ recType +'" value="0" contenteditable="true">'+ content +'</div>';
        html += '<script>';
        html += 'CKEDITOR.inline(\'editor_' + recTypeSize + '\', {';
        html += 'filebrowserBrowseUrl : \'/main/ckfinder/ckfinder.html\',';
        html += 'filebrowserUploadUrl: \'Upload.ashx\',';
        html += 'toolbar: [';
        html += '{ name: \'document\', items: [\'Source\', \'-\', \'Preview\', \'Table\', \'Image\'] },';
        html += '[\'JustifyLeft\', \'JustifyCenter\', \'JustifyRight\'],';
        html += '{ name: \'basicstyles\', items: [\'BulletedList\', \'Bold\', \'Italic\', \'Subscript\', \'Superscript\', \'Underline\', \'Link\', ] }';
        html += ']});';
        html += '</';
        html += 'script>';
        html += '</li>';

        $("#resultContent_" + panelName).append(html);
        $("#resultContent_" + panelName).html();

        $("#allRectTypeCount").val(String(recTypeSize + 1));
    }

    function getContentsFromServer( idxList ){

        var obj = {};
        obj.idxList = idxList;

        $.ajax({
            type: "POST",
            url: "EntryEdit.aspx/getContents",
            data: JSON.stringify(obj) ,
            contentType: "application/json; charset=utf-8",
            dataType:"json",
            success: function( contents ) {
                pasteContents(contents.d);
            }
        });
	    
    }

    function pasteContents( contents ){

        var parentMode = "";
        var parentNumber = 0;
        $.each($.parseJSON(contents) , function(i ,object){
            if( object.RecType == 'INDEXCONTENT' ){

                parentMode = object.RecType;
                var IndexContentNumber = Number($("#indexContentNumber").val());
                parentNumber = IndexContentNumber;
                addIndexContent(IndexContentNumber);
                var strIndexContentNumber = String(IndexContentNumber + 1);
                $("#indexContentNumber").val(strIndexContentNumber);
                $("#spanIndexContentNumber").text(strIndexContentNumber);

            }else if( object.RecType == 'QUIZ' ){

                parentMode = object.RecType;
                var QuizNumber = Number($("#QuizNumber").val());
                parentNumber = QuizNumber;
                addQuiz(QuizNumber);

                var strQuizNumber = String(QuizNumber + 1);
                $("#QuizNumber").val(strQuizNumber);
                $("#spanQuizNumber").text(strQuizNumber);

            }else if( object.RecType == 'EXAMPLE' ){
                //현재 추가된 Parent에 추가한다.
                
            }else{
                var panelName = parentMode + '_' + parentNumber;
                addContent(panelName , object.RecType , object.Content);
            }
        });
    }

    function deleteContent( deleteID ){
        $("#" + deleteID).remove();
    }

</script>
<body>

<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">

  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4 active"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->

	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">검수</h2>
		</div><!-- // title -->	
        <div class="section-button"><!-- section-button -->
			<div class="pull-right">
                <a class="btn btn-sm btn-primary" href="CheckList.aspx" >검수목록</a> 
			</div>
		</div><!-- // section-button -->
		<table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		    <colgroup>
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
			    <col style="width: 110px;">
			    <col style="width: auto;">
		    </colgroup>
		    <tbody>
			    <tr>
				    <th>작업명</th>
				    <td>
                        <asp:Label runat="server" ID="txtTaskTitle" class="large" />
				    </td>
				    <th>엔트리수</th>
				    <td>
                        <asp:Label runat="server" ID="txtEntryCount" class="medium" />
				    </td>
				    <th>관리자</th>
				    <td>
                        <asp:Label runat="server" ID="txtManager" class="medium" />
				    </td>
				    <th>작업자</th>
				    <td>
                        <asp:Label runat="server" ID="txtUser" class="medium" />
				    </td>
			    </tr>
			    <tr>
				    <th>지시내용</th>
				    <td colspan="3">
                        <asp:Label runat="server" ID="txtTaskContent" class="large" />
				    </td>
				    <th>작업배포일</th>
				    <td>
                        <asp:Label runat="server" ID="txtPublishDate" class="medium" />
				    </td>
				    <th>작업완료일</th>
				    <td>
                        <asp:Label runat="server" ID="txtFinishDate" class="medium" />
				    </td>
			    </tr>
		    </tbody>
        </table>

    <form name="addForm" runat="server">
        <asp:HiddenField id="saveString" runat="server"/>
        <!-- column start -->
        <div class="col-md-3 box" style="width: 25%; overflow-y: scroll; max-height: 1200px;"> 
		    <div class="title"><!-- title -->
                <asp:Button id="btnAllCheck" cssClass="btn btn-sm btn-danger" runat="server" Text="최종완료" OnClick="btnAllCheck_Click"></asp:Button>
			    <h4 class="title">타이틀 <asp:Label ID="txtCount" Forecolor="DarkGray" Font-Size="Small" runat="server"></asp:Label></h4>
		    </div><!-- // title -->	
			<div class="table-responsive">
                <table border="0" class="table table-list" style="padding:0; border-spacing:0"><!-- table-a -->
                    <asp:Repeater ID="EntryList" runat="server" >
                        <HeaderTemplate>
<%

    if (EntryList.Items.Count == 0)
    {
%>
                            <tr>
                                <td colspan="2" style="text-align:center">등록된 엔트리가 없습니다.</td>
                            </tr>                    
<%
    }
%>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="txtIDX" Value=<%#DataBinder.Eval(Container.DataItem , "Idx")%> runat="server" />
                                    <asp:HiddenField ID="txtStatus" Value=<%#DataBinder.Eval(Container.DataItem , "Status")%> runat="server" />
                                    <a href='CheckEdit.aspx?Idx=<%=strTaskIdx %>&EntryIdx=<%#DataBinder.Eval(Container.DataItem , "Idx")%>'>
                                        <asp:Label ID="lblTitle" Text=<%#DataBinder.Eval(Container.DataItem, "EntryTitle")%>  runat="server" />
                                    </a>
                                </td>
                                <td><asp:Label ID="lblStatus" Text=<%#DataBinder.Eval(Container.DataItem , "Data")%>  runat="server" /></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
			</div>
        </div>
        <!-- column end -->

        <div class="col-md-9 box" style="width: 75%;">
            <asp:Button class="btn btn-success" ID="btnView" runat="server" Text="미리보기" OnClick="btnView_Click"></asp:Button>
            <asp:Button class="btn btn-success active"  runat="server" Text="편집보기"></asp:Button>
            <a class="btn btn-success" id="view2" onclick="javascript:showPannel('1');">미리보기2</a>
            <a class="btn btn-success active"  id="edit2" onclick="javascript:showPannel('2');">편집보기2</a>
            
            <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>
                    <tr >
				        <th>엔트리코드</th>
				        <td>
				            <div class="input-group">
					            <asp:Label ID="txtEntryNo" CssClass="large" runat="server" />
				            </div>
				        </td>
				        <th>최종수정일</th>
				        <td>
					        <asp:Label ID="txtEditDate" CssClass="medium" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>분 류</th>
				        <td colspan=4>
                            <asp:Label ID="txtCategory" CssClass="medium" runat="server" />
					        <!--<a class="btn btn-success" onclick="windows.open('CategoryEdit.aspx', 600, 300);" >수정</a>-->
				        </td>
			        </tr>
		        </tbody>
		        </table><!-- // table-a -->
		        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		        <colgroup>
			        <col style="width: 110px;">
			        <col style="width: auto;">
			        <col style="width: 110px;">
			        <col style="width: auto;">
		        </colgroup>
		        <tbody>		
			        <tr>
				        <th>타이틀</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>국 문</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_K" CssClass="large" runat="server" />
				        </td>
			        </tr>
                    <tr>
				        <th>원어명</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_E" CssClass="large" runat="server" />
				        </td>
			        </tr>
			        <tr>
				        <th>한 자</th>
				        <td colspan=4>
					        <asp:TextBox ID="txtTitle_C" CssClass="large" runat="server" />
				        </td>
			        </tr>
		        </tbody>
            </table><!-- // table-a -->
        <div id="viewArea">  
            보이기 영역<br />
            보이기 영역<br />
            보이기 영역<br />
            보이기 영역<br />
            보이기 영역<br />
            보이기 영역<br />
            보이기 영역<br />

        </div>
        <div id="editArea">    
        <% 
            if (entryTable != null) { 
            DataRow[] indexContentRowList = entryTable.Select("ParentIdx=0 AND RecType='INDEXCONTENT'", "SortNo ASC");
        %>
        <input type="hidden" id="allRectTypeCount" value="<%=entryTable.Rows.Count + 1 %>" />
        <input type="hidden" id="indexContentNumber" value="<%=indexContentRowList.Length %>" />
        <div class="panel panel-gray" id="panelIndexContent"><!-- 대분류 panel 시작-->
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head"></span>			
				
				<span class="panel-title editor_head_title">대분류<span class="badge" id="spanIndexContentNumber"><%=indexContentRowList.Length %> </span></span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelIndexContent');"></a>
				
				<span class="editor_head_container">
				<button type="button" class="btn btn-sm btn-primary" id="addLUnit">대분류+</button>
				<a href="OpenContents.aspx?type=cont" target="_blank" class="btn btn-sm btn-primary">불러오기</a>  
				</span>
            </div>
            <div class="panel-body" >
            <ol class='editor vertical' id="olLUnit">
            
        <!-- 대분류 가져오기-->
        <% 
           
            for(int i = 0 ; i < indexContentRowList.Length ;i++) {
                DataRow indexContentRow = indexContentRowList[i];
        %>
            
                    <li id="list_<% =Convert.ToString( indexContentRow["SortNo"])%>" value="<%=Convert.ToString(indexContentRow["Idx"])%>" >
                    <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentRow["RecType"])%>" />
                    <div>
                        <span class="glyphicon glyphicon-move">분류</span> 
				        <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( indexContentRow["SortNo"])%>');"></a>
				        <span class="editor_control_container">
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','MAINTITLE');" onmouseover="" style="cursor: pointer;">대제목 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','SUBTITLE');" onmouseover="" style="cursor: pointer;">중제목 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BASIC');" onmouseover="" style="cursor: pointer;">기본1 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BASIC2');" onmouseover="" style="cursor: pointer;">기본2 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','BOX');" onmouseover="" style="cursor: pointer;">블럭 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','MATRIX_TABLE');" onmouseover="" style="cursor: pointer;">표 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','IMAGE');" onmouseover="" style="cursor: pointer;">이미지 </span>
				            <span class="label label-info editor_controls" onclick="addContent('INDEXCONTENT_<%=i%>','ANNOTATION');" onmouseover="" style="cursor: pointer;">주석 </span>			  
                        </span>     
                    </div>
                    <ol id="resultContent_INDEXCONTENT_<%=i%>">
            
        <%  
                int parentIdx = Convert.ToInt32(indexContentRow["Idx"]);
                DataRow[] childRowList = entryTable.Select("ParentIdx=" + parentIdx , "SortNo ASC");
                for (int j = 0; j < childRowList.Length; j++) {
                    DataRow indexContentChildRow = childRowList[j];

                    if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") || Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ) {
                        string content = Convert.ToString(indexContentChildRow["Content"]);
        %>
                        <li id="list_<%=Convert.ToString( indexContentRow["SortNo"] ) %>>" value="<%=Convert.ToString(indexContentRow["Idx"])%>">
                            <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"]) %>" />
                            <div>
                                <span class="glyphicon glyphicon-move"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( indexContentRow["SortNo"])%>');"></a>
                            </div>
                            <ol>
                                <li id="list_CAPTION" value="-2" draggable="false">
                                    <input type="hidden" id="rectype" value="CAPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">캡션</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<CAPTION>");
                                            if(captionStartIndex != -1){
                                                string captionEndTag = "</CAPTION>";
                                                int captionEndIndex = content.IndexOf(captionEndTag);
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex + captionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_DESCRIPTION" value="-2">
                                    <input type="hidden" id="rectype" value="DESCRIPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">설명</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>1" contenteditable="true" >
                                        <%
                                            
                                            int descriptionStartIndex = content.IndexOf("<DESCRIPTION>");
                                            if (descriptionStartIndex != -1)
                                            {
                                                string descriptionEndTag = "</DESCRIPTION>";
                                                int descriptionEndIndex = content.IndexOf(descriptionEndTag);
                                                string caption = content.Substring(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                                                content = content.Remove(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex + descriptionEndTag.Length);
                                                Response.Write(convertXmlToHtml(caption));
                                            }
                                        %>
                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>1', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>

                                </li>
                                <% if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE")){ %>
                                <li id="list_LINK" value="-2">
                                    <input type="hidden" id="rectype" value="LINK" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">링크</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>2" contenteditable="true" >
                                        <%
                                            
                                            int linkStartIndex = content.IndexOf("<LINK>");
                                            if(linkStartIndex != -1){
                                                int linkEndIndex = content.IndexOf("</LINK>");
                                                string link = content.Substring(linkStartIndex, linkEndIndex - linkStartIndex);
                                                content = content.Remove(linkStartIndex, linkEndIndex - linkStartIndex);
                                                Response.Write(convertXmlToHtml(link));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>2', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>
                                </li>
                                <% } %>

                                <li id="list_<%=Convert.ToString(indexContentChildRow["RecType"]) %>" value="-2">
                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move"><%=getRecTypeName(Convert.ToString(indexContentChildRow["RecType"])) %></span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>3" contenteditable="true" >
                                        <%
                                            if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") ){
                 
                                                Response.Write(convertXmlToHtml(content));
                                                
                                            }else if(Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ) {
                                                
                                                Response.Write(convertXmlToHtml(content));
                                                
                                            }
                                        %>

                                    </div>
                                    <% if (Convert.ToString(indexContentChildRow["RecType"]).Equals("IMAGE") ){ %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Image'] }
                                            ]

                                        });
                                    </script>
                                    <% }else if (Convert.ToString(indexContentChildRow["RecType"]).Equals("MATRIX_TABLE") ){ %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(indexContentChildRow["RecType"]) + Convert.ToString( indexContentChildRow["SortNo"])%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Table'] }
                                            ]
                                        });
                                    </script>
                                    <% }%>

                                </li>
                            </ol>
                        </li>

        <%            
                    }else{
        %>
                            <li id="list_<% =Convert.ToString( indexContentChildRow["SortNo"])%>" value="<%=Convert.ToString(indexContentChildRow["Idx"])%>">
                                <input type="hidden" id="rectype" value="<%=Convert.ToString(indexContentChildRow["RecType"])%>" />
                                <div>
				                    <span class="glyphicon glyphicon-move"> <%=getRecTypeName(Convert.ToString( indexContentChildRow["RecType"])) %></span>
				
				                    <a href="#!" class="btn-close" onclick="deleteContent('list_<% =Convert.ToString( indexContentChildRow["SortNo"])%>');" style='float:right'></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea"  id ="editor_<% =Convert.ToString( indexContentChildRow["SortNo"])%>" contenteditable="true" >
                                    
                                    
                                    <%=convertXmlToHtml(Convert.ToString(indexContentChildRow["Content"])) %>
           

                                </div>
                                <script>
                                    CKEDITOR.inline('editor_<% =Convert.ToString( indexContentChildRow["SortNo"])%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        toolbar: [
                                            { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                            { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                        ]
                                    });
                                </script>

                            </li>
         <%           
                    }
                }//end for
         %>
                    </ol>     
               </li>
               
            
        <%        
            }//end for
        %>
                </ol><!-- olLunit -->
            </div><!-- panel body -->
            </div><!-- 대분류 panel 끝 --> 

        <!-- 퀴즈 가져오기 -->
        <% 
            DataRow[] quizRowList = entryTable.Select("ParentIdx=0 AND RecType='QUIZ'", "SortNo ASC");
            
            %>
        <input type="hidden" id="QuizNumber" value="<%=quizRowList.Length %>" />
        <div class="panel panel-gray" id="panelQuiz"><!-- 퀴즈 panel 시작-->    
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head"></span>			
				
				<span class="panel-title editor_head_title">문제<span class="badge" id="spanQuizNumber"><%=quizRowList.Length%></span></span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelQuiz');"><!-- close --></a>
				
				<span class="editor_head_container">
				<button type="button" class="btn btn-sm btn-primary" id="addQuiz">퀴즈+</button>
				<a href="OpenContents.aspx?type=quiz" target="_blank" class="btn btn-sm btn-primary">불러오기</a>  
				</span>
            </div>
            <div class="panel-body" > <!-- 퀴즈 패널 바디 시작 -->
            <ol class='editor vertical' id="olQuiz">
        <% 
            
            for(int i = 0 ; i < quizRowList.Length ;i++) {
                DataRow quizRow = quizRowList[i];
        %>
                    <li id="list_<% =Convert.ToString( quizRow["SortNo"])%>" value="<%=Convert.ToString(quizRow["Idx"])%>" >
                    <input type="hidden" id="rectype" value="<%=Convert.ToString(quizRow["RecType"])%>" />
                    <div>
				        <span class="glyphicon glyphicon-move">문제 시작</span> 
				        <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( quizRow["SortNo"])%>');"></a>
				        <span class="editor_control_container">
                            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','QUESTION');" onmouseover="" style="cursor: pointer;">문제 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','EXAMPLE');" onmouseover="" style="cursor: pointer;">보기 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','DISTRACTOR');" onmouseover="" style="cursor: pointer;">선택지 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','ANSWER');" onmouseover="" style="cursor: pointer;">정답 </span>
				            <span class="label label-info editor_controls" onclick="addContent('QUIZ_<%=i%>','EXPLANATION');" onmouseover="" style="cursor: pointer;">해설 </span>
                        </span>     
                    </div>
                    <ol id="resultContent_QUIZ_<%=i%>">
            
        <%  
                int parentIdx = Convert.ToInt32(quizRow["Idx"]);
                DataRow[] quizChildRowList = entryTable.Select("ParentIdx=" + parentIdx , "SortNo ASC");
                for (int j = 0; j < quizChildRowList.Length; j++) {
                    DataRow quizChildRow = quizChildRowList[j];
                    
                    if(Convert.ToString(quizChildRow["RecType"]).Equals("EXAMPLE") ){
                    
        %>
                    <li id="list_<% =Convert.ToString( quizChildRow["SortNo"])%>" value="<%=Convert.ToString(quizChildRow["Idx"])%>" >
                    <div>
				        <span class="glyphicon glyphicon-move">보기</span> 
				        <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( quizChildRow["SortNo"])%>')"></a>
				        <span class="editor_control_container">
                            <span class="label label-info editor_controls" onclick="addContent(<%=Convert.ToString(i) + Convert.ToString(j)  %>,'BASIC');" onmouseover="" style="cursor: pointer;">기본1 </span>
				            <span class="label label-info editor_controls" onclick="addContent(<%=Convert.ToString(i) + Convert.ToString(j)  %>,'BASIC2');" onmouseover="" style="cursor: pointer;">기본2 </span>
				            <span class="label label-info editor_controls" onclick="addContent(<%=Convert.ToString(i) + Convert.ToString(j)  %>,'B)X');" onmouseover="" style="cursor: pointer;">블럭 </span>
				            <span class="label label-info editor_controls" onclick="addContent(<%=Convert.ToString(i) + Convert.ToString(j)  %>,'MATRIX_TABLE');" onmouseover="" style="cursor: pointer;">표 </span>
				            <span class="label label-info editor_controls" onclick="addContent(<%=Convert.ToString(i) + Convert.ToString(j)  %>,'IMAGE');" onmouseover="" style="cursor: pointer;">이미지 </span>
				        </span>
                    </div>
                    <ol id="resultContent_EXAMPLE_<%=Convert.ToString(i) + Convert.ToString(j)  %>">

                    <%
                        int exampleIdx = Convert.ToInt32(quizChildRow["Idx"]);
                        DataRow[] exampleRowList = entryTable.Select("ParentIdx=" + exampleIdx , "SortNo ASC");
                        for (int k = 0; k < exampleRowList.Length;k++ ){
                            DataRow exampleRow = exampleRowList[k];
                         %>>

                        <li id="list_<% =Convert.ToString( exampleRow["SortNo"])%>" value="<%=Convert.ToString(exampleRow["Idx"])%>">
                                <input type="hidden" id="rectype" value="<%=Convert.ToString(exampleRow["RecType"])%>" />
                                <div>
				                    <span class="glyphicon glyphicon-move"> <%=getRecTypeName(Convert.ToString( exampleRow["RecType"])) %></span>
				
				                    <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( exampleRow["SortNo"])%>');"></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea"  id ="editor_<% =Convert.ToString( exampleRow["SortNo"])%>" contenteditable="true" ><%=convertXmlToHtml( Convert.ToString(exampleRow["Content"]) ) %></div>
                                <script>
                                    CKEDITOR.inline('editor_<% =Convert.ToString( exampleRow["SortNo"])%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        toolbar: [
                                            { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                            { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                        ]
                                    });
                                </script>

                            </li>
                    <%
                        }
                         %>

                    </ol>
                    </li>
         <%
                    }
                    else if (Convert.ToString(quizChildRow["RecType"]).Equals("IMAGE") || Convert.ToString(quizChildRow["RecType"]).Equals("MATRIX_TABLE"))
                    {
                        string content = Convert.ToString(quizChildRow["Content"]);
        %>
                        <li id="list_<%=Convert.ToString( quizChildRow["SortNo"] ) %>>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                            <div>
                                <span class="glyphicon glyphicon-move"><%=getRecTypeName(Convert.ToString(quizChildRow["RecType"])) %></span> 
				                <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( quizChildRow["SortNo"])%>');"></a>
                            </div>
                            <ol>
                                <li id="list_CAPTION" value="-2">
                                    <input type="hidden" id="rectype" value="CAPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">캡션</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>0" contenteditable="true" >
                                        <%
                                            int captionStartIndex = content.IndexOf("<CAPTION>");
                                            if(captionStartIndex != -1){
                                                int captionEndIndex = content.IndexOf("</CAPTION>");
                                                string caption = content.Substring(captionStartIndex, captionEndIndex - captionStartIndex);
                                                content = content.Remove(captionStartIndex, captionEndIndex - captionStartIndex);
                                                Response.Write(convertXmlToHtml(caption));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>

                                </li>
                                <li id="list_DESCRIPTION" value="-2">
                                    <input type="hidden" id="rectype" value="DESCRIPTION" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">설명</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>1" contenteditable="true" >
                                        <%
                                            
                                            int descriptionStartIndex = content.IndexOf("<DESCRIPTION>");
                                            if (descriptionStartIndex != -1)
                                            {
                                                int descriptionEndIndex = content.IndexOf("</DESCRIPTION>");
                                                string description = content.Substring(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex);
                                                content = content.Remove(descriptionStartIndex, descriptionEndIndex - descriptionStartIndex);
                                                Response.Write(convertXmlToHtml(description));
                                            }
                                        %>


                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>0', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>

                                </li>
                                <% if (Convert.ToString(quizChildRow["RecType"]).Equals("IMAGE")){ %>
                                <li id="list_LINK" value="-2">
                                    <input type="hidden" id="rectype" value="LINK" />
                                    <div>
				                        <span class="glyphicon glyphicon-move">링크</span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>2" contenteditable="true" >
                                        <%
                                            
                                            int linkStartIndex = content.IndexOf("<LINK>");
                                            if(linkStartIndex != -1){
                                                int linkEndIndex = content.IndexOf("</LINK>");
                                                string link = content.Substring(linkStartIndex, linkEndIndex - linkStartIndex);
                                                content = content.Remove(linkStartIndex, linkEndIndex - linkStartIndex);
                                                Response.Write(convertXmlToHtml(link));
                                            }
                                        %>

                                    </div>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>2', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                                ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                                { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                            ]
                                        });
                                    </script>
                                </li>
                                <% } %>

                                <li id="list_<%=Convert.ToString(quizChildRow["RecType"]) %>" value="-2">
                                    <input type="hidden" id="rectype" value="" />
                                    <div>
				                        <span class="glyphicon glyphicon-move"><%=getRecTypeName(Convert.ToString(quizChildRow["RecType"])) %></span>
				                        <span class="editor_control_container">
				                        </span>
		                            </div>
                                    <div class="editor_textarea"  id ="editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>3" contenteditable="true" >
                                        <%
                        if (Convert.ToString(quizChildRow["RecType"]).Equals("IMAGE"))
                        {
                                Response.Write(convertXmlToHtml(content));

                        }
                        else if (Convert.ToString(quizChildRow["RecType"]).Equals("MATRIX_TABLE"))
                        {
                                Response.Write(convertXmlToHtml(content));
                                                
                         }
                                        %>

                                    </div>
                                   <% if (Convert.ToString(quizChildRow["RecType"]).Equals("IMAGE")) { %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Image'] }
                                            ]

                                        });
                                    </script>
                                    <% }
                                       else if (Convert.ToString(quizChildRow["RecType"]).Equals("MATRIX_TABLE"))
                                       { %>
                                    <script>
                                        CKEDITOR.inline('editor_<% =Convert.ToString(quizChildRow["RecType"]) + Convert.ToString( quizChildRow["SortNo"])%>3', {
                                            filebrowserUploadUrl: 'Upload.ashx',
                                            toolbar: [
                                                { name: 'document', items: ['Table'] }
                                            ]
                                        });
                                    </script>
                                    <% }%>

                                </li>
                            </ol>
                        </li>

        <%            
                    }else{
        %>

                            <li id="list_<% =Convert.ToString( quizChildRow["SortNo"])%>" value="<%=Convert.ToString(quizChildRow["Idx"])%>">
                                <input type="hidden" id="rectype" value="<%=Convert.ToString(quizChildRow["RecType"])%>" />
                                <div>
				                    <span class="glyphicon glyphicon-move"> <%=getRecTypeName(Convert.ToString( quizChildRow["RecType"])) %></span>
				
				                    <a href="#!" class="btn-close" style='float:right' onclick="deleteContent('list_<% =Convert.ToString( quizChildRow["SortNo"])%>');"></a>
				                    <span class="editor_control_container">
				                    </span>
		                        </div>
                                <div class="editor_textarea"  id ="editor_<% =Convert.ToString( quizChildRow["SortNo"])%>" contenteditable="true" ><%=convertXmlToHtml( Convert.ToString(quizChildRow["Content"]) ) %></div>
                                <script>
                                    CKEDITOR.inline('editor_<% =Convert.ToString( quizChildRow["SortNo"])%>', {
                                        filebrowserUploadUrl: 'Upload.ashx',
                                        toolbar: [
                                            { name: 'document', items: ['Source', '-', 'Preview', 'Table', 'Image'] },
                                            ['JustifyLeft', 'JustifyCenter', 'JustifyRight'],
                                            { name: 'basicstyles', items: ['BulletedList', 'Bold', 'Italic', 'Subscript', 'Superscript', 'Underline', 'Link', ] }
                                        ]
                                    });
                                </script>

                            </li>
         <%     
                    }      
          }//end for
         %>
                    </ol>     
               </li>
               
            
            
        <%        
            }//end for
        %>
        </ol>  
            </div><!-- 퀴즈 패널 바디 끝 -->
        </div><!-- 퀴즈 panel 끝--> 

       
        <!-- 태그 가져오기 -->
        <div class="panel panel-gray" id="panelTag"><!-- 태그 panel 시작-->    
            <div class="panel-heading">

				<span class="glyphicon glyphicon-chevron-up btn-head"></span>			
				
				<span class="panel-title editor_head_title">태그</span>
				
				<a href="#!" class="btn-close" onclick="deleteContent('panelTag');"><!-- close --></a>
				<span class="editor_head_container">
				</span>
            </div>
            <div class="panel-body" > <!-- 태그 패널 바디 시작 -->
                <ol class="editor vertical" id="olTag">
                    <li id="list_tag" value="<%=txtEntryNo.Text%>">
                        <input type="hidden" id="rectype" value="TAG" />
                        <div>
				            <span class="editor_control_container"></span>
		                </div>
                        <div class="editor_textarea" contenteditable="true" ><%=entryTag %></div>
                    </li>
                </ol>
            </div><!-- 태그 패널 바디 끝 -->
        </div><!-- 태그 panel 끝--> 


        <%
        }//Null 체크 IF
             %>		 
	    </div>
		<div class="section-button" ><!-- section-button -->
			<div class="pull-center">
                <button id="preView" type="button" class="btn btn-sm btn-primary">미리보기</button>
                <span id="btnSection" runat="server">
                    <button id="save" type="button" class="btn btn-sm btn-primary">저장</button>
                    <asp:Button id="btnCheck" cssClass="btn btn-lg btn-danger" runat="server" Text="검수완료" OnClick="btnCheck_Click"></asp:Button>
                </span>
			</div>
		</div>
		
    </div><!-- Contents -->

	    <footer id="footer">
		    <img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	    </footer>

        <div id="markup_dialog" title="Basic dialog">
        </div>
<script type="text/javascript">
    function showPannel(typeVal)
    {
        alert(document.getElementById('editArea'));

        if (typeVal == '1')
        {
            
            $("#editArea").hide();
            $("#viewArea").show();
            //document.getElementById('editArea').style.display = none;
            //document.getElementById('viewArea').style.display = block;
        }
        else
        {
            
            $("#viewArea").hide();
            $("#editArea").show();
            //document.getElementById('viewArea').style.display = none;
            //document.getElementById('editArea').style.display = block;
        }
            

    }
</script>
    </form>

</div><!-- // container -->
</body>
</html>
