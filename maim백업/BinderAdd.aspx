﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BinderAdd.aspx.cs" Inherits="main_BinderAdd" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link rel="shortcut icon" href="../images/favicon.ico" /> 
    <link href="../css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body>
<header id="header"><!-- header -->
	<div class="container">
		<h1><a href="#!"><img src="../img/logo.png" alt="천재교육" /></a></h1>

		<div id="utility"><!-- utility -->
			<asp:Label ID="lblLogIn" runat="server"></asp:Label>
			<a href="../logout.aspx" class="btn btn-sm btn-default">로그아웃</a>
		</div><!-- // utility -->
	</div>
</header><!-- header -->

<div class="container">
  
	<nav class="navbar navbar-default"><!-- navbar -->
		<div id="navbar">
			<ul class="nav navbar-nav">
				<li class="nth-child-1"><a href="TaskList.aspx">엔트리생성</a></li>
				<li class="nth-child-2 active"><a href="BinderList.aspx">바인더관리</a></li>
				<li class="nth-child-3 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">엔트리관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="EntryList.aspx">엔트리조회</a></li>
						<li><a href="EntryAddList.aspx">엔트리등록</a></li>
						<li><a href="EntryOrder.aspx">순서관리</a></li>
					</ul>
				</li>
				<li class="nth-child-4"><a href="CheckList.aspx">검수관리</a></li>
				<li class="nth-child-5 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Data 관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="QueryList.aspx">쿼리관리</a></li>
						<li><a href="TemplateList.aspx">템플릿관리</a></li>
						<li><a href="ExportList.aspx">Export</a></li>
					</ul>
				</li>
				<li class="nth-child-6 dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">단원관리</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="LUnitList.aspx">대단원관리</a></li>
						<li><a href="MUnitList.aspx">중단원관리</a></li>
					</ul>
				</li>
				<li class="nth-child-7 dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">시스템관리</a>
					<ul class="dropdown-menu" role="menu" style="left:-520px;">
						<li><a href="Statistics.aspx">콘텐츠통계</a></li>
						<li><a href="StatTask.aspx">작업자통계</a></li>
						<li><a href="StatKeyWord.aspx">콘텐츠주제별현황</a></li>
						<li><a href="CodeList.aspx">코드관리</a></li>
						<li><a href="UserList.aspx">사용자관리</a></li>
					</ul>
				</li>
				<li class="nth-child-8"><a href="NoticeList.aspx">공지사항</a></li>
			</ul>
		</div>
	</nav><!-- // navbar -->
    <form id="editForm" runat="server">
	<div id="contents"><!-- contents -->

		<div class="title"><!-- title -->
			<h2 class="title">바인더등록</h2>
		</div><!-- // title -->	

		<div class="title"><!-- title -->
			<h3 class="title title-success">바인더정보</h3>
		</div><!-- // title -->
        <asp:HiddenField ID="hdParentID" runat="server"/>
        <asp:HiddenField ID="hdLevel" runat="server"/>
        <asp:HiddenField ID="hdBinderIdx" runat="server" />
        <table border="0" id="SelectTable" runat="server" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
		<colgroup>
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
			<col style="width: auto;">
		</colgroup>
		<tbody>
			<tr>
				<th>분류</th>
                <td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory1_SelectedIndexChanged"></asp:DropDownList>
					</div>
				</td>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory2_SelectedIndexChanged"></asp:DropDownList>					
					</div>
				</td>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory3_SelectedIndexChanged"></asp:DropDownList>					
					</div>
				</td>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory4" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory4_SelectedIndexChanged"></asp:DropDownList>					
					</div>
				</td>
				<td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory5" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory5_SelectedIndexChanged"></asp:DropDownList>					
					</div>
				</td>
                <td>
					<div class="de-select">
                        <asp:DropDownList class="de-select" ID="ddlCategory6" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory6_SelectedIndexChanged"></asp:DropDownList>
					</div>
				</td>
            </tr>
            <tr>
				<th>바인더명</th>
                <td colspan="6">
					<asp:TextBox runat="server" ID="txtBinderTitle" class="large" />
                </td>
            </tr>
            <tr>
				<th>설명</th>
                <td colspan="6">
					<asp:TextBox runat="server" ID="txtBinderMemo" class="large" />
                </td>
            </tr>
		</tbody>
		</table><!-- // table-a -->

        <div class="section-button"><!-- section-button -->
			<asp:Button runat="server" ID="AddButton" class="btn btn-lg btn-success" text="저장" OnClick="AddBinderButton_Click"/>
		</div><!-- // section-button -->

		<div class="title"><!-- title -->
			<h3 class="title title-success">
                바인더관계
                <asp:Button runat="server" ID="AddReationButton" class="btn btn-sm btn-success pull-right" text="관계추가" PostBackUrl="~/main/BinderRelationAdd.aspx" />
			</h3>
		</div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table">

            <asp:Repeater ID="rpRelationList" runat="server"  OnItemDataBound ="rpRelationList_ItemDataBound" OnItemCommand="reRelationList_ItemCommand">
                <HeaderTemplate>
                    <colgroup>
			            <col style="width: 5%;">
			            <col style="width: 30%;">
			            <col style="width: 20%;">
			            <col style="width: 20%;">
			            <col style="width: 5%;">
		            </colgroup>
                    <tr>
				        <th>No.</th>
                        <th>종류</th>
                        <th>바인더명</th>
                        <th>관계</th>
                        <th></th>
                    </tr>
<%
    if (rpRelationList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="5" style="text-align:center">조회된 바인더가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblCategory" Text='<%#DataBinder.Eval(Container.DataItem , "category")%>' runat="server"/>
                            </td>
                            <td>
                                <asp:Label ID="lblBinderName" Text='<%#DataBinder.Eval(Container.DataItem , "BinderTitle")%>'  runat="server" />
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlRelation" runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnDeleteRelation" CssClass="btn-close" runat="server"/>
                            </td>
                        </tr>
                    </ItemTemplate>
            </asp:Repeater>


		</table><!-- // table-a -->

        <div class="title"><!-- title -->
		    <h3 class="title title-success">
                엔트리
                <asp:Button runat="server" ID="AddEntryButton" class="btn btn-sm btn-success pull-right" text="엔트리 추가" PostBackUrl="~/main/BinderEntryAdd.aspx"/>
		    </h3>
	    </div><!-- // title -->
        <table border="0" cellpadding="0" cellspacing="0" class="table"><!-- table-a -->
            
            <asp:Repeater ID="rpEntryList" runat="server" OnItemCommand="rpEntryList_ItemCommand">
                <HeaderTemplate>
                    <colgroup>
			            <col style="width: 10%;">
			            <col style="width: 60%;">
			            <col style="width: 20%;">
			            <col style="width: 10%;">
		            </colgroup>
                    <tr>
				        <th>Entry No</th>
				        <th>Category</th>
				        <th>타이틀</th>
				        <th></th>
                    </tr>
<%
    if (rpEntryList.Items.Count == 0)
    {
%>
                    <tr>
                        <td colspan="4" style="text-align:center">등록된 Entry가 없습니다.</td>
                    </tr>                    
<%
    }
%>
                </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblNo" Text=<%#DataBinder.Eval(Container.DataItem , "entryidx")%> runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblCategory" Text=<%#DataBinder.Eval(Container.DataItem , "category")%> runat ="server" />
                            </td>
                            <td>
                                <asp:Label ID="lblTitle" Text=<%#DataBinder.Eval(Container.DataItem , "entrytitle")%> runat="server" />
                            </td>
                            <td>
                                <asp:Button ID="btnDeleteEntry" runat="server" CssClass="btn-close" />
                            </td>
                        </tr>
                    </ItemTemplate>
            </asp:Repeater>
        </table><!-- // table-a -->
    </div><!-- // contents -->
    </form>

	<footer id="footer">
		<img src="../img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="../js/jquery-2.1.3.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/jquery.jqtransform.js"></script>
<script src="../js/ui.js"></script>
</body>
</html>