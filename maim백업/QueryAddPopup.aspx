﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="QueryAddPopup.aspx.cs" Inherits="main_QueryAddPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title></title>
<script src="../js/jquery-2.1.3.min.js"></script>
<script>
    $(document).ready(function () {
        $('#btnClose').click(function () {
            opener.setResultCount($('#lblResultCount').text());
            window.close();
        });
    });
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:Label Text="조회건수" runat="server"/>
       <asp:Label ID="lblResultCount" runat="server" />
        <br />
       <input type="button" ID="btnClose" value="닫기"/>
    </div>
    </form>
</body>
</html>
