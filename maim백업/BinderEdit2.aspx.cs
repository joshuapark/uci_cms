﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

public partial class main_BinderEdit : System.Web.UI.Page
{
    private string connectionString = string.Empty;
    private string rootPath = string.Empty;

    private string pageNo = "1";
    private string pageSize = "50";
    private int binderIdx = 0;

    private ListItemCollection relationItems = new ListItemCollection
    {
        new ListItem("없음" , "0"),
        new ListItem("이전" , "1"),
        new ListItem("다음" , "2"),
        new ListItem("속함" , "3"),
        new ListItem("반대" , "4"),
        new ListItem("유사" , "5"),
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        //로그인 처리
        if (Session["uid"] == null)
        {
            string script = "<script>alert('로그인 정보가 만료되었습니다. 다시 로그인 해주세요.');location.href='../Default.aspx?target=" + Request.ServerVariables["PATH_INFO"].ToString() + "';</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        //##### 권한 처리 시작
        int uAuth = 1;
        if (Session["uauth"].ToString().Length > 0)
        {
            uAuth = Convert.ToInt32(Session["uauth"].ToString());
        }
        if (uAuth < 9)
        {
            string script = "<script>history.go(-1);alert('권한이 없습니다.');</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            return;
        }
        else
            Session.Timeout = 120;
        //##### 권한 처리 끝

        if (!IsPostBack) {

            if (Request.Params["pageNo"] != null)
                pageNo = Request.Params["pageNo"].ToString();
            if (Request.Params["pageSize"] != null)
                pageSize = Request.Params["pageSize"].ToString();
            if (Request.Params["idx"] != null)
                binderIdx = Convert.ToInt32(Request.Params["idx"].ToString());
            
            DBFileInfo();
            SqlConnection Conn = new SqlConnection(connectionString);
            Conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM TBinder WHERE DelFlag=0 AND BinderIdx=" + binderIdx, Conn);
            DataTable dt = new DataTable();

            sda.Fill(dt);
            DataRow binderRow = dt.Rows[0];

            txtBinderTitle.Text = Convert.ToString( binderRow["BinderTitle"] );
            txtBinderMemo.Text = Convert.ToString(binderRow["BinderMemo"]);

            Conn.Close();
        }
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }

    protected void ddlCategory1_SelectedIndexChanged(object sender, EventArgs e) { 
    }

    protected void ddlCategory2_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void ddlCategory3_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void ddlCategory4_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void ddlCategory5_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void ddlCategory6_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void AddBinderButton_Click(object sender, EventArgs e)
    { 
    }

    protected void rpRelationList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    { 
    }
    protected void rpEntryList_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
    }
    protected void reRelationList_ItemCommand(object source, RepeaterCommandEventArgs e) { 
    }



}