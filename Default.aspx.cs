﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.Security.Cryptography;

public partial class main_Default : System.Web.UI.Page
{
    //
    // 로그인 결과 반환 
    // 필수 기본값
    //
    private string connectionString = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void LoginButton_Click(object sender, EventArgs e)
    {
        DBFileInfo();
        

        SqlConnection Con = new SqlConnection(connectionString);
        SqlCommand Cmd = new SqlCommand();
        Cmd.Connection = Con;

        Cmd.Parameters.Add("@uid", SqlDbType.VarChar, 20);
        Cmd.Parameters.Add("@pwd", SqlDbType.VarChar, 20);

        Cmd.Parameters["@uid"].Value = txtID.Text.ToString();
        Cmd.Parameters["@pwd"].Value = EncodeSHA(txtPassword.Text.ToString());

        Cmd.CommandType = CommandType.StoredProcedure;
        Cmd.CommandText = "USP_User_LOGIN";
        Con.Open();

        SqlDataReader reader = Cmd.ExecuteReader(CommandBehavior.CloseConnection);
       
        if (reader.Read())
        {
            if (Convert.ToBoolean(reader["Enabled"]))
            {
                Session["uid"] = txtID.Text.ToString();
                string strIdx = reader["Idx"].ToString();
                Session["uidx"] = strIdx;
                Session["uname"] = reader["UserName"].ToString();
                Session["uauth"] = reader["UserTypeIdx"].ToString();
                Session.Timeout = 120;
                HttpCookie cookie = null;
                cookie = new HttpCookie("chunjae");
                cookie.Value = txtID.Text.ToString();
                Response.Cookies.Add(cookie);
                string target = "main/TaskList.aspx";
                if (reader["UserTypeIdx"].ToString().Equals("1"))
                    target = "main/EntryAddList.aspx";

                if (Request.Params["target"] != null)
                    target = Request.Params["target"].ToString();
                Response.Redirect(target);
            }
            else 
            {
                string script = "<script>alert('사용 대기 중입니다. 관리자에게 문의하세요.'); history.back();</script>";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
            }
        }
        else
        {
            string script = "<script>alert('아이디 또는 비밀번호가 일치하지 않습니다. 확인 후 다시 입력해 주세요.'); history.back();</script>";
            ClientScript.RegisterClientScriptBlock(this.GetType(), "done", script);
        }

        reader.Close();
        if (Con.State == ConnectionState.Open)
            Con.Close();

        Cmd = null;
        Con = null;
    }

    private string EncodeSHA(string pw)
    {
        SHA256 crypt = SHA256Managed.Create();

        string hash = String.Empty;
        byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(pw), 0, Encoding.ASCII.GetByteCount(pw));
        foreach (byte bit in crypto)
        {
            hash += bit.ToString("x2");
        }
        hash = hash.Substring(0, 16);
        return hash;
    }

    private void DBFileInfo()
    {
        string serverIP = Request.ServerVariables["LOCAL_ADDR"];
        if (serverIP == "106.245.23.124" || serverIP == "127.0.0.1" || serverIP == "::1") //-- 테스트 서버 
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["JConnectionString"].ConnectionString;
        }
        else //-- 서비스서버
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["SConnectionString"].ConnectionString;
        }
    }
}