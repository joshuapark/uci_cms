﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="main_Default" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>천재교육CMS</title>
    <link href="css/import.css" rel="stylesheet">
	<!--[if lt IE 9]>
		<script src="../js/html5shiv.js" type="text/javascript"></script>
		<script src="../js/respond.min.js" type="text/javascript"></script>
	<![endif]-->
</head>

<body class="login">

<div class="login-box"><!-- login -->
	<h1><img src="img/login_logo.png" alt="천재교육" /></h1>

	<div class="login-wrap">
		<h2>CMS LOGIN</h2>
		<div class="login-body">

			<form class="form-signin" runat="server" defaultbutton="LoginButton" defaultfocus="txtID">
				<p>
					<label for="id">ID</label>
					<asp:TextBox Runat="server" ID="txtID" placeholder="아이디 입력"></asp:TextBox>
				</p>
				<p>
					<label for="pw">PW</label>
					<asp:TextBox Runat="server" ID="txtPassword" TextMode="Password" placeholder="비밀번호 입력"></asp:TextBox>
				</p>

				<div class="section-button">
                    <asp:Button class="btn btn-lg btn-danger" ID="LoginButton" runat="server" text="로그인" onclick="LoginButton_Click"></asp:Button>  
				</div>
			</form>
		</div>
	</div>
</div><!-- // login -->

<div class="container"><!-- container -->

	<footer id="footer">
		<img src="img/footer.png" alt="COPYRIGHT 2015 CHUNJAE EDUCATION INC. ALL RIGHTS RESERVED." />
	</footer>


</div><!-- // container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-2.1.3.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.jqtransform.js"></script>
<script src="js/ui.js"></script>
</body>
</html>